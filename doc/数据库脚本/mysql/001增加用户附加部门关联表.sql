SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_user_department
-- ----------------------------
DROP TABLE IF EXISTS `base_user_department`;
CREATE TABLE `base_user_department` (
  `id` bigint(30) NOT NULL COMMENT '用户角色ID',
  `userId` bigint(30) DEFAULT NULL COMMENT '用户ID',
  `departmentId` bigint(30) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户部门\r\n用户附加部门关联表，用于一个用户属于多个部门的情况\r\n';
