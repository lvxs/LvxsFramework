
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cms_classify
-- ----------------------------
DROP TABLE IF EXISTS `cms_classify`;
CREATE TABLE `cms_classify` (
  `classifyId` bigint(20) NOT NULL COMMENT '栏目ID',
  `classifyName` varchar(255) DEFAULT NULL COMMENT '栏目名称',
  `webSiteId` bigint(20) DEFAULT NULL COMMENT '网站ID',
  `parentId` bigint(20) DEFAULT NULL COMMENT '父级栏目ID',
  `classifyNumber` varchar(255) DEFAULT NULL COMMENT '栏目编号',
  `sortNumber` int(11) DEFAULT '100' COMMENT '排序编号\r\n越小越靠前显示，默认值100',
  `keyWords` varchar(255) DEFAULT NULL COMMENT 'SEO关键词\r\n每个关键词之间使用半角逗号分割。',
  `description` varchar(1000) DEFAULT NULL COMMENT '栏目内容描述',
  `ico1Url` varchar(255) DEFAULT NULL COMMENT '栏目图标1URL',
  `ico2Url` varchar(255) DEFAULT NULL COMMENT '栏目图标2URL',
  `ico3Url` varchar(255) DEFAULT NULL COMMENT '栏目图标3URL',
  `classifyHtml` text COMMENT '栏目HTML内容',
  `userLevel` int(11) DEFAULT '0' COMMENT '用户级别要求',
  `browseCount` bigint(255) DEFAULT '0' COMMENT '浏览次数',
  `status` int(11) DEFAULT NULL COMMENT '状态\r\n-1:逻辑删除;0:已禁用/待审核;1:正常',
  `remark` text COMMENT '备注说明',
  `otherJson` text COMMENT '其他设置JSON字符串',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` bigint(20) DEFAULT NULL COMMENT '创建用户ID',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` bigint(20) DEFAULT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`classifyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网站栏目基础信息\r\nCMS的网站栏目基础信息表';
SET FOREIGN_KEY_CHECKS=1;
