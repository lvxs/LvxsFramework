
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_attachment
-- ----------------------------
DROP TABLE IF EXISTS `base_attachment`;

CREATE TABLE `base_attachment` (
	`attachmentId`  bigint(20) UNSIGNED NOT NULL COMMENT '附件ID' ,
	`systemId`  bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '附件所属的系统ID' ,
	`title`  text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '标题' ,
	`path`  varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路径' ,
	`mimeType`  varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'mime' ,
	`suffix`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '附件的后缀' ,
	`type`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型' ,
	`flag`  varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标示' ,
	`lat`  decimal(20,16) NULL DEFAULT NULL COMMENT '经度' ,
	`lng`  decimal(20,16) NULL DEFAULT NULL COMMENT '纬度' ,
	`sortNumber`  int(11) NULL DEFAULT NULL COMMENT '排序字段' ,
	`status`  int(11) NULL DEFAULT NULL COMMENT '状态\r\n-1:逻辑删除;0:已禁用/待审核;1:正常' ,
	`remark`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注说明' ,
	`otherJson`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '其他设置JSON字符串' ,
	`createTime`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
	`createUserId`  bigint(20) NULL DEFAULT NULL COMMENT '创建用户ID' ,
	`updateTime`  datetime NULL DEFAULT NULL COMMENT '更新时间' ,
	`updateUserId`  bigint(20) NULL DEFAULT NULL COMMENT '更新用户ID' ,
	PRIMARY KEY (`attachmentId`),
	INDEX `systemId` (`systemId`) USING BTREE ,
	INDEX `createTime` (`createTime`) USING BTREE ,
	INDEX `suffix` (`suffix`) USING BTREE ,
	INDEX `mimeType` (`mimeType`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci
COMMENT='附件表\r\n用于保存用户上传的附件内容。';

