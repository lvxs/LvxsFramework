SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for base_action
-- ----------------------------
DROP TABLE IF EXISTS `base_action`;
CREATE TABLE `base_action` (
  `id` bigint(30) NOT NULL COMMENT '动作ID',
  `menuId` bigint(30) DEFAULT NULL COMMENT '系统菜单ID',
  `sortNumber` int(11) DEFAULT NULL COMMENT '排序编号',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `actionType` int(11) DEFAULT NULL COMMENT '动作类型\r\n1:列表页面;2:新增页面;3:修改页面;4:查看页面;5:其他页面;11:保存;12:更新;13:删除;14:批量删除;15:查询结果列表JSON;16:其他操作;',
  `methodName` varchar(100) DEFAULT NULL COMMENT '方法名',
  `fullUri` varchar(300) DEFAULT NULL COMMENT '完整URI',
  `jsMethod` varchar(300) DEFAULT NULL COMMENT 'JavaScript方法',
  `linkUri` varchar(300) DEFAULT NULL COMMENT '链接URI\r\n页面内链接URI',
  `status` int(11) DEFAULT NULL COMMENT '状态\r\n-1:逻辑删除;0:已禁用/待审核;1:正常',
  `remark` text COMMENT '备注说明',
  `otherJson` text COMMENT '其他设置JSON字符串',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` bigint(20) DEFAULT NULL COMMENT '创建用户ID',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` bigint(20) DEFAULT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='页面动作\r\n页面中所有按钮、链接等动作的基本信息\r\n';

-- ----------------------------
-- Records of base_action
-- ----------------------------
INSERT INTO `base_action` VALUES ('1483502737794014', '1482640318563308', '1', '列表页面', '1', 'index', '/admin/baseUser/index', null, 'index', '1', null, null, '2017-01-04 12:05:37', '1481163390092603', '2017-01-04 12:05:37', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483507073600142', '1482640318563308', '3', '新增页面', '2', 'add', '/admin/baseUser/add', 'add();', 'add', '1', null, null, '2017-01-04 13:17:53', '1481163390092603', '2017-01-04 13:17:53', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483507093561121', '1482640318563308', '5', '修改页面', '3', 'edit', '/admin/baseUser/edit', 'edit();', 'edit', '1', null, null, '2017-01-04 13:18:13', '1481163390092603', '2017-01-04 13:18:13', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483507099547878', '1482640318563308', '9', '查看页面', '4', 'view', '/admin/baseUser/view', 'view();', 'view', '1', null, null, '2017-01-04 13:18:19', '1481163390092603', '2017-01-04 13:18:19', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483507113765259', '1482640318563308', '4', '新增保存', '11', 'save', '/admin/baseUser/save', null, 'save', '1', null, null, '2017-01-04 13:18:33', '1481163390092603', '2017-01-04 13:18:33', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483507125536603', '1482640318563308', '7', '删除', '13', 'delete', '/admin/baseUser/delete', 'del();', 'delete', '1', null, null, '2017-01-04 13:18:45', '1481163390092603', '2017-01-04 13:18:45', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483507173939364', '1482640318563308', '6', '修改保存', '12', 'update', '/admin/baseUser/update', null, 'update', '1', null, null, '2017-01-04 13:19:33', '1481163390092603', '2017-01-04 13:23:59', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483507241280435', '1482640318563308', '8', '批量删除', '14', 'delete', '/admin/baseUser/delete', 'batchDel();', 'delete', '1', null, null, '2017-01-04 13:20:41', '1481163390092603', '2017-01-04 13:20:41', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483507422310202', '1482640318563308', '2', '查询结果列表JSON', '15', 'searchData', '/admin/baseUser/searchData', null, 'searchData', '1', null, null, '2017-01-04 13:23:42', '1481163390092603', '2017-01-04 13:23:42', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483783326584802', '1482637008202383', '1', '列表页面', '1', 'index', '/admin/baseSystem/index', null, 'index', '1', null, null, '2017-01-07 18:02:06', '1481163390092603', '2017-01-07 18:02:06', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483783358740559', '1482637008202383', '3', '新增页面', '2', 'add', '/admin/baseSystem/add', 'add();', 'add', '1', null, null, '2017-01-07 18:02:38', '1481163390092603', '2017-01-07 18:02:38', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483783377637700', '1482637008202383', '5', '修改页面', '3', 'edit', '/admin/baseSystem/edit', 'edit();', 'edit', '1', null, null, '2017-01-07 18:02:57', '1481163390092603', '2017-01-07 18:02:57', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483784347267060', '1482637008202383', '9', '查看页面', '4', 'view', '/admin/baseSystem/view', 'view();', 'view', '1', null, null, '2017-01-07 18:19:07', '1481163390092603', '2017-01-07 18:19:07', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483784507807654', null, '7', '删除', '13', 'delete', '/admin/baseSystem/delete', 'del();', 'delete', '1', null, null, '2017-01-07 18:21:47', '1481163390092603', '2017-01-07 18:21:47', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483787859123312', '1482637008202383', '7', '删除', '13', 'delete', '/admin/baseSystem/delete', 'del();', 'delete', '1', null, null, '2017-01-07 19:17:39', '1481163390092603', '2017-01-07 19:17:39', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483787875609507', '1482637008202383', '8', '批量删除', '14', 'delete', '/admin/baseSystem/delete', 'batchDel();', 'delete', '1', null, null, '2017-01-07 19:17:55', '1481163390092603', '2017-01-07 19:17:55', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483788063301364', '1482637008202383', '15', '系统菜单管理', '5', 'menuManage', '/admin/baseSystemMenu/menuManage', 'menuManage();', '/admin/baseSystemMenu/menuManage', '1', '子系统系统菜单管理', null, '2017-01-07 19:21:03', '1481163390092603', '2017-01-07 19:21:03', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483788274741651', '1482640379709261', '4', '新增保存', '11', 'save', '/admin/baseDepartment/save', 'addRootNode();', 'save', '1', null, null, '2017-01-07 19:24:34', '1481163390092603', '2017-01-07 19:24:34', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483788340725798', '1482640379709261', '6', '修改保存', '12', 'update', '/admin/baseDepartment/update', 'doSubmit();', 'update', '1', null, null, '2017-01-07 19:25:40', '1481163390092603', '2017-01-07 19:25:40', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483788650367637', '1482640379709261', '7', '删除', '13', 'delete', '/admin/baseDepartment/delete', 'zTreeOnRemove();', 'delete', '1', null, null, '2017-01-07 19:30:50', '1481163390092603', '2017-01-07 19:30:50', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483788723282838', '1482640379709261', '20', '拖拽保存', '16', 'drop', '/admin/baseDepartment/drop', 'zTreeOnDrop();', 'drop', '1', null, null, '2017-01-07 19:32:03', '1481163390092603', '2017-01-07 19:32:03', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483788805511342', '1482639945265710', '1', '列表页面', '1', 'menuManage', '/admin/baseSystemMenu/menuManage', 'menuManage();', 'menuManage', '1', null, null, '2017-01-07 19:33:25', '1481163390092603', '2017-01-07 19:33:25', '1481163390092603');
INSERT INTO `base_action` VALUES ('1483840201083782', '1482640410448990', '2', '查询结果列表JSON', '15', 'searchData', '/admin/baseRole/searchData', null, 'searchData', '1', null, null, '2017-01-08 09:50:01', '1483798229772386', '2017-01-08 09:50:01', '1483798229772386');
INSERT INTO `base_action` VALUES ('1483840225689469', '1482640410448990', '3', '新增页面', '2', 'add', '/admin/baseRole/add', 'add();', 'add', '1', null, null, '2017-01-08 09:50:25', '1483798229772386', '2017-01-08 09:50:25', '1483798229772386');
INSERT INTO `base_action` VALUES ('1483840231988865', '1482640410448990', '4', '新增保存', '11', 'save', '/admin/baseRole/save', null, 'save', '1', null, null, '2017-01-08 09:50:31', '1483798229772386', '2017-01-08 09:50:31', '1483798229772386');
INSERT INTO `base_action` VALUES ('1483840238098982', '1482640410448990', '5', '修改页面', '3', 'edit', '/admin/baseRole/edit', 'edit();', 'edit', '1', null, null, '2017-01-08 09:50:38', '1483798229772386', '2017-01-08 09:50:38', '1483798229772386');
INSERT INTO `base_action` VALUES ('1483840242568317', '1482640410448990', '6', '修改保存', '12', 'update', '/admin/baseRole/update', null, 'update', '1', null, null, '2017-01-08 09:50:42', '1483798229772386', '2017-01-08 09:50:42', '1483798229772386');
INSERT INTO `base_action` VALUES ('1483840248538503', '1482640410448990', '7', '删除', '13', 'delete', '/admin/baseRole/delete', 'del();', 'delete', '1', null, null, '2017-01-08 09:50:48', '1483798229772386', '2017-01-08 09:50:48', '1483798229772386');
INSERT INTO `base_action` VALUES ('1483840253424572', '1482640410448990', '8', '批量删除', '14', 'delete', '/admin/baseRole/delete', 'batchDel();', 'delete', '1', null, null, '2017-01-08 09:50:53', '1483798229772386', '2017-01-08 09:50:53', '1483798229772386');

-- ----------------------------
-- Table structure for base_department
-- ----------------------------
DROP TABLE IF EXISTS `base_department`;
CREATE TABLE `base_department` (
  `id` bigint(30) NOT NULL COMMENT '部门ID',
  `systemId` bigint(30) DEFAULT NULL COMMENT '系统ID',
  `parentId` bigint(30) DEFAULT NULL COMMENT '父级部门ID',
  `sortNumber` int(11) DEFAULT NULL COMMENT '排序编号',
  `treeNumber` varchar(255) DEFAULT NULL COMMENT '树形编号',
  `name` varchar(100) DEFAULT NULL COMMENT '部门名称',
  `number` varchar(100) DEFAULT NULL COMMENT '部门编号',
  `briefDescription` varchar(255) DEFAULT NULL COMMENT '简要说明',
  `status` int(11) DEFAULT NULL COMMENT '状态\r\n-1:逻辑删除;0:已禁用/待审核;1:正常',
  `remark` text COMMENT '备注说明',
  `otherJson` text COMMENT '其他设置JSON字符串',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` bigint(20) DEFAULT NULL COMMENT '创建用户ID',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` bigint(20) DEFAULT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='部门信息\r\n部门或者其他分支机构基础信息';

-- ----------------------------
-- Records of base_department
-- ----------------------------
INSERT INTO `base_department` VALUES ('1483529702682869', '1', '0', '2', '002', '亚太大区', null, null, '1', null, null, '2017-01-04 19:35:02', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530000861797', '1', '0', '4', '004', '欧洲大区', null, null, '1', null, null, '2017-01-04 19:40:00', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530047366807', '1', '0', '6', '006', '非洲大区', null, null, '1', null, null, '2017-01-04 19:40:47', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530069598814', '1', '0', '3', '003', '美洲大区', null, null, '1', null, null, '2017-01-04 19:41:09', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530151002532', '1', '1483529702682869', '1', '002001', '大中华分区', null, null, '1', null, null, '2017-01-04 19:42:31', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530153908878', '1', '0', '5', '005', '中东海湾大区', null, null, '1', null, null, '2017-01-04 19:42:33', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530200740066', '1', '1483529702682869', '2', '002002', '东盟分区', null, null, '1', null, null, '2017-01-04 19:43:20', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530295553321', '1', '1483530151002532', '1', '002001001', '北京分公司', null, null, '1', null, null, '2017-01-04 19:44:55', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530315082211', '1', '1483530151002532', '2', '002001002', '上海分公司', null, null, '1', null, null, '2017-01-04 19:45:15', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530510132810', '1', '1483530151002532', '3', '002001003', '深圳分公司', null, null, '1', null, null, '2017-01-04 19:48:30', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483530538136353', '1', '1483530151002532', '4', '002001004', '香港分公司', null, null, '1', null, null, '2017-01-04 19:48:58', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534257340912', '1', '1483530151002532', '5', '002001005', '台北分公司', null, null, '1', null, null, '2017-01-04 20:50:57', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534264470672', '1', '0', '1', '001', '总部', null, null, '1', null, null, '2017-01-04 20:51:04', '1481163390092603', '2017-01-04 20:51:22', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534330700902', '1', '1483534264470672', '1', '001001', '董事会', null, null, '1', null, null, '2017-01-04 20:52:10', '1481163390092603', '2017-01-04 20:58:20', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534373678324', '1', '1483534264470672', '2', '001002', '股东会', null, null, '1', null, null, '2017-01-04 20:52:53', '1481163390092603', '2017-01-04 20:58:20', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534390480675', '1', '1483534264470672', '3', '001003', '总工会', null, null, '1', null, null, '2017-01-04 20:53:10', '1481163390092603', '2017-01-04 20:58:20', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534420747306', '1', '1483534264470672', '4', '001004', '规划决策部', null, null, '1', null, null, '2017-01-04 20:53:40', '1481163390092603', '2017-01-04 20:58:20', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534432769016', '1', '1483534264470672', '7', '001007', '人力资源部', null, null, '1', null, null, '2017-01-04 20:53:52', '1481163390092603', '2017-01-04 20:58:21', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534462232926', '1', '1483534264470672', '5', '001005', '财务部', null, null, '1', null, null, '2017-01-04 20:54:22', '1481163390092603', '2017-01-04 20:58:20', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534647738395', '1', '1483534264470672', '6', '001006', '信息技术部', null, null, '1', null, null, '2017-01-04 20:57:27', '1481163390092603', '2017-01-04 20:58:20', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534772057034', '1', '1483534264470672', '8', '001008', '市场宣传部', null, null, '1', null, null, '2017-01-04 20:59:32', '1481163390092603', '2017-01-04 20:59:32', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534795775442', '1', '1483534264470672', '9', '001009', '销售管理部', null, null, '1', null, null, '2017-01-04 20:59:55', '1481163390092603', '2017-01-04 21:02:26', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534857368172', '1', '1483530069598814', '1', '003001', '美国分区', null, null, '1', null, null, '2017-01-04 21:00:57', '1481163390092603', '2017-01-04 21:00:57', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534884577416', '1', '1483530069598814', '2', '003002', '加拿大分区', null, null, '1', null, null, '2017-01-04 21:01:24', '1481163390092603', '2017-01-04 21:01:24', '1481163390092603');
INSERT INTO `base_department` VALUES ('1483534938334021', '1', '1483534264470672', '10', '001010', '研发管理部', null, null, '1', null, null, '2017-01-04 21:02:18', '1481163390092603', '2017-01-04 21:02:18', '1481163390092603');

-- ----------------------------
-- Table structure for base_role
-- ----------------------------
DROP TABLE IF EXISTS `base_role`;
CREATE TABLE `base_role` (
  `id` bigint(30) NOT NULL COMMENT '角色ID',
  `systemId` bigint(30) DEFAULT NULL COMMENT '系统ID',
  `sortNumber` int(11) DEFAULT NULL COMMENT '排序编号',
  `name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `status` int(11) DEFAULT NULL COMMENT '状态\r\n-1:逻辑删除;0:已禁用/待审核;1:正常',
  `remark` text COMMENT '备注说明',
  `otherJson` text COMMENT '其他设置JSON字符串',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` bigint(20) DEFAULT NULL COMMENT '创建用户ID',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` bigint(20) DEFAULT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色信息\r\n角色基础信息\r\n';

-- ----------------------------
-- Records of base_role
-- ----------------------------
INSERT INTO `base_role` VALUES ('1', '1', '1', '超级系统管理员', '1', null, null, '2016-12-16 22:39:00', '1481163390092603', '2017-01-05 08:32:33', '1481163343534554');
INSERT INTO `base_role` VALUES ('1483576333993822', '1', '3', '系统管理员', '1', '拥有除多系统管理外的所有权限', null, '2017-01-05 08:32:13', '1481163343534554', '2017-01-05 08:32:13', '1481163343534554');
INSERT INTO `base_role` VALUES ('1483840657975899', '1', '10', '北京分公司老总', '1', null, null, '2017-01-08 09:57:37', '1483798229772386', '2017-01-08 09:57:37', '1483798229772386');

-- ----------------------------
-- Table structure for base_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `base_role_permission`;
CREATE TABLE `base_role_permission` (
  `id` bigint(30) NOT NULL COMMENT '角色权限ID',
  `menuId` bigint(30) DEFAULT NULL COMMENT '菜单ID',
  `actionId` bigint(30) DEFAULT NULL COMMENT '动作ID',
  `roleId` bigint(30) DEFAULT NULL COMMENT '角色ID',
  `dataPermission` int(11) DEFAULT '1' COMMENT '数据权限\r\n1:所有权限;2:本部门及子部门;3:本部门;4:仅本人;5:特定部门;6:特定人员',
  `specialIds` text COMMENT '特定IDs\r\ndataPermission=5时代表特定部门IDs；dataPermission=6时代表特定人员IDs。多个ID之间使用半角逗号分隔',
  `status` int(11) DEFAULT NULL COMMENT '状态\r\n-1:逻辑删除;0:已禁用/待审核;1:正常',
  `remark` text COMMENT '备注说明',
  `otherJson` text COMMENT '其他设置JSON字符串',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` bigint(20) DEFAULT NULL COMMENT '创建用户ID',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` bigint(20) DEFAULT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色权限\r\n角色拥有权限信息表\r\n';

-- ----------------------------
-- Records of base_role_permission
-- ----------------------------
INSERT INTO `base_role_permission` VALUES ('1483797309617165', '1482640318563308', '1483502737794014', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:09', '1481163343534554', '2017-01-07 21:55:09', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797309740310', '1482640318563308', '1483507422310202', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:09', '1481163343534554', '2017-01-07 21:55:09', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797309862788', '1482640318563308', '1483507073600142', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:09', '1481163343534554', '2017-01-07 21:55:09', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797309984500', '1482640318563308', '1483507113765259', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:09', '1481163343534554', '2017-01-07 21:55:09', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310106893', '1482640318563308', '1483507093561121', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310179245', '1482640318563308', '1483507173939364', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310238059', '1482640318563308', '1483507125536603', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310302301', '1482640318563308', '1483507241280435', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310361991', '1482640318563308', '1483507099547878', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310424001', '1482640379709261', '1483788274741651', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310483703', '1482640379709261', '1483788340725798', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310546249', '1482640379709261', '1483788650367637', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310605951', '1482640379709261', '1483788723282838', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310668961', '1482639945265710', '1483788805511342', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310727519', '1482637008202383', '1483783326584802', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310879366', '1482637008202383', '1483783358740559', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797310959305', '1482637008202383', '1483783377637700', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:10', '1481163343534554', '2017-01-07 21:55:10', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311000058', '1482637008202383', '1483787859123312', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311037202', '1482637008202383', '1483787875609507', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311078110', '1482637008202383', '1483784347267060', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311125605', '1482637008202383', '1483788063301364', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311178408', '1482468733903912', '0', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311214297', '1482636603448540', '0', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311244018', '1482637116384527', '0', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311269966', '1482637157762612', '0', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311311028', '1482637276555383', '0', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311348579', '1482468941453029', '0', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483797311378836', '1482640410448990', '0', '1', '1', null, '1', null, '{}', '2017-01-07 21:55:11', '1481163343534554', '2017-01-07 21:55:11', '1481163343534554');
INSERT INTO `base_role_permission` VALUES ('1483840304285647', '1482640318563308', '1483502737794014', '1483576333993822', '2', null, '1', null, '{}', '2017-01-08 09:51:44', '1483798229772386', '2017-01-08 09:51:44', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840304407215', '1482640318563308', '1483507422310202', '1483576333993822', '2', null, '1', null, '{}', '2017-01-08 09:51:44', '1483798229772386', '2017-01-08 09:51:44', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840304540012', '1482640318563308', '1483507073600142', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:44', '1483798229772386', '2017-01-08 09:51:44', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840304662260', '1482640318563308', '1483507113765259', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:44', '1483798229772386', '2017-01-08 09:51:44', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840304784971', '1482640318563308', '1483507093561121', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:44', '1483798229772386', '2017-01-08 09:51:44', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840304932354', '1482640318563308', '1483507173939364', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:44', '1483798229772386', '2017-01-08 09:51:44', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840304995163', '1482640318563308', '1483507125536603', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:44', '1483798229772386', '2017-01-08 09:51:44', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305054832', '1482640318563308', '1483507241280435', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305117267', '1482640318563308', '1483507099547878', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305176545', '1482640410448990', '1483840201083782', '1483576333993822', '2', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305239979', '1482640410448990', '1483840225689469', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305298793', '1482640410448990', '1483840231988865', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305361228', '1482640410448990', '1483840238098982', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305420650', '1482640410448990', '1483840242568317', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305483928', '1482640410448990', '1483840248538503', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305553891', '1482640410448990', '1483840253424572', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305616058', '1482640379709261', '1483788274741651', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305675748', '1482640379709261', '1483788340725798', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305739294', '1482640379709261', '1483788650367637', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305798429', '1482640379709261', '1483788723282838', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305861707', '1482639945265710', '1483788805511342', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305920142', '1482468733903912', '0', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840305982110', '1482636603448540', '0', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:45', '1483798229772386', '2017-01-08 09:51:45', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840306042620', '1482637116384527', '0', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:46', '1483798229772386', '2017-01-08 09:51:46', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840306093846', '1482637157762612', '0', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:46', '1483798229772386', '2017-01-08 09:51:46', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840306141641', '1482637276555383', '0', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:46', '1483798229772386', '2017-01-08 09:51:46', '1483798229772386');
INSERT INTO `base_role_permission` VALUES ('1483840306182538', '1482468941453029', '0', '1483576333993822', '1', null, '1', null, '{}', '2017-01-08 09:51:46', '1483798229772386', '2017-01-08 09:51:46', '1483798229772386');

-- ----------------------------
-- Table structure for base_system
-- ----------------------------
DROP TABLE IF EXISTS `base_system`;
CREATE TABLE `base_system` (
  `id` bigint(30) NOT NULL COMMENT '系统ID\r\nID=1的为默认基础框架系统',
  `fullName` varchar(100) DEFAULT NULL COMMENT '系统全称',
  `shortName` varchar(100) DEFAULT NULL COMMENT '系统简称',
  `url` varchar(255) DEFAULT NULL COMMENT '访问URL\r\n到应用存放路径(ContextPath)为止，不必以“/”结尾',
  `filePath` varchar(20) DEFAULT NULL COMMENT '文件路径\r\n附件、资源文件或者模板页面存储路径标识。本机上运行的子系统必填而且必须唯一，非本机系统应该为空',
  `loginUrl` varchar(255) DEFAULT '/login' COMMENT '用户登录地址',
  `adminUrl` varchar(255) DEFAULT NULL COMMENT '后台管理地址',
  `contactPerson` varchar(255) DEFAULT NULL COMMENT '联系人',
  `contactMobile` varchar(25) DEFAULT NULL COMMENT '联系手机',
  `contactQQ` varchar(255) DEFAULT NULL COMMENT '联系QQ',
  `contactEmail` varchar(255) DEFAULT NULL COMMENT '联系E-mail',
  `status` int(11) DEFAULT NULL COMMENT '状态\r\n-1:逻辑删除;0:已禁用/待审核;1:正常',
  `remark` text COMMENT '备注说明',
  `otherJson` text COMMENT '其他设置JSON字符串',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` bigint(20) DEFAULT NULL COMMENT '创建用户ID',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` bigint(20) DEFAULT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统信息\r\n各个子系统或者相关系统的基础信息\r\n';

-- ----------------------------
-- Records of base_system
-- ----------------------------
INSERT INTO `base_system` VALUES ('1', '旅行师快速开发平台', '旅行师开发平台', 'http://127.0.0.1', 'default', '/admin/login', '/admin/', '管理员', '18092738888', '83828282', 'admin@qq.com', '1', null, '{\"userName\":\"user\"}', '2016-12-07 21:55:00', '1', '2016-12-25 15:31:19', '1481163390092603');
INSERT INTO `base_system` VALUES ('1482629836003406', '旅行师旅游网站管理系统', '旅行师CMS', 'http://127.0.0.3', 'default', '/admin/login', '/admin/', '张小珺', '13291336565', '65765756', '65765756@qq.com', '1', null, null, null, null, '2016-12-25 15:30:56', '1481163390092603');

-- ----------------------------
-- Table structure for base_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_system_menu`;
CREATE TABLE `base_system_menu` (
  `id` bigint(30) NOT NULL COMMENT '菜单ID',
  `systemId` bigint(30) DEFAULT NULL COMMENT '系统ID',
  `parentId` bigint(30) DEFAULT NULL COMMENT '父级菜单ID',
  `sortNumber` int(11) DEFAULT NULL COMMENT '排序编号',
  `menuNumber` varchar(255) DEFAULT NULL COMMENT '菜单编号',
  `name` varchar(100) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(255) DEFAULT NULL COMMENT '链接URL\r\n访问外部链接地址使用http开始的完整URL地址，访问本系统或者子系统使用以/开头的相对路径URL',
  `target` varchar(20) DEFAULT NULL COMMENT '目标窗口\r\n目标窗口名称，新建窗口_blank，顶级窗口_top，本窗口请留空或者_self，其他固定名称的窗口请直接填写窗口名称',
  `helpUrl` varchar(255) DEFAULT NULL COMMENT '帮助文档URL',
  `iconSkin` varchar(255) DEFAULT NULL COMMENT '图标\r\n字体样式图标直接填写CSS样式名称如fa-home、icon-attachment等，图片URL',
  `briefDescription` varchar(255) DEFAULT NULL COMMENT '简要说明',
  `status` int(11) DEFAULT NULL COMMENT '状态\r\n-1:逻辑删除;0:已禁用/待审核;1:正常',
  `remark` text COMMENT '备注说明',
  `otherJson` text COMMENT '其他设置JSON字符串',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` bigint(20) DEFAULT NULL COMMENT '创建用户ID',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` bigint(20) DEFAULT NULL COMMENT '更新用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统菜单信息\r\n各个子系统或者相关系统的基础信息\r\n';

-- ----------------------------
-- Records of base_system_menu
-- ----------------------------
INSERT INTO `base_system_menu` VALUES ('1482468733903912', '1', '0', '1', '001', '首页', '/admin/', '', '/111.html', 'icon icon-home2 ', '说明，简要说明', '1', '备注说明，备注说明，BeiZhuShuoMing', null, null, null, '2016-12-25 11:37:35', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482468941453029', '1', '0', '3', '003', '系统管理设置', '/admin/baseSystem/', null, null, 'icon icon-hammer-wrench ', null, '1', null, null, null, null, '2016-12-25 11:37:36', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482629982799431', '1482629836003406', '0', '1', '001', '首页', '/admin/', null, null, 'icon icon-home2 ', null, '1', null, null, '2016-12-25 09:39:42', '1481163390092603', '2016-12-25 09:41:05', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630072185476', '1482629836003406', '0', '2', '002', '常规内容管理', null, null, null, 'fa fa-th  ', null, '1', null, null, '2016-12-25 09:41:12', '1481163390092603', '2016-12-25 09:43:35', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630238553746', '1482629836003406', '0', '3', '003', '旅游内容管理', null, null, null, 'fa fa-paper-plane ', null, '1', null, null, '2016-12-25 09:43:58', '1481163390092603', '2016-12-25 09:44:35', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630280054439', '1482629836003406', '0', '4', '004', '微信公众账号', null, null, null, 'fa fa-weixin ', null, '1', null, null, '2016-12-25 09:44:40', '1481163390092603', '2016-12-25 09:45:04', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630375127272', '1482629836003406', '0', '5', '005', '资源附件', null, null, null, 'icon icon-attachment ', null, '1', null, null, '2016-12-25 09:46:15', '1481163390092603', '2016-12-25 09:46:45', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630408370446', '1482629836003406', '0', '6', '006', '网站栏目管理', null, null, null, 'icon icon-tree7 ', null, '1', null, null, '2016-12-25 09:46:48', '1481163390092603', '2016-12-25 09:47:30', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630453609090', '1482629836003406', '0', '7', '007', '用户管理', null, null, null, 'icon icon-users ', null, '1', null, null, '2016-12-25 09:47:33', '1481163390092603', '2016-12-25 09:49:28', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630573564533', '1482629836003406', '0', '8', '008', '系统设置', null, null, null, 'icon icon-hammer-wrench ', null, '1', null, null, '2016-12-25 09:49:33', '1481163390092603', '2016-12-25 09:50:04', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630750174914', '1482629836003406', '1482630573564533', '1', '008001', '网站参数', null, null, null, 'icon icon-earth ', null, '1', null, null, '2016-12-25 09:52:30', '1481163390092603', '2016-12-25 09:52:30', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630776686788', '1482629836003406', '1482630573564533', '2', '008002', '附件上传设置', null, null, null, null, null, '1', null, null, '2016-12-25 09:52:56', '1481163390092603', '2016-12-25 09:52:56', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630801200554', '1482629836003406', '1482630573564533', '3', '008003', '支付宝参数', null, null, null, null, null, '1', null, null, '2016-12-25 09:53:21', '1481163390092603', '2016-12-25 09:53:21', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630831594704', '1482629836003406', '1482630573564533', '4', '008004', '分组设置', null, null, null, 'fa fa-group ', null, '1', null, null, '2016-12-25 09:53:51', '1481163390092603', '2016-12-25 09:53:51', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630878785661', '1482629836003406', '1482630573564533', '5', '008005', '角色权限', null, null, null, null, null, '1', null, null, '2016-12-25 09:54:38', '1481163390092603', '2016-12-25 09:54:38', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630948555472', '1482629836003406', '1482630573564533', '6', '008006', '网站管理', null, null, null, 'icon icon-earth ', null, '1', null, null, '2016-12-25 09:55:48', '1481163390092603', '2016-12-25 09:55:48', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482630990796387', '1482629836003406', '1482630453609090', '1', '007001', '用户列表', null, null, null, null, null, '1', null, null, '2016-12-25 09:56:30', '1481163390092603', '2016-12-25 09:56:30', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631005602126', '1482629836003406', '1482630453609090', '2', '007002', '消息通知', null, null, null, null, null, '1', null, null, '2016-12-25 09:56:45', '1481163390092603', '2016-12-25 09:56:45', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631022114723', '1482629836003406', '1482630453609090', '3', '007003', '评论管理', null, null, null, null, null, '1', null, null, '2016-12-25 09:57:02', '1481163390092603', '2016-12-25 09:57:02', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631039864509', '1482629836003406', '1482630453609090', '4', '007004', '充值记录', null, null, null, null, null, '1', null, null, '2016-12-25 09:57:19', '1481163390092603', '2016-12-25 09:57:19', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631059981191', '1482629836003406', '1482630453609090', '5', '007005', '访问日志', null, null, null, null, null, '1', null, null, '2016-12-25 09:57:39', '1481163390092603', '2016-12-25 09:57:39', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631075079921', '1482629836003406', '1482630453609090', '6', '007006', '流量统计', null, null, null, null, null, '1', null, null, '2016-12-25 09:57:55', '1481163390092603', '2016-12-25 09:57:55', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631097903399', '1482629836003406', '1482630408370446', '1', '006001', '内容分类', null, null, null, null, null, '1', null, null, '2016-12-25 09:58:17', '1481163390092603', '2016-12-25 09:58:17', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631110780654', '1482629836003406', '1482630408370446', '2', '006002', '专题设置', null, null, null, null, null, '1', null, null, '2016-12-25 09:58:30', '1481163390092603', '2016-12-25 09:58:30', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631125180225', '1482629836003406', '1482630408370446', '3', '006003', '标签设置', null, null, null, null, null, '1', null, null, '2016-12-25 09:58:45', '1481163390092603', '2016-12-25 09:58:45', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631149706742', '1482629836003406', '1482630408370446', '4', '006004', '网站栏目设置', null, null, null, null, null, '1', null, null, '2016-12-25 09:59:09', '1481163390092603', '2016-12-25 09:59:09', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631197200828', '1482629836003406', '1482630375127272', '1', '005001', '图片管理', null, null, null, 'icon icon-stack-picture ', null, '1', null, null, '2016-12-25 09:59:57', '1481163390092603', '2016-12-25 09:59:57', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631271832225', '1482629836003406', '1482630375127272', '2', '005002', '音频视频', null, null, null, 'icon icon-film4 ', null, '1', null, null, '2016-12-25 10:01:11', '1481163390092603', '2016-12-25 10:01:11', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631387723738', '1482629836003406', '1482630375127272', '3', '005003', '其他附件', null, null, null, 'icon icon-file-text3 ', null, '1', null, null, '2016-12-25 10:03:07', '1481163390092603', '2016-12-25 10:03:07', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631432116754', '1482629836003406', '1482630375127272', '4', '005004', '附件上传', null, null, null, 'icon icon-file-upload2 ', null, '1', null, null, '2016-12-25 10:03:52', '1481163390092603', '2016-12-25 10:03:52', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631454798538', '1482629836003406', '1482630280054439', '1', '004001', '消息发送', null, null, null, null, null, '1', null, null, '2016-12-25 10:04:14', '1481163390092603', '2016-12-25 10:04:14', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631467789048', '1482629836003406', '1482630280054439', '2', '004002', '微信资源', null, null, null, null, null, '1', null, null, '2016-12-25 10:04:27', '1481163390092603', '2016-12-25 10:04:27', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631481721993', '1482629836003406', '1482630280054439', '3', '004003', '用户列表', null, null, null, null, null, '1', null, null, '2016-12-25 10:04:41', '1481163390092603', '2016-12-25 10:04:41', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631499659402', '1482629836003406', '1482630280054439', '4', '004004', '自动回复设置', null, null, null, null, null, '1', null, null, '2016-12-25 10:04:59', '1481163390092603', '2016-12-25 10:04:59', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631513116057', '1482629836003406', '1482630280054439', '5', '004005', '微信菜单设置', null, null, null, null, null, '1', null, null, '2016-12-25 10:05:13', '1481163390092603', '2016-12-25 10:05:13', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631532849773', '1482629836003406', '1482630280054439', '6', '004006', '微信参数设置', null, null, null, null, null, '1', null, null, '2016-12-25 10:05:32', '1481163390092603', '2016-12-25 10:05:32', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631693716672', '1482629836003406', '1482630238553746', '1', '003001', '旅游线路', null, null, null, null, null, '1', null, null, '2016-12-25 10:08:13', '1481163390092603', '2016-12-25 10:08:13', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631719786592', '1482629836003406', '1482630238553746', '2', '003002', '景点门票', null, null, null, null, null, '1', null, null, '2016-12-25 10:08:39', '1481163390092603', '2016-12-25 10:08:39', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631746527675', '1482629836003406', '1482630238553746', '3', '003003', '酒店民宿', null, null, null, null, null, '1', null, null, '2016-12-25 10:09:06', '1481163390092603', '2016-12-25 10:09:06', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631762539194', '1482629836003406', '1482630238553746', '4', '003004', '包车租车', null, null, null, null, null, '1', null, null, '2016-12-25 10:09:22', '1481163390092603', '2016-12-25 10:09:22', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631774327347', '1482629836003406', '1482630238553746', '5', '003005', '陪游导游', null, null, null, null, null, '1', null, null, '2016-12-25 10:09:34', '1481163390092603', '2016-12-25 10:10:51', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631809279554', '1482629836003406', '1482630238553746', '6', '003006', '订单管理', null, null, null, null, null, '1', null, null, '2016-12-25 10:10:09', '1481163390092603', '2016-12-25 10:10:09', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631895929091', '1482629836003406', '1482630072185476', '1', '002001', '新闻文章', null, null, null, null, null, '1', null, null, '2016-12-25 10:11:35', '1481163390092603', '2016-12-25 10:11:35', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631917336900', '1482629836003406', '1482630072185476', '2', '002002', '图文列表', null, null, null, null, null, '1', null, null, '2016-12-25 10:11:57', '1481163390092603', '2016-12-25 10:11:57', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631938620637', '1482629836003406', '1482630072185476', '3', '002003', '视频资源', null, null, null, null, null, '1', null, null, '2016-12-25 10:12:18', '1481163390092603', '2016-12-25 10:12:18', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482631964513121', '1482629836003406', '1482630072185476', '4', '002004', '所有内容', null, null, null, null, null, '1', null, null, '2016-12-25 10:12:44', '1481163390092603', '2016-12-25 10:12:44', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482636603448540', '1', '0', '2', '002', '一级菜单演示', null, null, null, 'icon icon-city ', null, '1', null, null, '2016-12-25 11:30:03', '1481163390092603', '2016-12-25 11:37:36', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482637008202383', '1', '1482468941453029', '5', '003005', '子系统管理', '/admin/baseSystem/', null, null, 'icon icon-color-sampler ', null, '1', null, null, '2016-12-25 11:36:48', '1481163390092603', '2017-01-07 19:34:51', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482637116384527', '1', '1482636603448540', '1', '002001', '二级菜单演示', null, null, null, 'icon icon-menu2 ', null, '1', null, null, '2016-12-25 11:38:36', '1481163390092603', '2016-12-25 11:38:36', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482637157762612', '1', '1482637116384527', '1', '002001001', '三级菜单演示', null, null, null, 'icon icon-address-book3 ', null, '1', null, null, '2016-12-25 11:39:17', '1481163390092603', '2016-12-25 11:39:17', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482637276555383', '1', '1482637157762612', '1', '002001001001', '4级菜单演示', null, null, null, 'icon icon-design ', null, '1', '建议最多四级菜单,菜单级数大于4级可能会影响系统菜单显示效果', null, '2016-12-25 11:41:16', '1481163390092603', '2016-12-25 11:41:16', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482639945265710', '1', '1482468941453029', '4', '003004', '功能菜单管理', 'javascript:layer.open({type:2,content:basePath+\'/admin/baseSystemMenu/menuManage\',area:[\'97%\',\'95%\']});', null, null, 'icon icon-tree7 ', null, '1', null, null, '2016-12-25 12:25:45', '1481163390092603', '2017-01-07 19:34:51', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482640318563308', '1', '1482468941453029', '1', '003001', '用户管理', '/admin/baseUser/', null, null, 'icon icon-users ', null, '1', null, null, '2016-12-25 12:31:58', '1481163390092603', '2017-01-07 19:34:50', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482640379709261', '1', '1482468941453029', '3', '003003', '组织架构设置', '/admin/baseDepartment/', null, null, 'icon icon-tree6 ', null, '1', null, null, '2016-12-25 12:32:59', '1481163390092603', '2017-01-07 19:34:51', '1481163390092603');
INSERT INTO `base_system_menu` VALUES ('1482640410448990', '1', '1482468941453029', '2', '003002', '角色管理', '/admin/baseRole/', null, null, 'icon icon-user-check ', null, '1', null, null, '2016-12-25 12:33:30', '1481163390092603', '2017-01-07 19:34:51', '1481163390092603');

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user` (
  `id` bigint(20) NOT NULL COMMENT '用户ID',
  `userName` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '登录密码',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-mail',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机',
  `otherContact` varchar(255) DEFAULT NULL COMMENT '其他联系方式',
  `nikename` varchar(255) DEFAULT NULL COMMENT '昵称',
  `photoURL` varchar(255) DEFAULT NULL COMMENT '照片URL',
  `realName` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `roleId` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `departmentId` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `status` int(11) DEFAULT NULL COMMENT '状态\r\n-1:逻辑删除;0:已禁用/待审核;1:正常',
  `remark` text COMMENT '备注说明',
  `otherJson` text COMMENT '其他设置JSON字符串',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `createUserId` bigint(20) DEFAULT NULL COMMENT '创建用户ID',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateUserId` bigint(20) DEFAULT NULL COMMENT '更新用户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息\r\n公用的系统用户基础信息';

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES ('1481163343534554', 'mayier', '05570cd71f87c90d3afef25def5f33ed', 'mayier@163.com', '1112221212', '435435', '马三哥', '/12121/dfd.jpg', '马一二', '1', '1483534390480675', '1', null, '{\"style\":\"lightblue\"}', null, null, '2017-01-08 09:40:43', '1481163343534554');
INSERT INTO `base_user` VALUES ('1481163390092603', 'madj', 'a72f7149bac8318344635c6711af9bfb', 'madj@163.com', '1232323232', '876343454', '笨笨马', '/123/123.gif', '马登军', '1483576333993822', '1483534432769016', '1', '备注说明说明', '{\"style\":\"default\"}', null, '1', null, '1');
INSERT INTO `base_user` VALUES ('1483798229772386', '管理员', '05570cd71f87c90d3afef25def5f33ed', 'admin@lvxs.cn', '13555905909', null, '超管哥', null, '管理员', '1483576333993822', '1483530295553321', '1', null, '{\"style\":\"darkblue\"}', '2017-01-07 22:10:29', '1481163343534554', '2017-01-08 09:42:13', '1481163343534554');
INSERT INTO `base_user` VALUES ('1483814559239470', '王五', '05570cd71f87c90d3afef25def5f33ed', 'wangwu@163.com', '13243434343', '45546546', null, null, '王五五', '1', '1483534390480675', '1', null, null, '2017-01-08 02:42:39', '1481163343534554', '2017-01-08 02:43:18', '1481163343534554');

-- ----------------------------
-- Table structure for base_user_role
-- ----------------------------
DROP TABLE IF EXISTS `base_user_role`;
CREATE TABLE `base_user_role` (
  `id` bigint(30) NOT NULL COMMENT '用户角色ID',
  `userId` bigint(30) DEFAULT NULL COMMENT '用户ID',
  `roleId` bigint(30) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户角色\r\n用户附加角色关联表\r\n';

-- ----------------------------
-- Records of base_user_role
-- ----------------------------
INSERT INTO `base_user_role` VALUES ('1483814598740765', '1483814559239470', '1483576333993822');
INSERT INTO `base_user_role` VALUES ('1483839644065859', '1481163343534554', '1483576333993822');
INSERT INTO `base_user_role` VALUES ('1483839733754908', '1483798229772386', '1483576333993822');

-- ----------------------------
-- Table structure for cms_website
-- ----------------------------
DROP TABLE IF EXISTS `cms_website`;
CREATE TABLE `cms_website` (
  `siteId` bigint(30) NOT NULL,
  `siteName` varchar(100) NOT NULL COMMENT '网站名称',
  `shortName` varchar(100) DEFAULT NULL COMMENT '简短名称',
  `domain` varchar(50) NOT NULL COMMENT '域名',
  `sitePath` varchar(20) NOT NULL COMMENT '路径',
  `domainAlias` varchar(255) DEFAULT NULL COMMENT '域名别名',
  `domainRedirect` varchar(255) DEFAULT NULL COMMENT '域名重定向',
  `description` varchar(255) DEFAULT NULL COMMENT '站点描述',
  `keywords` varchar(255) DEFAULT NULL COMMENT '站点关键字',
  `finalStep` tinyint(4) DEFAULT '2' COMMENT '终审级别',
  `afterCheck` tinyint(4) DEFAULT '2' COMMENT '审核后\r\n1:不能修改删除;2:修改后退回;3:修改后不变',
  `tplIndex` varchar(255) DEFAULT NULL COMMENT '首页模板',
  `defImg` varchar(255) DEFAULT '/r/default/images/no_picture.gif' COMMENT '图片不存在时默认图片',
  `loginUrl` varchar(255) DEFAULT '/login' COMMENT '用户登录地址',
  `adminUrl` varchar(255) DEFAULT NULL COMMENT '后台管理地址',
  `markOn` tinyint(1) DEFAULT '1' COMMENT '开启图片水印',
  `markWidth` int(11) DEFAULT '120' COMMENT '图片最小宽度',
  `markHeight` int(11) DEFAULT '120' COMMENT '图片最小高度',
  `markImage` varchar(100) DEFAULT '/r/default/images/watermark.png' COMMENT '图片水印',
  `markContent` varchar(100) DEFAULT 'www.lvxs.cn' COMMENT '文字水印内容',
  `markSize` int(11) DEFAULT '20' COMMENT '文字水印大小',
  `markColor` varchar(10) DEFAULT '#FF0000' COMMENT '文字水印颜色',
  `markAlpha` int(11) DEFAULT '50' COMMENT '水印透明度（0-100）',
  `markPosition` int(11) DEFAULT '1' COMMENT '水印位置(0-5)',
  `markOffsetX` int(11) DEFAULT '0' COMMENT 'x坐标偏移量',
  `markOffsetY` int(11) DEFAULT '0' COMMENT 'y坐标偏移量',
  `downloadCode` varchar(32) DEFAULT 'LvxsCMS' COMMENT '下载防盗链md5混淆码',
  `downloadTime` int(11) DEFAULT '12' COMMENT '下载有效时间（小时）',
  `emailHost` varchar(50) DEFAULT NULL COMMENT '邮件发送服务器',
  `emailEncoding` varchar(20) DEFAULT NULL COMMENT '邮件发送编码',
  `emailUsername` varchar(100) DEFAULT NULL COMMENT '邮箱用户名',
  `emailPassword` varchar(100) DEFAULT NULL COMMENT '邮箱密码',
  `emailPersonal` varchar(100) DEFAULT NULL COMMENT '邮箱发件人',
  `emailValidate` tinyint(1) DEFAULT '0' COMMENT '开启邮箱验证',
  `viewOnlyChecked` tinyint(1) DEFAULT '0' COMMENT '只有终审才能浏览内容页',
  `contactPerson` varchar(255) DEFAULT NULL COMMENT '联系人',
  `contactMobile` varchar(25) DEFAULT NULL COMMENT '联系手机',
  `contactQQ` varchar(255) DEFAULT NULL COMMENT '联系QQ',
  `contactEmail` varchar(255) DEFAULT NULL COMMENT '联系E-mail',
  `serviceEndDate` date DEFAULT NULL COMMENT '服务到期时间',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `status` int(11) DEFAULT NULL COMMENT '状态\r\n-1逻辑删除、0审批中/暂停、1正常',
  PRIMARY KEY (`siteId`),
  UNIQUE KEY `ak_domain_path` (`domain`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='站点基础信息';
