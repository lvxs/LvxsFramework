package org.cnitti.interceptor;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.cnitti.Consts;
import org.cnitti.model.BaseSystem;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
/**
 * 多系统系统识别拦截器
 * @author madj
 */
public class BaseSystemInterceptor implements Interceptor {

	public void intercept(Invocation inv) {

		Controller controller = inv.getController();
		HttpServletRequest request = controller.getRequest();
		String cpath = request.getContextPath();
		String basePath=StringUtils.isNotBlank(cpath)?cpath:"";
		controller.setAttr("basePath",basePath);
		if(request.getServletContext().getAttribute(Consts.SYSTEM_LIST)==null){
			List<BaseSystem> systemList = BaseSystem.DAO.find("Select * from base_system");
			request.getServletContext().setAttribute(Consts.SYSTEM_LIST,systemList);
		}
		if(controller.getSessionAttr(Consts.SYSTEM)==null){
			StringBuffer requestURL = request.getRequestURL();
			String systemUrl = requestURL.substring(0,requestURL.indexOf("/",requestURL.indexOf("//")+2));
			if(StrKit.notBlank(cpath)){
				//systemUrl += cpath;
			}
			@SuppressWarnings("unchecked")
			List<BaseSystem> systemList = (List<BaseSystem>)request.getServletContext().getAttribute(Consts.SYSTEM_LIST);
			for(BaseSystem system:systemList){
				if(system.getUrl()!=null && system.getUrl().equalsIgnoreCase(systemUrl)){
					controller.setSessionAttr(Consts.SYSTEM,system);
					break;
				}
			}
		}
		if(controller.getSessionAttr(Consts.SYSTEM)==null){
			@SuppressWarnings("unchecked")
			List<BaseSystem> systemList = (List<BaseSystem>)request.getServletContext().getAttribute(Consts.SYSTEM_LIST);
			if(systemList!=null && !systemList.isEmpty()){
				controller.setSessionAttr(Consts.SYSTEM,systemList.get(0));
			}
		}
		BaseSystem system = controller.getSessionAttr(Consts.SYSTEM);
		if(system==null){ 
			system = new BaseSystem();
			system.setId(1L);
			system.setFullName("基础权限框架系统");
			system.setShortName("权限框架");
			system.setUrl("default");
			system.setFilePath("default");
			system.setLoginUrl("/");
			system.setAdminUrl("/admin/");
			system.setContactEmail("admin@qq.com");
			system.setContactMobile("");
			system.setContactPerson("管理员");
			system.setContactQQ("");
			system.setStatus(1);
			system.setOtherJson("{}");
			system.setRemark("");
			system.setCreateTime(new Date());
			system.setCreateUserId(1L);
			system.setUpdateTime(new Date());
			system.setUpdateUserId(1L);
			system.save();
		}
		controller.setAttr("resPath",basePath+"/res/"+system.getFilePath()+"/");
		controller.setAttr(Consts.SYSTEM,system);
		String adminThemes = controller.getSessionAttr(Consts.ADMIN_THEMES);
		if(StrKit.isBlank(adminThemes)){
			adminThemes="admin";
			controller.setSessionAttr(Consts.ADMIN_THEMES,adminThemes);
		}
		inv.invoke();
	}

}
