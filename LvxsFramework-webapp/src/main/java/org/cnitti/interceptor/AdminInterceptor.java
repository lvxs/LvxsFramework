package org.cnitti.interceptor;

import org.cnitti.Consts;
import org.cnitti.model.BaseUser;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * 管理员登录身份验证拦截器
 * @author madj
 *
 */
public class AdminInterceptor implements Interceptor {

	public void intercept(Invocation inv) {

		Controller controller = inv.getController();
		
		String target = controller.getRequest().getRequestURI();
		String cpath = controller.getRequest().getContextPath();

		if (!target.startsWith(cpath + "/admin")) {
			inv.invoke();
			return;
		}

		BaseUser user = InterceptorUtils.tryToGetUser(inv);
		if (user != null) {
			controller.setAttr(Consts.ATTR_USER, user);
			inv.invoke();
			return;
		}
		inv.invoke();
		return;
	}

}
