package org.cnitti.interceptor;

import java.math.BigInteger;

import org.cnitti.Consts;
import org.cnitti.model.BaseUser;

import com.jfinal.aop.Invocation;


public class InterceptorUtils {

	public static BaseUser tryToGetUser(Invocation inv) {

		String userId = inv.getController().getCookie(Consts.COOKIE_LOGINED_USER);
		if (userId != null && !"".equals(userId)) {
			return BaseUser.DAO.findById(new BigInteger(userId));
		}
		return null;
	}

}
