package org.cnitti.interceptor;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.cnitti.Consts;
import org.cnitti.model.BaseUser;
import org.cnitti.utils.StringUtils;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * 简单后台管理登录拦截
 */
public class ManagerInterceptor implements Interceptor {

	public void intercept(Invocation ai) {
		Controller controller = ai.getController();
		// 获取shiro中的session
		Subject subject = SecurityUtils.getSubject();
		String style = controller.getCookie("style");
		BaseUser loginUser = (BaseUser) subject.getSession().getAttribute(Consts.ADMIN_USER);
		// session为空
		if (null == loginUser) {
			controller.setAttr("style",style);
		} else {
			controller.setAttr("loginUser", loginUser);
			style = loginUser.getDefaultStyle();
			if(!StringUtils.isNotEmpty(style)){
				style = controller.getCookie("style");
			}
			controller.setAttr("style",style);
			controller.setAttr("c", controller.getPara("c"));
			controller.setAttr("p", controller.getPara("p"));
			controller.setAttr("m", controller.getPara("m"));
			controller.setAttr("t", controller.getPara("t"));
			controller.setAttr("s", controller.getPara("s"));
			controller.setAttr("page", controller.getPara("page"));
		}		
		ai.invoke();// 注意 一定要执行此方法
	}

	
}
