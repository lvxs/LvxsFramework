package org.cnitti.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.jfinal.kit.PathKit;
import com.jfinal.upload.UploadFile;

/**
 * 附件操作工具类
 * 
 * @author Administrator
 *
 */
public class AttachmentUtils {

	static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy" + File.separator + "MMdd");

	/**
	 * 保存附件文件到attachment目录下
	 * @param uploadFile 文件上传对象
	 * @return 保存文件的相对于Web目录的相对路径
	 */
	public static String moveFile(UploadFile uploadFile) {
		return moveFile(uploadFile,null);
	}

	/**
	 * 保存附件文件到指定目录下
	 * @param uploadFile 文件上传对象
	 * @param folderPath 指定目录
	 * @return 保存文件的相对于Web目录的相对路径
	 */
	public static String moveFile(UploadFile uploadFile, String folderPath) {
		if (uploadFile == null)
			return null;

		File file = uploadFile.getFile();
		if (!file.exists()) {
			return null;
		}
		if (StringUtils.isEmpty(folderPath)) {
			folderPath = "attachment/" + dateFormat.format(new Date());
		} else {
			folderPath = folderPath+"/" + dateFormat.format(new Date());
		}

		String webRoot = PathKit.getWebRootPath();

		String uuid = UUID.randomUUID().toString().replace("-", "");

		String newFileName = webRoot + "/" + folderPath + "/" + uuid + FileUtils.getSuffix(file.getName());
		
		newFileName = newFileName.replaceAll("\\\\","/");
		while(newFileName.contains("//")){
			newFileName = newFileName.replaceAll("//","/");
		}
		
		File newfile = new File(newFileName);

		if (!newfile.getParentFile().exists()) {
			newfile.getParentFile().mkdirs();
		}

		file.renameTo(newfile);

		return FileUtils.removePrefix(newfile.getAbsolutePath(), webRoot);
	}

	static List<String> imageSuffix = new ArrayList<String>();

	static {
		imageSuffix.add(".jpg");
		imageSuffix.add(".jpeg");
		imageSuffix.add(".png");
		imageSuffix.add(".bmp");
		imageSuffix.add(".gif");
	}

	/**
	 * 通过文件路径判断文件是否是图片
	 * 
	 * @param path
	 *            文件路径
	 * @return 是图片返回true，否则返回false
	 */
	public static boolean isImage(String path) {
		String sufffix = FileUtils.getSuffix(path);
		if (StringUtils.isNotBlank(sufffix))
			return imageSuffix.contains(sufffix.toLowerCase());
		return false;
	}
}
