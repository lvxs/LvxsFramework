package org.cnitti.utils;

/**
 * 特殊字符、字符转义处理方法类，继承自org.apache.commons.lang3.StringEscapeUtils。
 * 目前没有具体定制的方法，以后有的话需要写在这里面
 * @author 马登军
 */
public class EscapeUtils extends org.apache.commons.lang3.StringEscapeUtils{
	
}
