package org.cnitti.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import com.jfinal.core.JFinal;
import com.jfinal.log.Log;

public class StringUtils extends org.apache.commons.lang3.StringUtils {
	private static final Log log = Log.getLog(StringUtils.class);
	public static String urlDecode(String string) {
		try {
			return URLDecoder.decode(string, JFinal.me().getConstants().getEncoding());
		} catch (UnsupportedEncodingException e) {
			log.error("urlDecode is error", e);
		}
		return string;
	}

	public static String urlEncode(String string) {
		try {
			return URLEncoder.encode(string, JFinal.me().getConstants().getEncoding());
		} catch (UnsupportedEncodingException e) {
			log.error("urlEncode is error", e);
		}
		return string;
	}
	
	public static String urlRedirect(String redirect){
		try {
			redirect = new String(redirect.getBytes(JFinal.me().getConstants().getEncoding()), "ISO8859_1");
		} catch (UnsupportedEncodingException e) {
			log.error("urlRedirect is error", e);
		}
		return redirect;
	}

	public static boolean areNotEmpty(String... strings) {
		if (strings == null || strings.length == 0)
			return false;

		for (String string : strings) {
			if (string == null || "".equals(string)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNotEmpty(String string) {
		return string != null && !string.equals("");
	}

	public static boolean areNotBlank(String... strings) {
		if (strings == null || strings.length == 0)
			return false;

		for (String string : strings) {
			if (string == null || "".equals(string.trim())) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNotBlank(String string) {
		return string != null && !string.trim().equals("");
	}

	public static long toLong(String value, Long defaultValue) {
		try {
			if (value == null || "".equals(value.trim()))
				return defaultValue;
			value = value.trim().replace("N","-").replace("n","-");
			return Long.parseLong(value);
		} catch (Exception e) {
		}
		return defaultValue;
	}

	public static int toInt(String value, int defaultValue) {
		try {
			if (value == null || "".equals(value.trim()))
				return defaultValue;
			value = value.trim().replace("N","-").replace("n","-");
			return Integer.parseInt(value);
		} catch (Exception e) {
		}
		return defaultValue;
	}
	/**
	 * 字符串转BigInteger数值
	 * @param value 字符串
	 * @param defaultValue 默认值
	 * @return BigInteger数值
	 */
	public static BigInteger toBigInteger(String value, BigInteger defaultValue) {
		try {
			if (value == null || "".equals(value.trim()))
				return defaultValue;
			value = value.trim().replace("N","-").replace("n","-");
			return new BigInteger(value);
		} catch (Exception e) {
		}
		return defaultValue;
	}
	/**
	 * 从字符串中获取逗号分隔的BigInteger数组
	 */
	public static BigInteger[] getBigIntegerArray(String para) {
		if(para!=null){
			para = para.trim().replace("N","-").replace("n","-");
			String[] typeIds = para.split(",");
			ArrayList<BigInteger> typeIdList = new ArrayList<BigInteger>();
			for(String typeId:typeIds){
				typeIdList.add(toBigInteger(typeId.trim(),BigInteger.ZERO));
			}
			if(typeIdList.size()>0){
				BigInteger[] supplierTypeIds = new BigInteger[typeIdList.size()];
				return typeIdList.toArray(supplierTypeIds);
			}else{
				return new BigInteger[0];
			}
		}else{
			return new BigInteger[0];
		}
	}

	/**
	 * 字符串转Long型数字
	 * @param para 字符串
	 * @return Long型数字
	 */
	public static Long getLong(String para) {
		try{
			if(para!=null){
				para = para.trim().replace("N","-").replace("n","-");
				return Long.parseLong(para);
			}
		} catch (Exception err){
		}
		return 0L; 
	}

	/**
	 * 字符串转Integer型数字
	 * @param para 字符串
	 * @return Integer型数字
	 */
	public static Integer getInteger(String para) {
		try{
			if(para!=null){
				para = para.trim().replace("N","-").replace("n","-");
				return Integer.parseInt(para);
			}
		} catch (Exception err){
		}
		return 0; 
	}
	/**
	 * 从字符串中获取逗号分隔的长整形数组
	 */
	public static Long[] getLongArray(String para) {
		if(isNotBlank(para)){
			para = para.trim().replace("N","-").replace("n","-");
			String[] typeIds = para.split(",");
			ArrayList<Long> typeIdList = new ArrayList<Long>();
			for(String typeId:typeIds){
				typeIdList.add(getLong(typeId.trim()));
			}
			if(typeIdList.size()>0){
				Long[] supplierTypeIds = new Long[typeIdList.size()];
				return typeIdList.toArray(supplierTypeIds);
			}else{
				return new Long[0];
			}
		}else{
			return new Long[0];
		}
	}

	/**
	 * 从字符串中获取逗号分隔的长整形数组
	 */
	public static long[] getlongArray(final String para) {
		Long[] array = getLongArray(para);
		long[] value = new long[array.length];
		for(int i=0;i<array.length;i++){
			value[i]=array[i].longValue();
		}
		return value;
	}
	/**
	 * 从字符串中获取逗号分隔的整形数组
	 */
	public static Integer[] getIntegerArray(String para) {
		if(para!=null){
			para = para.trim().replace("N","-").replace("n","-");
			String[] typeIds = para.split(",");
			ArrayList<Integer> typeIdList = new ArrayList<Integer>();
			for(String typeId:typeIds){
				typeIdList.add(getInteger(typeId));
			}
			if(typeIdList.size()>0){
				Integer[] supplierTypeIds = new Integer[typeIdList.size()];
				return typeIdList.toArray(supplierTypeIds);
			}else{
				return new Integer[0];
			}
		}else{
			return new Integer[0];
		}
	}

	
}
