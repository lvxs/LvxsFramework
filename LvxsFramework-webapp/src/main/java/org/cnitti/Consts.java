package org.cnitti;

public class Consts {
	
	public static final String COOKIE_LOGINED_USER = "user";

	public static final String CHARTSET_UTF8 = "UTF-8";

	public static final String ROUTER_CONTENT = "/c";
	public static final String ROUTER_TAXONOMY = "/t";
	public static final String ROUTER_USER = "/user";
	public static final String ROUTER_USER_CENTER = ROUTER_USER + "/center";
	public static final String ROUTER_USER_LOGIN = ROUTER_USER + "/login";
	public static final String ROUTER_ADMIN = "/admin";
	public static final String ROUTER_ADMIN_CENTER = ROUTER_ADMIN + "/index";
	public static final String ROUTER_ADMIN_LOGIN = ROUTER_ADMIN + "/login";

	public static final int ERROR_CODE_NOT_VALIDATE_CAPTHCHE = 1;
	public static final int ERROR_CODE_USERNAME_EMPTY = 2;
	public static final int ERROR_CODE_USERNAME_EXIST = 3;
	public static final int ERROR_CODE_EMAIL_EMPTY = 4;
	public static final int ERROR_CODE_EMAIL_EXIST = 5;
	public static final int ERROR_CODE_PHONE_EMPTY = 6;
	public static final int ERROR_CODE_PHONE_EXIST = 7;
	public static final int ERROR_CODE_PASSWORD_EMPTY = 8;

	public static final String ATTR_USER = "user";

	public static final String SITE_LIST = "siteList";

	public static final String WEB_SITE = "webSite";

	public static final String SYSTEM_LIST= "systemList";

	public static final String SYSTEM = "system";

	public static final String ADMIN_USER = "admin_user";

	public static final String ADMIN_THEMES = "admin_themes";

	public static final String TABLE_META_MAP = "tableMetaMap";

	// Shiro default
	public static final String DEFAULT_AUTHENTICATION_QUERY = "SELECT * FROM base_user WHERE (userName = ? or email = ? or mobile = ? )";
	public static final String DEFAULT_USER_ROLES_QUERY = "SELECT * FROM base_role WHERE id = (select roleId from base_user where id = ? ) or id in (select roleId from base_user_role where userId = ? )";
	public static final String DEFAULT_PERMISSIONS_QUERY = "SELECT id FROM base_role_permission WHERE roleId = ?";
	public static final boolean PERMISSIONS_LOOKUP_ENABLED = true; // 权限查看设置

	public static final String ERROR_CODE = "errorCode";
	public static final String MESSAGE = "message";
}
