package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 系统信息
 * 各个子系统或者相关系统的基础信息

 */
@SuppressWarnings("serial")
public class _BaseSystem <M extends _BaseSystem<M>> extends Model<M> implements IBean  {
	/**
	 * 获取系统ID
	* ID=1的为默认基础框架系统
	*/
	 public java.lang.Long getId() {
		return get("id");
	}
	
	/**
	 * 设置系统ID
	* ID=1的为默认基础框架系统
	* @param  系统ID
	*/
	 public void setId(java.lang.Long id) {
		set("id",id);
	}
	
	/**
	 * 获取系统全称
	*/
	 public java.lang.String getFullName() {
		return get("fullName");
	}
	
	/**
	 * 设置系统全称
	* @param  系统全称
	*/
	 public void setFullName(java.lang.String fullName) {
		set("fullName",fullName);
	}
	
	/**
	 * 获取系统简称
	*/
	 public java.lang.String getShortName() {
		return get("shortName");
	}
	
	/**
	 * 设置系统简称
	* @param  系统简称
	*/
	 public void setShortName(java.lang.String shortName) {
		set("shortName",shortName);
	}
	
	/**
	 * 获取访问URL
	* 到应用存放路径(ContextPath)为止，不必以“/”结尾
	*/
	 public java.lang.String getUrl() {
		return get("url");
	}
	
	/**
	 * 设置访问URL
	* 到应用存放路径(ContextPath)为止，不必以“/”结尾
	* @param  访问URL
	*/
	 public void setUrl(java.lang.String url) {
		set("url",url);
	}
	
	/**
	 * 获取文件路径
	* 附件、资源文件或者模板页面存储路径标识。本机上运行的子系统必填而且必须唯一，非本机系统应该为空
	*/
	 public java.lang.String getFilePath() {
		return get("filePath");
	}
	
	/**
	 * 设置文件路径
	* 附件、资源文件或者模板页面存储路径标识。本机上运行的子系统必填而且必须唯一，非本机系统应该为空
	* @param  文件路径
	*/
	 public void setFilePath(java.lang.String filePath) {
		set("filePath",filePath);
	}
	
	/**
	 * 获取用户登录地址
	*/
	 public java.lang.String getLoginUrl() {
		return get("loginUrl");
	}
	
	/**
	 * 设置用户登录地址
	* @param  用户登录地址
	*/
	 public void setLoginUrl(java.lang.String loginUrl) {
		set("loginUrl",loginUrl);
	}
	
	/**
	 * 获取后台管理地址
	*/
	 public java.lang.String getAdminUrl() {
		return get("adminUrl");
	}
	
	/**
	 * 设置后台管理地址
	* @param  后台管理地址
	*/
	 public void setAdminUrl(java.lang.String adminUrl) {
		set("adminUrl",adminUrl);
	}
	
	/**
	 * 获取联系人
	*/
	 public java.lang.String getContactPerson() {
		return get("contactPerson");
	}
	
	/**
	 * 设置联系人
	* @param  联系人
	*/
	 public void setContactPerson(java.lang.String contactPerson) {
		set("contactPerson",contactPerson);
	}
	
	/**
	 * 获取联系手机
	*/
	 public java.lang.String getContactMobile() {
		return get("contactMobile");
	}
	
	/**
	 * 设置联系手机
	* @param  联系手机
	*/
	 public void setContactMobile(java.lang.String contactMobile) {
		set("contactMobile",contactMobile);
	}
	
	/**
	 * 获取联系QQ
	*/
	 public java.lang.String getContactQQ() {
		return get("contactQQ");
	}
	
	/**
	 * 设置联系QQ
	* @param  联系QQ
	*/
	 public void setContactQQ(java.lang.String contactQQ) {
		set("contactQQ",contactQQ);
	}
	
	/**
	 * 获取联系E-mail
	*/
	 public java.lang.String getContactEmail() {
		return get("contactEmail");
	}
	
	/**
	 * 设置联系E-mail
	* @param  联系E-mail
	*/
	 public void setContactEmail(java.lang.String contactEmail) {
		set("contactEmail",contactEmail);
	}
	
	/**
	 * 获取状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
	/**
	 * 获取备注说明
	*/
	 public java.lang.String getRemark() {
		return get("remark");
	}
	
	/**
	 * 设置备注说明
	* @param  备注说明
	*/
	 public void setRemark(java.lang.String remark) {
		set("remark",remark);
	}
	
	/**
	 * 获取其他设置JSON字符串
	*/
	 public java.lang.String getOtherJson() {
		return get("otherJson");
	}
	
	/**
	 * 设置其他设置JSON字符串
	* @param  其他设置JSON字符串
	*/
	 public void setOtherJson(java.lang.String otherJson) {
		set("otherJson",otherJson);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取创建用户ID
	*/
	 public java.lang.Long getCreateUserId() {
		return get("createUserId");
	}
	
	/**
	 * 设置创建用户ID
	* @param  创建用户ID
	*/
	 public void setCreateUserId(java.lang.Long createUserId) {
		set("createUserId",createUserId);
	}
	
	/**
	 * 获取更新时间
	*/
	 public java.util.Date getUpdateTime() {
		return get("updateTime");
	}
	
	/**
	 * 设置更新时间
	* @param  更新时间
	*/
	 public void setUpdateTime(java.util.Date updateTime) {
		set("updateTime",updateTime);
	}
	
	/**
	 * 获取更新用户ID
	*/
	 public java.lang.Long getUpdateUserId() {
		return get("updateUserId");
	}
	
	/**
	 * 设置更新用户ID
	* @param  更新用户ID
	*/
	 public void setUpdateUserId(java.lang.Long updateUserId) {
		set("updateUserId",updateUserId);
	}
	
}
