package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 自定义表单
 * 自定义表单基础信息
 */
@SuppressWarnings("serial")
public class _OaCustomForm <M extends _OaCustomForm<M>> extends Model<M> implements IBean  {
	/**
	 * 获取ID
	*/
	 public java.lang.Long getId() {
		return get("id");
	}
	
	/**
	 * 设置ID
	* @param  ID
	*/
	 public void setId(java.lang.Long id) {
		set("id",id);
	}
	
	/**
	 * 获取表单名称
	*/
	 public java.lang.String getName() {
		return get("name");
	}
	
	/**
	 * 设置表单名称
	* @param  表单名称
	*/
	 public void setName(java.lang.String name) {
		set("name",name);
	}
	
	/**
	 * 获取物理表名称
	*/
	 public java.lang.String getPhysicalName() {
		return get("physicalName");
	}
	
	/**
	 * 设置物理表名称
	* @param  物理表名称
	*/
	 public void setPhysicalName(java.lang.String physicalName) {
		set("physicalName",physicalName);
	}
	
	/**
	 * 获取设计源码
	*/
	 public java.lang.String getDesignCode() {
		return get("designCode");
	}
	
	/**
	 * 设置设计源码
	* @param  设计源码
	*/
	 public void setDesignCode(java.lang.String designCode) {
		set("designCode",designCode);
	}
	
	/**
	 * 获取附加JavaScript和Css样式代码
	*/
	 public java.lang.String getJsAndCss() {
		return get("jsAndCss");
	}
	
	/**
	 * 设置附加JavaScript和Css样式代码
	* @param  附加JavaScript和Css样式代码
	*/
	 public void setJsAndCss(java.lang.String jsAndCss) {
		set("jsAndCss",jsAndCss);
	}
	
	/**
	 * 获取列表页面查询SQL
	* 列表页面查询Where条件附加SQL
	*/
	 public java.lang.String getListWhereSql() {
		return get("listWhereSql");
	}
	
	/**
	 * 设置列表页面查询SQL
	* 列表页面查询Where条件附加SQL
	* @param  列表页面查询SQL
	*/
	 public void setListWhereSql(java.lang.String listWhereSql) {
		set("listWhereSql",listWhereSql);
	}
	
	/**
	 * 获取备注说明
	*/
	 public java.lang.String getRemark() {
		return get("remark");
	}
	
	/**
	 * 设置备注说明
	* @param  备注说明
	*/
	 public void setRemark(java.lang.String remark) {
		set("remark",remark);
	}
	
	/**
	 * 获取类型
	* 1:自定义表单;2:实体表格;3:实体代码
	*/
	 public java.lang.Integer getType() {
		return get("type");
	}
	
	/**
	 * 设置类型
	* 1:自定义表单;2:实体表格;3:实体代码
	* @param  类型
	*/
	 public void setType(java.lang.Integer type) {
		set("type",type);
	}
	
	/**
	 * 获取排序编号
	*/
	 public java.lang.Integer getSortNumber() {
		return get("sortNumber");
	}
	
	/**
	 * 设置排序编号
	* @param  排序编号
	*/
	 public void setSortNumber(java.lang.Integer sortNumber) {
		set("sortNumber",sortNumber);
	}
	
	/**
	 * 获取状态
	* 1:正常;0:审批中;-1:逻辑删除
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* 1:正常;0:审批中;-1:逻辑删除
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
	/**
	 * 获取其他设置JSON字符串
	*/
	 public java.lang.String getOtherJson() {
		return get("otherJson");
	}
	
	/**
	 * 设置其他设置JSON字符串
	* @param  其他设置JSON字符串
	*/
	 public void setOtherJson(java.lang.String otherJson) {
		set("otherJson",otherJson);
	}
	
	/**
	 * 获取创建用户ID
	*/
	 public java.lang.Long getCreateUserId() {
		return get("createUserId");
	}
	
	/**
	 * 设置创建用户ID
	* @param  创建用户ID
	*/
	 public void setCreateUserId(java.lang.Long createUserId) {
		set("createUserId",createUserId);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取更新用户ID
	*/
	 public java.lang.Long getUpdateUserId() {
		return get("updateUserId");
	}
	
	/**
	 * 设置更新用户ID
	* @param  更新用户ID
	*/
	 public void setUpdateUserId(java.lang.Long updateUserId) {
		set("updateUserId",updateUserId);
	}
	
	/**
	 * 获取更新时间
	*/
	 public java.util.Date getUpdateTime() {
		return get("updateTime");
	}
	
	/**
	 * 设置更新时间
	* @param  更新时间
	*/
	 public void setUpdateTime(java.util.Date updateTime) {
		set("updateTime",updateTime);
	}
	
}
