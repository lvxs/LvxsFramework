package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 站点基础信息
 * 
 */
@SuppressWarnings("serial")
public class _CmsWebsite <M extends _CmsWebsite<M>> extends Model<M> implements IBean  {
	/**
	 * 获取siteId
	*/
	 public java.lang.Long getSiteId() {
		return get("siteId");
	}
	
	/**
	 * 设置siteId
	* @param  siteId
	*/
	 public void setSiteId(java.lang.Long siteId) {
		set("siteId",siteId);
	}
	
	/**
	 * 获取网站名称
	*/
	 public java.lang.String getSiteName() {
		return get("siteName");
	}
	
	/**
	 * 设置网站名称
	* @param  网站名称
	*/
	 public void setSiteName(java.lang.String siteName) {
		set("siteName",siteName);
	}
	
	/**
	 * 获取简短名称
	*/
	 public java.lang.String getShortName() {
		return get("shortName");
	}
	
	/**
	 * 设置简短名称
	* @param  简短名称
	*/
	 public void setShortName(java.lang.String shortName) {
		set("shortName",shortName);
	}
	
	/**
	 * 获取域名
	*/
	 public java.lang.String getDomain() {
		return get("domain");
	}
	
	/**
	 * 设置域名
	* @param  域名
	*/
	 public void setDomain(java.lang.String domain) {
		set("domain",domain);
	}
	
	/**
	 * 获取路径
	*/
	 public java.lang.String getSitePath() {
		return get("sitePath");
	}
	
	/**
	 * 设置路径
	* @param  路径
	*/
	 public void setSitePath(java.lang.String sitePath) {
		set("sitePath",sitePath);
	}
	
	/**
	 * 获取域名别名
	*/
	 public java.lang.String getDomainAlias() {
		return get("domainAlias");
	}
	
	/**
	 * 设置域名别名
	* @param  域名别名
	*/
	 public void setDomainAlias(java.lang.String domainAlias) {
		set("domainAlias",domainAlias);
	}
	
	/**
	 * 获取域名重定向
	*/
	 public java.lang.String getDomainRedirect() {
		return get("domainRedirect");
	}
	
	/**
	 * 设置域名重定向
	* @param  域名重定向
	*/
	 public void setDomainRedirect(java.lang.String domainRedirect) {
		set("domainRedirect",domainRedirect);
	}
	
	/**
	 * 获取站点描述
	*/
	 public java.lang.String getDescription() {
		return get("description");
	}
	
	/**
	 * 设置站点描述
	* @param  站点描述
	*/
	 public void setDescription(java.lang.String description) {
		set("description",description);
	}
	
	/**
	 * 获取站点关键字
	*/
	 public java.lang.String getKeywords() {
		return get("keywords");
	}
	
	/**
	 * 设置站点关键字
	* @param  站点关键字
	*/
	 public void setKeywords(java.lang.String keywords) {
		set("keywords",keywords);
	}
	
	/**
	 * 获取终审级别
	*/
	 public java.lang.Integer getFinalStep() {
		return get("finalStep");
	}
	
	/**
	 * 设置终审级别
	* @param  终审级别
	*/
	 public void setFinalStep(java.lang.Integer finalStep) {
		set("finalStep",finalStep);
	}
	
	/**
	 * 获取审核后
	* 1:不能修改删除;2:修改后退回;3:修改后不变
	*/
	 public java.lang.Integer getAfterCheck() {
		return get("afterCheck");
	}
	
	/**
	 * 设置审核后
	* 1:不能修改删除;2:修改后退回;3:修改后不变
	* @param  审核后
	*/
	 public void setAfterCheck(java.lang.Integer afterCheck) {
		set("afterCheck",afterCheck);
	}
	
	/**
	 * 获取首页模板
	*/
	 public java.lang.String getTplIndex() {
		return get("tplIndex");
	}
	
	/**
	 * 设置首页模板
	* @param  首页模板
	*/
	 public void setTplIndex(java.lang.String tplIndex) {
		set("tplIndex",tplIndex);
	}
	
	/**
	 * 获取图片不存在时默认图片
	*/
	 public java.lang.String getDefImg() {
		return get("defImg");
	}
	
	/**
	 * 设置图片不存在时默认图片
	* @param  图片不存在时默认图片
	*/
	 public void setDefImg(java.lang.String defImg) {
		set("defImg",defImg);
	}
	
	/**
	 * 获取用户登录地址
	*/
	 public java.lang.String getLoginUrl() {
		return get("loginUrl");
	}
	
	/**
	 * 设置用户登录地址
	* @param  用户登录地址
	*/
	 public void setLoginUrl(java.lang.String loginUrl) {
		set("loginUrl",loginUrl);
	}
	
	/**
	 * 获取后台管理地址
	*/
	 public java.lang.String getAdminUrl() {
		return get("adminUrl");
	}
	
	/**
	 * 设置后台管理地址
	* @param  后台管理地址
	*/
	 public void setAdminUrl(java.lang.String adminUrl) {
		set("adminUrl",adminUrl);
	}
	
	/**
	 * 获取开启图片水印
	*/
	 public java.lang.Boolean getMarkOn() {
		return get("markOn");
	}
	
	/**
	 * 设置开启图片水印
	* @param  开启图片水印
	*/
	 public void setMarkOn(java.lang.Boolean markOn) {
		set("markOn",markOn);
	}
	
	/**
	 * 获取图片最小宽度
	*/
	 public java.lang.Integer getMarkWidth() {
		return get("markWidth");
	}
	
	/**
	 * 设置图片最小宽度
	* @param  图片最小宽度
	*/
	 public void setMarkWidth(java.lang.Integer markWidth) {
		set("markWidth",markWidth);
	}
	
	/**
	 * 获取图片最小高度
	*/
	 public java.lang.Integer getMarkHeight() {
		return get("markHeight");
	}
	
	/**
	 * 设置图片最小高度
	* @param  图片最小高度
	*/
	 public void setMarkHeight(java.lang.Integer markHeight) {
		set("markHeight",markHeight);
	}
	
	/**
	 * 获取图片水印
	*/
	 public java.lang.String getMarkImage() {
		return get("markImage");
	}
	
	/**
	 * 设置图片水印
	* @param  图片水印
	*/
	 public void setMarkImage(java.lang.String markImage) {
		set("markImage",markImage);
	}
	
	/**
	 * 获取文字水印内容
	*/
	 public java.lang.String getMarkContent() {
		return get("markContent");
	}
	
	/**
	 * 设置文字水印内容
	* @param  文字水印内容
	*/
	 public void setMarkContent(java.lang.String markContent) {
		set("markContent",markContent);
	}
	
	/**
	 * 获取文字水印大小
	*/
	 public java.lang.Integer getMarkSize() {
		return get("markSize");
	}
	
	/**
	 * 设置文字水印大小
	* @param  文字水印大小
	*/
	 public void setMarkSize(java.lang.Integer markSize) {
		set("markSize",markSize);
	}
	
	/**
	 * 获取文字水印颜色
	*/
	 public java.lang.String getMarkColor() {
		return get("markColor");
	}
	
	/**
	 * 设置文字水印颜色
	* @param  文字水印颜色
	*/
	 public void setMarkColor(java.lang.String markColor) {
		set("markColor",markColor);
	}
	
	/**
	 * 获取水印透明度（0-100）
	*/
	 public java.lang.Integer getMarkAlpha() {
		return get("markAlpha");
	}
	
	/**
	 * 设置水印透明度（0-100）
	* @param  水印透明度（0-100）
	*/
	 public void setMarkAlpha(java.lang.Integer markAlpha) {
		set("markAlpha",markAlpha);
	}
	
	/**
	 * 获取水印位置(0-5)
	*/
	 public java.lang.Integer getMarkPosition() {
		return get("markPosition");
	}
	
	/**
	 * 设置水印位置(0-5)
	* @param  水印位置(0-5)
	*/
	 public void setMarkPosition(java.lang.Integer markPosition) {
		set("markPosition",markPosition);
	}
	
	/**
	 * 获取x坐标偏移量
	*/
	 public java.lang.Integer getMarkOffsetX() {
		return get("markOffsetX");
	}
	
	/**
	 * 设置x坐标偏移量
	* @param  x坐标偏移量
	*/
	 public void setMarkOffsetX(java.lang.Integer markOffsetX) {
		set("markOffsetX",markOffsetX);
	}
	
	/**
	 * 获取y坐标偏移量
	*/
	 public java.lang.Integer getMarkOffsetY() {
		return get("markOffsetY");
	}
	
	/**
	 * 设置y坐标偏移量
	* @param  y坐标偏移量
	*/
	 public void setMarkOffsetY(java.lang.Integer markOffsetY) {
		set("markOffsetY",markOffsetY);
	}
	
	/**
	 * 获取下载防盗链md5混淆码
	*/
	 public java.lang.String getDownloadCode() {
		return get("downloadCode");
	}
	
	/**
	 * 设置下载防盗链md5混淆码
	* @param  下载防盗链md5混淆码
	*/
	 public void setDownloadCode(java.lang.String downloadCode) {
		set("downloadCode",downloadCode);
	}
	
	/**
	 * 获取下载有效时间（小时）
	*/
	 public java.lang.Integer getDownloadTime() {
		return get("downloadTime");
	}
	
	/**
	 * 设置下载有效时间（小时）
	* @param  下载有效时间（小时）
	*/
	 public void setDownloadTime(java.lang.Integer downloadTime) {
		set("downloadTime",downloadTime);
	}
	
	/**
	 * 获取邮件发送服务器
	*/
	 public java.lang.String getEmailHost() {
		return get("emailHost");
	}
	
	/**
	 * 设置邮件发送服务器
	* @param  邮件发送服务器
	*/
	 public void setEmailHost(java.lang.String emailHost) {
		set("emailHost",emailHost);
	}
	
	/**
	 * 获取邮件发送编码
	*/
	 public java.lang.String getEmailEncoding() {
		return get("emailEncoding");
	}
	
	/**
	 * 设置邮件发送编码
	* @param  邮件发送编码
	*/
	 public void setEmailEncoding(java.lang.String emailEncoding) {
		set("emailEncoding",emailEncoding);
	}
	
	/**
	 * 获取邮箱用户名
	*/
	 public java.lang.String getEmailUsername() {
		return get("emailUsername");
	}
	
	/**
	 * 设置邮箱用户名
	* @param  邮箱用户名
	*/
	 public void setEmailUsername(java.lang.String emailUsername) {
		set("emailUsername",emailUsername);
	}
	
	/**
	 * 获取邮箱密码
	*/
	 public java.lang.String getEmailPassword() {
		return get("emailPassword");
	}
	
	/**
	 * 设置邮箱密码
	* @param  邮箱密码
	*/
	 public void setEmailPassword(java.lang.String emailPassword) {
		set("emailPassword",emailPassword);
	}
	
	/**
	 * 获取邮箱发件人
	*/
	 public java.lang.String getEmailPersonal() {
		return get("emailPersonal");
	}
	
	/**
	 * 设置邮箱发件人
	* @param  邮箱发件人
	*/
	 public void setEmailPersonal(java.lang.String emailPersonal) {
		set("emailPersonal",emailPersonal);
	}
	
	/**
	 * 获取开启邮箱验证
	*/
	 public java.lang.Boolean getEmailValidate() {
		return get("emailValidate");
	}
	
	/**
	 * 设置开启邮箱验证
	* @param  开启邮箱验证
	*/
	 public void setEmailValidate(java.lang.Boolean emailValidate) {
		set("emailValidate",emailValidate);
	}
	
	/**
	 * 获取只有终审才能浏览内容页
	*/
	 public java.lang.Boolean getViewOnlyChecked() {
		return get("viewOnlyChecked");
	}
	
	/**
	 * 设置只有终审才能浏览内容页
	* @param  只有终审才能浏览内容页
	*/
	 public void setViewOnlyChecked(java.lang.Boolean viewOnlyChecked) {
		set("viewOnlyChecked",viewOnlyChecked);
	}
	
	/**
	 * 获取联系人
	*/
	 public java.lang.String getContactPerson() {
		return get("contactPerson");
	}
	
	/**
	 * 设置联系人
	* @param  联系人
	*/
	 public void setContactPerson(java.lang.String contactPerson) {
		set("contactPerson",contactPerson);
	}
	
	/**
	 * 获取联系手机
	*/
	 public java.lang.String getContactMobile() {
		return get("contactMobile");
	}
	
	/**
	 * 设置联系手机
	* @param  联系手机
	*/
	 public void setContactMobile(java.lang.String contactMobile) {
		set("contactMobile",contactMobile);
	}
	
	/**
	 * 获取联系QQ
	*/
	 public java.lang.String getContactQQ() {
		return get("contactQQ");
	}
	
	/**
	 * 设置联系QQ
	* @param  联系QQ
	*/
	 public void setContactQQ(java.lang.String contactQQ) {
		set("contactQQ",contactQQ);
	}
	
	/**
	 * 获取联系E-mail
	*/
	 public java.lang.String getContactEmail() {
		return get("contactEmail");
	}
	
	/**
	 * 设置联系E-mail
	* @param  联系E-mail
	*/
	 public void setContactEmail(java.lang.String contactEmail) {
		set("contactEmail",contactEmail);
	}
	
	/**
	 * 获取服务到期时间
	*/
	 public java.util.Date getServiceEndDate() {
		return get("serviceEndDate");
	}
	
	/**
	 * 设置服务到期时间
	* @param  服务到期时间
	*/
	 public void setServiceEndDate(java.util.Date serviceEndDate) {
		set("serviceEndDate",serviceEndDate);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取状态
	* -1逻辑删除、0审批中/暂停、1正常
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* -1逻辑删除、0审批中/暂停、1正常
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
}
