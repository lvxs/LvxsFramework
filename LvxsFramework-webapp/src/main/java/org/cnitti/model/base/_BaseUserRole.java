package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 用户角色
 * 用户附加角色关联表

 */
@SuppressWarnings("serial")
public class _BaseUserRole <M extends _BaseUserRole<M>> extends Model<M> implements IBean  {
	/**
	 * 获取用户角色ID
	*/
	 public java.lang.Long getId() {
		return get("id");
	}
	
	/**
	 * 设置用户角色ID
	* @param  用户角色ID
	*/
	 public void setId(java.lang.Long id) {
		set("id",id);
	}
	
	/**
	 * 获取用户ID
	*/
	 public java.lang.Long getUserId() {
		return get("userId");
	}
	
	/**
	 * 设置用户ID
	* @param  用户ID
	*/
	 public void setUserId(java.lang.Long userId) {
		set("userId",userId);
	}
	
	/**
	 * 获取角色ID
	*/
	 public java.lang.Long getRoleId() {
		return get("roleId");
	}
	
	/**
	 * 设置角色ID
	* @param  角色ID
	*/
	 public void setRoleId(java.lang.Long roleId) {
		set("roleId",roleId);
	}
	
}
