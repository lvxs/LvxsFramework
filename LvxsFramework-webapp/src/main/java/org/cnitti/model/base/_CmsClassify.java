package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 网站栏目基础信息
 * CMS的网站栏目基础信息表
 */
@SuppressWarnings("serial")
public class _CmsClassify <M extends _CmsClassify<M>> extends Model<M> implements IBean  {
	/**
	 * 获取栏目ID
	*/
	 public java.lang.Long getClassifyId() {
		return get("classifyId");
	}
	
	/**
	 * 设置栏目ID
	* @param  栏目ID
	*/
	 public void setClassifyId(java.lang.Long classifyId) {
		set("classifyId",classifyId);
	}
	
	/**
	 * 获取栏目名称
	*/
	 public java.lang.String getClassifyName() {
		return get("classifyName");
	}
	
	/**
	 * 设置栏目名称
	* @param  栏目名称
	*/
	 public void setClassifyName(java.lang.String classifyName) {
		set("classifyName",classifyName);
	}
	
	/**
	 * 获取网站ID
	*/
	 public java.lang.Long getWebSiteId() {
		return get("webSiteId");
	}
	
	/**
	 * 设置网站ID
	* @param  网站ID
	*/
	 public void setWebSiteId(java.lang.Long webSiteId) {
		set("webSiteId",webSiteId);
	}
	
	/**
	 * 获取父级栏目ID
	*/
	 public java.lang.Long getParentId() {
		return get("parentId");
	}
	
	/**
	 * 设置父级栏目ID
	* @param  父级栏目ID
	*/
	 public void setParentId(java.lang.Long parentId) {
		set("parentId",parentId);
	}
	
	/**
	 * 获取栏目编号
	*/
	 public java.lang.String getClassifyNumber() {
		return get("classifyNumber");
	}
	
	/**
	 * 设置栏目编号
	* @param  栏目编号
	*/
	 public void setClassifyNumber(java.lang.String classifyNumber) {
		set("classifyNumber",classifyNumber);
	}
	
	/**
	 * 获取排序编号
	* 越小越靠前显示，默认值100
	*/
	 public java.lang.Integer getSortNumber() {
		return get("sortNumber");
	}
	
	/**
	 * 设置排序编号
	* 越小越靠前显示，默认值100
	* @param  排序编号
	*/
	 public void setSortNumber(java.lang.Integer sortNumber) {
		set("sortNumber",sortNumber);
	}
	
	/**
	 * 获取SEO关键词
	* 每个关键词之间使用半角逗号分割。
	*/
	 public java.lang.String getKeyWords() {
		return get("keyWords");
	}
	
	/**
	 * 设置SEO关键词
	* 每个关键词之间使用半角逗号分割。
	* @param  SEO关键词
	*/
	 public void setKeyWords(java.lang.String keyWords) {
		set("keyWords",keyWords);
	}
	
	/**
	 * 获取栏目内容描述
	*/
	 public java.lang.String getDescription() {
		return get("description");
	}
	
	/**
	 * 设置栏目内容描述
	* @param  栏目内容描述
	*/
	 public void setDescription(java.lang.String description) {
		set("description",description);
	}
	
	/**
	 * 获取栏目图标1URL
	*/
	 public java.lang.String getIco1Url() {
		return get("ico1Url");
	}
	
	/**
	 * 设置栏目图标1URL
	* @param  栏目图标1URL
	*/
	 public void setIco1Url(java.lang.String ico1Url) {
		set("ico1Url",ico1Url);
	}
	
	/**
	 * 获取栏目图标2URL
	*/
	 public java.lang.String getIco2Url() {
		return get("ico2Url");
	}
	
	/**
	 * 设置栏目图标2URL
	* @param  栏目图标2URL
	*/
	 public void setIco2Url(java.lang.String ico2Url) {
		set("ico2Url",ico2Url);
	}
	
	/**
	 * 获取栏目图标3URL
	*/
	 public java.lang.String getIco3Url() {
		return get("ico3Url");
	}
	
	/**
	 * 设置栏目图标3URL
	* @param  栏目图标3URL
	*/
	 public void setIco3Url(java.lang.String ico3Url) {
		set("ico3Url",ico3Url);
	}
	
	/**
	 * 获取栏目HTML内容
	*/
	 public java.lang.String getClassifyHtml() {
		return get("classifyHtml");
	}
	
	/**
	 * 设置栏目HTML内容
	* @param  栏目HTML内容
	*/
	 public void setClassifyHtml(java.lang.String classifyHtml) {
		set("classifyHtml",classifyHtml);
	}
	
	/**
	 * 获取用户级别要求
	*/
	 public java.lang.Integer getUserLevel() {
		return get("userLevel");
	}
	
	/**
	 * 设置用户级别要求
	* @param  用户级别要求
	*/
	 public void setUserLevel(java.lang.Integer userLevel) {
		set("userLevel",userLevel);
	}
	
	/**
	 * 获取浏览次数
	*/
	 public java.lang.Long getBrowseCount() {
		return get("browseCount");
	}
	
	/**
	 * 设置浏览次数
	* @param  浏览次数
	*/
	 public void setBrowseCount(java.lang.Long browseCount) {
		set("browseCount",browseCount);
	}
	
	/**
	 * 获取状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
	/**
	 * 获取备注说明
	*/
	 public java.lang.String getRemark() {
		return get("remark");
	}
	
	/**
	 * 设置备注说明
	* @param  备注说明
	*/
	 public void setRemark(java.lang.String remark) {
		set("remark",remark);
	}
	
	/**
	 * 获取其他设置JSON字符串
	*/
	 public java.lang.String getOtherJson() {
		return get("otherJson");
	}
	
	/**
	 * 设置其他设置JSON字符串
	* @param  其他设置JSON字符串
	*/
	 public void setOtherJson(java.lang.String otherJson) {
		set("otherJson",otherJson);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取创建用户ID
	*/
	 public java.lang.Long getCreateUserId() {
		return get("createUserId");
	}
	
	/**
	 * 设置创建用户ID
	* @param  创建用户ID
	*/
	 public void setCreateUserId(java.lang.Long createUserId) {
		set("createUserId",createUserId);
	}
	
	/**
	 * 获取更新时间
	*/
	 public java.util.Date getUpdateTime() {
		return get("updateTime");
	}
	
	/**
	 * 设置更新时间
	* @param  更新时间
	*/
	 public void setUpdateTime(java.util.Date updateTime) {
		set("updateTime",updateTime);
	}
	
	/**
	 * 获取更新用户ID
	*/
	 public java.lang.Long getUpdateUserId() {
		return get("updateUserId");
	}
	
	/**
	 * 设置更新用户ID
	* @param  更新用户ID
	*/
	 public void setUpdateUserId(java.lang.Long updateUserId) {
		set("updateUserId",updateUserId);
	}
	
}
