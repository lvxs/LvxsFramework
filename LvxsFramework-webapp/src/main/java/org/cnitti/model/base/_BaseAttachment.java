package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 附件表
 * 用于保存用户上传的附件内容。
 */
@SuppressWarnings("serial")
public class _BaseAttachment <M extends _BaseAttachment<M>> extends Model<M> implements IBean  {
	/**
	 * 获取附件ID
	*/
	 public java.math.BigInteger getAttachmentId() {
		return get("attachmentId");
	}
	
	/**
	 * 设置附件ID
	* @param  附件ID
	*/
	 public void setAttachmentId(java.math.BigInteger attachmentId) {
		set("attachmentId",attachmentId);
	}
	
	/**
	 * 获取附件所属的系统ID
	*/
	 public java.math.BigInteger getSystemId() {
		return get("systemId");
	}
	
	/**
	 * 设置附件所属的系统ID
	* @param  附件所属的系统ID
	*/
	 public void setSystemId(java.math.BigInteger systemId) {
		set("systemId",systemId);
	}
	
	/**
	 * 获取标题
	*/
	 public java.lang.String getTitle() {
		return get("title");
	}
	
	/**
	 * 设置标题
	* @param  标题
	*/
	 public void setTitle(java.lang.String title) {
		set("title",title);
	}
	
	/**
	 * 获取路径
	*/
	 public java.lang.String getPath() {
		return get("path");
	}
	
	/**
	 * 设置路径
	* @param  路径
	*/
	 public void setPath(java.lang.String path) {
		set("path",path);
	}
	
	/**
	 * 获取mime
	*/
	 public java.lang.String getMimeType() {
		return get("mimeType");
	}
	
	/**
	 * 设置mime
	* @param  mime
	*/
	 public void setMimeType(java.lang.String mimeType) {
		set("mimeType",mimeType);
	}
	
	/**
	 * 获取附件的后缀
	*/
	 public java.lang.String getSuffix() {
		return get("suffix");
	}
	
	/**
	 * 设置附件的后缀
	* @param  附件的后缀
	*/
	 public void setSuffix(java.lang.String suffix) {
		set("suffix",suffix);
	}
	
	/**
	 * 获取类型
	*/
	 public java.lang.String getType() {
		return get("type");
	}
	
	/**
	 * 设置类型
	* @param  类型
	*/
	 public void setType(java.lang.String type) {
		set("type",type);
	}
	
	/**
	 * 获取标示
	*/
	 public java.lang.String getFlag() {
		return get("flag");
	}
	
	/**
	 * 设置标示
	* @param  标示
	*/
	 public void setFlag(java.lang.String flag) {
		set("flag",flag);
	}
	
	/**
	 * 获取经度
	*/
	 public java.math.BigDecimal getLat() {
		return get("lat");
	}
	
	/**
	 * 设置经度
	* @param  经度
	*/
	 public void setLat(java.math.BigDecimal lat) {
		set("lat",lat);
	}
	
	/**
	 * 获取纬度
	*/
	 public java.math.BigDecimal getLng() {
		return get("lng");
	}
	
	/**
	 * 设置纬度
	* @param  纬度
	*/
	 public void setLng(java.math.BigDecimal lng) {
		set("lng",lng);
	}
	
	/**
	 * 获取排序字段
	*/
	 public java.lang.Integer getSortNumber() {
		return get("sortNumber");
	}
	
	/**
	 * 设置排序字段
	* @param  排序字段
	*/
	 public void setSortNumber(java.lang.Integer sortNumber) {
		set("sortNumber",sortNumber);
	}
	
	/**
	 * 获取状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
	/**
	 * 获取备注说明
	*/
	 public java.lang.String getRemark() {
		return get("remark");
	}
	
	/**
	 * 设置备注说明
	* @param  备注说明
	*/
	 public void setRemark(java.lang.String remark) {
		set("remark",remark);
	}
	
	/**
	 * 获取其他设置JSON字符串
	*/
	 public java.lang.String getOtherJson() {
		return get("otherJson");
	}
	
	/**
	 * 设置其他设置JSON字符串
	* @param  其他设置JSON字符串
	*/
	 public void setOtherJson(java.lang.String otherJson) {
		set("otherJson",otherJson);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取创建用户ID
	*/
	 public java.lang.Long getCreateUserId() {
		return get("createUserId");
	}
	
	/**
	 * 设置创建用户ID
	* @param  创建用户ID
	*/
	 public void setCreateUserId(java.lang.Long createUserId) {
		set("createUserId",createUserId);
	}
	
	/**
	 * 获取更新时间
	*/
	 public java.util.Date getUpdateTime() {
		return get("updateTime");
	}
	
	/**
	 * 设置更新时间
	* @param  更新时间
	*/
	 public void setUpdateTime(java.util.Date updateTime) {
		set("updateTime",updateTime);
	}
	
	/**
	 * 获取更新用户ID
	*/
	 public java.lang.Long getUpdateUserId() {
		return get("updateUserId");
	}
	
	/**
	 * 设置更新用户ID
	* @param  更新用户ID
	*/
	 public void setUpdateUserId(java.lang.Long updateUserId) {
		set("updateUserId",updateUserId);
	}
	
}
