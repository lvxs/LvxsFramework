package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 系统菜单信息
 * 各个子系统或者相关系统的基础信息

 */
@SuppressWarnings("serial")
public class _BaseSystemMenu <M extends _BaseSystemMenu<M>> extends Model<M> implements IBean  {
	/**
	 * 获取菜单ID
	*/
	 public java.lang.Long getId() {
		return get("id");
	}
	
	/**
	 * 设置菜单ID
	* @param  菜单ID
	*/
	 public void setId(java.lang.Long id) {
		set("id",id);
	}
	
	/**
	 * 获取系统ID
	*/
	 public java.lang.Long getSystemId() {
		return get("systemId");
	}
	
	/**
	 * 设置系统ID
	* @param  系统ID
	*/
	 public void setSystemId(java.lang.Long systemId) {
		set("systemId",systemId);
	}
	
	/**
	 * 获取父级菜单ID
	*/
	 public java.lang.Long getParentId() {
		return get("parentId");
	}
	
	/**
	 * 设置父级菜单ID
	* @param  父级菜单ID
	*/
	 public void setParentId(java.lang.Long parentId) {
		set("parentId",parentId);
	}
	
	/**
	 * 获取排序编号
	*/
	 public java.lang.Integer getSortNumber() {
		return get("sortNumber");
	}
	
	/**
	 * 设置排序编号
	* @param  排序编号
	*/
	 public void setSortNumber(java.lang.Integer sortNumber) {
		set("sortNumber",sortNumber);
	}
	
	/**
	 * 获取菜单编号
	*/
	 public java.lang.String getMenuNumber() {
		return get("menuNumber");
	}
	
	/**
	 * 设置菜单编号
	* @param  菜单编号
	*/
	 public void setMenuNumber(java.lang.String menuNumber) {
		set("menuNumber",menuNumber);
	}
	
	/**
	 * 获取菜单名称
	*/
	 public java.lang.String getName() {
		return get("name");
	}
	
	/**
	 * 设置菜单名称
	* @param  菜单名称
	*/
	 public void setName(java.lang.String name) {
		set("name",name);
	}
	
	/**
	 * 获取链接URL
	* 访问外部链接地址使用http开始的完整URL地址，访问本系统或者子系统使用以/开头的相对路径URL
	*/
	 public java.lang.String getUrl() {
		return get("url");
	}
	
	/**
	 * 设置链接URL
	* 访问外部链接地址使用http开始的完整URL地址，访问本系统或者子系统使用以/开头的相对路径URL
	* @param  链接URL
	*/
	 public void setUrl(java.lang.String url) {
		set("url",url);
	}
	
	/**
	 * 获取目标窗口
	* 目标窗口名称，新建窗口_blank，顶级窗口_top，本窗口请留空或者_self，其他固定名称的窗口请直接填写窗口名称
	*/
	 public java.lang.String getTarget() {
		return get("target");
	}
	
	/**
	 * 设置目标窗口
	* 目标窗口名称，新建窗口_blank，顶级窗口_top，本窗口请留空或者_self，其他固定名称的窗口请直接填写窗口名称
	* @param  目标窗口
	*/
	 public void setTarget(java.lang.String target) {
		set("target",target);
	}
	
	/**
	 * 获取帮助文档URL
	*/
	 public java.lang.String getHelpUrl() {
		return get("helpUrl");
	}
	
	/**
	 * 设置帮助文档URL
	* @param  帮助文档URL
	*/
	 public void setHelpUrl(java.lang.String helpUrl) {
		set("helpUrl",helpUrl);
	}
	
	/**
	 * 获取图标
	* 字体样式图标直接填写CSS样式名称如fa-home、icon-attachment等，图片URL
	*/
	 public java.lang.String getIconSkin() {
		return get("iconSkin");
	}
	
	/**
	 * 设置图标
	* 字体样式图标直接填写CSS样式名称如fa-home、icon-attachment等，图片URL
	* @param  图标
	*/
	 public void setIconSkin(java.lang.String iconSkin) {
		set("iconSkin",iconSkin);
	}
	
	/**
	 * 获取简要说明
	*/
	 public java.lang.String getBriefDescription() {
		return get("briefDescription");
	}
	
	/**
	 * 设置简要说明
	* @param  简要说明
	*/
	 public void setBriefDescription(java.lang.String briefDescription) {
		set("briefDescription",briefDescription);
	}
	
	/**
	 * 获取状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
	/**
	 * 获取备注说明
	*/
	 public java.lang.String getRemark() {
		return get("remark");
	}
	
	/**
	 * 设置备注说明
	* @param  备注说明
	*/
	 public void setRemark(java.lang.String remark) {
		set("remark",remark);
	}
	
	/**
	 * 获取其他设置JSON字符串
	*/
	 public java.lang.String getOtherJson() {
		return get("otherJson");
	}
	
	/**
	 * 设置其他设置JSON字符串
	* @param  其他设置JSON字符串
	*/
	 public void setOtherJson(java.lang.String otherJson) {
		set("otherJson",otherJson);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取创建用户ID
	*/
	 public java.lang.Long getCreateUserId() {
		return get("createUserId");
	}
	
	/**
	 * 设置创建用户ID
	* @param  创建用户ID
	*/
	 public void setCreateUserId(java.lang.Long createUserId) {
		set("createUserId",createUserId);
	}
	
	/**
	 * 获取更新时间
	*/
	 public java.util.Date getUpdateTime() {
		return get("updateTime");
	}
	
	/**
	 * 设置更新时间
	* @param  更新时间
	*/
	 public void setUpdateTime(java.util.Date updateTime) {
		set("updateTime",updateTime);
	}
	
	/**
	 * 获取更新用户ID
	*/
	 public java.lang.Long getUpdateUserId() {
		return get("updateUserId");
	}
	
	/**
	 * 设置更新用户ID
	* @param  更新用户ID
	*/
	 public void setUpdateUserId(java.lang.Long updateUserId) {
		set("updateUserId",updateUserId);
	}
	
}
