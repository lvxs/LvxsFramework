package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 页面动作
 * 页面中所有按钮、链接等动作的基本信息

 */
@SuppressWarnings("serial")
public class _BaseAction <M extends _BaseAction<M>> extends Model<M> implements IBean  {
	/**
	 * 获取动作ID
	*/
	 public java.lang.Long getId() {
		return get("id");
	}
	
	/**
	 * 设置动作ID
	* @param  动作ID
	*/
	 public void setId(java.lang.Long id) {
		set("id",id);
	}
	
	/**
	 * 获取系统菜单ID
	*/
	 public java.lang.Long getMenuId() {
		return get("menuId");
	}
	
	/**
	 * 设置系统菜单ID
	* @param  系统菜单ID
	*/
	 public void setMenuId(java.lang.Long menuId) {
		set("menuId",menuId);
	}
	
	/**
	 * 获取排序编号
	*/
	 public java.lang.Integer getSortNumber() {
		return get("sortNumber");
	}
	
	/**
	 * 设置排序编号
	* @param  排序编号
	*/
	 public void setSortNumber(java.lang.Integer sortNumber) {
		set("sortNumber",sortNumber);
	}
	
	/**
	 * 获取名称
	*/
	 public java.lang.String getName() {
		return get("name");
	}
	
	/**
	 * 设置名称
	* @param  名称
	*/
	 public void setName(java.lang.String name) {
		set("name",name);
	}
	
	/**
	 * 获取动作类型
	* 1:列表页面;2:新增页面;3:修改页面;4:查看页面;5:其他页面;11:保存;12:更新;13:删除;14:批量删除;15:查询结果列表JSON;16:其他操作;
	*/
	 public java.lang.Integer getActionType() {
		return get("actionType");
	}
	
	/**
	 * 设置动作类型
	* 1:列表页面;2:新增页面;3:修改页面;4:查看页面;5:其他页面;11:保存;12:更新;13:删除;14:批量删除;15:查询结果列表JSON;16:其他操作;
	* @param  动作类型
	*/
	 public void setActionType(java.lang.Integer actionType) {
		set("actionType",actionType);
	}
	
	/**
	 * 获取方法名
	*/
	 public java.lang.String getMethodName() {
		return get("methodName");
	}
	
	/**
	 * 设置方法名
	* @param  方法名
	*/
	 public void setMethodName(java.lang.String methodName) {
		set("methodName",methodName);
	}
	
	/**
	 * 获取完整URI
	*/
	 public java.lang.String getFullUri() {
		return get("fullUri");
	}
	
	/**
	 * 设置完整URI
	* @param  完整URI
	*/
	 public void setFullUri(java.lang.String fullUri) {
		set("fullUri",fullUri);
	}
	
	/**
	 * 获取JavaScript方法
	*/
	 public java.lang.String getJsMethod() {
		return get("jsMethod");
	}
	
	/**
	 * 设置JavaScript方法
	* @param  JavaScript方法
	*/
	 public void setJsMethod(java.lang.String jsMethod) {
		set("jsMethod",jsMethod);
	}
	
	/**
	 * 获取链接URI
	* 页面内链接URI
	*/
	 public java.lang.String getLinkUri() {
		return get("linkUri");
	}
	
	/**
	 * 设置链接URI
	* 页面内链接URI
	* @param  链接URI
	*/
	 public void setLinkUri(java.lang.String linkUri) {
		set("linkUri",linkUri);
	}
	
	/**
	 * 获取状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
	/**
	 * 获取备注说明
	*/
	 public java.lang.String getRemark() {
		return get("remark");
	}
	
	/**
	 * 设置备注说明
	* @param  备注说明
	*/
	 public void setRemark(java.lang.String remark) {
		set("remark",remark);
	}
	
	/**
	 * 获取其他设置JSON字符串
	*/
	 public java.lang.String getOtherJson() {
		return get("otherJson");
	}
	
	/**
	 * 设置其他设置JSON字符串
	* @param  其他设置JSON字符串
	*/
	 public void setOtherJson(java.lang.String otherJson) {
		set("otherJson",otherJson);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取创建用户ID
	*/
	 public java.lang.Long getCreateUserId() {
		return get("createUserId");
	}
	
	/**
	 * 设置创建用户ID
	* @param  创建用户ID
	*/
	 public void setCreateUserId(java.lang.Long createUserId) {
		set("createUserId",createUserId);
	}
	
	/**
	 * 获取更新时间
	*/
	 public java.util.Date getUpdateTime() {
		return get("updateTime");
	}
	
	/**
	 * 设置更新时间
	* @param  更新时间
	*/
	 public void setUpdateTime(java.util.Date updateTime) {
		set("updateTime",updateTime);
	}
	
	/**
	 * 获取更新用户ID
	*/
	 public java.lang.Long getUpdateUserId() {
		return get("updateUserId");
	}
	
	/**
	 * 设置更新用户ID
	* @param  更新用户ID
	*/
	 public void setUpdateUserId(java.lang.Long updateUserId) {
		set("updateUserId",updateUserId);
	}
	
}
