package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 角色权限
 * 角色拥有权限信息表

 */
@SuppressWarnings("serial")
public class _BaseRolePermission <M extends _BaseRolePermission<M>> extends Model<M> implements IBean  {
	/**
	 * 获取角色权限ID
	*/
	 public java.lang.Long getId() {
		return get("id");
	}
	
	/**
	 * 设置角色权限ID
	* @param  角色权限ID
	*/
	 public void setId(java.lang.Long id) {
		set("id",id);
	}
	
	/**
	 * 获取菜单ID
	*/
	 public java.lang.Long getMenuId() {
		return get("menuId");
	}
	
	/**
	 * 设置菜单ID
	* @param  菜单ID
	*/
	 public void setMenuId(java.lang.Long menuId) {
		set("menuId",menuId);
	}
	
	/**
	 * 获取动作ID
	*/
	 public java.lang.Long getActionId() {
		return get("actionId");
	}
	
	/**
	 * 设置动作ID
	* @param  动作ID
	*/
	 public void setActionId(java.lang.Long actionId) {
		set("actionId",actionId);
	}
	
	/**
	 * 获取角色ID
	*/
	 public java.lang.Long getRoleId() {
		return get("roleId");
	}
	
	/**
	 * 设置角色ID
	* @param  角色ID
	*/
	 public void setRoleId(java.lang.Long roleId) {
		set("roleId",roleId);
	}
	
	/**
	 * 获取数据权限
	* 1:所有权限;2:本部门及子部门;3:本部门;4:仅本人;5:特定部门;6:特定人员;7:SQL语句
	*/
	 public java.lang.Integer getDataPermission() {
		return get("dataPermission");
	}
	
	/**
	 * 设置数据权限
	* 1:所有权限;2:本部门及子部门;3:本部门;4:仅本人;5:特定部门;6:特定人员;7:SQL语句
	* @param  数据权限
	*/
	 public void setDataPermission(java.lang.Integer dataPermission) {
		set("dataPermission",dataPermission);
	}
	
	/**
	 * 获取特定IDs
	* dataPermission=5时代表特定部门IDs；dataPermission=6时代表特定人员IDs。多个ID之间使用半角逗号分隔
	*/
	 public java.lang.String getSpecialIds() {
		return get("specialIds");
	}
	
	/**
	 * 设置特定IDs
	* dataPermission=5时代表特定部门IDs；dataPermission=6时代表特定人员IDs。多个ID之间使用半角逗号分隔
	* @param  特定IDs
	*/
	 public void setSpecialIds(java.lang.String specialIds) {
		set("specialIds",specialIds);
	}
	
	/**
	 * 获取附加Where查询条件
	*/
	 public java.lang.String getWhereSql() {
		return get("whereSql");
	}
	
	/**
	 * 设置附加Where查询条件
	* @param  附加Where查询条件
	*/
	 public void setWhereSql(java.lang.String whereSql) {
		set("whereSql",whereSql);
	}
	
	/**
	 * 获取状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
	/**
	 * 获取备注说明
	*/
	 public java.lang.String getRemark() {
		return get("remark");
	}
	
	/**
	 * 设置备注说明
	* @param  备注说明
	*/
	 public void setRemark(java.lang.String remark) {
		set("remark",remark);
	}
	
	/**
	 * 获取其他设置JSON字符串
	*/
	 public java.lang.String getOtherJson() {
		return get("otherJson");
	}
	
	/**
	 * 设置其他设置JSON字符串
	* @param  其他设置JSON字符串
	*/
	 public void setOtherJson(java.lang.String otherJson) {
		set("otherJson",otherJson);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取创建用户ID
	*/
	 public java.lang.Long getCreateUserId() {
		return get("createUserId");
	}
	
	/**
	 * 设置创建用户ID
	* @param  创建用户ID
	*/
	 public void setCreateUserId(java.lang.Long createUserId) {
		set("createUserId",createUserId);
	}
	
	/**
	 * 获取更新时间
	*/
	 public java.util.Date getUpdateTime() {
		return get("updateTime");
	}
	
	/**
	 * 设置更新时间
	* @param  更新时间
	*/
	 public void setUpdateTime(java.util.Date updateTime) {
		set("updateTime",updateTime);
	}
	
	/**
	 * 获取更新用户ID
	*/
	 public java.lang.Long getUpdateUserId() {
		return get("updateUserId");
	}
	
	/**
	 * 设置更新用户ID
	* @param  更新用户ID
	*/
	 public void setUpdateUserId(java.lang.Long updateUserId) {
		set("updateUserId",updateUserId);
	}
	
}
