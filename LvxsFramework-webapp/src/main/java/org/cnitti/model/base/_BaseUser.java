package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 用户信息
 * 公用的系统用户基础信息
 */
@SuppressWarnings("serial")
public class _BaseUser <M extends _BaseUser<M>> extends Model<M> implements IBean  {
	/**
	 * 获取用户ID
	*/
	 public java.lang.Long getId() {
		return get("id");
	}
	
	/**
	 * 设置用户ID
	* @param  用户ID
	*/
	 public void setId(java.lang.Long id) {
		set("id",id);
	}
	
	/**
	 * 获取用户名
	*/
	 public java.lang.String getUserName() {
		return get("userName");
	}
	
	/**
	 * 设置用户名
	* @param  用户名
	*/
	 public void setUserName(java.lang.String userName) {
		set("userName",userName);
	}
	
	/**
	 * 获取登录密码
	*/
	 public java.lang.String getPassword() {
		return get("password");
	}
	
	/**
	 * 设置登录密码
	* @param  登录密码
	*/
	 public void setPassword(java.lang.String password) {
		set("password",password);
	}
	
	/**
	 * 获取E-mail
	*/
	 public java.lang.String getEmail() {
		return get("email");
	}
	
	/**
	 * 设置E-mail
	* @param  E-mail
	*/
	 public void setEmail(java.lang.String email) {
		set("email",email);
	}
	
	/**
	 * 获取手机
	*/
	 public java.lang.String getMobile() {
		return get("mobile");
	}
	
	/**
	 * 设置手机
	* @param  手机
	*/
	 public void setMobile(java.lang.String mobile) {
		set("mobile",mobile);
	}
	
	/**
	 * 获取其他联系方式
	*/
	 public java.lang.String getOtherContact() {
		return get("otherContact");
	}
	
	/**
	 * 设置其他联系方式
	* @param  其他联系方式
	*/
	 public void setOtherContact(java.lang.String otherContact) {
		set("otherContact",otherContact);
	}
	
	/**
	 * 获取昵称
	*/
	 public java.lang.String getNikename() {
		return get("nikename");
	}
	
	/**
	 * 设置昵称
	* @param  昵称
	*/
	 public void setNikename(java.lang.String nikename) {
		set("nikename",nikename);
	}
	
	/**
	 * 获取照片URL
	*/
	 public java.lang.String getPhotoURL() {
		return get("photoURL");
	}
	
	/**
	 * 设置照片URL
	* @param  照片URL
	*/
	 public void setPhotoURL(java.lang.String photoURL) {
		set("photoURL",photoURL);
	}
	
	/**
	 * 获取真实姓名
	*/
	 public java.lang.String getRealName() {
		return get("realName");
	}
	
	/**
	 * 设置真实姓名
	* @param  真实姓名
	*/
	 public void setRealName(java.lang.String realName) {
		set("realName",realName);
	}
	
	/**
	 * 获取角色ID
	*/
	 public java.lang.Long getRoleId() {
		return get("roleId");
	}
	
	/**
	 * 设置角色ID
	* @param  角色ID
	*/
	 public void setRoleId(java.lang.Long roleId) {
		set("roleId",roleId);
	}
	
	/**
	 * 获取部门ID
	*/
	 public java.lang.Long getDepartmentId() {
		return get("departmentId");
	}
	
	/**
	 * 设置部门ID
	* @param  部门ID
	*/
	 public void setDepartmentId(java.lang.Long departmentId) {
		set("departmentId",departmentId);
	}
	
	/**
	 * 获取状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* -1:逻辑删除;0:已禁用/待审核;1:正常
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
	/**
	 * 获取备注说明
	*/
	 public java.lang.String getRemark() {
		return get("remark");
	}
	
	/**
	 * 设置备注说明
	* @param  备注说明
	*/
	 public void setRemark(java.lang.String remark) {
		set("remark",remark);
	}
	
	/**
	 * 获取其他设置JSON字符串
	*/
	 public java.lang.String getOtherJson() {
		return get("otherJson");
	}
	
	/**
	 * 设置其他设置JSON字符串
	* @param  其他设置JSON字符串
	*/
	 public void setOtherJson(java.lang.String otherJson) {
		set("otherJson",otherJson);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取创建用户ID
	*/
	 public java.lang.Long getCreateUserId() {
		return get("createUserId");
	}
	
	/**
	 * 设置创建用户ID
	* @param  创建用户ID
	*/
	 public void setCreateUserId(java.lang.Long createUserId) {
		set("createUserId",createUserId);
	}
	
	/**
	 * 获取更新时间
	*/
	 public java.util.Date getUpdateTime() {
		return get("updateTime");
	}
	
	/**
	 * 设置更新时间
	* @param  更新时间
	*/
	 public void setUpdateTime(java.util.Date updateTime) {
		set("updateTime",updateTime);
	}
	
	/**
	 * 获取更新用户Id
	*/
	 public java.lang.Long getUpdateUserId() {
		return get("updateUserId");
	}
	
	/**
	 * 设置更新用户Id
	* @param  更新用户Id
	*/
	 public void setUpdateUserId(java.lang.Long updateUserId) {
		set("updateUserId",updateUserId);
	}
	
}
