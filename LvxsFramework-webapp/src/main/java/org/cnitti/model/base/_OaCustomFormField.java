package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 自定义表单字段
 * 自定义表单字段设置表
 */
@SuppressWarnings("serial")
public class _OaCustomFormField <M extends _OaCustomFormField<M>> extends Model<M> implements IBean  {
	/**
	 * 获取ID
	*/
	 public java.lang.Long getId() {
		return get("id");
	}
	
	/**
	 * 设置ID
	* @param  ID
	*/
	 public void setId(java.lang.Long id) {
		set("id",id);
	}
	
	/**
	 * 获取表单ID
	*/
	 public java.lang.Long getFormId() {
		return get("formId");
	}
	
	/**
	 * 设置表单ID
	* @param  表单ID
	*/
	 public void setFormId(java.lang.Long formId) {
		set("formId",formId);
	}
	
	/**
	 * 获取字段名称
	*/
	 public java.lang.String getName() {
		return get("name");
	}
	
	/**
	 * 设置字段名称
	* @param  字段名称
	*/
	 public void setName(java.lang.String name) {
		set("name",name);
	}
	
	/**
	 * 获取物理字段名称
	*/
	 public java.lang.String getPhysicalName() {
		return get("physicalName");
	}
	
	/**
	 * 设置物理字段名称
	* @param  物理字段名称
	*/
	 public void setPhysicalName(java.lang.String physicalName) {
		set("physicalName",physicalName);
	}
	
	/**
	 * 获取字段类型
	* 1:文本;2:大文本;3:日期;4:日期时间;5:整型;6:大整型;7:数值;8:自增整型;
	*/
	 public java.lang.Integer getFieldType() {
		return get("fieldType");
	}
	
	/**
	 * 设置字段类型
	* 1:文本;2:大文本;3:日期;4:日期时间;5:整型;6:大整型;7:数值;8:自增整型;
	* @param  字段类型
	*/
	 public void setFieldType(java.lang.Integer fieldType) {
		set("fieldType",fieldType);
	}
	
	/**
	 * 获取字符串长度
	*/
	 public java.lang.Integer getTextLength() {
		return get("textLength");
	}
	
	/**
	 * 设置字符串长度
	* @param  字符串长度
	*/
	 public void setTextLength(java.lang.Integer textLength) {
		set("textLength",textLength);
	}
	
	/**
	 * 获取是否必填
	* 0:选填;1:必填;
	*/
	 public java.lang.Integer getIsRequired() {
		return get("isRequired");
	}
	
	/**
	 * 设置是否必填
	* 0:选填;1:必填;
	* @param  是否必填
	*/
	 public void setIsRequired(java.lang.Integer isRequired) {
		set("isRequired",isRequired);
	}
	
	/**
	 * 获取是否主键
	* 1:是;0:否
	*/
	 public java.lang.Integer getIsPrimaryKey() {
		return get("isPrimaryKey");
	}
	
	/**
	 * 设置是否主键
	* 1:是;0:否
	* @param  是否主键
	*/
	 public void setIsPrimaryKey(java.lang.Integer isPrimaryKey) {
		set("isPrimaryKey",isPrimaryKey);
	}
	
	/**
	 * 获取默认值
	*/
	 public java.lang.String getDefaultValue() {
		return get("defaultValue");
	}
	
	/**
	 * 设置默认值
	* @param  默认值
	*/
	 public void setDefaultValue(java.lang.String defaultValue) {
		set("defaultValue",defaultValue);
	}
	
	/**
	 * 获取输入框类型
	* 1:文本框;2:大文本框;3:日期;4:日期时间;5:Email;6:密码;7:数值;8:整型;9:单选框;10:复选框;11:单选下拉列表;12:多选下拉列表;13:HTML编辑器;14:附件上传;15:图片上传;16:部门选择;17:用户选择;18:隐藏域;19:只读文本;
	*/
	 public java.lang.Integer getInputType() {
		return get("inputType");
	}
	
	/**
	 * 设置输入框类型
	* 1:文本框;2:大文本框;3:日期;4:日期时间;5:Email;6:密码;7:数值;8:整型;9:单选框;10:复选框;11:单选下拉列表;12:多选下拉列表;13:HTML编辑器;14:附件上传;15:图片上传;16:部门选择;17:用户选择;18:隐藏域;19:只读文本;
	* @param  输入框类型
	*/
	 public void setInputType(java.lang.Integer inputType) {
		set("inputType",inputType);
	}
	
	/**
	 * 获取附加的js和css样式
	* 附加的JavaScript事件和Css样式
	*/
	 public java.lang.String getJsAndCss() {
		return get("jsAndCss");
	}
	
	/**
	 * 设置附加的js和css样式
	* 附加的JavaScript事件和Css样式
	* @param  附加的js和css样式
	*/
	 public void setJsAndCss(java.lang.String jsAndCss) {
		set("jsAndCss",jsAndCss);
	}
	
	/**
	 * 获取可选项类型
	* 0:不适用;1:固定值列表;2:查询SQL语句;3:JSON数据源;4:XML数据源;5:系统字典;
	*/
	 public java.lang.Integer getOptionType() {
		return get("optionType");
	}
	
	/**
	 * 设置可选项类型
	* 0:不适用;1:固定值列表;2:查询SQL语句;3:JSON数据源;4:XML数据源;5:系统字典;
	* @param  可选项类型
	*/
	 public void setOptionType(java.lang.Integer optionType) {
		set("optionType",optionType);
	}
	
	/**
	 * 获取可选项值
	* 固定至列表使用“1:文本框；2:大文本框；3:日期”格式说明描述；查询SQL语句直接写SQL语句；JSON数据源和XML数据源填写数据源URL；系统字典表填写字典表关键字
	*/
	 public java.lang.String getOptionValue() {
		return get("optionValue");
	}
	
	/**
	 * 设置可选项值
	* 固定至列表使用“1:文本框；2:大文本框；3:日期”格式说明描述；查询SQL语句直接写SQL语句；JSON数据源和XML数据源填写数据源URL；系统字典表填写字典表关键字
	* @param  可选项值
	*/
	 public void setOptionValue(java.lang.String optionValue) {
		set("optionValue",optionValue);
	}
	
	/**
	 * 获取列表页面是否显示
	* 1:显示;0:隐藏;
	*/
	 public java.lang.Integer getDisplayList() {
		return get("displayList");
	}
	
	/**
	 * 设置列表页面是否显示
	* 1:显示;0:隐藏;
	* @param  列表页面是否显示
	*/
	 public void setDisplayList(java.lang.Integer displayList) {
		set("displayList",displayList);
	}
	
	/**
	 * 获取新增页面是否显示
	* 1:显示;0:隐藏;
	*/
	 public java.lang.Integer getDisplayAdd() {
		return get("displayAdd");
	}
	
	/**
	 * 设置新增页面是否显示
	* 1:显示;0:隐藏;
	* @param  新增页面是否显示
	*/
	 public void setDisplayAdd(java.lang.Integer displayAdd) {
		set("displayAdd",displayAdd);
	}
	
	/**
	 * 获取修改页面是否显示
	* 1:显示;0:隐藏;
	*/
	 public java.lang.Integer getDisplayEdit() {
		return get("displayEdit");
	}
	
	/**
	 * 设置修改页面是否显示
	* 1:显示;0:隐藏;
	* @param  修改页面是否显示
	*/
	 public void setDisplayEdit(java.lang.Integer displayEdit) {
		set("displayEdit",displayEdit);
	}
	
	/**
	 * 获取显示页面是否显示
	* 1:显示;0:隐藏;
	*/
	 public java.lang.Integer getDisplayView() {
		return get("displayView");
	}
	
	/**
	 * 设置显示页面是否显示
	* 1:显示;0:隐藏;
	* @param  显示页面是否显示
	*/
	 public void setDisplayView(java.lang.Integer displayView) {
		set("displayView",displayView);
	}
	
	/**
	 * 获取排序编号
	*/
	 public java.lang.Integer getSortNumber() {
		return get("sortNumber");
	}
	
	/**
	 * 设置排序编号
	* @param  排序编号
	*/
	 public void setSortNumber(java.lang.Integer sortNumber) {
		set("sortNumber",sortNumber);
	}
	
	/**
	 * 获取输入框宽度
	* 12:100%页宽;6:50%页宽;9:75%页宽;3:25%页宽;8:2/3页宽;4:1/3页宽;11:11/12页宽;10:5/6页宽;7:7/12页宽;5:5/12页宽;2:1/6页宽;1:1/12页宽;
	*/
	 public java.lang.Integer getInputColSpan() {
		return get("inputColSpan");
	}
	
	/**
	 * 设置输入框宽度
	* 12:100%页宽;6:50%页宽;9:75%页宽;3:25%页宽;8:2/3页宽;4:1/3页宽;11:11/12页宽;10:5/6页宽;7:7/12页宽;5:5/12页宽;2:1/6页宽;1:1/12页宽;
	* @param  输入框宽度
	*/
	 public void setInputColSpan(java.lang.Integer inputColSpan) {
		set("inputColSpan",inputColSpan);
	}
	
	/**
	 * 获取备注说明
	*/
	 public java.lang.String getRemark() {
		return get("remark");
	}
	
	/**
	 * 设置备注说明
	* @param  备注说明
	*/
	 public void setRemark(java.lang.String remark) {
		set("remark",remark);
	}
	
	/**
	 * 获取状态
	* 1:正常;0:审批中;-1:逻辑删除
	*/
	 public java.lang.Integer getStatus() {
		return get("status");
	}
	
	/**
	 * 设置状态
	* 1:正常;0:审批中;-1:逻辑删除
	* @param  状态
	*/
	 public void setStatus(java.lang.Integer status) {
		set("status",status);
	}
	
	/**
	 * 获取其他设置JSON字符串
	*/
	 public java.lang.String getOtherJson() {
		return get("otherJson");
	}
	
	/**
	 * 设置其他设置JSON字符串
	* @param  其他设置JSON字符串
	*/
	 public void setOtherJson(java.lang.String otherJson) {
		set("otherJson",otherJson);
	}
	
	/**
	 * 获取创建用户ID
	*/
	 public java.lang.Long getCreateUserId() {
		return get("createUserId");
	}
	
	/**
	 * 设置创建用户ID
	* @param  创建用户ID
	*/
	 public void setCreateUserId(java.lang.Long createUserId) {
		set("createUserId",createUserId);
	}
	
	/**
	 * 获取创建时间
	*/
	 public java.util.Date getCreateTime() {
		return get("createTime");
	}
	
	/**
	 * 设置创建时间
	* @param  创建时间
	*/
	 public void setCreateTime(java.util.Date createTime) {
		set("createTime",createTime);
	}
	
	/**
	 * 获取更新用户ID
	*/
	 public java.lang.Long getUpdateUserId() {
		return get("updateUserId");
	}
	
	/**
	 * 设置更新用户ID
	* @param  更新用户ID
	*/
	 public void setUpdateUserId(java.lang.Long updateUserId) {
		set("updateUserId",updateUserId);
	}
	
	/**
	 * 获取更新时间
	*/
	 public java.util.Date getUpdateTime() {
		return get("updateTime");
	}
	
	/**
	 * 设置更新时间
	* @param  更新时间
	*/
	 public void setUpdateTime(java.util.Date updateTime) {
		set("updateTime",updateTime);
	}
	
}
