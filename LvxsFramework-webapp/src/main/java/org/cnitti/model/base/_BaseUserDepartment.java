package org.cnitti.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * 用户部门
 * 用户附加部门关联表，用于一个用户属于多个部门的情况

 */
@SuppressWarnings("serial")
public class _BaseUserDepartment <M extends _BaseUserDepartment<M>> extends Model<M> implements IBean  {
	/**
	 * 获取用户角色ID
	*/
	 public java.lang.Long getId() {
		return get("id");
	}
	
	/**
	 * 设置用户角色ID
	* @param  用户角色ID
	*/
	 public void setId(java.lang.Long id) {
		set("id",id);
	}
	
	/**
	 * 获取用户ID
	*/
	 public java.lang.Long getUserId() {
		return get("userId");
	}
	
	/**
	 * 设置用户ID
	* @param  用户ID
	*/
	 public void setUserId(java.lang.Long userId) {
		set("userId",userId);
	}
	
	/**
	 * 获取部门ID
	*/
	 public java.lang.Long getDepartmentId() {
		return get("departmentId");
	}
	
	/**
	 * 设置部门ID
	* @param  部门ID
	*/
	 public void setDepartmentId(java.lang.Long departmentId) {
		set("departmentId",departmentId);
	}
	
}
