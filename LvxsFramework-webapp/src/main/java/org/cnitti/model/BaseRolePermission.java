package org.cnitti.model;

import org.cnitti.model.core.Table;
import org.cnitti.model.base._BaseRolePermission;

import com.alibaba.fastjson.*;

/**
 * 角色权限
 * 角色拥有权限信息表

 */
@Table(tableName="base_role_permission",primaryKey="id")
public class BaseRolePermission extends _BaseRolePermission<BaseRolePermission> {
	private static final long serialVersionUID = 1L;
	
	// 创建默认数据操作对象DAO
	public static final BaseRolePermission DAO = new BaseRolePermission();
		// 菜单 
		private BaseSystemMenu menu;
		
		/**
		 * 获取菜单
		 */
	 	public BaseSystemMenu getMenu() {
			return menu;
		}
	
		/**
		 * 设置菜单
		 * @param 菜单
		 */
		 public void setMenu(BaseSystemMenu menu) {
			this.menu = menu;
		}
		// 动作 
		private BaseAction action;
		
		/**
		 * 获取动作
		 */
	 	public BaseAction getAction() {
			return action;
		}
	
		/**
		 * 设置动作
		 * @param 动作
		 */
		 public void setAction(BaseAction action) {
			this.action = action;
		}
		// 角色 
		private BaseRole role;
		
		/**
		 * 获取角色
		 */
	 	public BaseRole getRole() {
			return role;
		}
	
		/**
		 * 设置角色
		 * @param 角色
		 */
		 public void setRole(BaseRole role) {
			this.role = role;
		}
		// 其他设置JSON字符串对应的JSON对象 
		private JSONObject otherJsonObject;
		
		/**
		 * 获取其他设置JSON字符串对应的JSON对象
		 */
	 	public JSONObject getOtherJsonObject() {
			return otherJsonObject;
		}
	
		/**
		 * 设置其他设置JSON字符串对应的JSON对象
		 * @param 其他设置JSON字符串对应的JSON对象
		 */
		 public void setOtherJsonObject(JSONObject otherJsonObject) {
			this.otherJsonObject = otherJsonObject;
		}
		// 创建用户 
		private BaseUser createUser;
		
		/**
		 * 获取创建用户
		 */
	 	public BaseUser getCreateUser() {
			return createUser;
		}
	
		/**
		 * 设置创建用户
		 * @param 创建用户
		 */
		 public void setCreateUser(BaseUser createUser) {
			this.createUser = createUser;
		}
		// 更新用户 
		private BaseUser updateUser;
		
		/**
		 * 获取更新用户
		 */
	 	public BaseUser getUpdateUser() {
			return updateUser;
		}
	
		/**
		 * 设置更新用户
		 * @param 更新用户
		 */
		 public void setUpdateUser(BaseUser updateUser) {
			this.updateUser = updateUser;
		}
}
