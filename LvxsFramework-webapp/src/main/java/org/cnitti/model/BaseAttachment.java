package org.cnitti.model;

import org.cnitti.model.core.Table;
import org.cnitti.model.base._BaseAttachment;

import com.alibaba.fastjson.*;

/**
 * 附件表
 * 用于保存用户上传的附件内容。
 */
@Table(tableName="base_attachment",primaryKey="attachmentId")
public class BaseAttachment extends _BaseAttachment<BaseAttachment> {
	private static final long serialVersionUID = 1L;
	
	//创建默认数据操作对象DAO
	public static final BaseAttachment DAO = new BaseAttachment();
	//附件所属的系统 
	private BaseSystem system;
	
	/**
	 * 获取附件所属的系统
	 */
 	public BaseSystem getSystem() {
		return system;
	}

	/**
	 * 设置附件所属的系统
	  * @param 附件所属的系统
	 */
	 public void setSystem(BaseSystem system) {
		this.system = system;
	}
	//其他设置JSON字符串对应的JSON对象 
	private JSONObject otherJsonObject;
	
	/**
	 * 获取其他设置JSON字符串对应的JSON对象
	 */
 	public JSONObject getOtherJsonObject() {
		return otherJsonObject;
	}

	/**
	 * 设置其他设置JSON字符串对应的JSON对象
	 * @param 其他设置JSON字符串对应的JSON对象
	 */
	 public void setOtherJsonObject(JSONObject otherJsonObject) {
		this.otherJsonObject = otherJsonObject;
	}
	//创建用户 
	private BaseUser createUser;
	
	/**
	 * 获取创建用户
	 */
 	public BaseUser getCreateUser() {
		return createUser;
	}

	/**
	 * 设置创建用户
	 * @param 创建用户
	 */
	 public void setCreateUser(BaseUser createUser) {
		this.createUser = createUser;
	}
	//更新用户 
	private BaseUser updateUser;
	
	/**
	 * 获取更新用户
	 */
 	public BaseUser getUpdateUser() {
		return updateUser;
	}

	/**
	 * 设置更新用户
	 * @param 更新用户
	 */
	 public void setUpdateUser(BaseUser updateUser) {
		this.updateUser = updateUser;
	}
	
	
	/**
	 * 填充关联信息
	 */
	public void fillRelated(){
		//附件所属的系统
		BaseSystem system = BaseSystem.DAO.findById(this.getSystemId());
		this.setSystem(system);
		
		//其他设置JSON字符串对应的JSON对象 
		JSONObject otherJsonObject = new JSONObject();
		try{
			otherJsonObject = JSONObject.parseObject(this.getOtherJson());
		} catch (Exception _err){
			otherJsonObject = new JSONObject();
		}
		this.setOtherJsonObject(otherJsonObject);
		
		//创建用户
		BaseUser createUser = BaseUser.DAO.findById(this.getCreateUserId());
		this.setCreateUser(createUser);
		
		//更新用户
		BaseUser updateUser = BaseUser.DAO.findById(this.getUpdateUserId());
		this.setUpdateUser(updateUser);
		
		/** 对于一对多的关联模式可以参照以下代码进行关联查询封装
		//取出用户所属部门列表
		List<BaseDepartment> departmentList = BaseDepartment.DAO.find("Select * from base_department where id in (Select departmentId from base_user_department where userId=?) or id=?",this.getId(),this.getDepartmentId());
		StringBuffer departmentIds = new StringBuffer();
		for(BaseDepartment department1:departmentList){
			departmentIds.append(",").append(department1.getId());
		}
		if(departmentIds.length()>1){
			this.setDepartmentIds(departmentIds.substring(1));
		}
		this.setDepartmentList(departmentList);
		*/
	}
}
