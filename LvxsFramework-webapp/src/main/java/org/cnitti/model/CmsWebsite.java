package org.cnitti.model;

import org.cnitti.model.core.Table;
import org.cnitti.model.base._CmsWebsite;

import com.alibaba.fastjson.*;

/**
 * 站点基础信息
 * 
 */
@Table(tableName="cms_website",primaryKey="siteId")
public class CmsWebsite extends _CmsWebsite<CmsWebsite> {
	private static final long serialVersionUID = 1L;
	
	//创建默认数据操作对象DAO
	public static final CmsWebsite DAO = new CmsWebsite();
	
	
	/**
	 * 填充关联信息
	 */
	public void fillRelated(){
		/** 对于一对多的关联模式可以参照以下代码进行关联查询封装
		//取出用户所属部门列表
		List<BaseDepartment> departmentList = BaseDepartment.DAO.find("Select * from base_department where id in (Select departmentId from base_user_department where userId=?) or id=?",this.getId(),this.getDepartmentId());
		StringBuffer departmentIds = new StringBuffer();
		for(BaseDepartment department1:departmentList){
			departmentIds.append(",").append(department1.getId());
		}
		if(departmentIds.length()>1){
			this.setDepartmentIds(departmentIds.substring(1));
		}
		this.setDepartmentList(departmentList);
		*/
	}
}
