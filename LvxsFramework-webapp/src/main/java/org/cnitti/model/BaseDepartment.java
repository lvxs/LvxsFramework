package org.cnitti.model;

import java.util.List;

import org.cnitti.model.base._BaseDepartment;
import org.cnitti.model.core.Table;

import com.alibaba.fastjson.JSONObject;

/**
 * 部门信息 部门或者其他分支机构基础信息
 */
@Table(tableName = "base_department", primaryKey = "id")
public class BaseDepartment extends _BaseDepartment<BaseDepartment> {
	private static final long serialVersionUID = 1L;

	// 创建默认数据操作对象DAO
	public static final BaseDepartment DAO = new BaseDepartment();
	// 系统
	private BaseSystem system;

	/**
	 * 获取系统
	 */
	public BaseSystem getSystem() {
		return system;
	}

	/**
	 * 设置系统
	 * 
	 * @param 系统
	 */
	public void setSystem(BaseSystem system) {
		this.system = system;
	}

	// 父级部门
	private BaseDepartment parent;

	/**
	 * 获取父级部门
	 */
	public BaseDepartment getParent() {
		return parent;
	}

	/**
	 * 设置父级部门
	 * 
	 * @param 父级部门
	 */
	public void setParent(BaseDepartment parent) {
		this.parent = parent;
	}

	// 其他设置JSON字符串对应的JSON对象
	private JSONObject otherJsonObject;

	/**
	 * 获取其他设置JSON字符串对应的JSON对象
	 */
	public JSONObject getOtherJsonObject() {
		return otherJsonObject;
	}

	/**
	 * 设置其他设置JSON字符串对应的JSON对象
	 * 
	 * @param 其他设置JSON字符串对应的JSON对象
	 */
	public void setOtherJsonObject(JSONObject otherJsonObject) {
		this.otherJsonObject = otherJsonObject;
	}

	// 创建用户
	private BaseUser createUser;

	/**
	 * 获取创建用户
	 */
	public BaseUser getCreateUser() {
		return createUser;
	}

	/**
	 * 设置创建用户
	 * 
	 * @param 创建用户
	 */
	public void setCreateUser(BaseUser createUser) {
		this.createUser = createUser;
	}

	// 更新用户
	private BaseUser updateUser;

	/**
	 * 获取更新用户
	 */
	public BaseUser getUpdateUser() {
		return updateUser;
	}

	/**
	 * 设置更新用户
	 * 
	 * @param 更新用户
	 */
	public void setUpdateUser(BaseUser updateUser) {
		this.updateUser = updateUser;
	}


	// 子级菜单列表
	private List<BaseDepartment> children;
	/**
	 * 获取子级菜单列表
	 */
	public List<BaseDepartment> getChildren() {
		return children;
	}

	/**
	 * 设置子级菜单列表
	 */
	public void setChildren(List<BaseDepartment> children) {
		this.children = children;
	}
}
