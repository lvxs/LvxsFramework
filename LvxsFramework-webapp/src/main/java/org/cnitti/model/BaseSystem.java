package org.cnitti.model;

import org.cnitti.model.core.Table;
import org.cnitti.model.base._BaseSystem;

/**
 * 系统信息
 * 各个子系统或者相关系统的基础信息

 */
@Table(tableName="base_system",primaryKey="id")
public class BaseSystem extends _BaseSystem<BaseSystem> {
	private static final long serialVersionUID = 1L;
	
	// 创建默认数据操作对象DAO
	public static final BaseSystem DAO = new BaseSystem();
		// 创建用户 
		private BaseUser createUser;
		
		/**
		 * 获取创建用户
		 */
	 	public BaseUser getCreateUser() {
			return createUser;
		}
	
		/**
		 * 设置创建用户
		 * @param 创建用户
		 */
		 public void setCreateUser(BaseUser createUser) {
			this.createUser = createUser;
		}
		// 更新用户 
		private BaseUser updateUser;
		
		/**
		 * 获取更新用户
		 */
	 	public BaseUser getUpdateUser() {
			return updateUser;
		}
	
		/**
		 * 设置更新用户
		 * @param 更新用户
		 */
		 public void setUpdateUser(BaseUser updateUser) {
			this.updateUser = updateUser;
		}
}
