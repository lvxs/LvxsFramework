package org.cnitti.model;

import org.cnitti.model.base._BaseUserDepartment;
import org.cnitti.model.core.Table;

/**
 * 用户部门
 * 用户附加部门关联表，用于一个用户属于多个部门的情况

 */
@Table(tableName="base_user_department",primaryKey="id")
public class BaseUserDepartment extends _BaseUserDepartment<BaseUserDepartment> {
	private static final long serialVersionUID = 1L;
	
	// 创建默认数据操作对象DAO
	public static final BaseUserDepartment DAO = new BaseUserDepartment();
		// 用户 
		private BaseUser user;
		
		/**
		 * 获取用户
		 */
	 	public BaseUser getUser() {
			return user;
		}
	
		/**
		 * 设置用户
		 * @param 用户
		 */
		 public void setUser(BaseUser user) {
			this.user = user;
		}
		// 部门 
		private BaseDepartment department;
		
		/**
		 * 获取部门
		 */
	 	public BaseDepartment getDepartment() {
			return department;
		}
	
		/**
		 * 设置部门
		 * @param 部门
		 */
		 public void setDepartment(BaseDepartment department) {
			this.department = department;
		}
}
