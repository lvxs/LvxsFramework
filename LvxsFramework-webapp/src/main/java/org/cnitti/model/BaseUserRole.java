package org.cnitti.model;

import org.cnitti.model.base._BaseUserRole;
import org.cnitti.model.core.Table;

/**
 * 用户角色
 * 用户附加角色关联表
 */
@Table(tableName="base_user_role",primaryKey="id")
public class BaseUserRole extends _BaseUserRole<BaseUserRole> {
	private static final long serialVersionUID = 1L;
	
	// 创建默认数据操作对象DAO
	public static final BaseUserRole DAO = new BaseUserRole();
		// 用户 
		private BaseUser user;
		
		/**
		 * 获取用户
		 */
	 	public BaseUser getUser() {
			return user;
		}
	
		/**
		 * 设置用户
		 * @param 用户
		 */
		 public void setUser(BaseUser user) {
			this.user = user;
		}
		// 角色 
		private BaseRole role;
		
		/**
		 * 获取角色
		 */
	 	public BaseRole getRole() {
			return role;
		}
	
		/**
		 * 设置角色
		 * @param 角色
		 */
		 public void setRole(BaseRole role) {
			this.role = role;
		}
}
