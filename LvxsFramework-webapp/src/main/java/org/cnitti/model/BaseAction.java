package org.cnitti.model;

import org.cnitti.model.core.Table;
import org.cnitti.model.base._BaseAction;

import com.alibaba.fastjson.*;

/**
 * 页面动作
 * 页面中所有按钮、链接等动作的基本信息

 */
@Table(tableName="base_action",primaryKey="id")
public class BaseAction extends _BaseAction<BaseAction> {
	private static final long serialVersionUID = 1L;
	
	// 创建默认数据操作对象DAO
	public static final BaseAction DAO = new BaseAction();
		// 系统菜单 
		private BaseSystemMenu menu;
		
		/**
		 * 获取系统菜单
		 */
	 	public BaseSystemMenu getMenu() {
			return menu;
		}
	
		/**
		 * 设置系统菜单
		  * @param 系统菜单
		 */
		 public void setMenu(BaseSystemMenu menu) {
			this.menu = menu;
		}
		// 其他设置JSON字符串对应的JSON对象 
		private JSONObject otherJsonObject;
		
		/**
		 * 获取其他设置JSON字符串对应的JSON对象
		 */
	 	public JSONObject getOtherJsonObject() {
			return otherJsonObject;
		}
	
		/**
		 * 设置其他设置JSON字符串对应的JSON对象
		 * @param 其他设置JSON字符串对应的JSON对象
		 */
		 public void setOtherJsonObject(JSONObject otherJsonObject) {
			this.otherJsonObject = otherJsonObject;
		}
		// 创建用户 
		private BaseUser createUser;
		
		/**
		 * 获取创建用户
		 */
	 	public BaseUser getCreateUser() {
			return createUser;
		}
	
		/**
		 * 设置创建用户
		 * @param 创建用户
		 */
		 public void setCreateUser(BaseUser createUser) {
			this.createUser = createUser;
		}
		// 更新用户 
		private BaseUser updateUser;
		
		/**
		 * 获取更新用户
		 */
	 	public BaseUser getUpdateUser() {
			return updateUser;
		}
	
		/**
		 * 设置更新用户
		 * @param 更新用户
		 */
		 public void setUpdateUser(BaseUser updateUser) {
			this.updateUser = updateUser;
		}
}
