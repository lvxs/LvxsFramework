package org.cnitti.model;

import org.cnitti.model.core.Table;
import org.cnitti.model.base._CmsClassify;

import com.alibaba.fastjson.*;

/**
 * 网站栏目基础信息
 * CMS的网站栏目基础信息表
 */
@Table(tableName="cms_classify",primaryKey="classifyId")
public class CmsClassify extends _CmsClassify<CmsClassify> {
	private static final long serialVersionUID = 1L;
	
	//创建默认数据操作对象DAO
	public static final CmsClassify DAO = new CmsClassify();
	//网站   
	// TODO: 代码自动生成器无法判断的关联类型，请自行修改为相对应的类型，以保证程序能够正常编译、运行
	private CmsWebsite webSite;
	
	/**
	 * 获取网站
	 */
 	public CmsWebsite getWebSite() {
		return webSite;
	}

	/**
	 * 设置网站
	 * @param 网站
	 */
	 public void setWebSite(CmsWebsite webSite) {
		this.webSite = webSite;
	}
	//父级栏目   
	// TODO: 代码自动生成器无法判断的关联类型，请自行修改为相对应的类型，以保证程序能够正常编译、运行
	private CmsClassify parent;
	
	/**
	 * 获取父级栏目
	 */
 	public CmsClassify getParent() {
		return parent;
	}

	/**
	 * 设置父级栏目
	 * @param 父级栏目
	 */
	 public void setParent(CmsClassify parent) {
		this.parent = parent;
	}
	//其他设置JSON字符串对应的JSON对象 
	private JSONObject otherJsonObject;
	
	/**
	 * 获取其他设置JSON字符串对应的JSON对象
	 */
 	public JSONObject getOtherJsonObject() {
		return otherJsonObject;
	}

	/**
	 * 设置其他设置JSON字符串对应的JSON对象
	 * @param 其他设置JSON字符串对应的JSON对象
	 */
	 public void setOtherJsonObject(JSONObject otherJsonObject) {
		this.otherJsonObject = otherJsonObject;
	}
	//创建用户 
	private BaseUser createUser;
	
	/**
	 * 获取创建用户
	 */
 	public BaseUser getCreateUser() {
		return createUser;
	}

	/**
	 * 设置创建用户
	 * @param 创建用户
	 */
	 public void setCreateUser(BaseUser createUser) {
		this.createUser = createUser;
	}
	//更新用户 
	private BaseUser updateUser;
	
	/**
	 * 获取更新用户
	 */
 	public BaseUser getUpdateUser() {
		return updateUser;
	}

	/**
	 * 设置更新用户
	 * @param 更新用户
	 */
	 public void setUpdateUser(BaseUser updateUser) {
		this.updateUser = updateUser;
	}
	
	
	/**
	 * 填充关联信息
	 */
	public void fillRelated(){
		//网站   
		// TODO: 代码自动生成器无法判断的关联类型，请自行修改为相对应的类型，以保证程序能够正常编译、运行
		CmsWebsite webSite = CmsWebsite.DAO.findById(this.getWebSiteId());
		this.setWebSite(webSite);
		
		//父级栏目   
		// TODO: 代码自动生成器无法判断的关联类型，请自行修改为相对应的类型，以保证程序能够正常编译、运行
		CmsClassify parent = CmsClassify.DAO.findById(this.getParentId());
		this.setParent(parent);
		
		//其他设置JSON字符串对应的JSON对象 
		JSONObject otherJsonObject = new JSONObject();
		try{
			otherJsonObject = JSONObject.parseObject(this.getOtherJson());
		} catch (Exception _err){
			otherJsonObject = new JSONObject();
		}
		this.setOtherJsonObject(otherJsonObject);
		
		//创建用户
		BaseUser createUser = BaseUser.DAO.findById(this.getCreateUserId());
		this.setCreateUser(createUser);
		
		//更新用户
		BaseUser updateUser = BaseUser.DAO.findById(this.getUpdateUserId());
		this.setUpdateUser(updateUser);
		
		/** 对于一对多的关联模式可以参照以下代码进行关联查询封装
		//取出用户所属部门列表
		List<BaseDepartment> departmentList = BaseDepartment.DAO.find("Select * from base_department where id in (Select departmentId from base_user_department where userId=?) or id=?",this.getId(),this.getDepartmentId());
		StringBuffer departmentIds = new StringBuffer();
		for(BaseDepartment department1:departmentList){
			departmentIds.append(",").append(department1.getId());
		}
		if(departmentIds.length()>1){
			this.setDepartmentIds(departmentIds.substring(1));
		}
		this.setDepartmentList(departmentList);
		*/
	}
}
