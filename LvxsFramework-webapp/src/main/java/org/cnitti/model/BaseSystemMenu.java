package org.cnitti.model;

import java.util.List;

import org.cnitti.model.base._BaseSystemMenu;
import org.cnitti.model.core.Table;

import com.alibaba.fastjson.JSONObject;

/**
 * 系统菜单信息 各个子系统或者相关系统的基础信息
 * 
 */
@Table(tableName = "base_system_menu", primaryKey = "id")
public class BaseSystemMenu extends _BaseSystemMenu<BaseSystemMenu> {
	private static final long serialVersionUID = 1L;

	// 创建默认数据操作对象DAO
	public static final BaseSystemMenu DAO = new BaseSystemMenu();
	// 系统
	private BaseSystem system;

	/**
	 * 获取系统
	 */
	public BaseSystem getSystem() {
		return system;
	}

	/**
	 * 设置系统
	 * 
	 * @param 系统
	 */
	public void setSystem(BaseSystem system) {
		this.system = system;
	}

	// 父级菜单
	private BaseSystemMenu parent;

	/**
	 * 获取父级菜单
	 */
	public BaseSystemMenu getParent() {
		return parent;
	}

	/**
	 * 设置父级菜单
	 * 
	 * @param 父级菜单
	 */
	public void setParent(BaseSystemMenu parent) {
		this.parent = parent;
	}

	// 其他设置JSON字符串对应的JSON对象
	private JSONObject otherJsonObject;

	/**
	 * 获取其他设置JSON字符串对应的JSON对象
	 */
	public JSONObject getOtherJsonObject() {
		return otherJsonObject;
	}

	/**
	 * 设置其他设置JSON字符串对应的JSON对象
	 * 
	 * @param 其他设置JSON字符串对应的JSON对象
	 */
	public void setOtherJsonObject(JSONObject otherJsonObject) {
		this.otherJsonObject = otherJsonObject;
	}

	// 创建用户
	private BaseUser createUser;

	/**
	 * 获取创建用户
	 */
	public BaseUser getCreateUser() {
		return createUser;
	}

	/**
	 * 设置创建用户
	 * 
	 * @param 创建用户
	 */
	public void setCreateUser(BaseUser createUser) {
		this.createUser = createUser;
	}

	// 更新用户
	private BaseUser updateUser;

	/**
	 * 获取更新用户
	 */
	public BaseUser getUpdateUser() {
		return updateUser;
	}

	/**
	 * 设置更新用户
	 * 
	 * @param 更新用户
	 */
	public void setUpdateUser(BaseUser updateUser) {
		this.updateUser = updateUser;
	}


	// 子级菜单列表
	private List<BaseSystemMenu> children;
	/**
	 * 获取子级菜单列表
	 */
	public List<BaseSystemMenu> getChildren() {
		return children;
	}

	/**
	 * 设置子级菜单列表
	 */
	public void setChildren(List<BaseSystemMenu> children) {
		this.children = children;
	}

}
