package org.cnitti.model;

import org.cnitti.model.base._BaseRole;
import org.cnitti.model.core.Table;

import com.alibaba.fastjson.JSONObject;

/**
 * 角色信息 角色基础信息
 * 
 */
@Table(tableName = "base_role", primaryKey = "id")
public class BaseRole extends _BaseRole<BaseRole> {
	private static final long serialVersionUID = 1L;

	// 创建默认数据操作对象DAO
	public static final BaseRole DAO = new BaseRole();
	// 系统
	private BaseSystem system;

	/**
	 * 获取系统
	 */
	public BaseSystem getSystem() {
		return system;
	}

	/**
	 * 设置系统
	 * 
	 * @param 系统
	 */
	public void setSystem(BaseSystem system) {
		this.system = system;
	}

	// 其他设置JSON字符串对应的JSON对象
	private JSONObject otherJsonObject;

	/**
	 * 获取其他设置JSON字符串对应的JSON对象
	 */
	public JSONObject getOtherJsonObject() {
		return otherJsonObject;
	}

	/**
	 * 设置其他设置JSON字符串对应的JSON对象
	 * 
	 * @param 其他设置JSON字符串对应的JSON对象
	 */
	public void setOtherJsonObject(JSONObject otherJsonObject) {
		this.otherJsonObject = otherJsonObject;
	}

	// 创建用户
	private BaseUser createUser;

	/**
	 * 获取创建用户
	 */
	public BaseUser getCreateUser() {
		return createUser;
	}

	/**
	 * 设置创建用户
	 * 
	 * @param 创建用户
	 */
	public void setCreateUser(BaseUser createUser) {
		this.createUser = createUser;
	}

	// 更新用户
	private BaseUser updateUser;

	/**
	 * 获取更新用户
	 */
	public BaseUser getUpdateUser() {
		return updateUser;
	}

	/**
	 * 设置更新用户
	 * 
	 * @param 更新用户
	 */
	public void setUpdateUser(BaseUser updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * 填充用户关联信息
	 */
	public void fillRelated(){
		// 系统
		BaseSystem system_1 = BaseSystem.DAO.findById(this.getSystemId());
		this.setSystem(system_1);
		// 创建用户
		BaseUser createUser = BaseUser.DAO.findById(this.getCreateUserId());
		this.setCreateUser(createUser);
		// 更新用户
		BaseUser updateUser = BaseUser.DAO.findById(this.getUpdateUserId());
		this.setUpdateUser(updateUser);
	} 
}
