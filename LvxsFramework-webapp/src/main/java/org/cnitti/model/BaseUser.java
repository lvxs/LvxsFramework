package org.cnitti.model;

import org.cnitti.model.core.Table;
import org.cnitti.utils.StringUtils;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

import org.cnitti.model.base._BaseUser;

/**
 * 用户信息
 * 公用的系统用户基础信息
 */
@Table(tableName="base_user",primaryKey="id")
public class BaseUser extends _BaseUser<BaseUser> {
	private static final long serialVersionUID = 1L;
	
	// 创建默认数据操作对象DAO
	public static BaseUser DAO = new BaseUser();
	
	//用户拥有角色列表
	private List<BaseRole> roleList;
	
	//用户所在部门
	private BaseDepartment department;

	//创建用户
	private BaseUser createUser;
	
	// 更新用户
	private BaseUser updateUser;
	
	// 用户的主要角色
	private BaseRole role;
	
	//用户所在部门列表，主要用户一个用户隶属多个部门的情况
	private List<BaseDepartment> departmentList;
	
	//用户所属部门IDs
	private String departmentIds;
	
	//用户拥有角色IDs
	private String roleIds;
	
	//其他设置JSON字符串对应的JSON对象
	private JSONObject otherJsonObject;

	/**
	 * 获取用户拥有角色列表
	 * @return 用户拥有角色列表
	 */
	public List<BaseRole> getRoleList() {
		return roleList;
	}

	/**
	 * 设置用户拥有角色列表
	 * @param 用户拥有角色列表
	 */
	public void setRoleList(List<BaseRole> roleList) {
		this.roleList = roleList;
	}

	/**
	 * 获取用户所属部门
	 * @return 用户所属部门
	 */
	public BaseDepartment getDepartment() {
		return department;
	}

	/**
	 * 设置用户所属部门
	 * @param 用户所属部门
	 */
	public void setDepartment(BaseDepartment department) {
		this.department = department;
	}

	/**
	 * 获取创建用户
	 * @return 创建用户
	 */
	public BaseUser getCreateUser() {
		return createUser;
	}

	/**
	 * 设置创建用户
	 * @param 创建用户
	 */
	public void setCreateUser(BaseUser createUser) {
		this.createUser = createUser;
	}

	/**
	 * 获取更新用户
	 * @return 更新用户
	 */
	public BaseUser getUpdateUser() {
		return updateUser;
	}

	/**
	 * 设置更新用户
	 * @param 更新用户
	 */
	public void setUpdateUser(BaseUser updateUser) {
		this.updateUser = updateUser;
	}
	/**
	 * 获取用户默认的主题样式
	 * @return
	 */
	public String getDefaultStyle(){
		String style = "";
		if(StringUtils.isNotEmpty(getOtherJson())){
			try{
				JSONObject json = JSONObject.parseObject(getOtherJson());
				style = json.getString("style");
			} catch(Exception e){
			}
		}
		return style;
	}
	/**
	 * 获取用户的主要角色
	 * @return 角色信息
	 */
	public BaseRole getRole() {
		return role;
	}
	/**
	 * 设置用户的主要角色
	 * @param role 角色信息
	 */
	public void setRole(BaseRole role) {
		this.role = role;
	}

	/**
	 * @return 用户所在部门列表
	 */
	public List<BaseDepartment> getDepartmentList() {
		return departmentList;
	}

	/**
	 * @param departmentList 用户所在部门列表
	 */
	public void setDepartmentList(List<BaseDepartment> departmentList) {
		this.departmentList = departmentList;
	}

	/**
	 * @return 用户所属部门IDs
	 */
	public String getDepartmentIds() {
		return departmentIds;
	}

	/**
	 * @param departmentIds 用户所属部门IDs
	 */
	public void setDepartmentIds(String departmentIds) {
		this.departmentIds = departmentIds;
	}

	/**
	 * @return 用户拥有角色IDs
	 */
	public String getRoleIds() {
		return roleIds;
	}

	/**
	 * @param roleIds 用户拥有角色IDs
	 */
	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}
	/**
	 * 获取其他设置JSON字符串对应的JSON对象
	 * @return 其他设置JSON字符串对应的JSON对象
	 */
	public JSONObject getOtherJsonObject() {
		return otherJsonObject;
	}
	/**
	 * 设置其他设置JSON字符串对应的JSON对象
	 * @param otherJsonObject 其他设置JSON字符串对应的JSON对象
	 */
	public void setOtherJsonObject(JSONObject otherJsonObject) {
		this.otherJsonObject = otherJsonObject;
	}

	/**
	 * 填充用户关联信息
	 */
	public void fillRelated(){
		// 主要角色
		BaseRole role = BaseRole.DAO.findById(this.getRoleId());
		this.setRole(role);
		//取出用户拥有角色列表
		List<BaseRole> roleList = BaseRole.DAO.find("Select * from base_role where id in (Select roleId from base_user_role where userId=?) or id=?",this.getId(),this.getRoleId());
		StringBuffer roleIds = new StringBuffer();
		for(BaseRole role1:roleList){
			roleIds.append(",").append(role1.getId());
		}
		if(roleIds.length()>1){
			this.setRoleIds(roleIds.substring(1));
		}
		this.setRoleList(roleList);
		// 主要部门
		BaseDepartment department = BaseDepartment.DAO.findById(this.getDepartmentId());
		this.setDepartment(department);
		//取出用户所属部门列表
		List<BaseDepartment> departmentList = BaseDepartment.DAO.find("Select * from base_department where id in (Select departmentId from base_user_department where userId=?) or id=?",this.getId(),this.getDepartmentId());
		StringBuffer departmentIds = new StringBuffer();
		for(BaseDepartment department1:departmentList){
			departmentIds.append(",").append(department1.getId());
		}
		if(departmentIds.length()>1){
			this.setDepartmentIds(departmentIds.substring(1));
		}
		this.setDepartmentList(departmentList);
		//其他设置JSON字符串对应的JSON对象
		JSONObject otherJsonObject = new JSONObject();
		try{
			otherJsonObject = JSONObject.parseObject(this.getOtherJson());
		} catch (Exception _err){
			otherJsonObject = new JSONObject();
		}
		this.setOtherJsonObject(otherJsonObject);
		// 创建用户
		BaseUser createUser = BaseUser.DAO.findById(this.getCreateUserId());
		this.setCreateUser(createUser);
		// 更新用户
		BaseUser updateUser = BaseUser.DAO.findById(this.getUpdateUserId());
		this.setUpdateUser(updateUser);
	}
}
