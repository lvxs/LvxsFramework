package org.cnitti.controller.front;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;

public class ArticleController extends Controller {
	@ActionKey("/article")
	public void index() {
		renderText("前台文章");
	}
}