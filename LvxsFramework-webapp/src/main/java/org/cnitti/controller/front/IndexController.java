package org.cnitti.controller.front;

import org.cnitti.router.RouterMapping;

import com.jfinal.core.Controller;

@RouterMapping(url = "/")
public class IndexController extends Controller {
	
	public void index() {
		render("/WEB-INF/t/ryloop/front/index.jsp");
		//redirect("/admin/"); 
	}
	public void login(){
		//render(new RedirectRender("/user/login"));
		redirect("/admin/login"); 
	}
	
	public void index2() {
		render("/WEB-INF/t/ryloop/front/index2.jsp");
		//redirect("/admin/"); 
	}
	
	public void index3() {
		render("/WEB-INF/t/ryloop/front/index3.jsp");
		//redirect("/admin/"); 
	}
}