package org.cnitti.controller.front;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;

public class UserController extends Controller {
	@ActionKey("/user")
	public void index() {
		renderText("前台用户");
	}
	public void login(){
		render("login.html");
	}
}