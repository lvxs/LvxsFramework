package org.cnitti.controller.front;

import org.cnitti.Consts;
import org.cnitti.model.BaseSystem;
import org.cnitti.router.RouterMapping;
import org.cnitti.utils.AttachmentUtils;
import org.cnitti.utils.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.upload.UploadFile;

@RouterMapping(url = "/file")
public class FileController extends Controller {
	/**
	 * 文件上传
	 */
	public void upload() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		JSONObject json = new JSONObject();
		try{
			UploadFile file = getFile();
			String filePath = AttachmentUtils.moveFile(file, "/u/" + system.getFilePath());
			filePath = filePath.replaceAll("\\\\","/");
			if(StringUtils.isNotEmpty(filePath)){
				json.put("succ",true);
				json.put("url", filePath);
			} else {
				json.put("succ",false);
				json.put("message", "上传出现错误，请稍后再上传");
			}
		} catch (Exception e) {
			json.put("error", 1);
			json.put("succ",false);
			json.put("message",e.getMessage());
		}
		renderJson(json.toJSONString());
	}
	/*
	 * public void download(){ String path = getPara(0); String img =
	 * PathKit.getWebRootPath() + "/img/u/" + path.replaceAll("_", "/");
	 * ZipUtil.zip(img, PathKit.getWebRootPath() + "/img/temp/" + path);
	 * renderFile("/img/temp/" + path + ".zip"); }
	 */
}