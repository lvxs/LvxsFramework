package org.cnitti.controller.admin;

import java.math.BigInteger;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.cnitti.Consts;
import org.cnitti.core.JBaseCRUDController;
import org.cnitti.model.*;
import org.cnitti.router.RouterMapping;
import org.cnitti.utils.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;
/**
 * 网站栏目基础信息
 * CMS的网站栏目基础信息表
 */
@RouterMapping(url = "/admin/cmsClassify/", viewPath = "/WEB-INF/t")
public class _CmsClassifyController extends JBaseCRUDController<CmsClassify> { 
	/**
	  * 列表页面，模块默认主界面
	  */
	public void index() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/CmsClassify/list.jsp");
	}
	/**
	 * 返回JSON格式的条件查询或者分页显示数据
	 */
	public void searchData(){
		StringBuffer orderBys = new StringBuffer("");
		for(int i=0;i<5;i++){
			String column = getPara("order["+i+"][column]");
			column = getPara("columns["+column+"][data]");
			if(StrKit.notBlank(column)){
				if(orderBys.length()>0)orderBys.append(",");
				orderBys.append(" "+column);
				if("desc".equalsIgnoreCase(getPara("order["+i+"][dir]"))){
					orderBys.append(" desc");
				}
			} else {
				break;
			}
		}
		if(orderBys.length()>0)orderBys.insert(0," order by");
		StringBuffer wheres = new StringBuffer();
		if(StringUtils.areNotBlank(getPara("search[value]"))){
			String searchValue = getPara("search[value]").replaceAll("[']","");
			HttpServletRequest request = this.getRequest();
			String className = "CmsClassify";
			HashMap<String,ColumnMeta> colMetaMap = new HashMap<String,ColumnMeta>();
			@SuppressWarnings("unchecked")
			HashMap<String,TableMeta> tableMetaMap = (HashMap<String,TableMeta>)request.getServletContext().getAttribute(Consts.TABLE_META_MAP);
			if(tableMetaMap!=null && tableMetaMap.containsKey(className)){
				TableMeta tableMeta = tableMetaMap.get(className);
				List<ColumnMeta> colMetas = tableMeta.columnMetas;
				for(ColumnMeta col:colMetas){
					colMetaMap.put(col.attrName,col);
				}
			}
			for(int i=0;i<50;i++){
				String column =getPara("columns["+i+"][data]");
				boolean searchable = getParaToBoolean("columns["+i+"][searchable]",false);
				if(StrKit.notBlank(column) && searchable){
					ColumnMeta col = colMetaMap.get(column);
					if(col!=null && col.javaType.equalsIgnoreCase("java.lang.String")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("a."+column + " like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("createUser.realName")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("createUser.realName like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("updateUser.realName")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("updateUser.realName like '%"+searchValue+"%' ");
					}				} else if(StrKit.isBlank(column)){
					break;
				}
			}
			if(wheres.length()>0){
				wheres.insert(0,"(").append(")");
			}
		}
		if(wheres.length()>0){
			wheres.insert(0," WHERE ");
		}
		int draw = getParaToInt("draw",1);
		int start = getParaToInt("start",0);
		if(start<0)start=0;
		int length = getParaToInt("length",10);
		if(length<1 && length!=-1)length=1;
		JSONObject json = new JSONObject();
		StringBuffer select = new StringBuffer("select a.*");
		StringBuffer sqlExceptSelect = new StringBuffer(" from cms_classify as a ");
		sqlExceptSelect.append(" left outer join base_user as createUser on a.createUserId=createUser.id ");
		sqlExceptSelect.append(" left outer join base_user as updateUser on a.updateUserId=updateUser.id ");
		if(length==-1){
			List<CmsClassify> list = CmsClassify.DAO.find(select.toString() + sqlExceptSelect.toString() + wheres.toString() + orderBys.toString() + " limit 0,10000");
			fillRelatedList(list);
			json.put("recordsTotal",list.size());
			json.put("recordsFiltered",list.size());
			json.put("data",list);
		} else {
			Page<CmsClassify> page = CmsClassify.DAO.paginate((start/length)+1,length, select.toString(),sqlExceptSelect.toString() + wheres.toString() + orderBys.toString());
			fillRelatedList(page.getList());
			json.put("recordsTotal",page.getTotalRow());
			json.put("recordsFiltered",page.getTotalRow());
			json.put("data",page.getList());
		}
		json.put("draw",draw);
		renderJson(json);
	}
	
	/**
	 * 新增
	 */
	public void add() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/CmsClassify/add.jsp");
	}
	/**
	 * 修改
	 */
	public void edit() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("classifyId");
		if (id != null) {
			CmsClassify cmsClassify = CmsClassify.DAO.findById(id);
			if(cmsClassify!=null){
				cmsClassify.fillRelated();
			}
			setAttr("cmsClassify",cmsClassify);
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/CmsClassify/edit.jsp");
	}
	/**
	 * 删除
	 */
	public void delete() {
		Long[] idArray = getParaValuesToLong("classifyId");
		if(idArray==null || idArray.length==0){
			idArray = StringUtils.getLongArray(getPara("classifyId"));
		}
		if (idArray != null && idArray.length>0) {
			for(Long id:idArray){
				CmsClassify.DAO.deleteById(id);
			}
			renderAjaxResultForSuccess("删除成功");
		} else {
			renderAjaxResultForError();
		}
	}
	/**
	 * 获取详细JSON
	 */
	public void detailJSON() {
		BigInteger id = getParaToBigInteger("classifyId");
		if (id != null) {
			CmsClassify cmsClassify = CmsClassify.DAO.findById(id);
			if(cmsClassify!=null){
				cmsClassify.fillRelated();
				renderJson(cmsClassify);
			}else {
				JSONObject json = new JSONObject();
				json.put(Consts.ERROR_CODE,"-1");
				json.put(Consts.MESSAGE,"对象不存在");
				renderJson(json);
			}
		} else {
			JSONObject json = new JSONObject();
			json.put(Consts.ERROR_CODE,"-100");
			json.put(Consts.MESSAGE,"非法调用");
			renderJson(json);
		}
	}
	/**
	 * 查看
	 */
	public void view() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("classifyId");
		if (id != null) {
			CmsClassify cmsClassify = CmsClassify.DAO.findById(id);
			if(cmsClassify!=null){
				cmsClassify.fillRelated();
			}
			setAttr("cmsClassify",cmsClassify);
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/CmsClassify/view.jsp");
	}
	/**
	 * 自动填充网站栏目基础信息列表中网站栏目基础信息的关联信息
	 * @param userList	网站栏目基础信息列表
	 */
	private void fillRelatedList(List<CmsClassify> cmsClassifyList){
		for(CmsClassify cmsClassify:cmsClassifyList){
			cmsClassify.fillRelated();
		}
	}
	/**
	 * 保存执行前附加逻辑
	 * @return 返回是否继续执行保存
	 */
	@Override
	public boolean onModelSaveBefore(CmsClassify cmsClassify) {
		//当前登录用户
		BaseUser loginUser = getAttr("loginUser");
		if(cmsClassify.getClassifyId()==null || cmsClassify.getClassifyId().longValue()<1){
			//如果ID为空，则表示要新增数据
			Random rand = new Random(System.currentTimeMillis()%3333);
			//自动生成ID
			cmsClassify.setClassifyId(System.currentTimeMillis()*1000+rand.nextInt(999));
			//设置创建时间为当前时间
			cmsClassify.setCreateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置创建用户ID为当前登录用户的ID
				cmsClassify.setCreateUserId(loginUser.getId());
				//设置创建用户为当前登录用户
				cmsClassify.setCreateUser(loginUser);
			}
			//设置更新时间为当前时间
			cmsClassify.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置更新用户ID为当前登录用户的ID
				cmsClassify.setUpdateUserId(loginUser.getId());
				//设置更新用户为当前登录用户
				cmsClassify.setUpdateUser(loginUser);
			}
		} else {
			//设置更新时间为当前时间
			cmsClassify.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				cmsClassify.setUpdateUserId(loginUser.getId());
				cmsClassify.setUpdateUser(loginUser);
			}
		}
		return true;
	}
	/**
	 * 保存执行后附加逻辑
	 */
	@Override
	public boolean onModelSaveAfter(CmsClassify cmsClassify) {
		return true;
	}
}
