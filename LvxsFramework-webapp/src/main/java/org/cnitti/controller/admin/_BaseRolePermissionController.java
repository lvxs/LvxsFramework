package org.cnitti.controller.admin;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.cnitti.Consts;
import org.cnitti.core.JBaseCRUDController;
import org.cnitti.model.BaseAction;
import org.cnitti.model.BaseRole;
import org.cnitti.model.BaseRolePermission;
import org.cnitti.model.BaseSystem;
import org.cnitti.model.BaseSystemMenu;
import org.cnitti.model.BaseUser;
import org.cnitti.router.RouterMapping;
import org.cnitti.utils.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;
/**
 * 角色权限
 * 角色拥有权限信息表

 */
@RouterMapping(url = "/admin/baseRolePermission", viewPath = "/WEB-INF/t")
public class _BaseRolePermissionController extends JBaseCRUDController<BaseRolePermission> { 
	/**
	  * 列表页面，模块默认主界面
	  */
	public void index() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseRolePermission/list.jsp");
	}
	/**
	  * 角色权限设置页面
	  */
	public void showPowerTree() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger roleId = getParaToBigInteger("roleId");
		if (roleId != null) {
			BaseRole baseRole = BaseRole.DAO.findById(roleId);
			JSONObject json = new JSONObject();
			if(baseRole!=null){
				BaseSystem baseSystem = BaseSystem.DAO.findById(baseRole.getSystemId());
				baseRole.setSystem(baseSystem);
				setAttr("baseRole",baseRole);
				setAttr("baseSystem",baseSystem);
				List<BaseRolePermission> permissionList  = BaseRolePermission.DAO.find("select * from base_role_permission where roleId = " + roleId + " order by menuId");
				for(BaseRolePermission permission:permissionList){
					JSONObject otherJson = new JSONObject();
					try{
						otherJson = JSONObject.parseObject(permission.getOtherJson());
					}catch(Exception e){
						System.out.println(e.getMessage());
					}
					permission.setOtherJsonObject(otherJson);
				}
				json.put("permissionList", permissionList);
				List<BaseAction> actionList  = BaseAction.DAO.find("select a.* from base_action as a left outer join base_system_menu as b on a.menuId=b.id where b.systemId = " + baseRole.getSystemId() +" order by a.menuId,a.sortNumber");
				json.put("actionList", actionList);
			}
			setAttr("pageData",json.toJSONString());
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseRolePermission/showPowerTree.jsp");
	}
	/**
	 * 返回JSON格式的条件查询或者分页显示数据
	 */
	public void searchData(){
		StringBuffer orderBys = new StringBuffer("");
		for(int i=0;i<5;i++){
			String column = getPara("order["+i+"][column]");
			column = getPara("columns["+column+"][data]");
			if(StrKit.notBlank(column)){
				if(orderBys.length()>0)orderBys.append(",");
				orderBys.append(" "+column);
				if("desc".equalsIgnoreCase(getPara("order["+i+"][dir]"))){
					orderBys.append(" desc");
				}
			} else {
				break;
			}
		}
		if(orderBys.length()>0)orderBys.insert(0," order by");
		StringBuffer wheres = new StringBuffer();
		if(StringUtils.areNotBlank(getPara("search[value]"))){
			String searchValue = getPara("search[value]").replaceAll("'","");
			HttpServletRequest request = this.getRequest();
			String className = "BaseRolePermission";
			HashMap<String,ColumnMeta> colMetaMap = new HashMap<String,ColumnMeta>();
			@SuppressWarnings("unchecked")
			HashMap<String,TableMeta> tableMetaMap = (HashMap<String,TableMeta>)request.getServletContext().getAttribute(Consts.TABLE_META_MAP);
			if(tableMetaMap!=null && tableMetaMap.containsKey(className)){
				TableMeta tableMeta = tableMetaMap.get(className);
				List<ColumnMeta> colMetas = tableMeta.columnMetas;
				for(ColumnMeta col:colMetas){
					colMetaMap.put(col.attrName,col);
				}
			}
			for(int i=0;i<50;i++){
				String column =getPara("columns["+i+"][data]");
				boolean searchable = getParaToBoolean("columns["+i+"][searchable]",false);
				if(StrKit.notBlank(column) && searchable){
					ColumnMeta col = colMetaMap.get(column);
					if(col!=null && col.javaType.equalsIgnoreCase("java.lang.String")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("a."+column + " like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("roleName")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("role.name like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("createUserName")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("createUser.realName like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("updateUserName")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("updateUser.realName like '%"+searchValue+"%' ");
					}				} else if(StrKit.isBlank(column)){
					break;
				}
			}
			if(wheres.length()>0){
				wheres.insert(0,"(").append(")");
			}
		}
		if(wheres.length()>0){
			wheres.insert(0," WHERE ");
		}
		int draw = getParaToInt("draw",1);
		int start = getParaToInt("start",0);
		if(start<0)start=0;
		int length = getParaToInt("length",10);
		if(length<1 && length!=-1)length=1;
		JSONObject json = new JSONObject();
		StringBuffer select = new StringBuffer("select a.*");
		StringBuffer sqlExceptSelect = new StringBuffer(" from base_role_permission as a ");
		select.append(",role.name as roleName");
		sqlExceptSelect.append(" left outer join base_role as role on a.roleId=role.id ");
		select.append(",createUser.realName as createUserName");
		sqlExceptSelect.append(" left outer join base_user as createUser on a.createUserId=createUser.id ");
		select.append(",updateUser.realName as updateUserName");
		sqlExceptSelect.append(" left outer join base_user as updateUser on a.updateUserId=updateUser.id ");
		if(length==-1){
			List<BaseRolePermission> list = BaseRolePermission.DAO.find(select.toString() + sqlExceptSelect.toString() + wheres.toString() + orderBys.toString() + " limit 0,10000");
			json.put("recordsTotal",list.size());
			json.put("recordsFiltered",list.size());
			json.put("data",list);
		} else {
			Page<BaseRolePermission> page = BaseRolePermission.DAO.paginate((start/length)+1,length, select.toString(),sqlExceptSelect.toString() + wheres.toString() + orderBys.toString());
			json.put("recordsTotal",page.getTotalRow());
			json.put("recordsFiltered",page.getTotalRow());
			json.put("data",page.getList());
		}
		json.put("draw",draw);
		renderJson(json);
	}
	/**
	 * 新增
	 */
	public void add() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		List<BaseRole> roleList = BaseRole.DAO.find("select * from base_role where status>0");
		setAttr("roleList",roleList);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseRolePermission/add.jsp");
	}
	/**
	 * 修改
	 */
	public void edit() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			setAttr("baseRolePermission",BaseRolePermission.DAO.findById(id));
		}
		List<BaseRole> roleList = BaseRole.DAO.find("select * from base_role where status>0");
		setAttr("roleList",roleList);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseRolePermission/edit.jsp");
	}
	/**
	 * 保存角色权限
	 */
	public void saveRolePermission() {
		String selectActionIds = getPara("selectActionIds");
		String selectMenuIds = getPara("selectMenuIds");
		Long[] actionIdsArray = StringUtils.getLongArray(selectActionIds);
		Long[] menuIdsArray = StringUtils.getLongArray(selectMenuIds);
		List<Long> savedMenuIdList = new ArrayList<Long>();
		Long roleId = getParaToLong("roleId");
		Db.update("delete from base_role_permission where roleId=" + roleId);
		if (actionIdsArray != null && actionIdsArray.length>0) {
			for(Long actionId:actionIdsArray){
				BaseRolePermission rolePermission = new BaseRolePermission();
				rolePermission.setActionId(actionId);
				rolePermission.setRoleId(roleId);
				rolePermission.setMenuId(getParaToLong("menuId_"+actionId));
				rolePermission.setDataPermission(getParaToInt("permission_"+actionId));
				JSONObject otherJson = new JSONObject();
				if(rolePermission.getDataPermission()==5){
					rolePermission.setSpecialIds(getPara("specialIds_"+actionId));
					otherJson.put("specialNames",getPara("departmentSel_"+actionId));
				} else if(rolePermission.getDataPermission()==6){
					rolePermission.setSpecialIds(getPara("specialIds_"+actionId));
					otherJson.put("specialNames",getPara("userSel_"+actionId));
				}
				rolePermission.setOtherJson(otherJson.toJSONString());
				rolePermission.setStatus(1);
				onModelSaveBefore(rolePermission);
				rolePermission.save();
				onModelSaveAfter(rolePermission);
				if(!savedMenuIdList.contains(rolePermission.getMenuId()))
				savedMenuIdList.add(rolePermission.getMenuId());
			}
		}
		if (menuIdsArray != null && menuIdsArray.length>0) {
			for(Long menuId:menuIdsArray){
				if(!savedMenuIdList.contains(menuId)){
					BaseRolePermission rolePermission = new BaseRolePermission();
					rolePermission.setActionId(0L);
					rolePermission.setRoleId(roleId);
					rolePermission.setMenuId(menuId);
					rolePermission.setDataPermission(1);
					JSONObject otherJson = new JSONObject();
					rolePermission.setOtherJson(otherJson.toJSONString());
					rolePermission.setStatus(1);
					onModelSaveBefore(rolePermission);
					rolePermission.save();
					onModelSaveAfter(rolePermission);
				}
			}
		}
		renderAjaxResultForSuccess("保存成功");
	}
	/**
	 * 删除
	 */
	public void delete() {
		Long[] idArray = getParaValuesToLong("id");
		if(idArray==null || idArray.length==0){
			idArray = StringUtils.getLongArray(getPara("id"));
		}
		if (idArray != null && idArray.length>0) {
			for(Long id:idArray){
				BaseRolePermission.DAO.deleteById(id);
			}
			renderAjaxResultForSuccess("删除成功");
		} else {
			renderAjaxResultForError();
		}
	}
	/**
	 * 获取详细JSON
	 */
	public void detailJSON() {
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseRolePermission baseRolePermission = BaseRolePermission.DAO.findById(id);
			if(baseRolePermission!=null){
				// 菜单
				BaseSystemMenu  menu = BaseSystemMenu.DAO.findById(baseRolePermission.getMenuId());
				baseRolePermission.setMenu(menu);
				// 动作
				BaseAction  action = BaseAction.DAO.findById(baseRolePermission.getActionId());
				baseRolePermission.setAction(action);
				// 角色
				BaseRole role = BaseRole.DAO.findById(baseRolePermission.getRoleId());
				baseRolePermission.setRole(role);
				// 创建用户
				BaseUser createUser = BaseUser.DAO.findById(baseRolePermission.getCreateUserId());
				baseRolePermission.setCreateUser(createUser);
				// 更新用户
				BaseUser updateUser = BaseUser.DAO.findById(baseRolePermission.getUpdateUserId());
				baseRolePermission.setUpdateUser(updateUser);
				renderJson(baseRolePermission);
			}else {
				JSONObject json = new JSONObject();
				json.put(Consts.ERROR_CODE,"-1");
				json.put(Consts.MESSAGE,"对象不存在");
				renderJson(json);
			}
		} else {
			JSONObject json = new JSONObject();
			json.put(Consts.ERROR_CODE,"-100");
			json.put(Consts.MESSAGE,"非法调用");
			renderJson(json);
		}
	}
	/**
	 * 查看
	 */
	public void view() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseRolePermission baseRolePermission = BaseRolePermission.DAO.findById(id);
			if(baseRolePermission!=null){
				// 菜单
				BaseSystemMenu  menu = BaseSystemMenu.DAO.findById(baseRolePermission.getMenuId());
				baseRolePermission.setMenu(menu);
				// 动作
				BaseAction  action = BaseAction.DAO.findById(baseRolePermission.getActionId());
				baseRolePermission.setAction(action);
				// 角色
				BaseRole role = BaseRole.DAO.findById(baseRolePermission.getRoleId());
				baseRolePermission.setRole(role);
				// 创建用户
				BaseUser createUser = BaseUser.DAO.findById(baseRolePermission.getCreateUserId());
				baseRolePermission.setCreateUser(createUser);
				// 更新用户
				BaseUser updateUser = BaseUser.DAO.findById(baseRolePermission.getUpdateUserId());
				baseRolePermission.setUpdateUser(updateUser);
			}
			setAttr("baseRolePermission",baseRolePermission);
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseRolePermission/view.jsp");
	}
	/**
	 * 保存执行前附加逻辑
	 * @return 返回是否继续执行保存
	 */
	@Override
	public boolean onModelSaveBefore(BaseRolePermission baseRolePermission) {
		//当前登录用户
		BaseUser loginUser = getAttr("loginUser");
		if(baseRolePermission.getId()==null || baseRolePermission.getId().longValue()<1){
			//如果ID为空，则表示要新增数据
			Random rand = new Random(System.currentTimeMillis()%3333);
			//自动生成ID
			baseRolePermission.setId(System.currentTimeMillis()*1000+rand.nextInt(999));
			//设置创建时间为当前时间
			baseRolePermission.setCreateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置创建用户ID为当前登录用户的ID
				baseRolePermission.setCreateUserId(loginUser.getId());
				//设置创建用户为当前登录用户
				baseRolePermission.setCreateUser(loginUser);
			}
			//设置更新时间为当前时间
			baseRolePermission.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置更新用户ID为当前登录用户的ID
				baseRolePermission.setUpdateUserId(loginUser.getId());
				//设置更新用户为当前登录用户
				baseRolePermission.setUpdateUser(loginUser);
			}
		} else {
			//设置更新时间为当前时间
			baseRolePermission.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				baseRolePermission.setUpdateUserId(loginUser.getId());
				baseRolePermission.setUpdateUser(loginUser);
			}
		}
		return true;
	}
	/**
	 * 保存执行后附加逻辑
	 */
	@Override
	public boolean onModelSaveAfter(BaseRolePermission baseRolePermission) {
		return true;
	}
}
