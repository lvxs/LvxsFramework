package org.cnitti.controller.admin;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.cnitti.Consts;
import org.cnitti.model.BaseSystem;
import org.cnitti.model.BaseUser;
import org.cnitti.router.RouterMapping;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
@RouterMapping(url = "/admin/", viewPath = "/WEB-INF/t")
public class IndexController extends Controller {
	/**
	 * 后台管理主页面
	 */
	public void index() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(system.getFilePath()+"/"+adminThemes+"/index.jsp");
	}
	
	/**
	 * 后台管理用户登录界面
	 */
	public void login() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		Subject subject = SecurityUtils.getSubject();
		if(subject.isAuthenticated()){
			String uri = getRequest().getRequestURI();
			String contentPath = getAttr("basePath");
			if(StringUtils.isNoneBlank(uri,contentPath) && uri.startsWith(contentPath)){
				uri = uri.substring(contentPath.length());
			}
			if(uri.endsWith("/login")){
				redirect(uri.substring(0,uri.lastIndexOf("/login")+1),true);
			}else if(uri.endsWith("/login/") || uri.contains("/login/")){
				redirect(uri.substring(0,uri.lastIndexOf("/login/")+1),true);
			}else{
				redirect(uri.substring(0,uri.lastIndexOf("/")+1),true);
			}
		} else {
			render(system.getFilePath()+"/"+adminThemes+"/login.jsp");
		}
	}
	
	/**
	 * 安全退出登录
	 */
	public void logout() {
		Subject subject = SecurityUtils.getSubject();
		if(subject.isAuthenticated()){
			subject.logout();
		}
		String uri = getRequest().getRequestURI();
		String contentPath = getAttr("basePath");
		if(StringUtils.isNoneBlank(uri,contentPath) && uri.startsWith(contentPath)){
			uri = uri.substring(contentPath.length());
		}
		if(uri.endsWith("/logout")){
			redirect301(uri.substring(0,uri.lastIndexOf("/logout")+1),true);
		}else if(uri.endsWith("/logout/") || uri.contains("/logout/")){
			redirect301(uri.substring(0,uri.lastIndexOf("/logout/")+1),true);
		}else{
			redirect301(uri.substring(0,uri.lastIndexOf("/")+1),true);
		}
	}
	
	/**
	 * 后台管理用户登录界面
	 */
	public void loginCheck() {
		JSONObject json = new JSONObject();
		String username = getPara("username");
		String password = getPara("password");
		int rememberMe = getParaToInt("rememberMe",0);
		Subject subject = SecurityUtils.getSubject();
	    UsernamePasswordToken token = new UsernamePasswordToken(username,password);
	    token.setRememberMe(rememberMe==1);
	    try {  
	        //登录，即身份验证  
	        subject.login(token);  
	        //登录成功
	        json.put("errorCode", 0);
	    } catch (AuthenticationException e) {  
	        //身份验证失败
	    	if(e instanceof UnknownAccountException || e.getCause() instanceof UnknownAccountException){
	    		json.put("message","账号不存在");
	    	} else if(e instanceof LockedAccountException || e.getCause() instanceof LockedAccountException){
	    		json.put("message","账号被禁用或者锁定");
	    	} else if(e instanceof IncorrectCredentialsException || e.getCause() instanceof IncorrectCredentialsException){
	    		json.put("message","密码错误");
	    	} else{
	    		json.put("message","认证错误");
	    	}
	    }  
		renderJson(json);
	}
	
	public void saveStyle(){
		JSONObject json = new JSONObject();
		String style = getPara("style");
		this.setCookie("style", style,9999999);
		Subject subject = SecurityUtils.getSubject();
		if(subject.isAuthenticated()){
			BaseUser loginUser =  (BaseUser) subject.getSession().getAttribute(Consts.ADMIN_USER);
			if(loginUser!=null){
				JSONObject json1 = null;
				try{
					json1 =  JSONObject.parseObject(loginUser.getOtherJson());
				} catch(Exception e){
				}
				if(json1==null)	json1 = new JSONObject();
				json1.put("style",style);
				loginUser.setOtherJson(json1.toJSONString());
				loginUser.update();
		        json.put("errorCode", 0);
			}
		}
		renderJson(json);
	}
}