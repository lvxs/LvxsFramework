package org.cnitti.controller.admin;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.cnitti.Consts;
import org.cnitti.core.JBaseCRUDController;
import org.cnitti.model.BaseRole;
import org.cnitti.model.BaseSystem;
import org.cnitti.model.BaseUser;
import org.cnitti.model.BaseUserRole;
import org.cnitti.router.RouterMapping;
import org.cnitti.utils.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;
/**
 * 用户角色
 * 用户附加角色关联表

 */
@RouterMapping(url = "/admin/baseUserRole", viewPath = "/WEB-INF/t")
public class _BaseUserRoleController extends JBaseCRUDController<BaseUserRole> { 
	/**
	  * 列表页面，模块默认主界面
	  */
	public void index() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseUserRole/list.jsp");
	}
	/**
	 * 返回JSON格式的条件查询或者分页显示数据
	 */
	public void searchData(){
		StringBuffer orderBys = new StringBuffer("");
		for(int i=0;i<5;i++){
			String column = getPara("order["+i+"][column]");
			column = getPara("columns["+column+"][data]");
			if(StrKit.notBlank(column)){
				if(orderBys.length()>0)orderBys.append(",");
				orderBys.append(" "+column);
				if("desc".equalsIgnoreCase(getPara("order["+i+"][dir]"))){
					orderBys.append(" desc");
				}
			} else {
				break;
			}
		}
		if(orderBys.length()>0)orderBys.insert(0," order by");
		StringBuffer wheres = new StringBuffer();
		if(StringUtils.areNotBlank(getPara("search[value]"))){
			String searchValue = getPara("search[value]");
			HttpServletRequest request = this.getRequest();
			String className = "BaseUserRole";
			HashMap<String,ColumnMeta> colMetaMap = new HashMap<String,ColumnMeta>();
			@SuppressWarnings("unchecked")
			HashMap<String,TableMeta> tableMetaMap = (HashMap<String,TableMeta>)request.getServletContext().getAttribute(Consts.TABLE_META_MAP);
			if(tableMetaMap!=null && tableMetaMap.containsKey(className)){
				TableMeta tableMeta = tableMetaMap.get(className);
				List<ColumnMeta> colMetas = tableMeta.columnMetas;
				for(ColumnMeta col:colMetas){
					colMetaMap.put(col.attrName,col);
				}
			}
			for(int i=0;i<50;i++){
				String column =getPara("columns["+i+"][data]");
				boolean searchable = getParaToBoolean("columns["+i+"][searchable]",false);
				if(StrKit.notBlank(column) && searchable){
					ColumnMeta col = colMetaMap.get(column);
					if(col!=null && col.javaType.equalsIgnoreCase("java.lang.String")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("a."+column + " like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("userName")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("user.realName like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("roleName")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("role.name like '%"+searchValue+"%' ");
					}				} else if(StrKit.isBlank(column)){
					break;
				}
			}
			if(wheres.length()>0){
				wheres.insert(0,"(").append(")");
			}
		}
		if(wheres.length()>0){
			wheres.insert(0," WHERE ");
		}
		int draw = getParaToInt("draw",1);
		int start = getParaToInt("start",0);
		if(start<0)start=0;
		int length = getParaToInt("length",10);
		if(length<1 && length!=-1)length=1;
		JSONObject json = new JSONObject();
		StringBuffer select = new StringBuffer("select a.*");
		StringBuffer sqlExceptSelect = new StringBuffer(" from base_user_role as a ");
		select.append(",user.realName as userName");
		sqlExceptSelect.append(" left outer join base_user as user on a.userId=user.id ");
		select.append(",role.name as roleName");
		sqlExceptSelect.append(" left outer join base_role as role on a.roleId=role.id ");
		if(length==-1){
			List<BaseUserRole> list = BaseUserRole.DAO.find(select.toString() + sqlExceptSelect.toString() + wheres.toString() + orderBys.toString() + " limit 0,10000");
			json.put("recordsTotal",list.size());
			json.put("recordsFiltered",list.size());
			json.put("data",list);
		} else {
			Page<BaseUserRole> page = BaseUserRole.DAO.paginate((start/length)+1,length, select.toString(),sqlExceptSelect.toString() + wheres.toString() + orderBys.toString());
			json.put("recordsTotal",page.getTotalRow());
			json.put("recordsFiltered",page.getTotalRow());
			json.put("data",page.getList());
		}
		json.put("draw",draw);
		renderJson(json);
	}
	/**
	 * 新增
	 */
	public void add() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		List<BaseRole> roleList = BaseRole.DAO.find("select * from base_role where status>0");
		setAttr("roleList",roleList);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseUserRole/add.jsp");
	}
	/**
	 * 修改
	 */
	public void edit() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			setAttr("baseUserRole",BaseUserRole.DAO.findById(id));
		}
		List<BaseRole> roleList = BaseRole.DAO.find("select * from base_role where status>0");
		setAttr("roleList",roleList);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseUserRole/edit.jsp");
	}
	/**
	 * 删除
	 */
	public void delete() {
		BigInteger[] idArray = getParaValuesToBigInteger("id");
		if(idArray==null || idArray.length==0){
			idArray = getParaValuesToBigInteger("id");
		}
		if (idArray != null && idArray.length>0) {
			for(BigInteger id:idArray){
				BaseUserRole.DAO.deleteById(id);
			}
			renderAjaxResultForSuccess("删除成功");
		} else {
			renderAjaxResultForError();
		}
	}
	/**
	 * 获取详细JSON
	 */
	public void detailJSON() {
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseUserRole baseUserRole = BaseUserRole.DAO.findById(id);
			if(baseUserRole!=null){
				// 用户
				BaseUser user = BaseUser.DAO.findById(baseUserRole.getUserId());
				baseUserRole.setUser(user);
				// 角色
				BaseRole role = BaseRole.DAO.findById(baseUserRole.getRoleId());
				baseUserRole.setRole(role);
				renderJson(baseUserRole);
			}else {
				JSONObject json = new JSONObject();
				json.put(Consts.ERROR_CODE,"-1");
				json.put(Consts.MESSAGE,"对象不存在");
				renderJson(json);
			}
		} else {
			JSONObject json = new JSONObject();
			json.put(Consts.ERROR_CODE,"-100");
			json.put(Consts.MESSAGE,"非法调用");
			renderJson(json);
		}
	}
	/**
	 * 查看
	 */
	public void view() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseUserRole baseUserRole = BaseUserRole.DAO.findById(id);
			if(baseUserRole!=null){
				// 用户
				BaseUser user = BaseUser.DAO.findById(baseUserRole.getUserId());
				baseUserRole.setUser(user);
				// 角色
				BaseRole role = BaseRole.DAO.findById(baseUserRole.getRoleId());
				baseUserRole.setRole(role);
			}
			setAttr("baseUserRole",baseUserRole);
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseUserRole/view.jsp");
	}
	/**
	 * 保存执行前附加逻辑
	 * @return 返回是否继续执行保存
	 */
	@Override
	public boolean onModelSaveBefore(BaseUserRole baseUserRole) {
		//当前登录用户
		//BaseUser loginUser = getAttr("loginUser");
		if(baseUserRole.getId()==null || baseUserRole.getId().longValue()<1){
			//如果ID为空，则表示要新增数据
			Random rand = new Random(System.currentTimeMillis()%3333);
			//自动生成ID
			baseUserRole.setId(System.currentTimeMillis()*1000+rand.nextInt(999));
		} else {
		}
		return true;
	}
	/**
	 * 保存执行后附加逻辑
	 */
	@Override
	public boolean onModelSaveAfter(BaseUserRole baseUserRole) {
		return true;
	}
}
