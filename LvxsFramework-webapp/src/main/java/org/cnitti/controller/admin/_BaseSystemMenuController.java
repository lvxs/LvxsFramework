package org.cnitti.controller.admin;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.cnitti.Consts;
import org.cnitti.core.JBaseCRUDController;
import org.cnitti.model.BaseAction;
import org.cnitti.model.BaseRole;
import org.cnitti.model.BaseSystem;
import org.cnitti.model.BaseSystemMenu;
import org.cnitti.model.BaseUser;
import org.cnitti.router.RouterMapping;
import org.cnitti.utils.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;
/**
 * 系统信息
 * 各个子系统或者相关系统的基础信息

 */
@RouterMapping(url = "/admin/baseSystemMenu", viewPath = "/WEB-INF/t")
public class _BaseSystemMenuController extends JBaseCRUDController<BaseSystemMenu> { 
	/**
	  * 列表页面，模块默认主界面
	  */
	public void index() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseSystemMenu/list.jsp");
	}
	/**
	  * 系统功能菜单管理设置
	  */
	public void menuManage() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger systemId = getParaToBigInteger("systemId");
		if (systemId != null) {
			setAttr("baseSystem",BaseSystem.DAO.findById(systemId));
		} else {
			setAttr("baseSystem",system);
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseSystemMenu/menuManage.jsp");
	}
	/**
	 * 返回树形结构系统菜单JSON数据
	 */
	public void getTreeJson(){
		JSONObject json = new JSONObject();
		BigInteger systemId = getParaToBigInteger("systemId");
		if(systemId==null || systemId.longValue()<1){
			BaseSystem system = getAttr(Consts.SYSTEM);
			systemId = BigInteger.valueOf(system.getId());
		}
		String sql = "select * from base_system_menu where systemId=" +systemId;
		//当前登录用户
		BaseUser loginUser = getAttr("loginUser");
		StringBuffer roleIds = new StringBuffer();
		for(BaseRole role:loginUser.getRoleList()){
			roleIds.append(",").append(role.getId());
		}
		if(roleIds.length()>1 && loginUser.getRoleId()>1000){
			//当用户拥有的所有角色IDs字符串长度大于1，并且用户主要角色ID大于1000时，查询用户拥有的系统菜单增加角色ID的限制
			//角色ID小1000表示这个角色是在数据库中手工添加的，一般都拥有超级大的权限
			sql+=" and id in (select menuId from base_role_permission where roleId in ("+ roleIds.substring(1)+"))";
		}
		if (systemId != null) {
			List<BaseSystemMenu> list = BaseSystemMenu.DAO.find(sql+ " order by menuNumber");
			json.put("recordsTotal",list.size());
			json.put("recordsFiltered",list.size());
			json.put("data",list);
		}
		renderJson(json);
	}
	/**
	 * 树形目录移动
	 */
	public void drop() {
		JSONObject json = new JSONObject();
		try{
			//菜单所属系统ID
			//BigInteger systemId = getParaToBigInteger("systemId");
			//要移动的目录节点JSON字符串
			String treeNodesJSON = getPara("treeNodesJSON");
			treeNodesJSON = StringEscapeUtils.unescapeHtml4(treeNodesJSON);
			//要移动的目录节点JSON数组
			JSONArray treeNodes = JSONArray.parseArray(treeNodesJSON);
			//目标节点JSON字符串
			String targetNodeJSON = getPara("targetNodeJSON");
			targetNodeJSON = StringEscapeUtils.unescapeHtml4(targetNodeJSON);
			//目标节点JSON对象
			JSONObject targetNode = JSONObject.parseObject(targetNodeJSON);
			//目标同级节点JSON字符串
			String peerNodesJSON = getPara("peerNodesJSON");
			peerNodesJSON = StringEscapeUtils.unescapeHtml4(peerNodesJSON);
			//目标同级节点JSON数组
			JSONArray peerNodes = JSONArray.parseArray(peerNodesJSON);
			//移动类型，"inner"：移动成为目标节点的子节点，"prev"：成为目标节点的同级前一个节点，"next"：成为目标节点同级后一个节点
			String moveType = getPara("moveType");
			//是否复制
			boolean isCopy = getParaToBoolean("isCopy",false);
			if(moveType.equalsIgnoreCase("inner")){
				//移动成为目标节点的子节点
				for(int i=0;i<treeNodes.size();i++){
					//逐条遍历移动子节点列表中的子节点
					//子节点
					JSONObject treeNode = treeNodes.getJSONObject(i);
					//从数据库中获取子节点对应的系统菜单数据，防止树形菜单中数据不完善造成的复制或者移动节点后有关数据丢失的问题
					BaseSystemMenu baseSystemMenu = BaseSystemMenu.DAO.findById(treeNode.getBigInteger("id"));
					//设置父级菜单ID为目标节点的ID
					baseSystemMenu.setParentId(targetNode.getLong("id"));
					//设置排序编号为“目标节点子节点数量 - 移动节点个数 + 当前节点位置 + 1”
					int childrenSize=0;
					if(targetNode.containsKey("children") && targetNode.getJSONArray("children")!=null){
						childrenSize=targetNode.getJSONArray("children").size();
					}
					int sortNumber = childrenSize-treeNodes.size()+i+2;
					baseSystemMenu.setSortNumber(sortNumber);
					//菜单编号
					String menuNumber = "000"+sortNumber;
					//菜单编号=上级菜单编号+本级菜单编号，每一级别菜单编号为3位数字的排序编号，排序编号不足3位时用0补齐，大于3位时只取后三位
					menuNumber =targetNode.getString("menuNumber")+ menuNumber.substring(menuNumber.length()-3);
					baseSystemMenu.setMenuNumber(menuNumber);
					if(isCopy){
						//如果是复制菜单
						//获取菜单包含的Action列表
						List<BaseAction> actionList = BaseAction.DAO.find("Select * from base_action where menuId="+baseSystemMenu.getId());
						//设置菜单ID为空
						baseSystemMenu.setId(null);
						//调用保存执行前附加逻辑，补全系统自动赋值的一些字段
						onModelSaveBefore(baseSystemMenu);
						//保存为新的系统菜单
						baseSystemMenu.save();
						//将原来的actionList重新保存
						for(BaseAction action:actionList){
							action.setId(null);
							action.setMenuId(baseSystemMenu.getId());
							_BaseActionController actionController = new _BaseActionController();
							actionController.setHttpServletRequest(getRequest());
							actionController.onModelSaveBefore(action);
							action.save();
						}
						//设置节点ID为新保存的菜单的ID
						treeNode.put("id", baseSystemMenu.getId());
					}else{
						//调用保存执行前附加逻辑，补全系统自动赋值的一些字段
						onModelSaveBefore(baseSystemMenu);
						//更新系统菜单
						baseSystemMenu.update();
					}
					//如果当前节点有子节点
					if(treeNode.getJSONArray("children")!=null && treeNode.getJSONArray("children").size()>0){
						//设置当前节点的菜单编号
						treeNode.put("menuNumber", menuNumber);
						//更新所有子节点的菜单编号
						updateMenuNumber(treeNode,isCopy);
					}
				}
			}else{
				//移动成为目标节点的同级前一个节点或者后一个节点
				//父级节点菜单编号，先设置父级节点菜单编号为目标节点菜单编号
				String parentMenuNumber = targetNode.getString("menuNumber");
				if(parentMenuNumber!=null && parentMenuNumber.length()>3){
					//如果目标节点菜单编号字符长度大于3，则将目标节点编号去掉后三位作为父级节点编号
					parentMenuNumber = parentMenuNumber.substring(0,parentMenuNumber.length()-3);
				} else {
					//如果目标节点菜单编号为空或者字符长度小于等于3，则设置父级节点菜单编号为空字符串
					parentMenuNumber="";
				}
				for(int i=0;i<peerNodes.size();i++){
					//逐条遍历移动子节点列表中的子节点
					//子节点
					JSONObject treeNode = peerNodes.getJSONObject(i);
					//从数据库中获取子节点对应的系统菜单数据，防止树形菜单中数据不完善造成的复制或者移动节点后有关数据丢失的问题
					BaseSystemMenu baseSystemMenu = BaseSystemMenu.DAO.findById(treeNode.getBigInteger("id"));
					//设置父级菜单ID为目标节点的父级节点ID
					baseSystemMenu.setParentId(targetNode.getLong("parentId"));
					//设置排序编号为“当前节点位置 + 1”
					int sortNumber = i+1;
					baseSystemMenu.setSortNumber(sortNumber);
					//菜单编号
					String menuNumber = "000"+sortNumber;
					//菜单编号=上级菜单编号+本级菜单编号，每一级别菜单编号为3位数字的排序编号，排序编号不足3位时用0补齐，大于3位时只取后三位
					menuNumber =parentMenuNumber+ menuNumber.substring(menuNumber.length()-3);
					baseSystemMenu.setMenuNumber(menuNumber);
					if(isCopy){
						//如果是复制菜单
						//获取菜单包含的Action列表
						List<BaseAction> actionList = BaseAction.DAO.find("Select * from base_action where menuId="+baseSystemMenu.getId());
						//设置菜单ID为空
						baseSystemMenu.setId(null);
						//调用保存执行前附加逻辑，补全系统自动赋值的一些字段
						onModelSaveBefore(baseSystemMenu);
						//保存为新的系统菜单
						baseSystemMenu.save();
						//将原来的actionList重新保存
						for(BaseAction action:actionList){
							action.setId(null);
							action.setMenuId(baseSystemMenu.getId());
							_BaseActionController actionController = new _BaseActionController();
							actionController.setHttpServletRequest(getRequest());
							actionController.onModelSaveBefore(action);
							action.save();
						}
						//设置节点ID为新保存的菜单的ID
						treeNode.put("id", baseSystemMenu.getId());
					}else{
						//调用保存执行前附加逻辑，补全系统自动赋值的一些字段
						onModelSaveBefore(baseSystemMenu);
						//更新系统菜单
						baseSystemMenu.update();
					}
					//如果当前节点有子节点
					if(treeNode.getJSONArray("children")!=null && treeNode.getJSONArray("children").size()>0){
						//设置当前节点的菜单编号
						treeNode.put("menuNumber", menuNumber);
						//更新所有子节点的菜单编号
						updateMenuNumber(treeNode,isCopy);
					}
				}
			}
			json.put(Consts.ERROR_CODE,"1");
		}catch(Exception error){
			json.put(Consts.ERROR_CODE,"-1");
			json.put(Consts.MESSAGE,error.getMessage());
		}
		renderJson(json);
	}
	/**
	 * 更新父级节点下面所有层级节点的菜单编号
	 * @param parentNode 父级节点
	 * @param isCopy 是否复制为新节点
	 */
	private void updateMenuNumber(JSONObject parentNode,boolean isCopy){
		//移动成为目标节点的子节点
		for(int i=0;i<parentNode.getJSONArray("children").size();i++){
			JSONObject treeNode = parentNode.getJSONArray("children").getJSONObject(i);
			BaseSystemMenu baseSystemMenu = BaseSystemMenu.DAO.findById(treeNode.getBigInteger("id"));
			baseSystemMenu.setParentId(parentNode.getLong("id"));
			int sortNumber = i+1;
			baseSystemMenu.setSortNumber(sortNumber);
			String menuNumber = "000"+sortNumber;
			menuNumber = parentNode.getString("menuNumber")+menuNumber.substring(menuNumber.length()-3);
			baseSystemMenu.setMenuNumber(menuNumber);
			if(isCopy){
				//获取菜单包含的Action列表
				List<BaseAction> actionList = BaseAction.DAO.find("Select * from base_action where menuId="+baseSystemMenu.getId());
				baseSystemMenu.setId(null);
				onModelSaveBefore(baseSystemMenu);
				baseSystemMenu.save();
				//将原来的actionList重新保存
				for(BaseAction action:actionList){
					action.setId(null);
					action.setMenuId(baseSystemMenu.getId());
					_BaseActionController actionController = new _BaseActionController();
					actionController.onModelSaveBefore(action);
					action.save();
				}
				treeNode.put("id", baseSystemMenu.getId());
			}else{
				onModelSaveBefore(baseSystemMenu);
				baseSystemMenu.update();
			}
			if(treeNode.getJSONArray("children")!=null && treeNode.getJSONArray("children").size()>0){
				treeNode.put("menuNumber", menuNumber);
				updateMenuNumber(treeNode,isCopy);
			}
		}
	}
	/**
	 * 返回JSON格式的条件查询或者分页显示数据
	 */
	public void searchData(){
		StringBuffer orderBys = new StringBuffer("");
		for(int i=0;i<5;i++){
			String column = getPara("order["+i+"][column]");
			column = getPara("columns["+column+"][data]");
			if(StrKit.notBlank(column)){
				if(orderBys.length()>0)orderBys.append(",");
				orderBys.append(" "+column);
				if("desc".equalsIgnoreCase(getPara("order["+i+"][dir]"))){
					orderBys.append(" desc");
				}
			} else {
				break;
			}
		}
		if(orderBys.length()>0)orderBys.insert(0," order by");
		StringBuffer wheres = new StringBuffer();
		if(StringUtils.areNotBlank(getPara("search[value]"))){
			String searchValue = getPara("search[value]");
			HttpServletRequest request = this.getRequest();
			String className = "BaseSystemMenu";
			HashMap<String,ColumnMeta> colMetaMap = new HashMap<String,ColumnMeta>();
			@SuppressWarnings("unchecked")
			HashMap<String,TableMeta> tableMetaMap = (HashMap<String,TableMeta>)request.getServletContext().getAttribute(Consts.TABLE_META_MAP);
			if(tableMetaMap!=null && tableMetaMap.containsKey(className)){
				TableMeta tableMeta = tableMetaMap.get(className);
				List<ColumnMeta> colMetas = tableMeta.columnMetas;
				for(ColumnMeta col:colMetas){
					colMetaMap.put(col.attrName,col);
				}
			}
			for(int i=0;i<50;i++){
				String column =getPara("columns["+i+"][data]");
				boolean searchable = getParaToBoolean("columns["+i+"][searchable]",false);
				if(StrKit.notBlank(column) && searchable){
					ColumnMeta col = colMetaMap.get(column);
					if(col!=null && col.javaType.equalsIgnoreCase("java.lang.String")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append(column + " like '%"+searchValue+"%' ");
					}
				} else if(StrKit.isBlank(column)){
					break;
				}
			}
			if(wheres.length()>0){
				wheres.insert(0,"(").append(")");
			}
		}
		if(wheres.length()>0){
			wheres.insert(0," WHERE ");
		}
		int draw = getParaToInt("draw",1);
		int start = getParaToInt("start",0);
		if(start<0)start=0;
		int length = getParaToInt("length",10);
		if(length<1 && length!=-1)length=1;
		JSONObject json = new JSONObject();
		if(length==-1){
			List<BaseSystemMenu> list = BaseSystemMenu.DAO.find("select * from base_system_menu" + wheres.toString() + orderBys.toString() + " limit 0,10000");
			json.put("recordsTotal",list.size());
			json.put("recordsFiltered",list.size());
			json.put("data",list);
		} else {
			Page<BaseSystemMenu> page = BaseSystemMenu.DAO.paginate((start/length)+1,length, "select * ","from base_system_menu"  + wheres.toString() + orderBys.toString());
			json.put("recordsTotal",page.getTotalRow());
			json.put("recordsFiltered",page.getTotalRow());
			json.put("data",page.getList());
		}
		json.put("draw",draw);
		renderJson(json);
	}
	/**
	 * 新增
	 */
	public void add() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseSystemMenu/add.jsp");
	}
	/**
	 * 修改
	 */
	public void edit() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			setAttr("baseSystemMenu",BaseSystemMenu.DAO.findById(id));
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseSystemMenu/edit.jsp");
	}
	/**
	 * 删除
	 */
	public void delete() {
		BigInteger[] idArray = getParaValuesToBigInteger("id");
		if(idArray==null || idArray.length==0){
			idArray = getParaValuesToBigInteger("id");
		}
		if (idArray != null && idArray.length>0) {
			for(BigInteger id:idArray){
				BaseSystemMenu.DAO.deleteById(id);
				Db.update("delete from base_action where menuId="+id);
			}
			renderAjaxResultForSuccess("删除成功");
		} else {
			renderAjaxResultForError();
		}
	}
	/**
	 * 获取详细JSON
	 */
	public void detailJSON() {
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseSystemMenu baseSystemMenu = BaseSystemMenu.DAO.findById(id);
			if(baseSystemMenu!=null){
				// 系统
				BaseSystem system = BaseSystem.DAO.findById(baseSystemMenu.getSystemId());
				baseSystemMenu.setSystem(system);
				// 父级菜单
				BaseSystemMenu  parent = BaseSystemMenu.DAO.findById(baseSystemMenu.getParentId());
				baseSystemMenu.setParent(parent);
				// 创建用户
				BaseUser createUser = BaseUser.DAO.findById(baseSystemMenu.getCreateUserId());
				baseSystemMenu.setCreateUser(createUser);
				// 更新用户
				BaseUser updateUser = BaseUser.DAO.findById(baseSystemMenu.getUpdateUserId());
				baseSystemMenu.setUpdateUser(updateUser);
				renderJson(baseSystemMenu);
			}else {
				JSONObject json = new JSONObject();
				json.put(Consts.ERROR_CODE,"-1");
				json.put(Consts.MESSAGE,"对象不存在");
				renderJson(json);
			}
		} else {
			JSONObject json = new JSONObject();
			json.put(Consts.ERROR_CODE,"-100");
			json.put(Consts.MESSAGE,"非法调用");
			renderJson(json);
		}
	}
	/**
	 * 查看
	 */
	public void view() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseSystemMenu baseSystemMenu = BaseSystemMenu.DAO.findById(id);
			if(baseSystemMenu!=null){
				// 系统
				BaseSystem system_1 = BaseSystem.DAO.findById(baseSystemMenu.getSystemId());
				baseSystemMenu.setSystem(system_1);
				// 父级菜单
				BaseSystemMenu  parent = BaseSystemMenu.DAO.findById(baseSystemMenu.getParentId());
				baseSystemMenu.setParent(parent);
				// 创建用户
				BaseUser createUser = BaseUser.DAO.findById(baseSystemMenu.getCreateUserId());
				baseSystemMenu.setCreateUser(createUser);
				// 更新用户
				BaseUser updateUser = BaseUser.DAO.findById(baseSystemMenu.getUpdateUserId());
				baseSystemMenu.setUpdateUser(updateUser);
			}
			setAttr("baseSystemMenu",BaseSystemMenu.DAO.findById(id));
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseSystemMenu/view.jsp");
	}

	/**
	 * 保存执行前附加逻辑
	 * @return 返回是否继续执行保存
	 */
	@Override
	public boolean onModelSaveBefore(BaseSystemMenu baseSystemMenu) {
		//当前登录用户
		BaseUser loginUser = getAttr("loginUser");
		if(baseSystemMenu.getId()==null || baseSystemMenu.getId().longValue()<1){
			//如果ID为空，则表示要新增数据
			Random rand = new Random(System.currentTimeMillis()%3333);
			//自动生成ID
			baseSystemMenu.setId(System.currentTimeMillis()*1000+rand.nextInt(999));
			//设置创建时间为当前时间
			baseSystemMenu.setCreateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置创建用户ID为当前登录用户的ID
				baseSystemMenu.setCreateUserId(loginUser.getId());
				//设置创建用户为当前登录用户
				baseSystemMenu.setCreateUser(loginUser);
			}
			//设置更新时间为当前时间
			baseSystemMenu.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置更新用户ID为当前登录用户的ID
				baseSystemMenu.setUpdateUserId(loginUser.getId());
				//设置更新用户为当前登录用户
				baseSystemMenu.setUpdateUser(loginUser);
			}
		} else {
			//设置更新时间为当前时间
			baseSystemMenu.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置更新用户ID为当前登录用户的ID
				baseSystemMenu.setUpdateUserId(loginUser.getId());
				//设置更新用户为当前登录用户
				baseSystemMenu.setUpdateUser(loginUser);
			}
		}
		return true;
	}
	/**
	 * 保存执行后附加逻辑
	 */
	@Override
	public boolean onModelSaveAfter(BaseSystemMenu baseSystemMenu) {
		baseSystemMenu.setId(null);
		return true;
	}
}
