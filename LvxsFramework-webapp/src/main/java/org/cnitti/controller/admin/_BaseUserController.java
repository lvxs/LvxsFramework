package org.cnitti.controller.admin;

import java.math.BigInteger;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.cnitti.Consts;
import org.cnitti.core.JBaseCRUDController;
import org.cnitti.model.*;
import org.cnitti.router.RouterMapping;
import org.cnitti.utils.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;
/**
 * 用户信息
 * 公用的系统用户基础信息
 */
@RouterMapping(url = "/admin/baseUser", viewPath = "/WEB-INF/t")
public class _BaseUserController extends JBaseCRUDController<BaseUser> { 
	/**
	  * 列表页面，模块默认主界面
	  */
	public void index() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseUser/list.jsp");
	}
	/**
	 * 返回指定菜单下的所有Action的JSON数据
	 */
	public void getDataJson(){
		JSONObject json = new JSONObject();
		BigInteger systemId = getParaToBigInteger("systemId");
		if (systemId != null) {
			List<BaseUser> list = BaseUser.DAO.find("select a.* from base_user as a left outer join base_department as b on a.departmentId=b.id where b.systemId=" +systemId+ " order by createTime");
			json.put("recordsTotal",list.size());
			json.put("recordsFiltered",list.size());
			json.put("data",list);
		}else {
			json.put("recordsTotal",0);
			json.put("recordsFiltered",0);
		}
		renderJson(json);
	}
	/**
	 * 返回JSON格式的条件查询或者分页显示数据
	 */
	public void searchData(){
		StringBuffer orderBys = new StringBuffer("");
		for(int i=0;i<5;i++){
			String column = getPara("order["+i+"][column]");
			column = getPara("columns["+column+"][data]");
			if(StrKit.notBlank(column)){
				if(orderBys.length()>0)orderBys.append(",");
				orderBys.append(" "+column);
				if("desc".equalsIgnoreCase(getPara("order["+i+"][dir]"))){
					orderBys.append(" desc");
				}
			} else {
				break;
			}
		}
		if(orderBys.length()>0)orderBys.insert(0," order by");
		StringBuffer wheres = new StringBuffer();
		if(StringUtils.areNotBlank(getPara("search[value]"))){
			String searchValue = getPara("search[value]");
			HttpServletRequest request = this.getRequest();
			String className = "BaseUser";
			HashMap<String,ColumnMeta> colMetaMap = new HashMap<String,ColumnMeta>();
			@SuppressWarnings("unchecked")
			HashMap<String,TableMeta> tableMetaMap = (HashMap<String,TableMeta>)request.getServletContext().getAttribute(Consts.TABLE_META_MAP);
			if(tableMetaMap!=null && tableMetaMap.containsKey(className)){
				TableMeta tableMeta = tableMetaMap.get(className);
				List<ColumnMeta> colMetas = tableMeta.columnMetas;
				for(ColumnMeta col:colMetas){
					colMetaMap.put(col.attrName,col);
				}
			}
			for(int i=0;i<50;i++){
				String column =getPara("columns["+i+"][data]");
				boolean searchable = getParaToBoolean("columns["+i+"][searchable]",false);
				if(StrKit.notBlank(column) && searchable){
					ColumnMeta col = colMetaMap.get(column);
					if(col!=null && col.javaType.equalsIgnoreCase("java.lang.String")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("a."+column + " like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("role.name")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("role.name like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("department.name")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("department.name like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("createUser.realName")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("createUser.realName like '%"+searchValue+"%' ");
					} else if(column.equalsIgnoreCase("updateUser.realName")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append("updateUser.realName like '%"+searchValue+"%' ");
					}				
				} else if(StrKit.isBlank(column)){
					break;
				}
			}
			if(wheres.length()>0){
				wheres.insert(0,"(").append(")");
			}
		}
		if(wheres.length()>0){
			wheres.insert(0," WHERE ");
		}
		int draw = getParaToInt("draw",1);
		int start = getParaToInt("start",0);
		if(start<0)start=0;
		int length = getParaToInt("length",10);
		if(length<1 && length!=-1)length=1;
		JSONObject json = new JSONObject();
		StringBuffer select = new StringBuffer("select a.*");
		StringBuffer sqlExceptSelect = new StringBuffer(" from base_user as a ");
		//select.append(",role.name as roleName");
		sqlExceptSelect.append(" left outer join base_role as role on a.roleId=role.id ");
		//select.append(",department.name as departmentName");
		sqlExceptSelect.append(" left outer join base_department as department on a.departmentId=department.id ");
		//select.append(",createUser.realName as createUserName");
		sqlExceptSelect.append(" left outer join base_user as createUser on a.createUserId=createUser.id ");
		//select.append(",updateUser.realName as updateUserName");
		sqlExceptSelect.append(" left outer join base_user as updateUser on a.updateUserId=updateUser.id ");
		if(length==-1){
			List<BaseUser> list = BaseUser.DAO.find(select.toString() + sqlExceptSelect.toString() + wheres.toString() + orderBys.toString() + " limit 0,10000");
			fillRelatedList(list);
			json.put("recordsTotal",list.size());
			json.put("recordsFiltered",list.size());
			json.put("data",list);
		} else {
			Page<BaseUser> page = BaseUser.DAO.paginate((start/length)+1,length, select.toString(),sqlExceptSelect.toString() + wheres.toString() + orderBys.toString());
			fillRelatedList(page.getList());
			json.put("recordsTotal",page.getTotalRow());
			json.put("recordsFiltered",page.getTotalRow());
			json.put("data",page.getList());
		}
		json.put("draw",draw);
		renderJson(json);
	}
	/**
	 * 新增
	 */
	public void add() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseUser/add.jsp");
	}
	/**
	 * 修改
	 */
	public void edit() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseUser/edit.jsp");
	}
	/**
	 * 删除
	 */
	public void delete() {
		Long[] idArray = getParaValuesToLong("id");
		if(idArray==null || idArray.length==0){
			idArray = getParaValuesToLong("id");
		}
		if (idArray != null && idArray.length>0) {
			for(Long id:idArray){
				BaseUser.DAO.deleteById(id);
			}
			renderAjaxResultForSuccess("删除成功");
		} else {
			renderAjaxResultForError();
		}
	}
	
	/**
	 * 获取详细JSON
	 */
	public void detailJSON() {
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseUser baseUser = BaseUser.DAO.findById(id);
			if(baseUser!=null){
				baseUser.fillRelated();
				renderJson(baseUser);
			}else {
				JSONObject json = new JSONObject();
				json.put(Consts.ERROR_CODE,"-1");
				json.put(Consts.MESSAGE,"对象不存在");
				renderJson(json);
			}
		} else {
			JSONObject json = new JSONObject();
			json.put(Consts.ERROR_CODE,"-100");
			json.put(Consts.MESSAGE,"非法调用");
			renderJson(json);
		}
	}
	/**
	 * 查看
	 */
	public void view() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseUser baseUser = BaseUser.DAO.findById(id);
			if(baseUser!=null){
				baseUser.fillRelated();
			}
			setAttr("baseUser",baseUser);
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseUser/view.jsp");
	}
	/**
	 * 自动填充用户列表中用户的关联信息
	 * @param userList	用户列表
	 */
	private void fillRelatedList(List<BaseUser> userList){
		for(BaseUser user:userList){
			user.fillRelated();
		}
	}
	
	/**
	 * 保存执行前附加逻辑
	 * @return 返回是否继续执行保存
	 */
	@Override
	public boolean onModelSaveBefore(BaseUser baseUser) {
		//当前登录用户
		BaseUser loginUser = getAttr("loginUser");
		String oldPassword = null;
		if(baseUser.getId()==null || baseUser.getId().longValue()<1){
			//如果ID为空，则表示要新增数据
			Random rand = new Random(System.currentTimeMillis()%3333);
			//自动生成ID
			baseUser.setId(System.currentTimeMillis()*1000+rand.nextInt(999));
			//设置创建时间为当前时间
			baseUser.setCreateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置创建用户ID为当前登录用户的ID
				baseUser.setCreateUserId(loginUser.getId());
				//设置创建用户为当前登录用户
				baseUser.setCreateUser(loginUser);
			}
			//设置更新时间为当前时间
			baseUser.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置更新用户ID为当前登录用户的ID
				baseUser.setUpdateUserId(loginUser.getId());
				//设置更新用户为当前登录用户
				baseUser.setUpdateUser(loginUser);
			}
			
		} else {
			BaseUser oldUser = BaseUser.DAO.findById(baseUser.getId());
			//设置更新时间为当前时间
			baseUser.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				baseUser.setUpdateUserId(loginUser.getId());
				baseUser.setUpdateUser(loginUser);
			}
			if(oldUser!=null){
				oldPassword=oldUser.getPassword();
			}
		}
		if(baseUser.getPassword()==null || baseUser.getPassword().isEmpty()){
			//如果没有输入用户名登录密码，则设置密码原始密码
			baseUser.setPassword(oldPassword);
		} else {
			//如果登录密码不为空，则对密码进行MD5加密
			baseUser.setPassword(HashKit.md5(baseUser.getPassword()));
		}
		return true;
	}
	/**
	 * 保存执行后附加逻辑
	 */
	@Override
	public boolean onModelSaveAfter(BaseUser baseUser) {
		Long[] roleIdArray = getParaValuesToLong("baseUser.roleId");
		Db.update("delete from base_user_role where userId="+baseUser.getId());
		for(Long roleId:roleIdArray){
			if(roleId.longValue()!=baseUser.getRoleId().longValue()){
				BaseUserRole baseUserRole = new BaseUserRole();
				baseUserRole.setUserId(baseUser.getId());
				baseUserRole.setRoleId(roleId);
				//如果ID为空，则表示要新增数据
				Random rand = new Random(System.currentTimeMillis()%3333);
				//自动生成ID
				baseUserRole.setId(System.currentTimeMillis()*1000+rand.nextInt(999));
				baseUserRole.save();
			}
		}
		Long[] departmentIdArray = getParaValuesToLong("baseUser.departmentId");
		Db.update("delete from base_user_department where userId="+baseUser.getId());
		for(Long departmentId:departmentIdArray){
			if(departmentId.longValue()!=baseUser.getDepartmentId().longValue()){
				BaseUserDepartment baseUserDepartment = new BaseUserDepartment();
				baseUserDepartment.setUserId(baseUser.getId());
				baseUserDepartment.setDepartmentId(departmentId);
				//如果ID为空，则表示要新增数据
				Random rand = new Random(System.currentTimeMillis()%3333);
				//自动生成ID
				baseUserDepartment.setId(System.currentTimeMillis()*1000+rand.nextInt(999));
				baseUserDepartment.save();
			}
		}
		return true;
	}
}
