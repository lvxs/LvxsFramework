package org.cnitti.controller.admin;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.cnitti.Consts;
import org.cnitti.core.JBaseCRUDController;
import org.cnitti.model.BaseDepartment;
import org.cnitti.model.BaseSystem;
import org.cnitti.model.BaseUser;
import org.cnitti.router.RouterMapping;
import org.cnitti.utils.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;
/**
 * 部门信息
 * 部门或者其他分支机构基础信息
 */
@RouterMapping(url = "/admin/baseDepartment", viewPath = "/WEB-INF/t")
public class _BaseDepartmentController extends JBaseCRUDController<BaseDepartment> { 
	/**
	  * 列表页面，模块默认主界面
	  */
	public void index() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger systemId = getParaToBigInteger("systemId");
		if (systemId != null) {
			setAttr("baseSystem",BaseSystem.DAO.findById(systemId));
		} else {
			setAttr("baseSystem",system);
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseDepartment/list.jsp");
	}
	/**
	 * 返回树形结构部门JSON数据
	 */
	public void getTreeJson(){
		JSONObject json = new JSONObject();
		BigInteger systemId = getParaToBigInteger("systemId");
		if(systemId==null || systemId.longValue()<1){
			BaseSystem system = getAttr(Consts.SYSTEM);
			systemId = BigInteger.valueOf(system.getId());
		}
		if (systemId != null) {
			List<BaseDepartment> list = BaseDepartment.DAO.find("select * from base_department where systemId=" +systemId+ " order by treeNumber");
			json.put("recordsTotal",list.size());
			json.put("recordsFiltered",list.size());
			json.put("data",list);
		}
		renderJson(json);
	}
	/**
	 * 树形部门移动
	 */
	public void drop() {
		JSONObject json = new JSONObject();
		try{
			//部门所属系统ID
			//BigInteger systemId = getParaToBigInteger("systemId");
			//要移动的部门节点JSON字符串
			String treeNodesJSON = getPara("treeNodesJSON");
			treeNodesJSON = StringEscapeUtils.unescapeHtml4(treeNodesJSON);
			//要移动的部门节点JSON数组
			JSONArray treeNodes = JSONArray.parseArray(treeNodesJSON);
			//目标节点JSON字符串
			String targetNodeJSON = getPara("targetNodeJSON");
			targetNodeJSON = StringEscapeUtils.unescapeHtml4(targetNodeJSON);
			//目标节点JSON对象
			JSONObject targetNode = JSONObject.parseObject(targetNodeJSON);
			//目标同级节点JSON字符串
			String peerNodesJSON = getPara("peerNodesJSON");
			peerNodesJSON = StringEscapeUtils.unescapeHtml4(peerNodesJSON);
			//目标同级节点JSON数组
			JSONArray peerNodes = JSONArray.parseArray(peerNodesJSON);
			//移动类型，"inner"：移动成为目标节点的子节点，"prev"：成为目标节点的同级前一个节点，"next"：成为目标节点同级后一个节点
			String moveType = getPara("moveType");
			//是否复制
			boolean isCopy = getParaToBoolean("isCopy",false);
			if(moveType.equalsIgnoreCase("inner")){
				//移动成为目标节点的子节点
				for(int i=0;i<treeNodes.size();i++){
					//逐条遍历移动子节点列表中的子节点
					//子节点
					JSONObject treeNode = treeNodes.getJSONObject(i);
					//从数据库中获取子节点对应的部门数据，防止树形部门中数据不完善造成的复制或者移动节点后有关数据丢失的问题
					BaseDepartment baseDepartment = BaseDepartment.DAO.findById(treeNode.getBigInteger("id"));
					//设置父级部门ID为目标节点的ID
					baseDepartment.setParentId(targetNode.getLong("id"));
					//设置排序编号为“目标节点子节点数量 - 移动节点个数 + 当前节点位置 + 1”
					int childrenSize=0;
					if(targetNode.containsKey("children") && targetNode.getJSONArray("children")!=null){
						childrenSize=targetNode.getJSONArray("children").size();
					}
					int sortNumber = childrenSize-treeNodes.size()+i+2;
					baseDepartment.setSortNumber(sortNumber);
					//部门树形编号
					String treeNumber = "000"+sortNumber;
					//部门树形编号=上级部门树形编号+本级部门树形编号，每一级别部门树形编号为3位数字的排序编号，排序编号不足3位时用0补齐，大于3位时只取后三位
					treeNumber =targetNode.getString("treeNumber")+ treeNumber.substring(treeNumber.length()-3);
					baseDepartment.setTreeNumber(treeNumber);
					if(isCopy){
						//如果是复制部门
						//设置部门ID为空
						baseDepartment.setId(null);
						//调用保存执行前附加逻辑，补全系统自动赋值的一些字段
						onModelSaveBefore(baseDepartment);
						//保存为新的部门
						baseDepartment.save();
						//设置节点ID为新保存的部门的ID
						treeNode.put("id", baseDepartment.getId());
					}else{
						//调用保存执行前附加逻辑，补全系统自动赋值的一些字段
						onModelSaveBefore(baseDepartment);
						//更新部门
						baseDepartment.update();
					}
					//如果当前节点有子节点
					if(treeNode.getJSONArray("children")!=null && treeNode.getJSONArray("children").size()>0){
						//设置当前节点的部门树形编号
						treeNode.put("treeNumber", treeNumber);
						//更新所有子节点的部门树形编号
						updateTreeNumber(treeNode,isCopy);
					}
				}
			}else{
				//移动成为目标节点的同级前一个节点或者后一个节点
				//父级节点部门树形编号，先设置父级节点部门树形编号为目标节点部门树形编号
				String parentTreeNumber = targetNode.getString("treeNumber");
				if(parentTreeNumber!=null && parentTreeNumber.length()>3){
					//如果目标节点部门树形编号字符长度大于3，则将目标节点编号去掉后三位作为父级节点编号
					parentTreeNumber = parentTreeNumber.substring(0,parentTreeNumber.length()-3);
				} else {
					//如果目标节点部门树形编号为空或者字符长度小于等于3，则设置父级节点部门树形编号为空字符串
					parentTreeNumber="";
				}
				for(int i=0;i<peerNodes.size();i++){
					//逐条遍历移动子节点列表中的子节点
					//子节点
					JSONObject treeNode = peerNodes.getJSONObject(i);
					//从数据库中获取子节点对应的部门数据，防止树形部门中数据不完善造成的复制或者移动节点后有关数据丢失的问题
					BaseDepartment baseDepartment = BaseDepartment.DAO.findById(treeNode.getBigInteger("id"));
					//设置父级部门ID为目标节点的父级节点ID
					baseDepartment.setParentId(targetNode.getLong("parentId"));
					//设置排序编号为“当前节点位置 + 1”
					int sortNumber = i+1;
					baseDepartment.setSortNumber(sortNumber);
					//部门树形编号
					String treeNumber = "000"+sortNumber;
					//部门树形编号=上级部门树形编号+本级部门树形编号，每一级别部门树形编号为3位数字的排序编号，排序编号不足3位时用0补齐，大于3位时只取后三位
					treeNumber =parentTreeNumber+ treeNumber.substring(treeNumber.length()-3);
					baseDepartment.setTreeNumber(treeNumber);
					if(isCopy){
						//如果是复制部门
						//设置部门ID为空
						baseDepartment.setId(null);
						//调用保存执行前附加逻辑，补全系统自动赋值的一些字段
						onModelSaveBefore(baseDepartment);
						//保存为新的部门
						baseDepartment.save();
						//设置节点ID为新保存的部门的ID
						treeNode.put("id", baseDepartment.getId());
					}else{
						//调用保存执行前附加逻辑，补全系统自动赋值的一些字段
						onModelSaveBefore(baseDepartment);
						//更新部门
						baseDepartment.update();
					}
					//如果当前节点有子节点
					if(treeNode.getJSONArray("children")!=null && treeNode.getJSONArray("children").size()>0){
						//设置当前节点的部门树形编号
						treeNode.put("treeNumber", treeNumber);
						//更新所有子节点的部门树形编号
						updateTreeNumber(treeNode,isCopy);
					}
				}
			}
			json.put(Consts.ERROR_CODE,"1");
		}catch(Exception error){
			json.put(Consts.ERROR_CODE,"-1");
			json.put(Consts.MESSAGE,error.getMessage());
		}
		renderJson(json);
	}
	/**
	 * 更新父级节点下面所有层级节点的部门树形编号
	 * @param parentNode 父级节点
	 * @param isCopy 是否复制为新节点
	 */
	private void updateTreeNumber(JSONObject parentNode,boolean isCopy){
		//移动成为目标节点的子节点
		for(int i=0;i<parentNode.getJSONArray("children").size();i++){
			JSONObject treeNode = parentNode.getJSONArray("children").getJSONObject(i);
			BaseDepartment baseDepartment = BaseDepartment.DAO.findById(treeNode.getBigInteger("id"));
			baseDepartment.setParentId(parentNode.getLong("id"));
			int sortNumber = i+1;
			baseDepartment.setSortNumber(sortNumber);
			String treeNumber = "000"+sortNumber;
			treeNumber = parentNode.getString("treeNumber")+treeNumber.substring(treeNumber.length()-3);
			baseDepartment.setTreeNumber(treeNumber);
			if(isCopy){
				baseDepartment.setId(null);
				onModelSaveBefore(baseDepartment);
				baseDepartment.save();
				treeNode.put("id", baseDepartment.getId());
			}else{
				onModelSaveBefore(baseDepartment);
				baseDepartment.update();
			}
			if(treeNode.getJSONArray("children")!=null && treeNode.getJSONArray("children").size()>0){
				treeNode.put("treeNumber", treeNumber);
				updateTreeNumber(treeNode,isCopy);
			}
		}
	}
	/**
	 * 返回JSON格式的条件查询或者分页显示数据
	 */
	public void searchData(){
		StringBuffer orderBys = new StringBuffer("");
		for(int i=0;i<5;i++){
			String column = getPara("order["+i+"][column]");
			column = getPara("columns["+column+"][data]");
			if(StrKit.notBlank(column)){
				if(orderBys.length()>0)orderBys.append(",");
				orderBys.append(" "+column);
				if("desc".equalsIgnoreCase(getPara("order["+i+"][dir]"))){
					orderBys.append(" desc");
				}
			} else {
				break;
			}
		}
		if(orderBys.length()>0)orderBys.insert(0," order by");
		StringBuffer wheres = new StringBuffer();
		if(StringUtils.areNotBlank(getPara("search[value]"))){
			String searchValue = getPara("search[value]");
			HttpServletRequest request = this.getRequest();
			String className = "BaseDepartment";
			HashMap<String,ColumnMeta> colMetaMap = new HashMap<String,ColumnMeta>();
			@SuppressWarnings("unchecked")
			HashMap<String,TableMeta> tableMetaMap = (HashMap<String,TableMeta>)request.getServletContext().getAttribute(Consts.TABLE_META_MAP);
			if(tableMetaMap!=null && tableMetaMap.containsKey(className)){
				TableMeta tableMeta = tableMetaMap.get(className);
				List<ColumnMeta> colMetas = tableMeta.columnMetas;
				for(ColumnMeta col:colMetas){
					colMetaMap.put(col.attrName,col);
				}
			}
			for(int i=0;i<50;i++){
				String column =getPara("columns["+i+"][data]");
				boolean searchable = getParaToBoolean("columns["+i+"][searchable]",false);
				if(StrKit.notBlank(column) && searchable){
					ColumnMeta col = colMetaMap.get(column);
					if(col!=null && col.javaType.equalsIgnoreCase("java.lang.String")){
						if(wheres.length()>0)wheres.append(" or ");
						wheres.append(column + " like '%"+searchValue+"%' ");
					}
				} else if(StrKit.isBlank(column)){
					break;
				}
			}
			if(wheres.length()>0){
				wheres.insert(0,"(").append(")");
			}
		}
		if(wheres.length()>0){
			wheres.insert(0," WHERE ");
		}
		int draw = getParaToInt("draw",1);
		int start = getParaToInt("start",0);
		if(start<0)start=0;
		int length = getParaToInt("length",10);
		if(length<1 && length!=-1)length=1;
		JSONObject json = new JSONObject();
		if(length==-1){
			List<BaseDepartment> list = BaseDepartment.DAO.find("select * from base_department" + wheres.toString() + orderBys.toString() + " limit 0,10000");
			json.put("recordsTotal",list.size());
			json.put("recordsFiltered",list.size());
			json.put("data",list);
		} else {
			Page<BaseDepartment> page = BaseDepartment.DAO.paginate((start/length)+1,length, "select * ","from base_department"  + wheres.toString() + orderBys.toString());
			json.put("recordsTotal",page.getTotalRow());
			json.put("recordsFiltered",page.getTotalRow());
			json.put("data",page.getList());
		}
		json.put("draw",draw);
		renderJson(json);
	}
	/**
	 * 新增
	 */
	public void add() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseDepartment/add.jsp");
	}
	/**
	 * 修改
	 */
	public void edit() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			setAttr("baseDepartment",BaseDepartment.DAO.findById(id));
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseDepartment/edit.jsp");
	}
	/**
	 * 删除
	 */
	public void delete() {
		BigInteger[] idArray = getParaValuesToBigInteger("id");
		if(idArray==null || idArray.length==0){
			idArray = getParaValuesToBigInteger("id");
		}
		if (idArray != null && idArray.length>0) {
			for(BigInteger id:idArray){
				BaseDepartment.DAO.deleteById(id);
			}
			renderAjaxResultForSuccess("删除成功");
		} else {
			renderAjaxResultForError();
		}
	}
	/**
	 * 获取详细JSON
	 */
	public void detailJSON() {
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseDepartment baseDepartment = BaseDepartment.DAO.findById(id);
			if(baseDepartment!=null){
				// 系统
				BaseSystem system = BaseSystem.DAO.findById(baseDepartment.getSystemId());
				baseDepartment.setSystem(system);
				// 父级部门
				BaseDepartment parent = BaseDepartment.DAO.findById(baseDepartment.getParentId());
				baseDepartment.setParent(parent);
				// 创建用户
				BaseUser createUser = BaseUser.DAO.findById(baseDepartment.getCreateUserId());
				baseDepartment.setCreateUser(createUser);
				// 更新用户
				BaseUser updateUser = BaseUser.DAO.findById(baseDepartment.getUpdateUserId());
				baseDepartment.setUpdateUser(updateUser);
				renderJson(baseDepartment);
			}else {
				JSONObject json = new JSONObject();
				json.put(Consts.ERROR_CODE,"-1");
				json.put(Consts.MESSAGE,"对象不存在");
				renderJson(json);
			}
		} else {
			JSONObject json = new JSONObject();
			json.put(Consts.ERROR_CODE,"-100");
			json.put(Consts.MESSAGE,"非法调用");
			renderJson(json);
		}
	}
	/**
	 * 查看
	 */
	public void view() {
		BaseSystem system = getAttr(Consts.SYSTEM);
		String adminThemes = getSessionAttr(Consts.ADMIN_THEMES);
		BigInteger id = getParaToBigInteger("id");
		if (id != null) {
			BaseDepartment baseDepartment = BaseDepartment.DAO.findById(id);
			if(baseDepartment!=null){
				// 系统
				BaseSystem system_1 = BaseSystem.DAO.findById(baseDepartment.getSystemId());
				baseDepartment.setSystem(system_1);
				// 父级部门
				BaseDepartment parent = BaseDepartment.DAO.findById(baseDepartment.getParentId());
				baseDepartment.setParent(parent);
				// 创建用户
				BaseUser createUser = BaseUser.DAO.findById(baseDepartment.getCreateUserId());
				baseDepartment.setCreateUser(createUser);
				// 更新用户
				BaseUser updateUser = BaseUser.DAO.findById(baseDepartment.getUpdateUserId());
				baseDepartment.setUpdateUser(updateUser);
			}
			setAttr("baseDepartment",baseDepartment);
		}
		render(""+system.getFilePath()+"/"+adminThemes+"/BaseDepartment/view.jsp");
	}
	/**
	 * 保存执行前附加逻辑
	 * @return 返回是否继续执行保存
	 */
	@Override
	public boolean onModelSaveBefore(BaseDepartment baseDepartment) {
		//当前登录用户
		BaseUser loginUser = getAttr("loginUser");
		if(baseDepartment.getId()==null || baseDepartment.getId().longValue()<1){
			//如果ID为空，则表示要新增数据
			Random rand = new Random(System.currentTimeMillis()%3333);
			//自动生成ID
			baseDepartment.setId(System.currentTimeMillis()*1000+rand.nextInt(999));
			//设置创建时间为当前时间
			baseDepartment.setCreateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置创建用户ID为当前登录用户的ID
				baseDepartment.setCreateUserId(loginUser.getId());
				//设置创建用户为当前登录用户
				baseDepartment.setCreateUser(loginUser);
			}
			//设置更新时间为当前时间
			baseDepartment.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				//设置更新用户ID为当前登录用户的ID
				baseDepartment.setUpdateUserId(loginUser.getId());
				//设置更新用户为当前登录用户
				baseDepartment.setUpdateUser(loginUser);
			}
		} else {
			//设置更新时间为当前时间
			baseDepartment.setUpdateTime(new Date());
			//如果当前登录用户和当前登录用户的ID都不为空
			if(loginUser!=null && loginUser.getId()!=null){
				baseDepartment.setUpdateUserId(loginUser.getId());
				baseDepartment.setUpdateUser(loginUser);
			}
		}
		return true;
	}
	/**
	 * 保存执行后附加逻辑
	 */
	@Override
	public boolean onModelSaveAfter(BaseDepartment baseDepartment) {
		return true;
	}
}
