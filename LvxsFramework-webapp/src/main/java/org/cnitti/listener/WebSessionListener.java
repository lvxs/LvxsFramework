package org.cnitti.listener;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListenerAdapter;
import org.cnitti.Consts;

/**
 * session过期处理
 */
public class WebSessionListener extends SessionListenerAdapter {

	/**
	 * seesion 结束时调用
	 */
	@Override
    public void onStop(Session session) {
		super.onExpiration(session);
		session.removeAttribute(Consts.ADMIN_USER); // session结束，移除用户
    }
	
	/**
	 * session过期处理
	 */
	@Override
	public void onExpiration(Session session) {
		super.onExpiration(session);
		session.removeAttribute(Consts.ADMIN_USER); // 移除旧的session
	}
	
}
