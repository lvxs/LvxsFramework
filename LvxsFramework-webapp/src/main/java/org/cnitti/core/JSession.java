package org.cnitti.core;

import java.util.Enumeration;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.ehcache.CacheKit;

public abstract class JSession implements HttpSession {
	final Controller controller;
	private static final int TIME = 60 * 60 * 24 * 7;

	public JSession(Controller controller) {
		this.controller = controller;
	}

	private void doPut(String key, Object value) {
		CacheKit.put("session", key + tryToGetJsessionId(), value);
	}

	private void doRemove(String key) {
		CacheKit.remove("session", key + tryToGetJsessionId());
	}

	private Object doGet(String key) {
		return CacheKit.get("session", key + tryToGetJsessionId());
	}

	private String tryToGetJsessionId() {
		String sessionid = controller.getCookie("JPSESSIONID");
		if (sessionid == null || "".equals(sessionid.trim())) {
			sessionid = UUID.randomUUID().toString().replace("-", "");
			controller.setCookie("JPSESSIONID", sessionid, TIME, true);
		}
		return sessionid;
	}

	public Object getAttribute(String key) {
		return doGet(key);
	}

	public Enumeration<String> getAttributeNames() {
		throw new RuntimeException("getAttributeNames method not finished.");
	}

	public long getCreationTime() {
		return 0;
	}

	public String getId() {
		return tryToGetJsessionId();
	}

	public long getLastAccessedTime() {
		return 0;
	}

	public int getMaxInactiveInterval() {
		return 0;
	}

	public ServletContext getServletContext() {
		return JFinal.me().getServletContext();
	}

	public Object getValue(String key) {
		return doGet(key);
	}

	public String[] getValueNames() {
		// TODO Auto-generated method stub
		return null;
	}

	public void invalidate() {
		// do nothing
	}

	public boolean isNew() {
		return false;
	}

	public void putValue(String key, Object value) {
		doPut(key, value);
	}

	public void removeAttribute(String key) {
		doRemove(key);
	}

	public void removeValue(String key) {
		doRemove(key);
	}

	public void setAttribute(String key, Object value) {
		doPut(key, value);
	}

	public void setMaxInactiveInterval(int arg0) {
		// do noting,default 60 mins
	}

}
