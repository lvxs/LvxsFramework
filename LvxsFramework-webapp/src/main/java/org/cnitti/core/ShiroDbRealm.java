package org.cnitti.core;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.cnitti.Consts;
import org.cnitti.model.BaseRole;
import org.cnitti.model.BaseRolePermission;
import org.cnitti.model.BaseUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Shiro Realm
 */
public class ShiroDbRealm extends AuthorizingRealm {
	private Logger log = LoggerFactory.getLogger(ShiroDbRealm.class);

	protected String authenticationQuery = Consts.DEFAULT_AUTHENTICATION_QUERY;
	protected String userRolesQuery = Consts.DEFAULT_USER_ROLES_QUERY;
	protected String permissionsQuery = Consts.DEFAULT_PERMISSIONS_QUERY;
	protected boolean permissionsLookupEnabled = Consts.PERMISSIONS_LOOKUP_ENABLED;

	/**
	 * Enables lookup of permissions during authorization. The default is "false" - meaning that
	 * only roles are associated with a BaseUser. Set this to true in order to lookup roles <b>and</b>
	 * permissions.
	 * 
	 * @param permissionsLookupEnabled
	 *            true if permissions should be looked up during authorization, or false if only
	 *            roles should be looked up.
	 */
	public void setPermissionsLookupEnabled(boolean permissionsLookupEnabled) {
		this.permissionsLookupEnabled = permissionsLookupEnabled;
	}

	/**
	 * 此方法是登录时调用的，返回带有密码的用户信息即可
	 */
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken upToken = (UsernamePasswordToken) token;
		String username = upToken.getUsername();
		// Null username is invalid
		if (username == null) {
			throw new AccountException("Null usernames are not allowed by this realm.");
		}
		try {
			BaseUser loginUser = BaseUser.DAO.findFirst(authenticationQuery, username, username, username);
			if(loginUser!=null){
				if(loginUser.getStatus()==1){
					String password = loginUser.getPassword();
					Subject subject = SecurityUtils.getSubject();
					subject.getSession().setAttribute(Consts.ADMIN_USER, loginUser);
					SimpleAuthenticationInfo simpleAuth =  new SimpleAuthenticationInfo(loginUser, password.toCharArray(), getName());
					List<BaseRole> roles = BaseRole.DAO.find(userRolesQuery, loginUser.getId(), loginUser.getId());
					loginUser.setRoleList(roles);
					return simpleAuth;
				} else {
					throw new LockedAccountException("BaseUser [" + username + "] is locked");
				}
			} else {
				throw new UnknownAccountException("No account found for BaseUser [" + username + "]");
			}
		} catch (Exception e) {
			final String message = "There was a SQL error while authenticating BaseUser [" + username + "]";
			if (log.isErrorEnabled()) {
				log.error(message, e);
			}
			// Rethrow any SQL errors as an authentication exception
			throw new AuthenticationException(message, e);
		}
	}

	/**
	 * 此方法是为用户加Role、permission的 This implementation of the interface expects the principals
	 * collection to return a String username keyed off of this realm's {@link #getName() name}
	 * 
	 * @see #getAuthorizationInfo(org.apache.shiro.subject.PrincipalCollection)
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

		// null usernames are invalid
		if (principals == null) {
			throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
		}

		BaseUser loginUser = (BaseUser) getAvailablePrincipal(principals);
		// BaseUser loginUser = (BaseUser) principals.fromRealm(getName()).iterator().next();

		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		try {
			// Retrieve roles and permissions from database
			if (permissionsLookupEnabled) {
				getPermissions(info, loginUser.getUserName(), loginUser.getRoleList());
			}
		} catch (Exception e) {
			final String message = "There was a SQL error while authorizing BaseUser [" + loginUser.getUserName()+ "]";
			if (log.isErrorEnabled()) {
				log.error(message, e);
			}
			// Rethrow any SQL errors as an authorization exception
			throw new AuthorizationException(message, e);
		}

		return info;
	}

	protected void getPermissions(SimpleAuthorizationInfo info, String username, List<BaseRole> roles) {
		for (BaseRole role : roles) {
			List<BaseRolePermission> permissions = BaseRolePermission.DAO.find(permissionsQuery, role.getId());
			for (BaseRolePermission roleResc : permissions) {
				info.addStringPermission(roleResc.getId()+"");
			}
			info.addRole(role.getId()+"");
		}
	}

	protected String getSaltForUser(String username) {
		return username;
	}
}
