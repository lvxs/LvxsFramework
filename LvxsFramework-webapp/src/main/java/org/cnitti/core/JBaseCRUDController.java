package org.cnitti.core;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
/**
 * 基础Controller，包含基础的保存和更新方法
 * @author madj
 * @param <M>
 */
public class JBaseCRUDController<M extends Model<? extends Model<?>>> extends JBaseController {

	//日志
	private static final Log logger = Log.getLog(JBaseCRUDController.class);;
	private final Class<M> mClazz;
	protected final M mDao;
	/**
	 * 构建函数
	 */
	@SuppressWarnings("unchecked")
	public JBaseCRUDController() {
		ParameterizedType type = getParameterizedType(getClass());
		mClazz = (Class<M>) type.getActualTypeArguments()[0];
		mDao = getDao();
	}

	private ParameterizedType getParameterizedType(Class<?> clazz) {
		if (clazz == Object.class) {
			logger.error("get ParameterizedType error in _BaseController.class");
			return null;
		}
		Type genericSuperclass = clazz.getGenericSuperclass();
		if (genericSuperclass instanceof ParameterizedType) {
			return (ParameterizedType) genericSuperclass;
		} else {
			return getParameterizedType(clazz.getSuperclass());
		}
	}
	/**
	 * 新增保存
	 */
	public void save() {
		if (isMultipartRequest()) {
			getFile();
		}
		
		M m = getModel(mClazz);
		if (!onModelSaveBefore(m)) {
			renderAjaxResultForError();
			return;
		}
		m.save();
		if (!onModelSaveAfter(m)) {
			renderAjaxResultForError();
			return;
		}
		renderAjaxResultForSuccess("ok");
	}
	
	/**
	 * 更新
	 */
	public void update() {
		if (isMultipartRequest()) {
			getFile();
		}
		
		M m = getModel(mClazz);

		if (!onModelSaveBefore(m)) {
			renderAjaxResultForError();
			return;
		}
		m.update();

		if (!onModelSaveAfter(m)) {
			renderAjaxResultForError();
			return;
		}

		renderAjaxResultForSuccess("ok");
	}
	
	/**
	 * 获取Dao对象
	 * @return
	 */
	private M getDao() {
		M m = null;
		try {
			m = mClazz.newInstance();
		} catch (Exception e) {
			logger.error("get DAO error.", e);
		}
		return m;
	}

	public Page<M> onIndexDataLoad(int pageNumber, int pageSize) {
		return null;
	}

	/**
	 * 保存执行前附加逻辑
	 */
	public boolean onModelSaveBefore(M m) {
		// do nothing
		return true;
	}
	/**
	 * 保存执行后附加逻辑
	 */
	public boolean onModelSaveAfter(M m) {
		// do nothing
		return true;
	}

}
