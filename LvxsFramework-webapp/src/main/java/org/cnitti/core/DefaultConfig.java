package org.cnitti.core;

import java.util.HashMap;
import java.util.List;

import org.beetl.ext.jfinal.BeetlRenderFactory;
import org.cnitti.Consts;
import org.cnitti.interceptor.BaseSystemInterceptor;
import org.cnitti.interceptor.ManagerInterceptor;
import org.cnitti.model.core.Table;
import org.cnitti.router.RouterMapping;
import org.cnitti.utils.ClassScaner;
import org.cnitti.utils.StringUtils;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.ext.kit.JFinalKit;
import com.jfinal.ext.plugin.shiro.ShiroInterceptor;
import com.jfinal.ext.plugin.shiro.ShiroPlugin;
import com.jfinal.json.FastJsonFactory;
import com.jfinal.kit.DesKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.generator.MetaBuilder;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.jfinal.plugin.activerecord.tx.TxByActionKeys;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
/*
 * 默认系统JFinal配置
 */
public class DefaultConfig extends JFinalConfig {
	/*
	 * 配置 JFinal 常量值
	 * @see com.jfinal.config.JFinalConfig#configConstant(com.jfinal.config.Constants)
	 */
	public void configConstant(Constants me) {
		me.setDevMode(true);//设置开发模式
		//me.setViewType(ViewType.JSP); //设置视图类型
		me.setMainRenderFactory(new BeetlRenderFactory());
		//GroupTemplate gt = BeetlRenderFactory.groupTemplate;	//根据gt可以添加扩展函数，格式化函数，共享变量等，
		me.setJsonFactory(new FastJsonFactory());//设置默认的JSON渲染工厂为FastJson
	}
	/*
	 * 设置JFinal访问路由
	 * @see com.jfinal.config.JFinalConfig#configRoute(com.jfinal.config.Routes)
	 */
	@SuppressWarnings("unchecked")
	public void configRoute(Routes me) {
		List<Class<Controller>> controllerClassList = ClassScaner.scanSubClass(Controller.class);
		if (controllerClassList != null) {
			for (Class<?> clazz : controllerClassList) {
				RouterMapping urlMapping = clazz.getAnnotation(RouterMapping.class);
				if (null != urlMapping && StringUtils.isNotBlank(urlMapping.url())) {
					if (StrKit.notBlank(urlMapping.viewPath())) {
						me.add(urlMapping.url(), (Class<? extends Controller>) clazz, urlMapping.viewPath());
					} else {
						me.add(urlMapping.url(), (Class<? extends Controller>) clazz);
					}
				}
			}
		}
	}
	/*
	 * 配置 JFinal 的 Plugin
	 * @see com.jfinal.config.JFinalConfig#configPlugin(com.jfinal.config.Plugins)
	 */
	public void configPlugin(Plugins me) {
		String propertiesFileName = "config.properties";
		Prop p = PropKit.use(propertiesFileName); 
		p.setProperty("propertiesFileName",propertiesFileName);
		DruidPlugin druidPlugin = createDruidPlugin(p);
		me.add(druidPlugin);
		ActiveRecordPlugin activeRecordPlugin = createRecordPlugin(druidPlugin);
		me.add(activeRecordPlugin);
		
		// ehcache缓存插件
		me.add(new EhCachePlugin());

		//Shiro初始化,配置shiro插件
		ShiroPlugin shiroPlugin = new ShiroPlugin(JFinalKit.getRoutes());
		me.add(shiroPlugin);
	}


	public DruidPlugin createDruidPlugin(Prop p) {
		String defaultDbId = p.get("default_db_id","mysql");
		String password = p.get(defaultDbId+".password","");
		if(password.startsWith("DES{") && password.endsWith("}")){
			password = DesKit.decoderByDES(password.substring(4,password.length()-1),"cnitti.net");
		} else {
			p.setProperty(defaultDbId+".password","DES{"+DesKit.encoderByDES(password,"cnitti.net")+"}");
			p.savePropertiesFile(p.get("propertiesFileName"),"数据库密码加密"+defaultDbId+".password；");
		}
		String user =  p.get(defaultDbId+".user","root");
		String jdbcUrl = p.get(defaultDbId+".jdbcUrl","");
		DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl,user,password);
		return druidPlugin;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ActiveRecordPlugin createRecordPlugin(DruidPlugin druidPlugin) {
		ActiveRecordPlugin arPlugin = new ActiveRecordPlugin(druidPlugin);
		List<Class<Model>> modelClassList = ClassScaner.scanSubClass(Model.class);
		if (modelClassList != null) {
			String tablePrefix = PropKit.use("config.properties").get("db_tablePrefix");
			tablePrefix = (StrKit.isBlank(tablePrefix)) ? "" : (tablePrefix.trim());
			for (Class<?> clazz : modelClassList) {
				Table tb = clazz.getAnnotation(Table.class);
				if (tb == null)
					continue;
				String tname = tablePrefix + tb.tableName();
				if (StringUtils.isNotBlank(tb.primaryKey())) {
					arPlugin.addMapping(tname, tb.primaryKey(), (Class<? extends Model<?>>) clazz);
				} else {
					arPlugin.addMapping(tname, (Class<? extends Model<?>>) clazz);
				}
			}
		}
		arPlugin.setShowSql(JFinal.me().getConstants().getDevMode());
		return arPlugin;
	}
	/*
	 * 配置JFinal全局拦截器
	 * @see com.jfinal.config.JFinalConfig#configInterceptor(com.jfinal.config.Interceptors)
	 */
	public void configInterceptor(Interceptors me) {
		//基础权限框架，多系统识别拦截器
		me.add(new BaseSystemInterceptor());
		//CMS，多网站识别拦截器
		//me.add(new CmsWebsiteInterceptor());
		//基础权限框架，管理员登录身份验证拦截器
		me.add(new ManagerInterceptor());
		
		// shiro权限拦截器配置
		me.add(new ShiroInterceptor());
		
		// 解决session在freemarker中不能取得的问题 获取方法：${session["manager"].username}
		//me.add(new SessionInViewInterceptor());
		
		// 添加事物，对save、update和delete自动进行拦截
		me.add(new TxByActionKeys("save", "update", "delete"));
	}
	/*
	 * 配置JFinal的Handler
	 * @see com.jfinal.config.JFinalConfig#configHandler(com.jfinal.config.Handlers)
	 */
	public void configHandler(Handlers me) {
	}
	/*
	 * 在jFinal启动后执行的方法
	 * @see com.jfinal.config.JFinalConfig#afterJFinalStart()
	 */
	public void afterJFinalStart(){
		List<TableMeta> tableMetaList = new MetaBuilder(DbKit.getConfig().getDataSource()).build();
		HashMap<String,TableMeta> tableMetaMap = new HashMap<String,TableMeta>();
		for(TableMeta tableMeta:tableMetaList){
			tableMetaMap.put(tableMeta.modelName,tableMeta);
		}
		JFinal.me().getServletContext().setAttribute(Consts.TABLE_META_MAP,tableMetaMap);
	}
	/*
	 * 在jFinal停止前执行的方法
	 * @see com.jfinal.config.JFinalConfig#beforeJFinalStop()
	 */
	public void beforeJFinalStop(){
		//com.jfinal.log.Log.getLog(this.getClass()).debug("在jFinal停止前执行");
	}
}