(function($) {
    $.fn.iconPicker = function( options ) {
        var mouseOver=false;
        var $popup=null;
        var icons=new Array("icon-home","icon-home2","icon-home5","icon-home7","icon-home8","icon-home9","icon-office","icon-city","icon-newspaper","icon-magazine","icon-design",
        		"icon-pencil","icon-pencil3","icon-pencil4","icon-pencil5","icon-pencil6","icon-pencil7","icon-eraser","icon-eraser2","icon-eraser3","icon-quill2","icon-quill4",
        		"icon-pen","icon-pen-plus","icon-pen-minus","icon-pen2","icon-blog","icon-pen6","icon-brush","icon-spray","icon-color-sampler","icon-toggle","icon-bucket",
        		"icon-gradient","icon-eyedropper","icon-eyedropper2","icon-eyedropper3","icon-droplet","icon-droplet2","icon-color-clear","icon-paint-format","icon-stamp",
        		"icon-image2","icon-image-compare","icon-images2","icon-image3","icon-images3","icon-image4","icon-image5","icon-camera","icon-shutter","icon-headphones",
        		"icon-headset","icon-music","icon-album","icon-tape","icon-piano","icon-speakers","icon-play","icon-clapboard-play","icon-clapboard","icon-media",
        		"icon-presentation","icon-movie","icon-film","icon-film2","icon-film3","icon-film4","icon-video-camera","icon-video-camera2","icon-video-camera-slash",
        		"icon-video-camera3","icon-dice","icon-chess-king","icon-chess-queen","icon-chess","icon-megaphone","icon-new","icon-connection","icon-station",
        		"icon-satellite-dish2","icon-feed","icon-mic2","icon-mic-off2","icon-book","icon-book2","icon-book-play","icon-book3","icon-bookmark","icon-books","icon-archive",
        		"icon-reading","icon-library2","icon-graduation2","icon-file-text","icon-profile","icon-file-empty","icon-file-empty2","icon-files-empty","icon-files-empty2",
        		"icon-file-plus","icon-file-plus2","icon-file-minus","icon-file-minus2","icon-file-download","icon-file-download2","icon-file-upload","icon-file-upload2",
        		"icon-file-check","icon-file-check2","icon-file-eye","icon-file-eye2","icon-file-text2","icon-file-text3","icon-file-picture","icon-file-picture2",
        		"icon-file-music","icon-file-music2","icon-file-play","icon-file-play2","icon-file-video","icon-file-video2","icon-copy","icon-copy2","icon-file-zip",
        		"icon-file-zip2","icon-file-xml","icon-file-xml2","icon-file-css","icon-file-css2","icon-file-presentation","icon-file-presentation2","icon-file-stats",
        		"icon-file-stats2","icon-file-locked","icon-file-locked2","icon-file-spreadsheet","icon-file-spreadsheet2","icon-copy3","icon-copy4","icon-paste","icon-paste2",
        		"icon-paste3","icon-paste4","icon-stack","icon-stack2","icon-stack3","icon-folder","icon-folder-search","icon-folder-download","icon-folder-upload",
        		"icon-folder-plus","icon-folder-plus2","icon-folder-minus","icon-folder-minus2","icon-folder-check","icon-folder-heart","icon-folder-remove","icon-folder2",
        		"icon-folder-open","icon-folder3","icon-folder4","icon-folder-plus3","icon-folder-minus3","icon-folder-plus4","icon-folder-minus4","icon-folder-download2",
        		"icon-folder-upload2","icon-folder-download3","icon-folder-upload3","icon-folder5","icon-folder-open2","icon-folder6","icon-folder-open3","icon-certificate",
        		"icon-cc","icon-price-tag","icon-price-tag2","icon-price-tags","icon-price-tag3","icon-price-tags2","icon-barcode2","icon-qrcode","icon-ticket","icon-theater",
        		"icon-store","icon-store2","icon-cart","icon-cart2","icon-cart4","icon-cart5","icon-cart-add","icon-cart-add2","icon-cart-remove","icon-basket","icon-bag",
        		"icon-percent","icon-coins","icon-coin-dollar","icon-coin-euro","icon-coin-pound","icon-coin-yen","icon-piggy-bank","icon-wallet","icon-cash","icon-cash2",
        		"icon-cash3","icon-cash4","icon-credit-card","icon-credit-card2","icon-calculator4","icon-calculator2","icon-calculator3","icon-chip","icon-lifebuoy","icon-phone",
        		"icon-phone2","icon-phone-slash","icon-phone-wave","icon-phone-plus","icon-phone-minus","icon-phone-plus2","icon-phone-minus2","icon-phone-incoming",
        		"icon-phone-outgoing","icon-phone-hang-up","icon-address-book","icon-address-book2","icon-address-book3","icon-notebook","icon-envelop","icon-envelop2",
        		"icon-envelop3","icon-envelop4","icon-envelop5","icon-mailbox","icon-pushpin","icon-location3","icon-location4","icon-compass4","icon-map","icon-map4","icon-map5",
        		"icon-direction","icon-reset","icon-history","icon-watch","icon-watch2","icon-alarm","icon-alarm-add","icon-alarm-check","icon-alarm-cancel","icon-bell2",
        		"icon-bell3","icon-bell-plus","icon-bell-minus","icon-bell-check","icon-bell-cross","icon-calendar","icon-calendar2","icon-calendar3","icon-calendar52",
        		"icon-printer","icon-printer2","icon-printer4","icon-shredder","icon-mouse","icon-mouse-left","icon-mouse-right","icon-keyboard","icon-typewriter","icon-display",
        		"icon-display4","icon-laptop","icon-mobile","icon-mobile2","icon-tablet","icon-mobile3","icon-tv","icon-radio","icon-cabinet","icon-drawer","icon-drawer2",
        		"icon-drawer-out","icon-drawer-in","icon-drawer3","icon-box","icon-box-add","icon-box-remove","icon-download","icon-upload","icon-floppy-disk","icon-floppy-disks",
        		"icon-usb-stick","icon-drive","icon-server","icon-database","icon-database2","icon-database4","icon-database-menu","icon-database-add","icon-database-remove",
        		"icon-database-insert","icon-database-export","icon-database-upload","icon-database-refresh","icon-database-diff","icon-database-edit2","icon-database-check",
        		"icon-database-arrow","icon-database-time2","icon-undo","icon-redo","icon-rotate-ccw","icon-rotate-cw","icon-rotate-ccw2","icon-rotate-cw2","icon-rotate-ccw3",
        		"icon-rotate-cw3","icon-flip-vertical2","icon-flip-horizontal2","icon-flip-vertical3","icon-flip-vertical4","icon-angle","icon-shear","icon-align-left",
        		"icon-align-center-horizontal","icon-align-right","icon-align-top","icon-align-center-vertical","icon-align-bottom","icon-undo2","icon-redo2","icon-forward",
        		"icon-reply","icon-reply-all","icon-bubble","icon-bubbles","icon-bubbles2","icon-bubble2","icon-bubbles3","icon-bubbles4","icon-bubble-notification",
        		"icon-bubbles5","icon-bubbles6","icon-bubble6","icon-bubbles7","icon-bubble7","icon-bubbles8","icon-bubble8","icon-bubble-dots3","icon-bubble-lines3",
        		"icon-bubble9","icon-bubble-dots4","icon-bubble-lines4","icon-bubbles9","icon-bubbles10","icon-user","icon-users","icon-user-plus","icon-user-minus",
        		"icon-user-cancel","icon-user-block","icon-user-lock","icon-user-check","icon-users2","icon-users4","icon-user-tie","icon-collaboration","icon-vcard","icon-hat",
        		"icon-bowtie","icon-quotes-left","icon-quotes-right","icon-quotes-left2","icon-quotes-right2","icon-hour-glass","icon-hour-glass2","icon-hour-glass3",
        		"icon-spinner","icon-spinner2","icon-spinner3","icon-spinner4","icon-spinner6","icon-spinner9","icon-spinner10","icon-spinner11","icon-microscope",
        		"icon-enlarge","icon-shrink","icon-enlarge3","icon-shrink3","icon-enlarge5","icon-shrink5","icon-enlarge6","icon-shrink6","icon-enlarge7","icon-shrink7",
        		"icon-key","icon-lock","icon-lock2","icon-lock4","icon-unlocked","icon-lock5","icon-unlocked2","icon-safe","icon-wrench","icon-wrench2","icon-wrench3",
        		"icon-equalizer","icon-equalizer2","icon-equalizer3","icon-equalizer4","icon-cog","icon-cogs","icon-cog2","icon-cog3","icon-cog4","icon-cog52","icon-cog6",
        		"icon-cog7","icon-hammer","icon-hammer-wrench","icon-magic-wand","icon-magic-wand2","icon-pulse2","icon-aid-kit","icon-bug2","icon-construction",
        		"icon-traffic-cone","icon-traffic-lights","icon-pie-chart","icon-pie-chart2","icon-pie-chart3","icon-pie-chart4","icon-pie-chart5","icon-pie-chart6",
        		"icon-pie-chart7","icon-stats-dots","icon-stats-bars","icon-pie-chart8","icon-stats-bars2","icon-stats-bars3","icon-stats-bars4","icon-chart",
        		"icon-stats-growth","icon-stats-decline","icon-stats-growth2","icon-stats-decline2","icon-stairs-up","icon-stairs-down","icon-stairs","icon-ladder",
        		"icon-rating","icon-rating2","icon-rating3","icon-podium","icon-stars","icon-medal-star","icon-medal","icon-medal2","icon-medal-first","icon-medal-second",
        		"icon-medal-third","icon-crown","icon-trophy2","icon-trophy3","icon-diamond","icon-trophy4","icon-gift","icon-pipe","icon-mustache","icon-cup2","icon-coffee",
        		"icon-paw","icon-footprint","icon-rocket","icon-meter2","icon-meter-slow","icon-meter-fast","icon-hammer2","icon-balance","icon-fire","icon-fire2","icon-lab",
        		"icon-atom","icon-atom2","icon-bin","icon-bin2","icon-briefcase","icon-briefcase3","icon-airplane2","icon-airplane3","icon-airplane4","icon-paperplane",
        		"icon-car","icon-steering-wheel","icon-car2","icon-gas","icon-bus","icon-truck","icon-bike","icon-road","icon-train","icon-train2","icon-ship","icon-boat",
        		"icon-chopper","icon-cube","icon-cube2","icon-cube3","icon-cube4","icon-pyramid","icon-pyramid2","icon-package","icon-puzzle","icon-puzzle2","icon-puzzle3",
        		"icon-puzzle4","icon-glasses-3d2","icon-brain","icon-accessibility","icon-accessibility2","icon-strategy","icon-target","icon-target2","icon-shield-check",
        		"icon-shield-notice","icon-shield2","icon-racing","icon-finish","icon-power2","icon-power3","icon-switch","icon-switch22","icon-power-cord","icon-clipboard",
        		"icon-clipboard2","icon-clipboard3","icon-clipboard4","icon-clipboard5","icon-clipboard6","icon-playlist","icon-playlist-add","icon-list-numbered","icon-list",
        		"icon-list2","icon-more","icon-more2","icon-grid","icon-grid2","icon-grid3","icon-grid4","icon-grid52","icon-grid6","icon-grid7","icon-tree5","icon-tree6",
        		"icon-tree7","icon-lan","icon-lan2","icon-lan3","icon-menu","icon-circle-small","icon-menu2","icon-menu3","icon-menu4","icon-menu5","icon-menu62",
        		"icon-menu8","icon-menu9","icon-menu10","icon-cloud","icon-cloud-download","icon-cloud-upload","icon-cloud-check","icon-cloud2","icon-cloud-download2",
        		"icon-cloud-upload2","icon-cloud-check2","icon-import","icon-download4","icon-upload4","icon-download7","icon-upload7","icon-download10","icon-upload10",
        		"icon-sphere","icon-sphere3","icon-earth","icon-link","icon-unlink","icon-link2","icon-unlink2","icon-anchor","icon-flag3","icon-flag4","icon-flag7",
        		"icon-flag8","icon-attachment","icon-attachment2","icon-eye","icon-eye-plus","icon-eye-minus","icon-eye-blocked","icon-eye2","icon-eye-blocked2","icon-eye4",
        		"icon-bookmark2","icon-bookmark3","icon-bookmarks","icon-bookmark4","icon-spotlight2","icon-starburst","icon-snowflake","icon-weather-windy","icon-fan",
        		"icon-umbrella","icon-sun3","icon-contrast","icon-bed2","icon-furniture","icon-chair","icon-star-empty3","icon-star-half","icon-star-full2","icon-heart5",
        		"icon-heart6","icon-heart-broken2","icon-thumbs-up2","icon-thumbs-down2","icon-thumbs-up3","icon-thumbs-down3","icon-height","icon-man","icon-woman",
        		"icon-man-woman","icon-yin-yang","icon-cursor","icon-cursor2","icon-lasso2","icon-select2","icon-point-up","icon-point-right","icon-point-down",
        		"icon-point-left","icon-pointer","icon-reminder","icon-drag-left-right","icon-drag-left","icon-drag-right","icon-touch","icon-multitouch","icon-touch-zoom",
        		"icon-touch-pinch","icon-hand","icon-grab","icon-stack-empty","icon-stack-plus","icon-stack-minus","icon-stack-star","icon-stack-picture","icon-stack-down",
        		"icon-stack-up","icon-stack-cancel","icon-stack-check","icon-stack-text","icon-stack4","icon-stack-music","icon-stack-play","icon-move","icon-dots",
        		"icon-warning","icon-warning22","icon-notification2","icon-question3","icon-question4","icon-plus3","icon-minus3","icon-plus-circle2","icon-minus-circle2",
        		"icon-cancel-circle2","icon-blocked","icon-cancel-square","icon-cancel-square2","icon-spam","icon-cross2","icon-cross3","icon-checkmark","icon-checkmark3",
        		"icon-checkmark2","icon-checkmark4","icon-spell-check","icon-spell-check2","icon-enter","icon-exit","icon-enter2","icon-exit2","icon-enter3","icon-exit3",
        		"icon-wall","icon-fence","icon-play3","icon-pause","icon-stop","icon-previous","icon-next","icon-backward","icon-forward2","icon-play4","icon-pause2",
        		"icon-stop2","icon-backward2","icon-forward3","icon-first","icon-last","icon-previous2","icon-next2","icon-eject","icon-volume-high","icon-volume-medium",
        		"icon-volume-low","icon-volume-mute","icon-speaker-left","icon-speaker-right","icon-volume-mute2","icon-volume-increase","icon-volume-decrease",
        		"icon-volume-mute5","icon-loop","icon-loop3","icon-infinite-square","icon-infinite","icon-loop4","icon-shuffle","icon-wave","icon-wave2","icon-split",
        		"icon-merge","icon-arrow-up5","icon-arrow-right5","icon-arrow-down5","icon-arrow-left5","icon-arrow-up-left2","icon-arrow-up7","icon-arrow-up-right2",
        		"icon-arrow-right7","icon-arrow-down-right2","icon-arrow-down7","icon-arrow-down-left2","icon-arrow-left7","icon-arrow-up-left3","icon-arrow-up8",
        		"icon-arrow-up-right3","icon-arrow-right8","icon-arrow-down-right3","icon-arrow-down8","icon-arrow-down-left3","icon-arrow-left8","icon-circle-up2",
        		"icon-circle-right2","icon-circle-down2","icon-circle-left2","icon-arrow-resize7","icon-arrow-resize8","icon-square-up-left","icon-square-up",
        		"icon-square-up-right","icon-square-right","icon-square-down-right","icon-square-down","icon-square-down-left","icon-square-left","icon-arrow-up15",
        		"icon-arrow-right15","icon-arrow-down15","icon-arrow-left15","icon-arrow-up16","icon-arrow-right16","icon-arrow-down16","icon-arrow-left16","icon-menu-open",
        		"icon-menu-open2","icon-menu-close","icon-menu-close2","icon-enter5","icon-esc","icon-enter6","icon-backspace","icon-backspace2","icon-tab","icon-transmission",
        		"icon-sort","icon-move-up2","icon-move-down2","icon-sort-alpha-asc","icon-sort-alpha-desc","icon-sort-numeric-asc","icon-sort-numberic-desc",
        		"icon-sort-amount-asc","icon-sort-amount-desc","icon-sort-time-asc","icon-sort-time-desc","icon-battery-6","icon-battery-0","icon-battery-charging",
        		"icon-command","icon-shift","icon-ctrl","icon-opt","icon-checkbox-checked","icon-checkbox-unchecked","icon-checkbox-partial","icon-square","icon-triangle",
        		"icon-triangle2","icon-diamond3","icon-diamond4","icon-checkbox-checked2","icon-checkbox-unchecked2","icon-checkbox-partial2","icon-radio-checked",
        		"icon-radio-checked2","icon-radio-unchecked","icon-checkmark-circle","icon-circle","icon-circle2","icon-circles","icon-circles2","icon-crop","icon-crop2",
        		"icon-make-group","icon-ungroup","icon-vector","icon-vector2","icon-rulers","icon-pencil-ruler","icon-scissors","icon-filter3","icon-filter4","icon-font",
        		"icon-ampersand2","icon-ligature","icon-font-size","icon-typography","icon-text-height","icon-text-width","icon-height2","icon-width","icon-strikethrough2",
        		"icon-font-size2","icon-bold2","icon-underline2","icon-italic2","icon-strikethrough3","icon-omega","icon-sigma","icon-nbsp","icon-page-break","icon-page-break2",
        		"icon-superscript","icon-subscript","icon-superscript2","icon-subscript2","icon-text-color","icon-highlight","icon-pagebreak","icon-clear-formatting",
        		"icon-table","icon-table2","icon-insert-template","icon-pilcrow","icon-ltr","icon-rtl","icon-ltr2","icon-rtl2","icon-section","icon-paragraph-left2",
        		"icon-paragraph-center2","icon-paragraph-right2","icon-paragraph-justify2","icon-indent-increase","icon-indent-decrease","icon-paragraph-left3",
        		"icon-paragraph-center3","icon-paragraph-right3","icon-paragraph-justify3","icon-indent-increase2","icon-indent-decrease2","icon-share","icon-share2",
        		"icon-new-tab","icon-new-tab2","icon-popout","icon-embed","icon-embed2","icon-markup","icon-regexp","icon-regexp2","icon-code","icon-circle-css",
        		"icon-circle-code","icon-terminal","icon-unicode","icon-seven-segment-0","icon-seven-segment-1","icon-seven-segment-2","icon-seven-segment-3",
        		"icon-seven-segment-4","icon-seven-segment-5","icon-seven-segment-6","icon-seven-segment-7","icon-seven-segment-8","icon-seven-segment-9","icon-share3",
        		"icon-share4","icon-google","icon-google-plus","icon-google-plus2","icon-google-drive","icon-facebook","icon-facebook2","icon-instagram","icon-twitter",
        		"icon-twitter2","icon-feed2","icon-feed3","icon-youtube","icon-youtube2","icon-youtube3","icon-vimeo","icon-vimeo2","icon-lanyrd","icon-flickr","icon-flickr2",
        		"icon-flickr3","icon-picassa","icon-picassa2","icon-dribbble","icon-dribbble2","icon-dribbble3","icon-forrst","icon-forrst2","icon-deviantart",
        		"icon-deviantart2","icon-steam","icon-steam2","icon-dropbox","icon-onedrive","icon-github","icon-github4","icon-github5","icon-wordpress","icon-wordpress2",
        		"icon-joomla","icon-blogger","icon-blogger2","icon-tumblr","icon-tumblr2","icon-yahoo","icon-tux","icon-apple2","icon-finder","icon-android","icon-windows",
        		"icon-windows8","icon-soundcloud","icon-soundcloud2","icon-skype","icon-reddit","icon-linkedin","icon-linkedin2","icon-lastfm","icon-lastfm2","icon-delicious",
        		"icon-stumbleupon","icon-stumbleupon2","icon-stackoverflow","icon-pinterest2","icon-xing","icon-flattr","icon-foursquare","icon-paypal","icon-paypal2",
        		"icon-yelp","icon-file-pdf","icon-file-openoffice","icon-file-word","icon-file-excel","icon-libreoffice","icon-html5","icon-html52","icon-css3","icon-git",
        		"icon-svg","icon-codepen","icon-chrome","icon-firefox","icon-IE","icon-opera","icon-safari","icon-check2","icon-home4","icon-people","icon-checkmark-circle2",
        		"icon-arrow-up-left32","icon-arrow-up52","icon-arrow-up-right32","icon-arrow-right6","icon-arrow-down-right32","icon-arrow-down52","icon-arrow-down-left32",
        		"icon-arrow-left52","icon-calendar5","icon-move-alt1","icon-reload-alt","icon-move-vertical","icon-move-horizontal","icon-hash","icon-bars-alt","icon-eye8",
        		"icon-search4","icon-zoomin3","icon-zoomout3","icon-add","icon-subtract","icon-exclamation","icon-question6","icon-close2","icon-task","icon-inbox",
        		"icon-inbox-alt","icon-envelope","icon-compose","icon-newspaper2","icon-calendar22","icon-hyperlink","icon-trash","icon-trash-alt","icon-grid5","icon-grid-alt",
        		"icon-menu6","icon-list3","icon-gallery","icon-calculator","icon-windows2","icon-browser","icon-portfolio","icon-comments","icon-screen3","icon-iphone",
        		"icon-ipad","icon-googleplus5","icon-pin","icon-pin-alt","icon-cog5","icon-graduation","icon-air","icon-droplets","icon-statistics","icon-pie5","icon-cross",
        		"icon-minus2","icon-plus2","icon-info3","icon-info22","icon-question7","icon-help","icon-warning2","icon-add-to-list","icon-arrow-left12","icon-arrow-down12",
        		"icon-arrow-up12","icon-arrow-right13","icon-arrow-left22","icon-arrow-down22","icon-arrow-up22","icon-arrow-right22","icon-arrow-left32","icon-arrow-down32",
        		"icon-arrow-up32","icon-arrow-right32","icon-switch2","icon-checkmark5","icon-ampersand","icon-alert","icon-alignment-align","icon-alignment-aligned-to",
        		"icon-alignment-unalign","icon-arrow-down132","icon-arrow-up13","icon-arrow-left13","icon-arrow-right14","icon-arrow-small-down","icon-arrow-small-left",
        		"icon-arrow-small-right","icon-arrow-small-up","icon-check","icon-chevron-down","icon-chevron-left","icon-chevron-right","icon-chevron-up","icon-clippy",
        		"icon-comment","icon-comment-discussion","icon-dash","icon-diff","icon-diff-added","icon-diff-ignored","icon-diff-modified","icon-diff-removed",
        		"icon-diff-renamed","icon-file-media","icon-fold","icon-gear","icon-git-branch","icon-git-commit","icon-git-compare","icon-git-merge","icon-git-pull-request",
        		"icon-graph","icon-law","icon-list-ordered","icon-list-unordered","icon-mail5","icon-mail-read","icon-mention","icon-mirror","icon-move-down","icon-move-left",
        		"icon-move-right","icon-move-up","icon-person","icon-plus22","icon-primitive-dot","icon-primitive-square","icon-repo-forked","icon-screen-full",
        		"icon-screen-normal","icon-sync","icon-three-bars","icon-unfold","icon-versions","icon-x",
        		"glyphicon-asterisk","glyphicon-plus","glyphicon-euro","glyphicon-minus","glyphicon-cloud",
        		"glyphicon-envelope","glyphicon-pencil","glyphicon-glass","glyphicon-music","glyphicon-search",
        		"glyphicon-heart","glyphicon-star","glyphicon-star-empty","glyphicon-user","glyphicon-film",
        		"glyphicon-th-large","glyphicon-th","glyphicon-th-list","glyphicon-ok","glyphicon-remove",
        		"glyphicon-zoom-in","glyphicon-zoom-out","glyphicon-off","glyphicon-signal","glyphicon-cog",
        		"glyphicon-download-alt","glyphicon-download","glyphicon-upload","glyphicon-inbox","glyphicon-play-circle",
        		"glyphicon-repeat","glyphicon-refresh","glyphicon-list-alt","glyphicon-lock","glyphicon-flag",
        		"glyphicon-headphones","glyphicon-volume-off","glyphicon-volume-down","glyphicon-volume-up","glyphicon-qrcode",
        		"glyphicon-barcode","glyphicon-tag","glyphicon-tags","glyphicon-book","glyphicon-bookmark",
        		"glyphicon-print","glyphicon-camera","glyphicon-font","glyphicon-bold","glyphicon-italic",
        		"glyphicon-picture","glyphicon-map-marker","glyphicon-adjust","glyphicon-tint","glyphicon-edit",
        		"glyphicon-share","glyphicon-check","glyphicon-move","glyphicon-step-backward","glyphicon-fast-backward",
        		"glyphicon-backward","glyphicon-play","glyphicon-pause","glyphicon-stop","glyphicon-forward",
        		"glyphicon-fast-forward","glyphicon-step-forward","glyphicon-eject","glyphicon-chevron-left",
        		"glyphicon-chevron-right","glyphicon-plus-sign","glyphicon-minus-sign","glyphicon-remove-sign",
        		"glyphicon-ok-sign","glyphicon-question-sign","glyphicon-info-sign","glyphicon-trash","glyphicon-home",
        		"glyphicon-file","glyphicon-time","glyphicon-road","glyphicon-screenshot","glyphicon-remove-circle",
        		"glyphicon-ok-circle","glyphicon-ban-circle","glyphicon-arrow-left","glyphicon-arrow-right","glyphicon-arrow-up",
        		"glyphicon-arrow-down","glyphicon-share-alt","glyphicon-resize-full","glyphicon-resize-small",
        		"glyphicon-exclamation-sign","glyphicon-gift","glyphicon-leaf","glyphicon-text-height","glyphicon-text-width",
        		"glyphicon-align-left","glyphicon-align-center","glyphicon-align-right",
        		"glyphicon-fire","glyphicon-eye-open","glyphicon-eye-close","glyphicon-warning-sign","glyphicon-plane",
        		"glyphicon-calendar","glyphicon-random","glyphicon-comment","glyphicon-magnet","glyphicon-chevron-up",
        		"glyphicon-chevron-down","glyphicon-retweet","glyphicon-shopping-cart","glyphicon-folder-close",
        		"glyphicon-folder-open","glyphicon-resize-vertical","glyphicon-resize-horizontal","glyphicon-hdd",
        		"glyphicon-bullhorn","glyphicon-bell","glyphicon-certificate","glyphicon-thumbs-up","glyphicon-thumbs-down",
        		"glyphicon-hand-right","glyphicon-hand-left","glyphicon-hand-up","glyphicon-hand-down",
        		"glyphicon-circle-arrow-right","glyphicon-circle-arrow-left","glyphicon-circle-arrow-up","glyphicon-circle-arrow-down",
        		"glyphicon-globe","glyphicon-wrench","glyphicon-tasks","glyphicon-filter","glyphicon-briefcase",
        		"glyphicon-fullscreen","glyphicon-dashboard","glyphicon-paperclip","glyphicon-heart-empty","glyphicon-link",
        		"glyphicon-phone","glyphicon-pushpin","glyphicon-usd","glyphicon-gbp","glyphicon-sort",
        		"glyphicon-sort-by-alphabet","glyphicon-sort-by-alphabet-alt","glyphicon-align-justify","glyphicon-list",
        		"glyphicon-sort-by-order","glyphicon-sort-by-order-alt","glyphicon-sort-by-attributes",
        		"glyphicon-sort-by-attributes-alt","glyphicon-unchecked","glyphicon-expand","glyphicon-collapse-down",
        		"glyphicon-collapse-up","glyphicon-log-in","glyphicon-flash","glyphicon-log-out","glyphicon-new-window",
        		"glyphicon-record","glyphicon-save","glyphicon-open","glyphicon-saved","glyphicon-import",
        		"glyphicon-export","glyphicon-send","glyphicon-floppy-disk","glyphicon-floppy-saved","glyphicon-floppy-remove",
        		"glyphicon-floppy-save","glyphicon-floppy-open","glyphicon-credit-card","glyphicon-transfer",
        		"glyphicon-cutlery","glyphicon-header","glyphicon-compressed","glyphicon-earphone","glyphicon-phone-alt",
        		"glyphicon-tower","glyphicon-stats","glyphicon-sd-video","glyphicon-hd-video","glyphicon-subtitles",
        		"glyphicon-sound-stereo","glyphicon-sound-dolby","glyphicon-sound-5-1","glyphicon-sound-6-1",
        		"glyphicon-sound-7-1","glyphicon-copyright-mark","glyphicon-registration-mark","glyphicon-cloud-download",
        		"glyphicon-cloud-upload","glyphicon-tree-conifer","glyphicon-tree-deciduous","glyphicon-indent-left",
        		"glyphicon-indent-right","glyphicon-facetime-video",
        		"fa-adjust","fa-anchor","fa-archive","fa-area-chart","fa-arrows","fa-arrows-h","fa-arrows-v","fa-asterisk","fa-at",
        		"fa-automobile","fa-ban","fa-bank","fa-bar-chart","fa-bar-chart-o","fa-barcode","fa-bars","fa-bed","fa-beer","fa-bell",
        		"fa-bell-o","fa-bell-slash","fa-bell-slash-o","fa-bicycle","fa-binoculars","fa-birthday-cake","fa-bolt","fa-bomb","fa-book",
        		"fa-bookmark","fa-bookmark-o","fa-briefcase","fa-bug","fa-building","fa-building-o","fa-bullhorn","fa-bullseye","fa-bus","fa-cab",
        		"fa-calculator","fa-calendar","fa-calendar-o","fa-camera","fa-camera-retro","fa-car","fa-caret-square-o-down","fa-caret-square-o-left",
        		"fa-caret-square-o-right","fa-caret-square-o-up","fa-cart-arrow-down","fa-cart-plus","fa-cc","fa-certificate","fa-check",
        		"fa-check-circle","fa-check-circle-o","fa-check-square","fa-check-square-o","fa-child","fa-circle","fa-circle-o","fa-circle-o-notch",
        		"fa-circle-thin","fa-clock-o","fa-close","fa-cloud","fa-cloud-download","fa-cloud-upload","fa-code","fa-code-fork","fa-coffee",
        		"fa-cog","fa-cogs","fa-comment","fa-comment-o","fa-comments","fa-comments-o","fa-compass","fa-copyright","fa-credit-card",
        		"fa-crop","fa-crosshairs","fa-cube","fa-cubes","fa-cutlery","fa-dashboard","fa-database","fa-desktop","fa-diamond",
        		"fa-dot-circle-o","fa-download","fa-edit","fa-ellipsis-h","fa-ellipsis-v","fa-envelope","fa-envelope-o","fa-envelope-square",
        		"fa-eraser","fa-exchange","fa-exclamation","fa-exclamation-circle","fa-exclamation-triangle","fa-external-link","fa-external-link-square",
        		"fa-eye","fa-eye-slash","fa-eyedropper","fa-fax","fa-female","fa-fighter-jet","fa-file-archive-o","fa-file-audio-o","fa-file-code-o",
        		"fa-file-excel-o","fa-file-image-o","fa-file-movie-o","fa-file-pdf-o","fa-file-photo-o","fa-file-picture-o","fa-file-powerpoint-o",
        		"fa-file-sound-o","fa-file-video-o","fa-file-word-o","fa-file-zip-o","fa-film","fa-filter","fa-fire","fa-fire-extinguisher","fa-flag",
        		"fa-flag-checkered","fa-flag-o","fa-flash","fa-flask","fa-folder","fa-folder-o","fa-folder-open","fa-folder-open-o","fa-frown-o",
        		"fa-futbol-o","fa-gamepad","fa-gavel","fa-gear","fa-gears","fa-genderless","fa-gift","fa-glass","fa-globe","fa-graduation-cap",
        		"fa-group","fa-hdd-o","fa-headphones","fa-heart","fa-heart-o","fa-heartbeat","fa-history","fa-home","fa-hotel","fa-image",
        		"fa-inbox","fa-info","fa-info-circle","fa-institution","fa-key","fa-keyboard-o","fa-language","fa-laptop","fa-leaf","fa-legal",
        		"fa-lemon-o","fa-level-down","fa-level-up","fa-life-bouy","fa-life-buoy","fa-life-ring","fa-life-saver","fa-lightbulb-o",
        		"fa-line-chart","fa-location-arrow","fa-lock","fa-magic","fa-magnet","fa-mail-forward","fa-mail-reply","fa-mail-reply-all","fa-male",
        		"fa-map-marker","fa-meh-o","fa-microphone","fa-microphone-slash","fa-minus","fa-minus-circle","fa-minus-square","fa-minus-square-o",
        		"fa-mobile","fa-mobile-phone","fa-money","fa-moon-o","fa-mortar-board","fa-motorcycle","fa-music","fa-navicon","fa-newspaper-o",
        		"fa-paint-brush","fa-paper-plane","fa-paper-plane-o","fa-paw","fa-pencil","fa-pencil-square","fa-pencil-square-o","fa-phone",
        		"fa-phone-square","fa-photo","fa-picture-o","fa-pie-chart","fa-plane","fa-plug","fa-plus","fa-plus-circle","fa-plus-square",
        		"fa-plus-square-o","fa-power-off","fa-print","fa-puzzle-piece","fa-qrcode","fa-question","fa-question-circle","fa-quote-left",
        		"fa-quote-right","fa-random","fa-recycle","fa-refresh","fa-remove","fa-reorder","fa-reply","fa-reply-all","fa-retweet","fa-road",
        		"fa-rocket","fa-rss","fa-rss-square","fa-search","fa-search-minus","fa-search-plus","fa-send","fa-send-o","fa-server","fa-share",
        		"fa-share-alt","fa-share-alt-square","fa-share-square","fa-share-square-o","fa-shield","fa-ship","fa-shopping-cart","fa-sign-in",
        		"fa-sign-out","fa-signal","fa-sitemap","fa-sliders","fa-smile-o","fa-soccer-ball-o","fa-sort","fa-sort-alpha-asc",
        		"fa-sort-alpha-desc","fa-sort-amount-asc","fa-sort-amount-desc","fa-sort-asc","fa-sort-desc","fa-sort-down","fa-sort-numeric-asc",
        		"fa-sort-numeric-desc","fa-sort-up","fa-space-shuttle","fa-spinner","fa-spoon","fa-square","fa-square-o","fa-star","fa-star-half",
        		"fa-star-half-empty","fa-star-half-full","fa-star-half-o","fa-star-o","fa-street-view","fa-suitcase","fa-sun-o","fa-support",
        		"fa-tablet","fa-tachometer","fa-tag","fa-tags","fa-tasks","fa-taxi","fa-terminal","fa-thumb-tack","fa-thumbs-down",
        		"fa-thumbs-o-down","fa-thumbs-o-up","fa-thumbs-up","fa-ticket","fa-times","fa-times-circle","fa-times-circle-o","fa-tint",
        		"fa-toggle-down","fa-toggle-left","fa-toggle-off","fa-toggle-on","fa-toggle-right","fa-toggle-up","fa-trash","fa-trash-o","fa-tree",
        		"fa-trophy","fa-truck","fa-tty","fa-umbrella","fa-university","fa-unlock","fa-unlock-alt","fa-unsorted","fa-upload","fa-user",
        		"fa-user-plus","fa-user-secret","fa-user-times","fa-users","fa-video-camera","fa-volume-down","fa-volume-off","fa-volume-up",
        		"fa-warning","fa-wheelchair","fa-wifi","fa-wrench",
        		"fa-ambulance","fa-bicycle","fa-bus","fa-cab","fa-car","fa-fighter-jet","fa-motorcycle","fa-plane","fa-rocket",
        		"fa-ship","fa-space-shuttle","fa-subway","fa-taxi","fa-train","fa-truck","fa-wheelchair",
        		"fa-circle-thin","fa-genderless","fa-mars","fa-mars-double","fa-mars-stroke","fa-mars-stroke-h","fa-mars-stroke-v","fa-mercury",
        		"fa-neuter","fa-transgender","fa-transgender-alt","fa-venus","fa-venus-double","fa-venus-mars",
        		"fa-file","fa-file-archive-o","fa-file-audio-o","fa-file-code-o","fa-file-excel-o","fa-file-image-o","fa-file-movie-o","fa-file-o",
        		"fa-file-pdf-o","fa-file-photo-o","fa-file-picture-o","fa-file-powerpoint-o","fa-file-sound-o","fa-file-text","fa-file-text-o",
        		"fa-file-video-o","fa-file-word-o","fa-file-zip-o",
        		"fa-circle-o-notch","fa-cog","fa-gear","fa-refresh","fa-spinner",
        		"fa-check-square","fa-check-square-o","fa-circle","fa-circle-o","fa-dot-circle-o","fa-minus-square","fa-minus-square-o","fa-plus-square",
        		"fa-plus-square-o","fa-square","fa-square-o",
        		"fa-check-square","fa-check-square-o","fa-circle","fa-circle-o","fa-dot-circle-o","fa-minus-square","fa-minus-square-o","fa-plus-square",
        		"fa-plus-square-o","fa-square","fa-square-o",
        		"fa-area-chart","fa-bar-chart","fa-bar-chart-o","fa-line-chart","fa-pie-chart",
        		"fa-bitcoin","fa-btc","fa-cny","fa-dollar","fa-eur","fa-euro","fa-gbp","fa-ils","fa-inr","fa-jpy","fa-krw","fa-money",
        		"fa-rmb","fa-rouble","fa-rub","fa-ruble","fa-rupee","fa-shekel","fa-sheqel","fa-try","fa-turkish-lira","fa-usd","fa-won",
        		"fa-yen",
        		"fa-align-center","fa-align-justify","fa-align-left","fa-align-right","fa-bold","fa-chain","fa-chain-broken","fa-clipboard",
        		"fa-columns","fa-copy","fa-cut","fa-dedent","fa-eraser","fa-file","fa-file-o","fa-file-text","fa-file-text-o","fa-files-o",
        		"fa-floppy-o","fa-font","fa-header","fa-indent","fa-italic","fa-link","fa-list","fa-list-alt","fa-list-ol","fa-list-ul",
        		"fa-outdent","fa-paperclip","fa-paragraph","fa-paste","fa-repeat","fa-rotate-left","fa-rotate-right","fa-save","fa-scissors",
        		"fa-strikethrough","fa-subscript","fa-superscript","fa-table","fa-text-height","fa-text-width","fa-th","fa-th-large","fa-th-list",
        		"fa-underline","fa-undo","fa-unlink",
        		"fa-angle-double-down","fa-angle-double-left","fa-angle-double-right","fa-angle-double-up","fa-angle-down","fa-angle-left","fa-angle-right",
        		"fa-angle-up","fa-arrow-circle-down","fa-arrow-circle-left","fa-arrow-circle-o-down","fa-arrow-circle-o-left","fa-arrow-circle-o-right",
        		"fa-arrow-circle-o-up","fa-arrow-circle-right","fa-arrow-circle-up","fa-arrow-down","fa-arrow-left","fa-arrow-right","fa-arrow-up",
        		"fa-arrows","fa-arrows-alt","fa-arrows-h","fa-arrows-v","fa-caret-down","fa-caret-left","fa-caret-right","fa-caret-square-o-down",
        		"fa-caret-square-o-left","fa-caret-square-o-right","fa-caret-square-o-up","fa-caret-up","fa-chevron-circle-down","fa-chevron-circle-left",
        		"fa-chevron-circle-right","fa-chevron-circle-up","fa-chevron-down","fa-chevron-left","fa-chevron-right","fa-chevron-up","fa-hand-o-down",
        		"fa-hand-o-left","fa-hand-o-right","fa-hand-o-up","fa-long-arrow-down","fa-long-arrow-left","fa-long-arrow-right","fa-long-arrow-up",
        		"fa-toggle-down","fa-toggle-left","fa-toggle-right","fa-toggle-up",
        		"fa-arrows-alt","fa-backward","fa-compress","fa-eject","fa-expand","fa-fast-backward","fa-fast-forward","fa-forward","fa-pause",
        		"fa-play","fa-play-circle","fa-play-circle-o","fa-step-backward","fa-step-forward","fa-stop","fa-youtube-play",
        		"fa-adn","fa-android","fa-angellist","fa-apple","fa-behance","fa-behance-square","fa-bitbucket","fa-bitbucket-square","fa-bitcoin",
        		"fa-btc","fa-buysellads","fa-cc-amex","fa-cc-discover","fa-cc-mastercard","fa-cc-paypal","fa-cc-stripe","fa-cc-visa","fa-codepen",
        		"fa-connectdevelop","fa-css3","fa-dashcube","fa-delicious","fa-deviantart","fa-digg","fa-dribbble","fa-dropbox","fa-drupal",
        		"fa-empire","fa-facebook","fa-facebook-f","fa-facebook-official","fa-facebook-square","fa-flickr","fa-forumbee","fa-foursquare",
        		"fa-ge","fa-git","fa-git-square","fa-github","fa-github-alt","fa-github-square","fa-gittip","fa-google","fa-google-plus",
        		"fa-google-plus-square","fa-google-wallet","fa-gratipay","fa-hacker-news","fa-html5","fa-instagram","fa-ioxhost","fa-joomla",
        		"fa-jsfiddle","fa-lastfm","fa-lastfm-square","fa-leanpub","fa-linkedin","fa-linkedin-square","fa-linux","fa-maxcdn","fa-meanpath",
        		"fa-medium","fa-openid","fa-pagelines","fa-paypal","fa-pied-piper","fa-pied-piper-alt","fa-pinterest","fa-pinterest-p",
        		"fa-pinterest-square","fa-qq","fa-ra","fa-rebel","fa-reddit","fa-reddit-square","fa-renren","fa-sellsy","fa-share-alt",
        		"fa-share-alt-square","fa-shirtsinbulk","fa-simplybuilt","fa-skyatlas","fa-skype","fa-slack","fa-slideshare","fa-soundcloud",
        		"fa-spotify","fa-stack-exchange","fa-stack-overflow","fa-steam","fa-steam-square","fa-stumbleupon","fa-stumbleupon-circle",
        		"fa-tencent-weibo","fa-trello","fa-tumblr","fa-tumblr-square","fa-twitch","fa-twitter","fa-twitter-square","fa-viacoin",
        		"fa-vimeo-square","fa-vine","fa-vk","fa-wechat","fa-weibo","fa-weixin","fa-whatsapp","fa-windows","fa-wordpress","fa-xing",
        		"fa-xing-square","fa-yahoo","fa-yelp","fa-youtube","fa-youtube-play","fa-youtube-square",
        		"fa-ambulance","fa-h-square","fa-heart","fa-heart-o","fa-heartbeat","fa-hospital-o","fa-medkit","fa-plus-square","fa-stethoscope",
        		"fa-user-md","fa-wheelchair"
        );
        var settings = $.extend({}, options);
        return this.each( function() {
        	element=this;
            if(!settings.buttonOnly && $(this).data("iconPicker")==undefined ){
            	$this=$(this).addClass("form-control");
            	$wraper=$("<div/>",{class:"input-group"});
            	$this.wrap($wraper);
            	$button=$("<span class=\"input-group-addon pointer\"><i class=\"glyphicon glyphicon-picture\"></i></span>");
            	$this.after($button);
            	(function(ele){
	            	$button.click(function(){
			       		createUI(ele);
			       		showList(ele,icons,0,30);
	            	});
	            })($this);
            	$(this).data("iconPicker",{attached:true});
            }
	        function createUI($element){
	        	$popup=$('<div/>',{
	        		css: {
		        		'top':$element.offset().top+$element.outerHeight()+6,
		        		'left':$element.offset().left
		        	},
		        	class:'popup-icons'
	        	});
	        	$popup.html('<div class="ip-control"> \
						          <ul> \
						            <li><a href="javascript:;" class="btn" data-dir="-1"><span class="glyphicon glyphicon-fast-backward"></span></a></li> \
						            <li><input type="text" class="ip-search glyphicon  glyphicon-search" placeholder="Search" /></li> \
						            <li><a href="javascript:;"  class="btn" data-dir="1"><span class="glyphicon glyphicon-fast-forward"></span></a></li> \
						          </ul> \
						      </div> \
						     <div class="list-icons"> </div> \
					         ').appendTo("body");
	        	$popup.addClass('dropdown-menu').show();
				$popup.mouseenter(function() {  mouseOver=true;  }).mouseleave(function() { mouseOver=false;  });
				var lastVal="", start_index=0,per_page=30;
	        	$(".ip-control .btn",$popup).click(function(e){
	                e.stopPropagation();
	                var dir=$(this).attr("data-dir");
	                start_index=start_index+per_page*dir;
	                if(start_index<0 || start_index>icons.length)start_index=0;
	                $(".list-icons>ul").empty();
	                showList($element,icons,start_index,start_index+per_page);
	            });
	        	
	        	$('.ip-control .ip-search',$popup).on("keyup",function(e){
	                if(lastVal!=$(this).val()){
	                    lastVal=$(this).val();
	                    if(lastVal==""){
	                    	showList($element,icons,0,30);
	                    }else{
	                    	showList($element, $(icons).map(function(i,v){ 
								            if(v.toLowerCase().indexOf(lastVal.toLowerCase())!=-1){return v} 
								        }).get(),0,30);
						}
	                }
	            });  
	        	$(document).mouseup(function (e){
				    if (!$popup.is(e.target) && $popup.has(e.target).length === 0) {
				        removeInstance();
				    }
				});
	        }
	        function removeInstance(){
	        	$(".popup-icons").remove();
	        }
	        function showList($element,arrLis,start_Index,end_Index){
	        	$ul=$("<ul>");
	        	for (var i=start_Index;i<arrLis.length && i<end_Index;i++) {
	        		$ul.append("<li><a href=\"javascript:;\" title=\""+i+" "+arrLis[i]+"\"><span class=\""+(arrLis[i].split("-")[0])+" "+arrLis[i]+"\"></span></a></li>");
	        	};
	        	$(".list-icons",$popup).html($ul);
	        	$(".list-icons li a",$popup).click(function(e){
	        		e.preventDefault();
	        		var title=$(this).children("span").attr("class");
	        		$element.val(title+" ");
	        		removeInstance();
	        	});
	        }
        });
    }
}(jQuery));
