<script type="text/javascript">
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">查看站点信息</h4>				
			</div>
			<div class="panel-body">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
									<label>网站ID：</label>
									${website.siteId!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>网站名称：</label>
									${website.siteName!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>网站简称：</label>
									${website.shortName!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>网站主域名：</label>
									${website.domain!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>存放路径：</label>
									${website.sitePath!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>其他域名：</label>
									${website.domainAlias!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>跳转域名：</label>
									${website.domainRedirect!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>站点描述：</label>
									${website.description!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>关键字：</label>
									${website.keywords!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>终审级别：</label>
									${website.finalStep!}
							</div>
							<div class="col-md-6 col-sm-6">
								<label>终审后：</label>
								${decode(website.afterCheck,1,"不能修改删除",2,"修改后退回",3,"修改后不变","未知")}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>联系人：</label>
									${website.contactPerson!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>手机号码：</label>
									${website.contactMobile!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>QQ号码：</label>
									${website.contactQQ!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>E-mail：</label>
									${website.contactEmail!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>服务截至日期：</label>
									${website.serviceEndDate!}
							</div>
							<div class="col-md-6 col-sm-6">
									<label>状态：</label>
									${decode(website.status,1,"正常",0,"审批中/暂停使用",-1,"冻结/逻辑删除","未知")}
							</div>
						</div>
					</fieldset>
			</div>
		</div>