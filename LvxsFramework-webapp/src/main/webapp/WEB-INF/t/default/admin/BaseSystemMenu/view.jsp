<script type="text/javascript">
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看系统菜单信息</h4>				
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>系统：</label>
					<span id="systemId">${baseSystemMenu.system.fullName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>父级菜单：</label>
					<span id="parentId">${baseSystemMenu.parent.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>排序编号：</label>
					<span id="sortNumber">${baseSystemMenu.sortNumber!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>菜单编号：</label>
					<span id="menuNumber">${baseSystemMenu.menuNumber!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>菜单名称：</label>
					<span id="name">${baseSystemMenu.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>链接URL：</label>
					<span id="url"><a href="${baseSystemMenu.url!}" target="_blank">${baseSystemMenu.url!}</a></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>目标窗口：</label>
					<span id="target">${baseSystemMenu.target!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>帮助文档URL：</label>
					<span id="helpUrl">${baseSystemMenu.helpUrl!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>图标：</label>
					<span id="iconSkin">${baseSystemMenu.iconSkin!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>简要说明：</label>
					<span id="briefDescription">${baseSystemMenu.briefDescription!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>状态：</label>
					<span id="status">${decode(baseSystemMenu.status+"","-1","逻辑删除","0","已禁用/待审核","1","正常","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>备注说明：</label>
					<pre id="remark">${baseSystemMenu.remark!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>其他设置JSON字符串：</label>
					<pre id="otherJson">${baseSystemMenu.otherJson!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建时间：</label>
					<span id="createTime">${baseSystemMenu.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建用户：</label>
					<span id="createUserId">${baseSystemMenu.createUser.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新时间：</label>
					<span id="updateTime">${baseSystemMenu.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新用户：</label>
					<span id="updateUserId">${baseSystemMenu.updateUser.realName!}</span>
				</div>
			</div>
		</fieldset>
	</div>
</div>