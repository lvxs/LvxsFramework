<script type="text/javascript">
/*
//表单提交前数据预处理逻辑，处理成功返回True，处理失败请返回False，处理失败时则不再提交
function beforeSubmit(){
	return true;
}*/
function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){
		//如果存在表单提交前数据预处理逻辑，则执行，否则自动忽略
		if(typeof(beforeSubmit)=="function"){
			//如果数据预处理逻辑返回False，则提示并且终止数据提交
			if(!beforeSubmit()){
				layer.msg("数据预处理出错！");
				return false;
			}
		}
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
} 
$(document).ready(function(){
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	$( "#form" ).validVal();
});
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">新增站点基础信息</h4>				
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="save">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label>网站名称：</label>
								<input type="text" name="cmsWebsite.siteName" class="form-control required" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>简短名称：</label>
								<input type="text" name="cmsWebsite.shortName" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>域名：</label>
								<input type="text" name="cmsWebsite.domain" class="form-control required" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>路径：</label>
								<input type="text" name="cmsWebsite.sitePath" class="form-control required" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>域名别名：</label>
								<input type="text" name="cmsWebsite.domainAlias" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>域名重定向：</label>
								<input type="text" name="cmsWebsite.domainRedirect" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>站点描述：</label>
								<input type="text" name="cmsWebsite.description" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>站点关键字：</label>
								<input type="text" name="cmsWebsite.keywords" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>终审级别：</label>
								<input type="number" name="cmsWebsite.finalStep" class="form-control  number" placeholder="" value="2" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>审核后：</label>
								<div class="radio-list" placeholder="1:不能修改删除;2:修改后退回;3:修改后不变">
									<label><input type="radio" name="cmsWebsite.afterCheck" value="1" class=" requiredgroup:afterCheck">不能修改删除</label>
									<label><input type="radio" name="cmsWebsite.afterCheck" value="2" class=" requiredgroup:afterCheck">修改后退回</label>
									<label><input type="radio" name="cmsWebsite.afterCheck" value="3" class=" requiredgroup:afterCheck">修改后不变</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>首页模板：</label>
								<input type="text" name="cmsWebsite.tplIndex" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>图片不存在时默认图片：</label>
								<input type="text" name="cmsWebsite.defImg" class="form-control " placeholder="" value="/r/default/images/no_picture.gif" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>用户登录地址：</label>
								<input type="text" name="cmsWebsite.loginUrl" class="form-control " placeholder="" value="/login" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>后台管理地址：</label>
								<input type="text" name="cmsWebsite.adminUrl" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>开启图片水印：</label>
								<input type="text" name="cmsWebsite.markOn" class="form-control " placeholder="" value="1" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>图片最小宽度：</label>
								<input type="number" name="cmsWebsite.markWidth" class="form-control  number" placeholder="" value="120" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>图片最小高度：</label>
								<input type="number" name="cmsWebsite.markHeight" class="form-control  number" placeholder="" value="120" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>图片水印：</label>
								<input type="text" name="cmsWebsite.markImage" class="form-control " placeholder="" value="/r/default/images/watermark.png" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>文字水印内容：</label>
								<input type="text" name="cmsWebsite.markContent" class="form-control " placeholder="" value="www.lvxs.cn" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>文字水印大小：</label>
								<input type="number" name="cmsWebsite.markSize" class="form-control  number" placeholder="" value="20" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>文字水印颜色：</label>
								<input type="text" name="cmsWebsite.markColor" class="form-control " placeholder="" value="#FF0000" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>水印透明度（0-100）：</label>
								<input type="number" name="cmsWebsite.markAlpha" class="form-control  number" placeholder="" value="50" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>水印位置(0-5)：</label>
								<input type="number" name="cmsWebsite.markPosition" class="form-control  number" placeholder="" value="1" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>x坐标偏移量：</label>
								<input type="number" name="cmsWebsite.markOffsetX" class="form-control  number" placeholder="" value="0" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>y坐标偏移量：</label>
								<input type="number" name="cmsWebsite.markOffsetY" class="form-control  number" placeholder="" value="0" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>下载防盗链md5混淆码：</label>
								<input type="text" name="cmsWebsite.downloadCode" class="form-control " placeholder="" value="LvxsCMS" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>下载有效时间（小时）：</label>
								<input type="number" name="cmsWebsite.downloadTime" class="form-control  number" placeholder="" value="12" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>邮件发送服务器：</label>
								<input type="email" name="cmsWebsite.emailHost" class="form-control  email" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>邮件发送编码：</label>
								<input type="email" name="cmsWebsite.emailEncoding" class="form-control  email" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>邮箱用户名：</label>
								<input type="email" name="cmsWebsite.emailUsername" class="form-control  email" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>邮箱密码：</label>
								<input type="password" name="cmsWebsite.emailPassword" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>邮箱发件人：</label>
								<input type="email" name="cmsWebsite.emailPersonal" class="form-control  email" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>开启邮箱验证：</label>
								<input type="text" name="cmsWebsite.emailValidate" class="form-control " placeholder="" value="0" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>只有终审才能浏览内容页：</label>
								<input type="text" name="cmsWebsite.viewOnlyChecked" class="form-control " placeholder="" value="0" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>联系人：</label>
								<input type="text" name="cmsWebsite.contactPerson" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>联系手机：</label>
								<input type="text" name="cmsWebsite.contactMobile" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>联系QQ：</label>
								<input type="text" name="cmsWebsite.contactQQ" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>联系E-mail：</label>
								<input type="email" name="cmsWebsite.contactEmail" class="form-control  email" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>服务到期时间：</label>
								<input type="datetime" name="cmsWebsite.serviceEndDate" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>状态：</label>
								<div class="radio-list" placeholder="-1逻辑删除、0审批中/暂停、1正常">
									<label>
										<input type="radio" name="cmsWebsite.status" value="1" checked="checked" class=" requiredgroup:status">
										正常
									</label>
									<label>
										<input type="radio" name="cmsWebsite.status" value="0" class=" requiredgroup:status">
										审批中/暂停使用
									</label>
									<label>
										<input type="radio" name="cmsWebsite.status" value="-1" class=" requiredgroup:status">
										冻结/逻辑删除
									</label>
								</div>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>