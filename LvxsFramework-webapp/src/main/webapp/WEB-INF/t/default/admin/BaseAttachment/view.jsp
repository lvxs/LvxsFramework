<script type="text/javascript">
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看附件表</h4>				
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>附件所属的系统：</label>
					<span id="systemId">${baseAttachment.system.fullName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>标题：</label>
					<pre id="title">${baseAttachment.title!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>路径：</label>
					<span id="path">${baseAttachment.path!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>mime：</label>
					<span id="mimeType">${baseAttachment.mimeType!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>附件的后缀：</label>
					<span id="suffix">${baseAttachment.suffix!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>类型：</label>
					<span id="type">${baseAttachment.type!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>标示：</label>
					<span id="flag">${baseAttachment.flag!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>经度：</label>
					<span id="lat">${baseAttachment.lat!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>纬度：</label>
					<span id="lng">${baseAttachment.lng!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>排序字段：</label>
					<span id="sortNumber">${baseAttachment.sortNumber!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>状态：</label>
					<span id="status">${decode(baseAttachment.status+"","-1","逻辑删除","0","已禁用/待审核","1","正常","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>备注说明：</label>
					<pre id="remark">${baseAttachment.remark!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>其他设置JSON字符串：</label>
					<pre id="otherJson">${baseAttachment.otherJson!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建时间：</label>
					<span id="createTime">${baseAttachment.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建用户：</label>
					<span id="createUserId">${baseAttachment.createUser.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新时间：</label>
					<span id="updateTime">${baseAttachment.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新用户：</label>
					<span id="updateUserId">${baseAttachment.updateUser.realName!}</span>
				</div>
			</div>
		</fieldset>
	</div>
</div>