<script type="text/javascript">
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">查看部门信息</h4>				
			</div>
			<div class="panel-body">
					<fieldset>
						<div class="row">
									<div class="col-md-6 col-sm-6">
										<label>系统：</label>
											 ${baseDepartment.system.fullName!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>父级部门：</label>
											 ${baseDepartment.parent.name!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>排序编号：</label>
											${baseDepartment.sortNumber!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>树形编号：</label>
											${baseDepartment.treeNumber!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>部门名称：</label>
											${baseDepartment.name!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>部门编号：</label>
											${baseDepartment.number!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>简要说明：</label>
											${baseDepartment.briefDescription!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>状态：</label>
											${decode(baseDepartment.status,1,"正常",0,"审批中/暂停使用",-1,"冻结/逻辑删除","未知")}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>备注说明：</label>
											<pre>${baseDepartment.remark!}</pre>
									</div>
									<div class="col-md-6 col-sm-6">
										<label>其他设置JSON字符串：</label>
											<pre>${baseDepartment.otherJson!}</pre>
									</div>
									<div class="col-md-6 col-sm-6">
										<label>创建时间：</label>
											 ${baseDepartment.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>创建用户：</label>
											 ${baseDepartment.createUser.realName!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>更新时间：</label>
											 ${baseDepartment.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>更新用户：</label>
											 ${baseDepartment.updateUser.realName!}
									</div>
						</div>
					</fieldset>
			</div>
		</div>