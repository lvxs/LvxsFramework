<script type="text/javascript">
/*
//表单提交前数据预处理逻辑，处理成功返回True，处理失败请返回False，处理失败时则不再提交
function beforeSubmit(){
	return true;
}*/
function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){
		//如果存在表单提交前数据预处理逻辑，则执行，否则自动忽略
		if(typeof(beforeSubmit)=="function"){
			//如果数据预处理逻辑返回False，则提示并且终止数据提交
			if(!beforeSubmit()){
				layer.msg("数据预处理出错！");
				return false;
			}
		}
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
} 
$(document).ready(function(){
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	$('select').select2();
	$( "#form" ).validVal();
});
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">${(''==baseAttachment.attachmentId!'')?'新增':'修改'}附件表</h4>				
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="${(''==baseAttachment.attachmentId!'')?'save':'update'}">
					<input type="hidden" name="baseAttachment.attachmentId" value="${baseAttachment.attachmentId! }">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label>附件所属的系统ID：</label>
								<select class="select form-control required" name="baseAttachment.systemId">
									<option value="">请选择</option>
									<%for(baseSystem in systemList){%>
									<option value="${baseSystem.id }" ${baseAttachment.systemId! ==baseSystem.id ?'selected="selected"'} >${baseSystem.shortName }</option>
									<%} %>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>标题：</label>
								<textarea name="baseAttachment.title" class="form-control " placeholder="" rows="5" cols="5" >${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAttachment.title!)}</textarea>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>路径：</label>
								<input type="text" name="baseAttachment.path" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAttachment.path!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>mime：</label>
								<input type="text" name="baseAttachment.mimeType" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAttachment.mimeType!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>附件的后缀：</label>
								<input type="text" name="baseAttachment.suffix" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAttachment.suffix!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>类型：</label>
								<input type="text" name="baseAttachment.type" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAttachment.type!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>标示：</label>
								<input type="text" name="baseAttachment.flag" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAttachment.flag!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>经度：</label>
								<input type="text" name="baseAttachment.lat" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAttachment.lat!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>纬度：</label>
								<input type="text" name="baseAttachment.lng" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAttachment.lng!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>排序字段：</label>
								<input type="number" name="baseAttachment.sortNumber" class="form-control  number" placeholder="" value="${baseAttachment.sortNumber!}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>状态：</label>
								<div class="radio-list" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
									<label><input type="radio" name="baseAttachment.status" value="-1" ${baseAttachment.status+'' == '-1'?'checked="checked"'} class=" requiredgroup:status">逻辑删除</label>
									<label><input type="radio" name="baseAttachment.status" value="0" ${baseAttachment.status+'' == '0'?'checked="checked"'} class=" requiredgroup:status">已禁用/待审核</label>
									<label><input type="radio" name="baseAttachment.status" value="1" ${baseAttachment.status+'' == '1'?'checked="checked"'} class=" requiredgroup:status">正常</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>备注说明：</label>
								<textarea name="baseAttachment.remark" class="form-control " placeholder="" rows="5" cols="5" >${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAttachment.remark!)}</textarea>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>