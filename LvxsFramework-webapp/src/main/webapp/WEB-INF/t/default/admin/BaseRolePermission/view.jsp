<script type="text/javascript">
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看角色权限</h4>				
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>菜单：</label>
					<span id="menuId">${baseRolePermission.menu.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>动作：</label>
					<span id="actionId">${baseRolePermission.action.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>角色：</label>
					<span id="roleId">${baseRolePermission.role.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>数据权限：</label>
					<span id="dataPermission">${decode(baseRolePermission.dataPermission+"","1","所有权限","2","本部门及子部门","3","本部门","4","仅本人","5","特定部门","6","特定人员","7","SQL语句","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>特定IDs：</label>
					<pre id="specialIds">${baseRolePermission.specialIds!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>附加Where查询条件：</label>
					<span id="whereSql">${baseRolePermission.whereSql!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>状态：</label>
					<span id="status">${decode(baseRolePermission.status+"","-1","逻辑删除","0","已禁用/待审核","1","正常","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>备注说明：</label>
					<pre id="remark">${baseRolePermission.remark!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>其他设置JSON字符串：</label>
					<pre id="otherJson">${baseRolePermission.otherJson!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建时间：</label>
					<span id="createTime">${baseRolePermission.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建用户：</label>
					<span id="createUserId">${baseRolePermission.createUser.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新时间：</label>
					<span id="updateTime">${baseRolePermission.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新用户：</label>
					<span id="updateUserId">${baseRolePermission.updateUser.realName!}</span>
				</div>
			</div>
		</fieldset>
	</div>
</div>