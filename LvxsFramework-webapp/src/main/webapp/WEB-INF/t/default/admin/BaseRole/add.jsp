<script type="text/javascript">
function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
} 
$(document).ready(function(){
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	$( "#form" ).validVal();
});
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">新增角色信息</h4>				
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="save">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label>角色名称：</label>
									<input type="text" name="baseRole.name" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>排序编号：</label>
									<input type="number" name="baseRole.sortNumber" class="form-control  number" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>所属系统：</label>
									<select class="select form-control required" name="baseRole.systemId">
										<option value="">请选择</option>
										<%for(baseSystem in systemList){%>
										<option value="${baseSystem.id }">${baseSystem.shortName }</option>
										<%} %>
									</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>状态：</label>
									<div class="radio-list" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
									<label>
										<input type="radio" name="baseRole.status" value="1" ${baseRole.status! ==1?'checked="checked"'} class=" requiredgroup:status">
										正常
									</label>
									<label>
										<input type="radio" name="baseRole.status" value="0" ${baseRole.status! ==0?'checked="checked"'}  class=" requiredgroup:status">
										审批中/暂停使用
									</label>
									<label>
										<input type="radio" name="baseRole.status" value="-1" ${baseRole.status! ==-1?'checked="checked"'}  class=" requiredgroup:status">
										冻结/逻辑删除
									</label>
									</div>
							</div>
							<div class="col-xs-12">
								<label>备注说明：</label>
									<textarea name="baseRole.remark" class="form-control  email}" placeholder="" rows="3" cols="5" ></textarea>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>