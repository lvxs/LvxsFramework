<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>后台管理登录 -- ${system.shortName! }</title>
	<link href="${basePath}/static/bird/assets/images/favicon.ico" rel="apple-touch-icon" type="image/png">
	<link href="${basePath}/static/bird/assets/images/favicon.ico" rel="icon" type="image/png">
	<link href="${basePath}/static/bird/assets/images/favicon.ico" rel="shortcut icon">
	<link href="${basePath}/static/bird/fonts/fonts.css" rel="stylesheet" type="text/css">
    <link href="${basePath}/static/bird/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/main.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/bootstrap-extended.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/plugins.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/color-system.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/media.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/lvxsBase.css" rel="stylesheet" type="text/css">
	<link href="${isNotEmpty(style)?basePath+'/static/bird/css/styles/'+style+'.css':basePath+'/static/bird/css/styles/lightblue.css'}" type="text/css" rel="stylesheet" id="style">
	<script src="${basePath}/static/bird/js/jquery.min.js"></script>
	<script src="${basePath}/static/bird/js/bootstrap.min.js"></script>	
	<script src="${basePath}/static/bird/js/fancybox.min.js"></script>	
	<script src="${basePath}/static/bird/js/app.js"></script>
	<script src="${basePath}/static/bird/js/forms/uniform.min.js"></script>
	<script src="${basePath}/static/layer/layer.js"></script>
	<script src="${basePath}/static/bird/js/jquery.validVal/jquery.validVal.js"></script>
	<script src="${basePath}/static/bird/js/jquery.md5.js"></script>
	<script>
	function doSubmit(){
		var form_data = $( "#loginForm" ).triggerHandler( "submitForm" );
		if(form_data){
			//客户端对密码进行加密，防止网络拦截泄露密码
			$('[name="password"]').val($.md5("密码加密验证字符串，防止MD5反向解密"+$('#password').val()));
			$.ajax({
				url:$("#loginForm").attr("action"),
				data:$("#loginForm").serialize(), 
				dataType : "json", 
				success : function(data) { 
					if(data.errorCode == 0){
						layer.msg("登录成功");
						if('${parameter.returnUrl!}'==''){
							location.href="${basePath}/admin/";
						} else {
							location.href="${parameter.returnUrl!}";
						}
					}else{
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("信息提交错误！");
				}
			});
		} else {
			layer.msg("数据验证错误！");
		}
	 	return false;
	} 
	$(function() {
		//检测登录页面是否显示在iFrame、Frame窗口或者弹出层中打开，防止登录后在弹出层里面重复显示导航主菜单
		if(window.top.location != '${request.requestURL}${isEmpty(request.queryString)?"":"?"+request.queryString}'){
			//如果顶级窗口的Url和登录页面的Url不一致，则表示当前页面在iFrame、Frame窗口或者弹出层中打开
			//在顶级窗口打开当前页面，并将登录后的跳转页面Url修改为顶级页面Url
			if('${request.queryString!}'.indexOf("returnUrl=")>=0){
				//如果登录页面Url已经包含跳转页面Url，则直接设置顶级页面Url为登录页面Url
				window.top.location = '${request.requestURL}?${request.queryString!}';
			} else if('${request.queryString!}'!=''){
				//如果登录页面Url已经包含其他参数，则设置顶级页面Url为登录页面Url+跳转页面Url。跳转页面Url为经过Uri编码的顶级页面Url
				window.top.location = '${request.requestURL}?${request.queryString!}'+"&returnUrl="+encodeURI(window.top.location);
			} else {
				//如果登录页面Url不包含其他参数，则设置顶级页面Url为登录页面Url+跳转页面Url。跳转页面Url为经过Uri编码的顶级页面Url
				window.top.location = '${request.requestURL}?returnUrl='+encodeURI(window.top.location);
			}
		}
		$('#loginForm').validVal();
		$('#loginForm').on("submit",doSubmit);
		// 主题切换
		$(".style").on("click", function(){
			var stylesheet = $(this).attr('theme').toLowerCase();
			if(stylesheet==null || stylesheet=="" ){
				$('#style').attr('href','${basePath}/static/bird/css/styles/green.css');
			} else if(stylesheet!=""){
				$('#style').attr('href','${basePath}/static/bird/css/styles/'+stylesheet+'.css');
			} else {
				$('#style').attr('href','');
			}
			$.get({
				url:"${basePath}/admin/saveStyle?style="+stylesheet,
				success : function(data) { 
					if(data.errorCode == 0){
					}else{
					}
				},
				error : function() {
				}
			});
		});
	});
	</script>
</head>
<body>
	<div class="page-container login-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content">
					<form id="loginForm" action="${basePath}/admin/loginCheck">
						<input type="hidden" name="returnUrl" value="${parameter.returnUrl!}">
						<div class="panel panel-body login-form">
							<div class="text-center mb-20" id="themeSelector">
								<ul class="dropdown-menu-themes">
									<ul class="clearfix">
										<li class="color-option style green" data-popup="tooltip" title="绿色" theme="green"></li>
										<li class="color-option style darkblue" data-popup="tooltip" title="深蓝" theme="darkblue"></li>
										<li class="color-option style lightblue" data-popup="tooltip" title="浅蓝" theme="lightblue"></li>
										<li class="color-option style default" data-popup="tooltip" title="深紫" theme="default"></li>					
									</ul>
								</ul>
							</div>
							<div class="text-center mb-20">
								<div class="icon-object border-slate-300 text-slate-300"><i class="fa fa-user"></i></div>
								<h5 class="content-group">系统登录<small class="display-block">请输入登录账号和密码</small></h5>
							</div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control required" placeholder="用户名/E-mail/手机号码" name="username">
								<div class="form-control-feedback">
									<i class="fa fa-user text-muted"></i>
								</div>
							</div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control required" placeholder="登录密码" id="password">
								<input type="hidden" name="password">
								<div class="form-control-feedback">
									<i class="fa fa-lock text-muted"></i>
								</div>
							</div>

							<div class="login-options">
								<div class="row">
									<div class="col-sm-6">
										<div class="checkbox ml-5">
											<label>
												<input type="checkbox" class="styled" name="rememberMe" value="1">
												记住账号密码，自动登录。
											</label>
										</div>
									</div>

									<div class="col-sm-6 text-right mt-10">
										<a href="">忘记密码</a>
									</div>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-info btn-lg btn-labeled btn-labeled-right btn-block"><b><i class="fa fa-sign-in"></i></b>登录</button>								
							</div>						
						</div>
					</form>
					<div class="footer text-muted">
						&copy; ${system.fullName! } 版权所有
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>