<link href="${basePath}/static/bird/assets/font-awesome/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/assets/icomoon/icomoon.css" media="all" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/animate.min.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/main.css" rel="stylesheet" type="text/css">	
<link href="${basePath}/static/bird/css/lvxsBase.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/bootstrap-extended.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/plugins.css" rel="stylesheet" type="text/css">
<link href="${basePath }/static/ztree/css/awesomeStyle/awesome.css" rel="stylesheet" type="text/css">
<link href="${isNotEmpty(style)?basePath+'/static/bird/css/styles/'+style+'.css':basePath+'/static/bird/css/styles/lightblue.css'}" type="text/css" rel="stylesheet" id="style">
<script src="${basePath }/static/bird/js/jquery.min.js" type="text/javascript"></script>
<script src="${basePath }/static/bird/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/static/bird/js/jquery.validVal/jquery.validVal.js" type="text/javascript"></script>
<script src="${basePath }/static/ztree/js/jquery.ztree.core.js" type="text/javascript"></script>
<script src="${basePath }/static/ztree/js/jquery.ztree.excheck.js" type="text/javascript"></script>
<script src="${basePath }/static/ztree/js/jquery.ztree.exedit.js" type="text/javascript"></script>
<script src="${basePath}/static/layer/layer.js" type="text/javascript"></script>
<script src="${basePath}/static/Bootstrap-icon-picker/js/iconPicker.js" type="text/javascript"></script>
<style type="text/css">
	label{margin:0 15px 0 0;}
</style>
<script type="text/javascript">
	var pageData = ${pageData};
	//目录树属性设置
	var setting = {
        view: {
			showLine: true,
            selectedMulti: true,
            addDiyDom: addDiyDom
        },
        check: {
            enable: true ,chkStyle: 'checkbox'
        },
        data: {
            simpleData: {
                enable: true,
    			idKey: "id",
    			pIdKey: "parentId",
    			url:"",
    			rootPId: 0
            }
        },
		callback: {
			onClick: zTreeOnClick,onCheck:menuTreeOnCheck
		}
    };
	//组织架构树属性设置
	var setting1 = {
	        view: {
				showLine: true,
	            selectedMulti: true
	        },
	        check: {
	            enable: true ,chkStyle: 'checkbox'
	        },
	        data: {
	            simpleData: {
	                enable: true,
	    			idKey: "id",
	    			pIdKey: "parentId",
	    			url:"",
	    			rootPId: 0
	            }
	        },
			callback: {
				onClick: departmentTreeOnClick,
				onCheck:departmentTreeOnCheck
			}
	    };
    var menuTree,departmentTree,userTree;
    var newCount = 1,notSave=false;
    function initMenuTree(){
    	$.ajax({
			url : "${basePath}/admin/baseSystemMenu/getTreeJson?systemId=" + ${baseSystem.id},
			dataType : "json",
			success : function(data) {
				if (data.data!=null) {
					menuTree = $.fn.zTree.init($("#menuTree"), setting, data.data);
				} else {
					menuTree = $.fn.zTree.init($("#menuTree"), setting, []);
				}
				addAction();
				checkMenuTree();
				menuTree.expandAll(true);
			},
			error : function() {
				layer.msg("目录树加载错误");
			}
		});
    }
    function initDepartmentTree(){
    	$.ajax({
    		url : "${basePath}/admin/baseDepartment/getTreeJson?systemId=${baseSystem.id!}",
			dataType : "json",
			success : function(data) {
				if (data.data!=null) {
					departmentTree = $.fn.zTree.init($("#departmentTree"), setting1, data.data);
					userTree = $.fn.zTree.init($("#userTree"), setting1, data.data);
				} else {
					departmentTree = $.fn.zTree.init($("#departmentTree"), setting1,[]);
					userTree = $.fn.zTree.init($("#userTree"), setting1,[]);
				}
				addUser();
				departmentTree.expandAll(true);
				userTree.expandAll(true);
			},
			error : function() {
				layer.msg("组织架构加载错误");
			}
		});
    }
    function addUser() {
    	$.ajax({
    		url : "${basePath}/admin/baseUser/getDataJson?systemId=" + ${baseSystem.id},
			dataType : "json",
			success : function(data) {
				if (data.data!=null) {
			    	for(var i=0;i<data.data.length;i++){
			    		var user = data.data[i];
			    		user.name = user.realName;
			    		var node = userTree.getNodeByParam("id", user.departmentId, null);
			    		userTree.addNodes(node,user);
			    	}
				} else {
				}
			},
			error : function() {
				layer.msg("用户数据加载错误");
			}
    	});
    }
    function addAction() {
    	for(var i=0;i<pageData.actionList.length;i++){
    		var action = pageData.actionList[i];
    		var node = menuTree.getNodeByParam("id", action.menuId, null);
    		if(node!=null){
    			menuTree.addNodes(node,action);
    		} else {
    		}
    	}
    }
    function checkMenuTree(){
    	menuTree.setting.check.chkboxType= { "Y": "p", "N": "s" };
    	for(var i=0;i<pageData.permissionList.length;i++){
    		var permission = pageData.permissionList[i];
    		if(permission.actionId>0){
        		var node = menuTree.getNodeByParam("id", permission.actionId, null);
        		if(node!=null){
        			menuTree.checkNode(node,true,false,true);
        			var menuNode = menuTree.getNodeByParam("id", permission.menuId, null);
        			menuTree.checkNode(menuNode,true,false,true);
        			$('[name="permission_'+node.id+'\"]').val(permission.dataPermission);
        			$('[name="specialIds_'+node.id+'\"]').val(permission.specialIds);
        			if(permission.otherJsonObject!=null && permission.otherJsonObject.specialNames!=null){
           				var specialNames = permission.otherJsonObject.specialNames;
           				if(permission.dataPermission==5){
                   			$('[name="departmentSel_'+node.id+'\"]').val(specialNames);	
           				}else if(permission.dataPermission==6){
                   			$('[name="userSel_'+node.id+'\"]').val(specialNames);	
           				} 
        			}
        		}
        		if(permission.dataPermission==5){
        			$('[name="departmentSel_'+node.id+'\"]').show();
        		} else if(permission.dataPermission==6){
        			$('[name="userSel_'+node.id+'\"]').show();
        		} 
    		} else {
        		var node = menuTree.getNodeByParam("id", permission.menuId, null);
        		if(node!=null){
        			menuTree.checkNode(node,true,true,true);
        		}
    		}
    	}
    	menuTree.setting.check.chkboxType= { "Y": "ps", "N": "ps" };
    }
    function menuTreeOnCheck(event, treeId, treeNode) {
    	var selectMenuIds = "",selectActionIds="";
   		var nodes = menuTree.getCheckedNodes(true);
   		for(var i in nodes){
   			if(nodes[i].menuId!=null){
   				selectActionIds+=","+nodes[i].id;
   			} else {
   				selectMenuIds+=","+nodes[i].id;
   			}
   		}
    	if(selectMenuIds.length>0){
        	$('#selectMenuIds').val(selectMenuIds.substr(1));
    	} else {
    		$('#selectMenuIds').val("");
    	}
    	if(selectActionIds.length>0){
        	$('#selectActionIds').val(selectActionIds.substr(1));
    	} else {
    		$('#selectActionIds').val("");
    	}
    }
    function departmentTreeOnCheck(event, treeId, treeNode) {
    	var selectIds = "",selectNames="";
    	if(treeId=="userTree"){
    		var nodes = userTree.getCheckedNodes(true);
    		for(var i in nodes){
    			if(nodes[i].realName!=null){
    				selectIds+=","+nodes[i].id;
    				selectNames+=","+nodes[i].realName;
    			}
    		}
    	} else {
    		var nodes = departmentTree.getCheckedNodes(true);
    		for(var i in nodes){
    			if(nodes[i].name!=null){
    				selectIds+=","+nodes[i].id;
    				selectNames+=","+nodes[i].name;
    			}
    		}
    	}
    	if(selectIds.length>0){
        	$('#specialIds_'+currentActionId).val(selectIds.substr(1));
        	if(treeId=="userTree"){
        		$('#userSel_'+currentActionId).val(selectNames.substr(1));
        	} else {
        		$('#departmentSel_'+currentActionId).val(selectNames.substr(1));
        	}
    	}
    }
    function zTreeOnClick(event, treeId, treeNode) {
    	menuTree.checkNode(treeNode,true,true,true);
	}    
    function departmentTreeOnClick(event, treeId, treeNode) {
    	var treeObject = $.fn.zTree.getZTreeObj(treeId);
    	treeObject.checkNode(treeNode,!treeNode.checked,true,true);
	}
    function changePermission(actionId){
		$('#departmentSel_'+actionId+'').hide();
		$('#userSel_'+actionId+'').hide();
		if($('[name="permission_'+actionId+'"]').val()==5){
			$('#departmentSel_'+actionId+'').show();
		} else if($('[name="permission_'+actionId+'"]').val()==6){
			$('#userSel_'+actionId+'').show();
		}
    }
    function addDiyDom(treeId, treeNode) {
    	var aObj = $("#" + treeNode.tId + "_a");
    	aObj.removeAttr("href");
    	if ($("#diyBtn_"+treeNode.id).length>0) return;
    	if(treeNode.menuId!=null && treeNode.actionType!=2 && treeNode.actionType!=11){
	    	var editStr = "<span id='diyBtn_space_" +treeNode.id+ "' ><select name=\"permission_"+treeNode.id+"\" onchange=\"changePermission("+treeNode.id+");\">"
	    		+ 	"<option value=\"1\" selected>所有数据</option><option  value=\"2\">本部门及所有子部门</option>"
	    		+ 	"<option  value=\"3\">本部门</option><option  value=\"4\">仅本人</option>"
	    		+ 	"<option  value=\"5\">特定部门</option><option  value=\"6\">特定人员</option></select>"
	    		+ "<input id=\"departmentSel_"+treeNode.id+"\" name=\"departmentSel_"+treeNode.id+"\" type=\"text\" readonly style=\"width:200px;display:none;\" onclick=\"showDepartmentSel("+treeNode.id+");\" placeholder=\"点击选择部门\" />"
	    		+ "<input id=\"userSel_"+treeNode.id+"\" name=\"userSel_"+treeNode.id+"\" type=\"text\" readonly style=\"width:200px;display:none;\" onclick=\"showUserSel("+treeNode.id+");\" placeholder=\"点击选择人员\" />"
	    		+ "<input id=\"specialIds_"+treeNode.id+"\" name=\"specialIds_"+treeNode.id+"\" value=\"\" type=\"hidden\" />"
	    		+ "<input name=\"menuId_"+treeNode.id+"\" value=\""+treeNode.menuId+"\" type=\"hidden\" /></span>";
	    	aObj.append(editStr);
    	} else if(treeNode.menuId!=null){
    		var editStr = "<span id='diyBtn_space_" +treeNode.id+ "' style=\"display:none;\" >"
    		+ "<input name=\"permission_"+treeNode.id+"\" value=\"1\" type=\"hidden\" >"
    		+ "<input name=\"departmentSel_"+treeNode.id+"\" type=\"hidden\" />"
    		+ "<input name=\"userSel_"+treeNode.id+"\" type=\"hidden\" />"
    		+ "<input name=\"specialIds_"+treeNode.id+"\" value=\"\" type=\"hidden\" />"
    		+ "<input name=\"menuId_"+treeNode.id+"\" value=\""+treeNode.menuId+"\" type=\"hidden\" /></span>";
    	aObj.append(editStr);
    	}
    }
    var currentActionId = "";
    function showDepartmentSel(actionId) {
    	currentActionId = actionId;
    	var specialIds = $('#specialIds_'+actionId).val();
		var cityObj = $("#departmentSel_"+actionId);
		departmentTree.checkAllNodes(false);
		departmentTree.setting.check.chkboxType= { "Y": "p", "N": "s" };
		var cityOffset = $("#departmentSel_"+actionId).offset();
		$("#departmentContent").show();
		$("#departmentContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
		$("body").bind("mousedown", onBodyDown);
		if(specialIds!=null && specialIds.trim().length>0){
			specialIds = specialIds.split(",");
			for(var i in specialIds){
				var node = departmentTree.getNodeByParam("id", specialIds[i], null);
	    		if(node!=null){
	    			departmentTree.checkNode(node,true,false,true);
	    		} else {
	    		}
			}
		}
		departmentTree.setting.check.chkboxType= { "Y": "ps", "N": "ps" };
    }
    function showUserSel(actionId) {
    	currentActionId = actionId;
    	var specialIds = $('#specialIds_'+actionId).val();
		var cityObj = $("#userSel_"+actionId);
		userTree.checkAllNodes(false);
		userTree.setting.check.chkboxType= { "Y": "p", "N": "s" };
		var cityOffset = $("#userSel_"+actionId).offset();
		$("#userContent").show();
		$("#userContent").css({left:cityOffset.left + "px", top:cityOffset.top + cityObj.outerHeight() + "px"}).slideDown("fast");
		$("body").bind("mousedown", onBodyDown);
		if(specialIds!=null && specialIds.trim().length>0){
			specialIds = specialIds.split(",");
			for(var i in specialIds){
				var node = userTree.getNodeByParam("id", specialIds[i], null);
	    		if(node!=null){
	    			userTree.checkNode(node,true,false,true);
	    		} else {
	    		}
			}
		}
		userTree.setting.check.chkboxType= { "Y": "ps", "N": "ps" };
    }
	function hideMenu() {
		$("#departmentContent").fadeOut("fast");
		$("#userContent").fadeOut("fast");
		$("body").unbind("mousedown", onBodyDown);
	}
	function onBodyDown(event) {
		if (!(event.target.id == "departmentSel_"+currentActionId || $(event.target).parents("#departmentContent").length>0
				||event.target.id == "userSel_"+currentActionId || $(event.target).parents("#userContent").length>0)) {
			hideMenu();
		}
	}
    function doSubmit() {
		var form_data = $("#form").triggerHandler("submitForm");
		if (form_data) {
			$.post({
				url : $("#form").attr("action"),
				data : $("#form").serialize(),
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("保存成功",{time:1000}, function(){
							var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
							parent.layer.close(index);
						});
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("信息提交错误！");
				}
			});
		} else {
			layer.msg("数据验证错误！");
		}
		return false;
	}
    $(document).ready(function(){
    	initMenuTree();
    	initDepartmentTree();
    	$('#form').on("submit",doSubmit);
    	$( "#form" ).validVal();
    	$("#departmentKey").bind("propertychange", searchDepertmentNode).bind("input", searchDepertmentNode);
    	$("#userKey").bind("propertychange", searchUserNode).bind("input", searchUserNode);
    });
    function searchDepertmentNode(){
    	var keyWord = $("#departmentKey").val();
		updateNodes(departmentTree,false);
		if(keyWord!=""){
			var nodeList = departmentTree.getNodesByParamFuzzy("name", keyWord,null);
			for(var i in nodeList){
				nodeList[i].highlight = true;
				var liId = "#"+nodeList[i].tId+"";
				$(liId).addClass("highLight");
				if(i==0){
					$(liId).focus();
				}
			}
		}
		$("#departmentKey")[0].focus();
	}
    function searchUserNode(){
    	var keyWord = $("#userKey").val();
		updateNodes(userTree,false);
		if(keyWord!=""){
			var nodeList = userTree.getNodesByParamFuzzy("name", keyWord,null);
			for(var i in nodeList){
				nodeList[i].highlight = true;
				var liId = "#"+nodeList[i].tId+"";
				$(liId).addClass("highLight");
				if(i==0){
					$(liId).focus();
				}
			}
		}
		$("#userKey")[0].focus();
	}
	function updateNodes(treeObject,highlight) {
		var nodeList = treeObject.transformToArray(treeObject.getNodes());;
		for(var i in nodeList) {
			nodeList[i].highlight = highlight;
			if(highlight){
				$("#"+nodeList[i].tId+"").addClass("highLight");
			} else {
				$("#"+nodeList[i].tId+"").removeClass("highLight");
			}
		}
	}
</script>
<div id="departmentContent" class="departmentContent" style="display:none; position: absolute;z-index:999;background-color:#eee;">
	<input type="text" id="departmentKey" value="" placeholder="快速查找部门" style="width:200px;" />
	<ul id="departmentTree" class="ztree" style="margin-top:0; width:300px;height:300px;overflow:scroll;"></ul>
</div>
<div id="userContent" class="userContent" style="display:none; position: absolute;z-index:999;background-color:#eee;">
	<input type="text" id="userKey" value="" placeholder="快速查找人员" style="width:200px;" />
	<ul id="userTree" class="ztree" style="margin-top:0; width:300px;height:300px;overflow:scroll;"></ul>
</div>
<form id="form" class="panel panel-flat" action="${basePath }/admin/baseRolePermission/saveRolePermission" method="post">
	<input type="hidden" id="selectActionIds" name="selectActionIds" value="" />
	<input type="hidden" id="selectMenuIds" name="selectMenuIds" value="" />
	<input type="hidden"  name="roleId" value="${baseRole.id }" />
	<div class="panel-heading">
		<h4 class="panel-title">《${baseRole.name }》权限设置</h4>
	</div>
	<div class="panel-body">
		<div class="row">
			<div>
				<div class="panel .panel-white">
					<div class="panel-body sidebar" style="display:block;width:100%;">
						<ul id="menuTree" class="ztree"></ul>
					</div>
				</div>
			</div>
			<div class="form-wizard-actions  " style="margin:10px;">
				<input class="btn btn-default" id="validation-back" value="重置" type="reset">
				<input class="btn btn-info" id="validation-next" value="提交" type="submit">
			</div>
		</div>
	</div>
</form>