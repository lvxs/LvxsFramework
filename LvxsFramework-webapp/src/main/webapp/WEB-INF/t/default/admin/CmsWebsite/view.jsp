<script type="text/javascript">
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看站点基础信息</h4>				
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>网站名称：</label>
					<span id="siteName">${cmsWebsite.siteName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>简短名称：</label>
					<span id="shortName">${cmsWebsite.shortName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>域名：</label>
					<span id="domain">${cmsWebsite.domain!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>路径：</label>
					<span id="sitePath">${cmsWebsite.sitePath!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>域名别名：</label>
					<span id="domainAlias">${cmsWebsite.domainAlias!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>域名重定向：</label>
					<span id="domainRedirect">${cmsWebsite.domainRedirect!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>站点描述：</label>
					<span id="description">${cmsWebsite.description!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>站点关键字：</label>
					<span id="keywords">${cmsWebsite.keywords!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>终审级别：</label>
					<span id="finalStep">${cmsWebsite.finalStep!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>审核后：</label>
					<span id="afterCheck">${decode(cmsWebsite.afterCheck+"","1","不能修改删除","2","修改后退回","3","修改后不变","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>首页模板：</label>
					<span id="tplIndex">${cmsWebsite.tplIndex!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>图片不存在时默认图片：</label>
					<span id="defImg">${cmsWebsite.defImg!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>用户登录地址：</label>
					<span id="loginUrl">${cmsWebsite.loginUrl!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>后台管理地址：</label>
					<span id="adminUrl">${cmsWebsite.adminUrl!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>开启图片水印：</label>
					<span id="markOn">${cmsWebsite.markOn!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>图片最小宽度：</label>
					<span id="markWidth">${cmsWebsite.markWidth!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>图片最小高度：</label>
					<span id="markHeight">${cmsWebsite.markHeight!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>图片水印：</label>
					<span id="markImage">${cmsWebsite.markImage!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>文字水印内容：</label>
					<span id="markContent">${cmsWebsite.markContent!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>文字水印大小：</label>
					<span id="markSize">${cmsWebsite.markSize!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>文字水印颜色：</label>
					<span id="markColor">${cmsWebsite.markColor!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>水印透明度（0-100）：</label>
					<span id="markAlpha">${cmsWebsite.markAlpha!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>水印位置(0-5)：</label>
					<span id="markPosition">${cmsWebsite.markPosition!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>x坐标偏移量：</label>
					<span id="markOffsetX">${cmsWebsite.markOffsetX!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>y坐标偏移量：</label>
					<span id="markOffsetY">${cmsWebsite.markOffsetY!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>下载防盗链md5混淆码：</label>
					<span id="downloadCode">${cmsWebsite.downloadCode!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>下载有效时间（小时）：</label>
					<span id="downloadTime">${cmsWebsite.downloadTime!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>邮件发送服务器：</label>
					<span id="emailHost"><a href="mailto:${cmsWebsite.emailHost!}">${cmsWebsite.emailHost!}</a></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>邮件发送编码：</label>
					<span id="emailEncoding"><a href="mailto:${cmsWebsite.emailEncoding!}">${cmsWebsite.emailEncoding!}</a></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>邮箱用户名：</label>
					<span id="emailUsername"><a href="mailto:${cmsWebsite.emailUsername!}">${cmsWebsite.emailUsername!}</a></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>邮箱密码：</label>
					<span>**********(已加密)</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>邮箱发件人：</label>
					<span id="emailPersonal"><a href="mailto:${cmsWebsite.emailPersonal!}">${cmsWebsite.emailPersonal!}</a></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>开启邮箱验证：</label>
					<span id="emailValidate">${cmsWebsite.emailValidate!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>只有终审才能浏览内容页：</label>
					<span id="viewOnlyChecked">${cmsWebsite.viewOnlyChecked!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>联系人：</label>
					<span id="contactPerson">${cmsWebsite.contactPerson!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>联系手机：</label>
					<span id="contactMobile">${cmsWebsite.contactMobile!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>联系QQ：</label>
					<span id="contactQQ">${cmsWebsite.contactQQ!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>联系E-mail：</label>
					<span id="contactEmail"><a href="mailto:${cmsWebsite.contactEmail!}">${cmsWebsite.contactEmail!}</a></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>服务到期时间：</label>
					<span id="serviceEndDate">${cmsWebsite.serviceEndDate,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建时间：</label>
					<span id="createTime">${cmsWebsite.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>状态：</label>
					<span id="status">${decode(cmsWebsite.status,1,"正常",0,"审批中/暂停使用",-1,"冻结/逻辑删除","未知")}</span>
				</div>
			</div>
		</fieldset>
	</div>
</div>