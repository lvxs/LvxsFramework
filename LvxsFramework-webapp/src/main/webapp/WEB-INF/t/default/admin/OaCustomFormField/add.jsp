<script type="text/javascript">

//表单提交前数据预处理逻辑，处理成功返回True，处理失败请返回False，处理失败时则不再提交
function beforeSubmit(){
	if($('#displayList:checked').val()=="1"){
		$('[name="oaCustomFormField.displayList"]').val("1");
	} else {
		$('[name="oaCustomFormField.displayList"]').val("0");
	}
	if($('#displayAdd:checked').val()=="1"){
		$('[name="oaCustomFormField.displayAdd"]').val("1");
	} else {
		$('[name="oaCustomFormField.displayAdd"]').val("0");
	}
	if($('#displayEdit:checked').val()=="1"){
		$('[name="oaCustomFormField.displayEdit"]').val("1");
	} else {
		$('[name="oaCustomFormField.displayEdit"]').val("0");
	}
	if($('#displayView:checked').val()=="1"){
		$('[name="oaCustomFormField.displayView"]').val("1");
	} else {
		$('[name="oaCustomFormField.displayView"]').val("0");
	}
	return true;
}

function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){		
		//如果存在表单提交前数据预处理逻辑，则执行，否则自动忽略
		if(typeof(beforeSubmit)=="function"){
			//如果数据预处理逻辑返回False，则提示并且终止数据提交
			if(!beforeSubmit()){
				layer.msg("数据预处理出错！");
				return false;
			}
		}
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
} 

$(document).ready(function(){
	var formId = parseInt('${parameter.formId!}');
	if(isNaN(formId) || formId<1){
		layer.alert("非法操作！",{icon:2},function(){
			if(parent.layer!=null){
				var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
				if(index!=null){
					parent.layer.close(index);
				}else{
					layer.closeAll();
				}
			} else {
				layer.closeAll();
			}
		});
	}
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	$( "#form" ).validVal();
});

function changeFieldType(){
	var fieldType = $('[name="oaCustomFormField.fieldType"]').val();
	if(fieldType==1){
		$('#textLength').show();
	} else {
		$('#textLength').hide();
	}
}
function changeInputType(){
	var inputType = $('[name="oaCustomFormField.inputType"]').val();
	if(inputType==9 || inputType==10 || inputType==11 || inputType==12 ){
		$('#optionType').show();
		$('#optionValue').show();
	} else {
		$('#optionType').hide();
		$('#optionValue').hide();
	}
}
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">新增自定义表单字段</h4>
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="save">
					<input type="hidden" name="oaCustomFormField.formId" value="${parameter.formId }" >
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label>字段名称：</label>
								<input type="text" name="oaCustomFormField.name" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>物理字段名称：</label>
								<input type="text" name="oaCustomFormField.physicalName" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>字段类型：</label>
								<select name="oaCustomFormField.fieldType" class="form-control " placeholder="请选择" onchange="changeFieldType();">
									<option value="1" selected>文本</option>
									<option value="2" >大文本</option>
									<option value="3" >日期</option>
									<option value="4" >日期时间</option>
									<option value="5" >整型</option>
									<option value="6" >大整型</option>
									<option value="7" >数值</option>
									<option value="8" >自增整型</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6" id="textLength">
								<label>字符串最大长度：</label>
								<input type="number" name="oaCustomFormField.textLength" class="form-control  number" placeholder="" value="200" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>是否必填：</label>
								<div class="radio-list" placeholder="0:选填;1:必填;">
									<label><input type="radio" name="oaCustomFormField.isRequired" value="0" checked class=" requiredgroup:isRequired">选填</label>
									<label><input type="radio" name="oaCustomFormField.isRequired" value="1" class=" requiredgroup:isRequired">必填</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>默认值：</label>
								<input type="text" name="oaCustomFormField.defaultValue" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>输入框类型：</label>
								<select name="oaCustomFormField.inputType" class="form-control " placeholder="请选择" onchange="changeInputType();">
									<option value="1" selected>文本框</option>
									<option value="2" >大文本框</option>
									<option value="3" >日期</option>
									<option value="4" >日期时间</option>
									<option value="5" >Email</option>
									<option value="6" >密码</option>
									<option value="7" >数值</option>
									<option value="8" >整型</option>
									<option value="9" >单选框</option>
									<option value="10" >复选框</option>
									<option value="11" >单选下拉列表</option>
									<option value="12" >多选下拉列表</option>
									<option value="13" >HTML编辑器</option>
									<option value="14" >附件上传</option>
									<option value="15" >图片上传</option>
									<option value="16" >部门选择</option>
									<option value="17" >用户选择</option>
									<option value="18" >隐藏域</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6" id="optionType" style="display:none;">
								<label>可选项类型：</label>
								<select name="oaCustomFormField.optionType" class="form-control " placeholder="请选择">
									<option value="1" >固定值列表</option>
									<option value="2" >查询SQL语句</option>
									<option value="3" >JSON数据源</option>
									<option value="4" >XML数据源</option>
									<option value="5" >系统字典</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6" id="optionValue" style="display:none;">
								<label>可选项值：</label>
								<input type="text" name="oaCustomFormField.optionValue" class="form-control " placeholder="固定至列表使用“1:文本框；2:大文本框；3:日期”格式说明描述；查询SQL语句直接写SQL语句；JSON数据源和XML数据源填写数据源URL；系统字典表填写字典表关键字" value="${oaCustomFormField.optionValue!}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>附加的js和css样式：</label>
								<input type="text" name="oaCustomFormField.jsAndCss" class="form-control " placeholder="附加的JavaScript事件和Css样式" value=' oninput="" onchange="" onfocus="" onblur="" style="" class=""' >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>是否显示：</label>
								<div class="checkbox-list" placeholder="1:显示;0:隐藏;">
									<label><input type="checkbox" id="displayList" value="1" checked="checked">列表显示</label>
									<input type="hidden" name="oaCustomFormField.displayList" value="1" >
									<label><input type="checkbox" id="displayAdd" value="1" checked="checked">新增显示</label>
									<input type="hidden" name="oaCustomFormField.displayAdd" value="1" >
									<label><input type="checkbox" id="displayEdit" value="1" checked="checked">修改显示</label>
									<input type="hidden" name="oaCustomFormField.displayEdit" value="1" >
									<label><input type="checkbox" id="displayView" value="1" checked="checked">查看显示</label>
									<input type="hidden" name="oaCustomFormField.displayView" value="1" >
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>排序编号：</label>
								<input type="number" name="oaCustomFormField.sortNumber" class="form-control  number" placeholder="数值越小，排序越靠前" value="100" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>输入框宽度：</label>
								<select name="oaCustomFormField.inputColSpan" class="form-control " placeholder="请选择">
									<option value="12" >100%宽度</option>
									<option value="9" >75%宽度</option>
									<option value="8" >66%宽度</option>
									<option value="6" selected>50%宽度</option>
									<option value="4" >33%宽度</option>
									<option value="3" >25%宽度</option>
									<option value="11" >11/12宽度</option>
									<option value="10" >5/6宽度</option>
									<option value="7" >7/12宽度</option>
									<option value="5" >5/12宽度</option>
									<option value="2" >1/6宽度</option>
									<option value="1" >1/12宽度</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>备注说明：</label>
								<textarea name="oaCustomFormField.remark" class="form-control" placeholder="" rows="3" cols="5" ></textarea>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>状态：</label>
								<div class="radio-list" placeholder="1:正常;0:审批中;-1:逻辑删除">
									<label><input type="radio" name="oaCustomFormField.status" value="1" checked="checked" class=" requiredgroup:status">正常</label>
									<label><input type="radio" name="oaCustomFormField.status" value="0" class=" requiredgroup:status">审批中</label>
									<label><input type="radio" name="oaCustomFormField.status" value="-1" class=" requiredgroup:status">逻辑删除</label>
								</div>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>