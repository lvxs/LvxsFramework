<% 
var contentBody = {
	%>
	<div class="content">
		<div class="row sortable-heading">
			<div class="col-md-6 col-sm-6">
				<div class="panel panel-flat" id="daiBanShiYi">
					<div class="panel-heading">
						<h4 class="panel-title cursor-move">待办事宜</h4>
						<div class="heading-elements">
							<ul class="icons-list">								
								<li><a data-action="move"></a></li>
								<li><a class="close" data-action="close"></a></li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-condensed table-striped">
								<thead>
									<tr>
										<th width="45">序号</th>
										<th>名称</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="right">1</td>
										<td>【审批】请假单</td>
									</tr>
									<tr>
										<td align="right">2</td>
										<td>【填写】部门工作报表</td>
									</tr>
									<tr>
										<td align="right">3</td>
										<td>【填写】月度报销申请</td>
									</tr>
									<tr>
										<td align="right">4</td>
										<td>【审批】xxx工作周报</td>
									</tr>
									<tr>
										<td align="right">5</td>
										<td>【审批】差旅报销单</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-flat" id="jiShiXiaoXi">
					<div class="panel-heading">
						<h4 class="panel-title cursor-move">即时消息</h4>
						<div class="heading-elements">
							<ul class="icons-list">								
								<li><a data-action="move"></a></li>
								<li><a class="close" data-action="close"></a></li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-condensed table-striped">
								<thead>
									<tr>
										<th width="45">序号</th>
										<th>标题</th>
										<th>发件人</th>
										<th>发件时间</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="right">1</td>
										<td>下班后去九九九KTV唱歌</td>
										<td>明晓丽</td>
										<td>12-25 15:30</td>
									</tr>
									<tr>
										<td align="right">2</td>
										<td>周末一块去打羽毛球</td>
										<td>齐晓军</td>
										<td>12-25 14:20</td>
									</tr>
									<tr>
										<td align="right">3</td>
										<td>JFinal出新版本了，赶紧看看</td>
										<td>明晓丽</td>
										<td>12-25 11:30</td>
									</tr>
									<tr>
										<td align="right">4</td>
										<td>张艺谋的《长城》可以下载了</td>
										<td>李卫国</td>
										<td>12-25 11:05</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="panel panel-flat" id="anQuanRiZhi">
					<div class="panel-heading">
						<h4 class="panel-title cursor-move">安全日志</h4>
						<div class="heading-elements">
							<ul class="icons-list">								
								<li><a data-action="move"></a></li>
								<li><a class="close" data-action="close"></a></li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-condensed table-striped">
								<thead>
									<tr>
										<th width="45">序号</th>
										<th>登录时间</th>
										<th>IP地址</th>
										<th>退出时间</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="right">1</td>
										<td>2016-12-25 18:30:21</td>
										<td>192.168.1.165</td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="panel panel-flat" id="tongZhiGongGao">
					<div class="panel-heading">
						<h4 class="panel-title cursor-move">通知公告</h4>
						<div class="heading-elements">
							<ul class="icons-list">								
								<li><a data-action="move"></a></li>
								<li><a class="close" data-action="close"></a></li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-condensed table-striped">
								<thead>
									<tr>
										<th width="45">序号</th>
										<th>标题</th>
										<th>发布时间</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="right">1</td>
										<td>12月31日之前提交2016年的财务报销单</td>
										<td>12-25 15:30</td>
									</tr>
									<tr>
										<td align="right">2</td>
										<td>雾霾天户外作业暂停通知</td>
										<td>12-25 14:20</td>
									</tr>
									<tr>
										<td align="right">3</td>
										<td>严禁上班时间使用迅雷等下载工具</td>
										<td>12-25 11:30</td>
									</tr>
									<tr>
										<td align="right">4</td>
										<td>关于配合园区消防演练的通知</td>
										<td>11-05 11:05</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h4 class="panel-title cursor-move">可选组件</h4>
						<div class="heading-elements">
							<ul class="icons-list">								
								<li><a data-action="move"></a></li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-3 col-sm-3 smAnNiu" data-id="tongZhiGongGao" style="display:none;">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/9.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/9.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">通知公告</a></h6>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-3 smAnNiu" data-id="jiShiXiaoXi" style="display:none;">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/9.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/9.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">即时消息</a></h6>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-3 smAnNiu" data-id="daiBanShiYi" style="display:none;">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/9.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/9.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">待办事宜</a></h6>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-3 smAnNiu" data-id="anQuanRiZhi" style="display:none;">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/9.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/9.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">安全日志</a></h6>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-3">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/9.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/9.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">知识中心</a></h6>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-3">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/10.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/10.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">工作任务</a></h6>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-3">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/11.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/11.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">日程安排</a></h6>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-3">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/12.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/12.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">时政要闻</a></h6>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-3">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/1.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/1.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">生活常识</a></h6>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-sm-3">
								<div class="thumbnail">
									<div class="thumb">
										<img src="${basePath}/static/bird/assets/images/media/2.jpg" alt="">
										<div class="caption-overflow">
											<span>
												<a href="${basePath}/static/bird/assets/images/media/2.jpg" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="fa fa-plus"></i></a>
											</span>
										</div>
									</div>
									<div class="caption">
										<h6 class="no-margin"><a href="#" class="text-default">电子邮件</a></h6>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%
};
var scriptAndCss = {
	%>
	<script src="${basePath}/static/bird/js/jquery_ui/interactions.min.js"></script>
	<script src="${basePath}/static/bird/js/jquery_ui/touch.min.js"></script>
	<script type="text/javascript">
	$(function() {
		// Panel title movable
		$(".sortable-heading").sortable({
			connectWith: '.heading-sortable',
			items: '.panel',
			helper: 'original',
			cursor: 'move',
			handle: '.panel-title, [data-action=move]',
			revert: 100,
			containment: '.content-wrapper',
			forceHelperSize: true,
			placeholder: 'sortable-placeholder',
			forcePlaceholderSize: true,
			tolerance: 'pointer',
			start: function(e, ui){
				ui.placeholder.height(ui.item.outerHeight());
			}
		});
		$(".panel").on("click", ".close", function (e) {
			$(this).parents(".panel").addClass("animated zoomOutRight").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
				$(this).hide();
				$('.smAnNiu[data-id="'+$(this).attr("id")+'"]').show();
			});
		});
		$(".smAnNiu").on("click", "a", function (e) {
			var iidd = $(this).parents(".smAnNiu").attr("data-id");
			$('#'+iidd).removeClass("animated zoomOutRight");
			$('#'+iidd).show();
			$('#'+iidd).addClass("animated zoomInRight").one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
				$('.smAnNiu[data-id="'+$(this).attr("id")+'"]').hide();
			});
		});
	});
	function sortable(){
	}
	</script>
	<%
};%>
<% 
layout("_inc/_layout.jsp",{head:scriptAndCss,body:contentBody}){} %>