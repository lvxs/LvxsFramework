<script type="text/javascript">
function initUser(){
	$.ajax({
		url : "${basePath}/admin/baseUser/detailJSON?id=${parameter.id!}" ,
		dataType : "json",
		success : function(data) {
			if (data.id!=null && data.errorCode==null) {
				$('#userName').html(data.userName);
				$('#email a').html(data.email);
				$('#email a').attr("href","mailto:"+data.email);
				$('#mobile').html(data.mobile);
				$('#otherContact').html(data.otherContact);
				$('#nikename').html(data.nikename);
				$('#photoURL').html(data.photoURL);
				$('#realName').html(data.realName);
				var roleNames = "",departmentNames="",status="";
				for(var k in data.roleList){
					var role = data.roleList[k];
					if(k==0){
						roleNames = role.name;
					}else{
						roleNames+=","+ role.name;	
					}
				}
				$('#roleNames').html(roleNames);
				for(var k in data.departmentList){
					var department = data.departmentList[k];
					if(k==0){
						departmentNames = department.name;
					}else{
						departmentNames+=","+ department.name;	
					}
				}
				$('#departmentNames').html(departmentNames);
				if(data.status==1){status="正常";}
				else if(data.status==0){status="暂停使用";}
				else if(data.status==-1){status="逻辑删除";}
				$('#status').html(status);
				$('#remark').html(data.remark);
				$('#createUserName').html(data.createUser.realName);
				$('#updateUserName').html(data.updateUser.realName);
				$('#createTime').html(data.createTime);
				$('#updateTime').html(data.updateTime);
			} else {
				layer.msg(data.message);
			}
		},
		error : function() {
			layer.msg("用户信息加载错误");
		}
	});
}
$(document).ready(function(){
	initUser();
});
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看用户信息</h4>
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>用户名：</label><span id="userName"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>登录密码：</label><span>**********(已加密)</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>E-mail：</label><span id="email"><a href="mailto:"></a></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>手机：</label><span id="mobile"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>其他联系方式：</label><span id="otherContact"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>昵称：</label><span id="nikename"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>照片URL：</label><span id="photoURL"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>真实姓名：</label><span id="realName"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>角色：</label><span id="roleNames"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>部门：</label><span id="departmentNames"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>状态：</label>
					<span id="status"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>备注说明：</label>
					<pre id="remark"></pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>其他设置JSON字符串：</label>
					<pre id="otherJson"></pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建时间：</label><span id="createTime"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建用户：</label><span id="createUserName"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新时间：</label><span id="updateTime"></span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新用户：</label><span id="updateUserName"></span>
				</div>
			</div>
		</fieldset>
	</div>
</div>