<script type="text/javascript">
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看自定义表单字段</h4>				
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>表单：</label>
					<span id="formId">${oaCustomFormField.form.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>字段名称：</label>
					<span id="name">${oaCustomFormField.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>物理字段名称：</label>
					<span id="physicalName">${oaCustomFormField.physicalName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>字段类型：</label>
					<span id="fieldType">${decode(oaCustomFormField.fieldType+"","1","文本","2","大文本","3","日期","4","日期时间","5","整型","6","大整型","7","数值","8","自增整型","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>字符串长度：</label>
					<span id="textLength">${oaCustomFormField.textLength!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>是否必填：</label>
					<span id="isRequired">${decode(oaCustomFormField.isRequired+"","0","选填","1","必填","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>默认值：</label>
					<span id="defaultValue">${oaCustomFormField.defaultValue!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>输入框类型：</label>
					<span id="inputType">${decode(oaCustomFormField.inputType+"","1","文本框","2","大文本框","3","日期","4","日期时间","5","Email","6","密码","7","数值","8","整型","9","单选框","10","复选框","11","单选下拉列表","12","多选下拉列表","13","HTML编辑器","14","附件上传","15","图片上传","16","部门选择","17","用户选择","18","隐藏域","19","只读文本","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>附加的js和css样式：</label>
					<span id="jsAndCss">${oaCustomFormField.jsAndCss!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>可选项类型：</label>
					<span id="optionType">${decode(oaCustomFormField.optionType+"","0","不适用","1","固定值列表","2","查询SQL语句","3","JSON数据源","4","XML数据源","5","系统字典","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>可选项值：</label>
					<span id="optionValue">${oaCustomFormField.optionValue!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>列表页面是否显示：</label>
					<span id="displayList">${decode(oaCustomFormField.displayList+"","1","显示","0","隐藏","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>新增页面是否显示：</label>
					<span id="displayAdd">${decode(oaCustomFormField.displayAdd+"","1","显示","0","隐藏","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>修改页面是否显示：</label>
					<span id="displayEdit">${decode(oaCustomFormField.displayEdit+"","1","显示","0","隐藏","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>显示页面是否显示：</label>
					<span id="displayView">${decode(oaCustomFormField.displayView+"","1","显示","0","隐藏","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>排序编号：</label>
					<span id="sortNumber">${oaCustomFormField.sortNumber!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>输入框宽度：</label>
					<span id="inputColSpan">${decode(oaCustomFormField.inputColSpan+"","12","100%页宽","6","50%页宽","9","75%页宽","3","25%页宽","8","2/3页宽","4","1/3页宽","11","11/12页宽","10","5/6页宽","7","7/12页宽","5","5/12页宽","2","1/6页宽","1","1/12页宽","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>备注说明：</label>
					<pre id="remark">${oaCustomFormField.remark!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>状态：</label>
					<span id="status">${decode(oaCustomFormField.status+"","1","正常","0","审批中","-1","逻辑删除","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>其他设置JSON字符串：</label>
					<pre id="otherJson">${oaCustomFormField.otherJson!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建用户：</label>
					<span id="createUserId">${oaCustomFormField.createUser.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建时间：</label>
					<span id="createTime">${oaCustomFormField.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新用户：</label>
					<span id="updateUserId">${oaCustomFormField.updateUser.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新时间：</label>
					<span id="updateTime">${oaCustomFormField.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
			</div>
		</fieldset>
	</div>
</div>