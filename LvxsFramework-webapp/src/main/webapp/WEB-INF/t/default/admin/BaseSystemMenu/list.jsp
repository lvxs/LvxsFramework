<% 
var contentBody = {
	%>
	<div class="content">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">系统菜单信息管理</h4>
			</div>
			<div class="panel-body">
				各个子系统或者相关系统的基础信息

			</div>
			<div class="table-responsive">
				<table id="baseSystemMenu_tablelist"  class="table responsive nowrap table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr style="border:1px solid #888;">
							<th data-priority="1"  style="width:20px;" onclick="selectAll(this);" class="select-checkbox" data-orderable="false"><input type="checkbox" value="1" /></th>
							<th>序号</th>
							<th data-priority="10">系统</th>
						    <th data-priority="15">父级菜单ID</th>
						    <th data-priority="20">排序编号</th>
						    <th data-priority="25">菜单编号</th>
						    <th data-priority="30">菜单名称</th>
						    <th data-priority="35">链接URL</th>
						    <th data-priority="40">目标窗口</th>
						    <th data-priority="45">帮助文档URL</th>
						    <th data-priority="50">图标</th>
						    <th data-priority="55">简要说明</th>
						    <th data-priority="60">状态</th>
						    <th data-priority="65">备注说明</th>
						    <th data-priority="70">其他设置JSON字符串</th>
							<%/*<th data-priority="75">创建时间</th>*/%>
							<%/*<th data-priority="80">创建用户ID</th>*/%>
							<%/*<th data-priority="85">更新时间</th>*/%>
							<%/*<th data-priority="90">更新用户ID</th>*/%>
							<th style="width: 85px;" data-priority="2">操作</th>
							<th>展开</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	<%
};
var scriptAndCss = {
	%>
	<script type='text/javascript'>
	var _tablelist;
	function add() {
		$.ajax({
			url : "${basePath}/admin/baseSystemMenu/add",
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '新增系统菜单信息',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function edit(rowid) {
		$.ajax({
			url : "${basePath}/admin/baseSystemMenu/edit?id=" + rowid,
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '修改系统菜单信息',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function view(rowid) {
		$.ajax({
			url : "${basePath}/admin/baseSystemMenu/view?id=" + rowid,
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '查看系统菜单信息',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function del(rowid) {
		layer.confirm('确定要删除该记录吗？', {
			btn : [ '确定删除', '放弃删除' ]
		//按钮
		}, function() {
			$.ajax({
				url : "${basePath}/admin/baseSystemMenu/delete?id=" + rowid,
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("删除成功");
						location.reload();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("删除失败");
				}
			});
		}, function() {
		});
	}
	function batchDel() {
		if (_tablelist.rows({
			selected : true
		}).count() < 1) {
			layer.msg("请选择记录！");
			return false;
		}
		layer.confirm('确定要删除该记录吗？', {
			btn : [ '确定删除', '放弃删除' ]
		//按钮
		}, function() {
			var selectedIds = "";
			var rows = _tablelist.rows({
				selected : true
			});
			for (var i = 0; i < rows.count(); i++) {
				if (selectedIds.length > 0) {
					selectedIds += "," + rows.data()[i].id;
				} else {
					selectedIds += rows.data()[i].id;
				}
			}
			$.ajax({
				url : "${basePath}/admin/baseSystemMenu/delete",
				data : {
					id : selectedIds
				},
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("删除成功");
						location.reload();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("删除失败");
				}
			});
		}, function() {
		});
	}
	function selectAll(td) {
		if ($(td).find('input[type="checkbox"]').val() == "1") {
			_tablelist.rows().select();
			$(td).find('input[type="checkbox"]').val("0");
			$(td).find('input[type="checkbox"]')[0].checked = true;
		} else {
			_tablelist.rows().deselect();
			$(td).find('input[type="checkbox"]').val("1");
			$(td).find('input[type="checkbox"]')[0].checked = false;
		}
	}
	$(document).ready(function(){
		$.extend($.fn.dataTable.defaults, {
			responsive:{details:{type:'column',target:-1}},
			pages:5,
			pagingType:'full_numbers',
			autoWidth:true,
			colReorder:true,
			dom : 'fBtip',
			processing : true,
			buttons : [
				{text : '新增',action : add}, 'excel', 'print', 'colvis',
				'pageLength', {text : '删除',action : batchDel}
			],
			lengthMenu : [
					[ 10, 25, 50, 100, 200, -1 ],
					[ '10', '25', '50', '100', '200','显示所有记录' ] ],
			language:{
				buttons:{
					pageLength : {_ : "每页 %d 行",'-1' : "显示所有记录"},
					colvis : '选择显示列',print : '打印',excel : '导出Execl'
				},
				select:{rows:{_ : "选择了%d行",0:""}},
				search : '快速筛选: _INPUT_',
				searchPlaceholder : '输入关键字',
				lengthMenu : '每页_MENU_行',
				info : '第_PAGE_页，共_PAGES_页(_TOTAL_行)',
				infoEmpty : '没有符合条件的记录！',
				emptyTable : '没有符合条件的记录！',
				infoFiltered : ' - 筛选自_MAX_条记录',
				infoPostFix : '',
				zeroRecords : '没有匹配结果',
				paginate : {'first' : '首页','last' : '末页','next' : '下页','previous' : '上页'}
			}
		});
		$.fn.dataTable.Buttons.swfPath = '${basePath}/static/bird/js/datatables/extensions/swf/flashExport.swf';
		_tablelist = $('#baseSystemMenu_tablelist').DataTable({
			select : {style : 'multi'},
			"processing" : true,
			"serverSide" : true,
			ajax : {
				url : '${basePath}/admin/baseSystemMenu/searchData'
			},
			columns : [				{"class" : "select-checkbox","orderable" : false,"render" : function(data,type,row,meta) {	return "";}},
				{"class" : "text-right","orderable" : false,"render" : function(data,type,row,meta) {
						if (_tablelist != null && _tablelist.page.info() != null) {
							//服务器模式下获取分页信息，使用 DT 提供的 API 直接获取分页信息
							var page = _tablelist.page.info();
							//当前第几页，从0开始
							var pageno = page.page;
							//每页数据
							var length = page.length;
							//行号等于 页数*每页数据长度+行号
							return (pageno * length + meta.row + 1);
						} else {
							//如果获取不到分页信息
							return (meta.row + 1);
						}
					}
				},
				//{"data" : "id"},// id 菜单ID
				{"data" : "system.shortName"},// systemName 系统ID
				{"data" : "parentId"},// parentId 父级菜单ID
				{"data" : "sortNumber"},// sortNumber 排序编号
				{"data" : "menuNumber"},// menuNumber 菜单编号
				{"data" : "name"},// name 菜单名称
				{"data" : "url"},// url 链接URL
				{"data" : "target"},// target 目标窗口
				{"data" : "helpUrl"},// helpUrl 帮助文档URL
				{"data" : "iconSkin"},// iconSkin 图标
				{"data" : "briefDescription"},// briefDescription 简要说明
				{"data" : "status","render" : function(data,type,row,meta) {
					if(data=='-1')return "逻辑删除";
					if(data=='0')return "已禁用/待审核";
					if(data=='1')return "正常";
					return "";
				}},// status 状态
				{"data" : "remark"},// remark 备注说明
				{"data" : "otherJson"},// otherJson 其他设置JSON字符串
				//{"data" : "createTime","type":"date"},// createTime 创建时间
				//{"data" : "createUserId"},// createUserId 创建用户ID
				//{"data" : "updateTime","type":"date"},// updateTime 更新时间
				//{"data" : "updateUserId"},// updateUserId 更新用户ID
				{"data":"id","render":function(data,type,row,meta){
						return '<ul class="icons-list">'
								+ '<li><a href="${basePath}/admin/baseSystemMenu/view?id='+ data+ '" onclick="view(\''+ data+ '\');return false;" title="查看"><i class="fa fa-eye"></i></a></li>'
								+ '<li><a href="${basePath}/admin/baseSystemMenu/edit?id='+ data+ '" onclick="edit(\''+ data+ '\');return false;" title="修改"><i class="fa fa-edit"></i></a></li>'
								+ '<li><a href="${basePath}/admin/baseSystemMenu/delete?id='+ data+ '" onclick="del(\''+ data+ '\');return false;" title="删除"><i class="fa fa-trash"></i></a></li>'
								+ '</ul>';
					},
					"orderable" :false,
					"type" : "html",
					"width" : "30px"
				},
				{"data" : null,"class" : "control","orderable" : false,
					"render" : function(data,type,row,meta) {
						return "";
					}
				}],order : [ [ 2, 'desc' ] ]		});
	});
	</script>
	<%
};%>
<% 
layout("../_inc/_layout.jsp",{head:scriptAndCss,body:contentBody}){} %>