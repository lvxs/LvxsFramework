<% 
var contentBody = {
	%>
	<div class="content">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">《${form.name!}》字段管理</h4>
			</div>
			<div class="table-responsive">
				<table id="oaCustomFormField_tablelist"  class="table responsive nowrap table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr style="border:1px solid #888;">
							<th data-priority="1"  style="width:20px;" onclick="selectAll(this);" class="select-checkbox" data-orderable="false"><input type="checkbox" value="1" /></th>
							<th>序号</th>
						    <%/*<th data-priority="10">表单ID</th>*/%>
						    <th data-priority="15">字段名称</th>
						    <%/*<th data-priority="20">物理字段名称</th>*/%>
						    <th data-priority="25">字段类型</th>
						    <th data-priority="30">字符串长度</th>
						    <th data-priority="35">是否必填</th>
						    <th data-priority="40">默认值</th>
						    <th data-priority="45">输入框类型</th>
						    <%/*<th data-priority="50">附加的js和css样式</th>*/%>
						    <%/*<th data-priority="55">可选项类型</th>*/%>
						    <%/*<th data-priority="60">可选项值</th>*/%>
						    <th data-priority="65">列表页面是否显示</th>
						    <th data-priority="70">新增页面是否显示</th>
							<th data-priority="75">修改页面是否显示</th>
							<th data-priority="80">显示页面是否显示</th>
							<th data-priority="85">输入框宽度</th>
							<%/*<th data-priority="90">备注说明</th>*/%>
							<th data-priority="95">状态</th>
							<%/*<th data-priority="100">其他设置JSON字符串</th>*/%>
							<th data-priority="105">创建用户</th>
							<th data-priority="110">创建时间</th>
							<th data-priority="115">更新用户</th>
							<th data-priority="120">更新时间</th>
							<th style="width: 85px;" data-priority="2">操作</th>
							<th>展开</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	<%
};
var scriptAndCss = {
	%>
	<script type='text/javascript'>
	var _tablelist;
	function add() {
		$.ajax({
			url : "${basePath}/admin/oaCustomFormField/add?formId=${form.id!}",
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '新增自定义表单字段',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function edit(rowid) {
		$.ajax({
			url : "${basePath}/admin/oaCustomFormField/edit?id=" + rowid,
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '修改自定义表单字段',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function view(rowid) {
		$.ajax({
			url : "${basePath}/admin/oaCustomFormField/view?id=" + rowid,
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '查看自定义表单字段',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function del(rowid) {
		layer.confirm('确定要删除该记录吗？', {
			btn : [ '确定删除', '放弃删除' ]
		//按钮
		}, function() {
			$.ajax({
				url : "${basePath}/admin/oaCustomFormField/delete?id=" + rowid,
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("删除成功");
						location.reload();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("删除失败");
				}
			});
		}, function() {
		});
	}
	function batchDel() {
		if (_tablelist.rows({
			selected : true
		}).count() < 1) {
			layer.msg("请选择记录！");
			return false;
		}
		layer.confirm('确定要删除该记录吗？', {
			btn : [ '确定删除', '放弃删除' ]
		//按钮
		}, function() {
			var selectedIds = "";
			var rows = _tablelist.rows({
				selected : true
			});
			for (var i = 0; i < rows.count(); i++) {
				if (selectedIds.length > 0) {
					selectedIds += "," + rows.data()[i].id;
				} else {
					selectedIds += rows.data()[i].id;
				}
			}
			$.ajax({
				url : "${basePath}/admin/oaCustomFormField/delete",
				data : {
					id : selectedIds
				},
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("删除成功");
						location.reload();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("删除失败");
				}
			});
		}, function() {
		});
	}
	function selectAll(td) {
		if ($(td).find('input[type="checkbox"]').val() == "1") {
			_tablelist.rows().select();
			$(td).find('input[type="checkbox"]').val("0");
			$(td).find('input[type="checkbox"]')[0].checked = true;
		} else {
			_tablelist.rows().deselect();
			$(td).find('input[type="checkbox"]').val("1");
			$(td).find('input[type="checkbox"]')[0].checked = false;
		}
	}
	$(document).ready(function(){
		var formId = parseInt('${form.id!}');
		if(isNaN(formId) || formId<1){
			layer.alert("非法操作！",{icon:2},function(){
				if(parent.layer!=null){
					var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
					if(index!=null){
						parent.layer.close(index);
					}else{
						layer.closeAll();
					}
				} else {
					layer.closeAll();
				}
			});
		}
		$.extend($.fn.dataTable.defaults, {
			responsive : {
				details : {
					type : 'column',
					target : -1
				}
			},
			pages : 5,
			pagingType : 'full_numbers',
			autoWidth : true,
			colReorder : true,
			dom : 'fBtip',
			processing : true,
			buttons : [
				{text : '新增',action : add}, 'excel', 'print', 'colvis',
				'pageLength', {text : '删除',action : batchDel}
			],
			lengthMenu : [
					[ 10, 25, 50, 100, 200, -1 ],
					[ '10', '25', '50', '100', '200','显示所有记录' ] ],
			language : {
				buttons : {
					pageLength : {_ : "每页 %d 行",'-1' : "显示所有记录"},
					colvis : '选择显示列',print : '打印',excel : '导出Execl'
				},
				select : {
					rows : {_ : "选择了%d行",0 : ""	}
				},
				search : '快速筛选: _INPUT_',
				searchPlaceholder : '输入关键字',
				lengthMenu : '每页_MENU_行',
				info : '第_PAGE_页，共_PAGES_页(_TOTAL_行)',
				infoEmpty : '没有符合条件的记录！',
				emptyTable : '没有符合条件的记录！',
				infoFiltered : ' - 筛选自_MAX_条记录',
				infoPostFix : '',
				zeroRecords : '没有匹配结果',
				paginate : {'first' : '首页','last' : '末页','next' : '下页','previous' : '上页'}
			}
		});
		$.fn.dataTable.Buttons.swfPath = '${basePath}/static/bird/js/datatables/extensions/swf/flashExport.swf';
		_tablelist = $('#oaCustomFormField_tablelist').DataTable({
			select : {style : 'multi'},
			"processing" : true,
			"serverSide" : true,
			ajax : {
				url : '${basePath}/admin/oaCustomFormField/searchData?formId=${form.Id!}'
			},
			columns : [					{"class" : "select-checkbox","orderable" : false,"render" : function(data,type,row,meta) {	return "";}},
				{"class" : "text-right","orderable" : false,"render" : function(data,type,row,meta) {
						if (_tablelist != null && _tablelist.page.info() != null) {
							//服务器模式下获取分页信息，使用 DT 提供的 API 直接获取分页信息
							var page = _tablelist.page.info();
							//当前第几页，从0开始
							var pageno = page.page;
							//每页数据
							var length = page.length;
							//行号等于 页数*每页数据长度+行号
							return (pageno * length + meta.row + 1);
						} else {
							//如果获取不到分页信息
							return (meta.row + 1);
						}
					}
				},
				//{"data" : "id"},// id ID
				//{"data" : "formId"},// formId 表单ID
				{"data" : "name"},// name 字段名称
				//{"data" : "physicalName"},// physicalName 物理字段名称
				{"data" : "fieldType","render" : function(data,type,row,meta) {
					if(data==1)return "文本";
					if(data==2)return "大文本";
					if(data==3)return "日期";
					if(data==4)return "日期时间";
					if(data==5)return "整型";
					if(data==6)return "大整型";
					if(data==7)return "数值";
					if(data==8)return "自增整型";
					return "";
				}},// fieldType 字段类型
				{"data" : "textLength"},// textLength 字符串长度
				{"data" : "isRequired","render" : function(data,type,row,meta) {
					if(data==1)return "必填";
					if(data==0)return "选填";
					return "";
				}},// isRequired 是否必填
				{"data" : "defaultValue"},// defaultValue 默认值
				{"data" : "inputType","render" : function(data,type,row,meta) {
					if(data==1)return "文本框";
					if(data==2)return "大文本框";
					if(data==3)return "日期";
					if(data==4)return "日期时间";
					if(data==5)return "Email";
					if(data==6)return "密码";
					if(data==7)return "数值";
					if(data==8)return "整型";
					if(data==9)return "单选框";
					if(data==10)return "复选框";
					if(data==11)return "单选下拉列表";
					if(data==12)return "多选下拉列表";
					if(data==13)return "HTML编辑器";
					if(data==14)return "附件上传";
					if(data==15)return "图片上传";
					if(data==16)return "部门选择";
					if(data==17)return "用户选择";
					if(data==18)return "隐藏域";
					return "";
				}},// inputType 输入框类型
				//{"data" : "jsAndCss"},// jsAndCss 附加的js和css样式
				/* {"data" : "optionType","render" : function(data,type,row,meta) {
					if(data==1)return "固定值列表";
					if(data==2)return "查询SQL语句";
					if(data==3)return "JSON数据源";
					if(data==4)return "XML数据源";
					if(data==5)return "系统字典";
					return "";
				}}, */// optionType 可选项类型
				//{"data" : "optionValue"},// optionValue 可选项值
				{"data" : "displayList","render" : function(data,type,row,meta) {
					if(data==1)return "显示";
					if(data==0)return "隐藏";
					return "";
				}},// displayList 列表页面是否显示
				{"data" : "displayAdd","render" : function(data,type,row,meta) {
					if(data==1)return "显示";
					if(data==0)return "隐藏";
					return "";
				}},// displayAdd 新增页面是否显示
				{"data" : "displayEdit","render" : function(data,type,row,meta) {
					if(data==1)return "显示";
					if(data==0)return "隐藏";
					return "";
				}},// displayEdit 修改页面是否显示
				{"data" : "displayView","render" : function(data,type,row,meta) {
					if(data==1)return "显示";
					if(data==0)return "隐藏";
					return "";
				}},// displayView 显示页面是否显示
				{"data" : "inputColSpan","render" : function(data,type,row,meta) {
					if(data==1)return "1/12页宽";
					if(data==2)return "1/6页宽";
					if(data==3)return "25%页宽";
					if(data==4)return "1/3页宽";
					if(data==5)return "5/12页宽";
					if(data==6)return "50%页宽";
					if(data==7)return "7/12页宽";
					if(data==8)return "2/3页宽";
					if(data==9)return "75%页宽";
					if(data==10)return "5/6页宽";
					if(data==11)return "11/12页宽";
					if(data==12)return "100%页宽";
					return "";
				}},// inputColSpan 输入框宽度
				//{"data" : "remark"},// remark 备注说明
				{"data" : "status","render" : function(data,type,row,meta) {
					if(data==1)return "正常";
					if(data==0)return "暂停使用";
					if(data==-1)return "逻辑删除";
					return "";
				}},// status 状态
				//{"data" : "otherJson"},// otherJson 其他设置JSON字符串
				{"data" : "createUser.realName"},// createUserId 创建用户ID
				{"data" : "createTime","type":"date"},// createTime 创建时间
				{"data" : "updateUser.realName"},// updateUserId 更新用户ID
				{"data" : "updateTime","type":"date"},// updateTime 更新时间
				{"data" : "id",
					"render" : function(data,type,row,meta) {
						return '<ul class="icons-list">'
								+ '<li><a href="${basePath}/admin/oaCustomFormField/view?id='+ data+ '" onclick="view(\''+ data+ '\');return false;" title="查看"><i class="fa fa-eye"></i></a></li>'
								+ '<li><a href="${basePath}/admin/oaCustomFormField/edit?id='+ data+ '" onclick="edit(\''+ data+ '\');return false;" title="修改"><i class="fa fa-edit"></i></a></li>'
								+ '<li><a href="${basePath}/admin/oaCustomFormField/delete?id='+ data+ '" onclick="del(\''+ data+ '\');return false;" title="删除"><i class="fa fa-trash"></i></a></li>'
								+ '</ul>';
					},
					"orderable" :false,
					"type" : "html",
					"width" : "30px"
				},{"data" : null,"class" : "control","orderable" : false,
					"render" : function(data,type,row,meta) {
						return "";
					}
				}],order:[[ 2, 'desc' ]]});
	});
	</script>
	<%
};%>
<% 
layout("../_inc/_iframeLayout.jsp",{head:scriptAndCss,body:contentBody}){} %>