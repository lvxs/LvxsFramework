<script type="text/javascript">
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看页面动作</h4>				
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>系统菜单：</label>
					<span id="menuId">${baseAction.menu.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>排序编号：</label>
					<span id="sortNumber">${baseAction.sortNumber!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>名称：</label>
					<span id="name">${baseAction.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>动作类型：</label>
					<span id="actionType">${decode(baseAction.actionType+"","1","列表页面","2","新增页面","3","修改页面","4","查看页面","5","其他页面","11","保存","12","更新","13","删除","14","批量删除","15","查询结果列表JSON","16","其他操作","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>方法名：</label>
					<span id="methodName">${baseAction.methodName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>完整URI：</label>
					<span id="fullUri">${baseAction.fullUri!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>JavaScript方法：</label>
					<span id="jsMethod">${baseAction.jsMethod!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>链接URI：</label>
					<span id="linkUri">${baseAction.linkUri!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>状态：</label>
					<span id="status">${decode(baseAction.status+"","-1","逻辑删除","0","已禁用/待审核","1","正常","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>备注说明：</label>
					<pre id="remark">${baseAction.remark!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>其他设置JSON字符串：</label>
					<pre id="otherJson">${baseAction.otherJson!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建时间：</label>
					<span id="createTime">${baseAction.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建用户：</label>
					<span id="createUserId">${baseAction.createUser.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新时间：</label>
					<span id="updateTime">${baseAction.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新用户：</label>
					<span id="updateUserId">${baseAction.updateUser.realName!}</span>
				</div>
			</div>
		</fieldset>
	</div>
</div>