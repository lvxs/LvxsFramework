<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="${basePath}/static/bird/fonts/fonts.css" rel="stylesheet" type="text/css">
    <link href="${basePath}/static/bird/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="${basePath}/static/bird/assets/icomoon/icomoon.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/responsive.bootstrap.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/select.dataTables.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/select.bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/colReorder.bootstrap.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/main.css" rel="stylesheet" type="text/css">	
	<link href="${basePath}/static/bird/css/lvxsBase.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/bootstrap-extended.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/plugins.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/color-system.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/media.css" rel="stylesheet" type="text/css">
	<link href="${isNotEmpty(style)?basePath+'/static/bird/css/styles/'+style+'.css':basePath+'/static/bird/css/styles/lightblue.css'}" type="text/css" rel="stylesheet" id="style">
	<script src="${basePath}/static/bird/js/jquery.min.js"></script>
	<script src="${basePath}/static/bird/js/bootstrap.min.js"></script>	
	<script src="${basePath}/static/bird/js/fancybox.min.js"></script>
	<script src="${basePath}/static/layer/layer.js"></script>
	<script src="${basePath}/static/bird/js/datatables/datatables.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/responsive.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/col_reorder.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/fixed_header.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.bootstrap.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.colVis.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.flash.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.print.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/select.min.js"></script>
	<script src="${basePath}/static/bird/js/forms/select2.full.js"></script>
	<script src="${basePath}/static/bird/js/forms/uniform.min.js"></script>
	<script src="${basePath}/static/bird/js/forms/switchery.min.js"></script>
	<script src="${basePath}/static/bird/js/forms/switch.min.js"></script>
	<script src="${basePath}/static/bird/js/jquery.validVal/jquery.validVal.js"></script>
	<script type="text/javascript">
	var basePath = '${basePath}',pageUrl =window.location.pathname;
	window.UEDITOR_HOME_URL = "${basePath}/static/ueditor/";
	if(window.location.search!=null && window.location.search!=""){
		pageUrl+="?"+window.location.search;
	}
	</script>
	${head }
</head>
<body>
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<!-- Content area -->
				${body}
				<!-- /content area -->
			</div>
		</div>
	</div>
</body>
</html>