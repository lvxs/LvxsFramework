<script type="text/javascript">
/*
//表单提交前数据预处理逻辑，处理成功返回True，处理失败请返回False，处理失败时则不再提交
function beforeSubmit(){
	return true;
}*/
function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){
		//如果存在表单提交前数据预处理逻辑，则执行，否则自动忽略
		if(typeof(beforeSubmit)=="function"){
			//如果数据预处理逻辑返回False，则提示并且终止数据提交
			if(!beforeSubmit()){
				layer.msg("数据预处理出错！");
				return false;
			}
		}
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
} 
$(document).ready(function(){
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	$('select').select2();
	$( "#form" ).validVal();
});
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">${(''==baseRolePermission.id!'')?'新增':'修改'}角色权限</h4>				
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="${(''==baseRolePermission.id!'')?'save':'update'}">
					<input type="hidden" name="baseRolePermission.id" value="${baseRolePermission.id! }">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label>菜单ID：</label>
								<select class="select form-control required" name="baseRolePermission.menuId">
									<option value="">请选择</option>
									<option value="1" ${baseRolePermission.menuId! ==1?'selected'} >已选择的信息</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>动作ID：</label>
								<select class="select form-control required" name="baseRolePermission.actionId">
									<option value="">请选择</option>
									<option value="1" ${baseRolePermission.actionId! ==1?'selected'} >已选择的信息</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>角色ID：</label>
								<select class="select form-control required" name="baseRolePermission.roleId">
									<option value="">请选择</option>
									<%for(baseRole in roleList){%>
									<option value="${baseRole.id }" ${baseRolePermission.roleId! ==baseRole.id ?'selected="selected"'} >${baseRole.name }</option>
									<%} %>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>数据权限：</label>
								<select name="baseRolePermission.dataPermission" class="form-control " placeholder="请选择">
									<option value="1" ${baseRolePermission.dataPermission+'' == '1'?'selected="selected"'}>所有权限</option>
									<option value="2" ${baseRolePermission.dataPermission+'' == '2'?'selected="selected"'}>本部门及子部门</option>
									<option value="3" ${baseRolePermission.dataPermission+'' == '3'?'selected="selected"'}>本部门</option>
									<option value="4" ${baseRolePermission.dataPermission+'' == '4'?'selected="selected"'}>仅本人</option>
									<option value="5" ${baseRolePermission.dataPermission+'' == '5'?'selected="selected"'}>特定部门</option>
									<option value="6" ${baseRolePermission.dataPermission+'' == '6'?'selected="selected"'}>特定人员</option>
									<option value="7" ${baseRolePermission.dataPermission+'' == '7'?'selected="selected"'}>SQL语句</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>特定IDs：</label>
								<textarea name="baseRolePermission.specialIds" class="form-control " placeholder="dataPermission=5时代表特定部门IDs；dataPermission=6时代表特定人员IDs。多个ID之间使用半角逗号分隔" rows="5" cols="5" >${@org.cnitti.utils.EscapeUtils.escapeXml11(baseRolePermission.specialIds!)}</textarea>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>附加Where查询条件：</label>
								<input type="text" name="baseRolePermission.whereSql" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseRolePermission.whereSql!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>状态：</label>
								<div class="radio-list" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
									<label><input type="radio" name="baseRolePermission.status" value="-1" ${baseRolePermission.status+'' == '-1'?'checked="checked"'} class=" requiredgroup:status">逻辑删除</label>
									<label><input type="radio" name="baseRolePermission.status" value="0" ${baseRolePermission.status+'' == '0'?'checked="checked"'} class=" requiredgroup:status">已禁用/待审核</label>
									<label><input type="radio" name="baseRolePermission.status" value="1" ${baseRolePermission.status+'' == '1'?'checked="checked"'} class=" requiredgroup:status">正常</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>备注说明：</label>
								<textarea name="baseRolePermission.remark" class="form-control " placeholder="" rows="5" cols="5" >${@org.cnitti.utils.EscapeUtils.escapeXml11(baseRolePermission.remark!)}</textarea>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>