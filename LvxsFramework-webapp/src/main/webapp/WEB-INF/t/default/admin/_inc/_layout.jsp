<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>${webSite.siteName!(system.shortName!) }</title>
	<link href="${basePath}/static/bird/assets/images/favicon.ico" rel="icon" type="image/png">
	<link href="${basePath}/static/bird/assets/images/favicon.ico" rel="shortcut icon">

	<!-- Global stylesheets -->
	<link href="${basePath}/static/bird/fonts/fonts.css" rel="stylesheet" type="text/css">
    <link href="${basePath}/static/bird/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="${basePath}/static/bird/assets/icomoon/icomoon.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/responsive.bootstrap.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/select.dataTables.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/select.bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/colReorder.bootstrap.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/main.css" rel="stylesheet" type="text/css">	
	<link href="${basePath}/static/bird/css/lvxsBase.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/bootstrap-extended.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/plugins.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/color-system.css" rel="stylesheet" type="text/css">
	<link href="${basePath}/static/bird/css/media.css" rel="stylesheet" type="text/css">
	<link href="${isNotEmpty(style)?basePath+'/static/bird/css/styles/'+style+'.css':basePath+'/static/bird/css/styles/lightblue.css'}" type="text/css" rel="stylesheet" id="style">
	<link href="" type="text/css" rel="stylesheet" id="theme">
	<!-- /global stylesheets -->
	<!-- Core JS files -->	
	<script src="${basePath}/static/bird/js/jquery.min.js"></script>
	<script src="${basePath}/static/bird/js/bootstrap.min.js"></script>	
	<script src="${basePath}/static/bird/js/fancybox.min.js"></script>
	<script src="${basePath}/static/layer/layer.js"></script>
	<script src="${basePath}/static/bird/js/datatables/datatables.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/responsive.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/col_reorder.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/fixed_header.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.bootstrap.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.colVis.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.flash.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/buttons.print.min.js"></script>
	<script src="${basePath}/static/bird/js/datatables/extensions/select.min.js"></script>
	<script src="${basePath}/static/bird/js/forms/select2.full.js"></script>
	<script src="${basePath}/static/bird/js/forms/uniform.min.js"></script>
	<script src="${basePath}/static/bird/js/forms/switchery.min.js"></script>
	<script src="${basePath}/static/bird/js/forms/switch.min.js"></script>
	<script src="${basePath}/static/bird/js/jquery.validVal/jquery.validVal.js"></script>
	<script type="text/javascript">
	var basePath = '${basePath}',pageUrl =window.location.pathname;
	window.UEDITOR_HOME_URL = "${basePath}/static/ueditor/";
	if(window.location.search!=null && window.location.search!=""){
		pageUrl+="?"+window.location.search;
	}
	$(function() {
		$.ajax({
			url : "${basePath}/admin/baseSystemMenu/getTreeJson",
			dataType : "json",
			success : function(data) {
				if (data.data!=null) {
					$("#navigationTree").html(getTreeHtml(data.data,null));
			        var element = document.createElement("script");
			        element.src = "${basePath}/static/bird/js/app.js";
			        document.body.appendChild(element);
				}
			},
			error : function() {
				layer.msg("目录树加载错误");
			}
		});
		// 主题切换
		$(".style").on("click", function(){
			var stylesheet = $(this).attr('theme').toLowerCase();
			if(stylesheet==null || stylesheet==""){
				$('#style').attr('href','${basePath}/static/bird/css/styles/green.css');
			} else if(stylesheet!=""){
				$('#style').attr('href','${basePath}/static/bird/css/styles/'+stylesheet+'.css');
			} else {
				$('#style').attr('href','');
			}
			$.get({
				url:"${basePath}/admin/saveStyle?style="+stylesheet,
				success : function(data) { 
					if(data.errorCode == 0){
						layer.msg("保存成功");
					}else{
						layer.msg("保存失败");
					}
				},
				error : function() {
					layer.msg("保存失败！");
				}
			});
		});
		$("#logout").on("click", function(){
			layer.confirm('确定要安全退出么？', {
				  btn: ['确定','取消'] //按钮
				}, function(){
				  window.location.href=$('#logout').attr("href");
			});
			return false;
		});
	});
	function getTreeHtml(childrenArray,parentId){
		var html = "",currentLevel=1;
		for(var i=0;i<childrenArray.length;i++){
			var treeNode = childrenArray[i];
			if(treeNode.menuNumber.length/3==currentLevel && i>0){
				html+="</li>";
			} else {
				while(treeNode.menuNumber.length/3>currentLevel){
					html+="<ul>";
					currentLevel++;
				}
				while(treeNode.menuNumber.length/3<currentLevel) {
					html+="</ul></li>";
					currentLevel--;
				}
			}
			var target=treeNode.target;
			if(target!=null && target.trim().length>0 && target.trim()!="_sefl" ){
				target=" target=\""+target+"\"";
			} else {
				target="";
			}
			var url=treeNode.url;
			if(url!=null && url.trim().length>0 && url.trim().indexOf("//")<0 && url.trim().indexOf(":")<0){
				url=" href=\"${basePath}"+url+"\"";
			} else if(url!=null && url.trim().length>0){
				url=" href=\""+url+"\"";
			} else {
				url="";
			}
			var activeClass = "";
			if(pageUrl==(basePath+treeNode.url)){
				activeClass = " class=\"active\"";
			}
			html+="<li"+activeClass+"><a "+url+target+"><i class=\""+treeNode.iconSkin+"\"></i><span> "+treeNode.name+"</span></a>";
		}
		while(1<currentLevel) {
			html+="</ul></li>";
			currentLevel--;
		}
		return html;
	}
	</script>
	${head }
</head>
<body>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-header">
			<a class="sidebar-mobile-toggle visible-xs"><i class="fa fa-bars"></i></a>
			<a class="navbar-brand" href="${basePath}/" target="_blank">${webSite.shortName!(system.shortName!) }</a>
			<a class="sidebar-control sidebar-toggle hidden-xs pull-right"><i class="fa fa-bars"></i></a>
		</div>
		<ul class="navbar-right">
			<li class="user-info">
				<div class="user-head">
					<a href="#"><img src="${basePath}/static/bird/assets/images/users/user6.png" class="img-circle img-sm" alt=""></a>
				</div>
				<div class="user-body hidden-xs">
					<span class="user-heading text-semibold text-muted">${loginUser.realName!}</span>
					<span class="user-role text-size-mini text-muted"><%if(isNotEmpty(loginUser.roleList)){for(role in loginUser.roleList){if(roleLP.index>1)print("，");print(role.name);}}%></span>
				</div>
			</li>
			<li class="dropdown notifications">
				<a href="${basePath}/static/bird/index.htm" target="_blank" title="birdAdmin后台管理界面示例"><i class="fa fa-book"></i><span class="badge badge-info">b</span></a>
			</li>
			<li class="dropdown notifications">
				<a href="${basePath}/static/datatables/examples/index.html" target="_blank" title="datatables帮助文档"><i class="fa fa-book"></i><span class="badge badge-info">d</span></a>
			</li>
			<li class="dropdown notifications">
				<a href="${basePath}/static/beetl.html" target="_blank" title="beetl模板引擎帮助文档"><i class="fa fa-book"></i><span class="badge badge-info">B</span></a>
			</li>
			<li class="dropdown clearfix"> 
				<a class="dropdown-toggle" id="settings" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-cog"></i></a> 
				<ul class="dropdown-menu dropdown-menu-themes" aria-labelledby="settings"> 
					<li class="dropdown-header">主题样式</li>
					<ul class="clearfix">
						<li class="color-option style green" data-popup="tooltip" title="绿色" theme="green"></li>
						<li class="color-option style darkblue" data-popup="tooltip" title="深蓝" theme="darkblue"></li>
						<li class="color-option style lightblue" data-popup="tooltip" title="浅蓝" theme="lightblue"></li>
						<li class="color-option style default" data-popup="tooltip" title="深紫" theme="default"></li>					
					</ul>
				</ul> 
			</li>
			<li class="dropdown notifications">
				<a class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-bell"></i>		
					<span class="badge badge-info">5</span>
				</a>
				<div class="dropdown-menu">
					<ul class="media-list media-list-linked">
						<li class="media">
							<div class="media-left">
								<img src="${basePath}/static/bird/assets/images/users/user1.png" class="img-circle img-sm" alt="">							
							</div>
							<div class="media-body">
								<a href="datatable_extension_column_reorder.htm#" class="media-heading clearfix">
									Jane Elliott
									<span class="media-annotation pull-right">04:58</span>
								</a>
								<span>Suspendisse vel dolor dui. Cras bibendum elit ac...</span>
							</div>
						</li>
					</ul>
					<div class="dropdown-content-footer">
						<a href="datatable_extension_column_reorder.htm#" data-popup="tooltip" title="All messages"><i class="fa fa-comments position-left"></i> Read all notifications</a>
					</div>
				</div>
			</li>
			<li>
				<a href="${basePath }/admin/logout" id="logout"><i class="fa fa-sign-out"></i></a>
			</li>
		</ul>
	</div>
	<div class="page-container">
		<div class="page-content">
			<div class="sidebar sidebar-main hidden-xs">
				<div class="sidebar-content">
					<div class="sidebar-user">
						<div class="category-content" style="height:8px;margin:0;padding:0;">
							<div class="media media-body">
							</div>
						</div>
					</div>
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul id="navigationTree" class="navigation navigation-main navigation-accordion"></ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content-wrapper">
				<div class="page-header">
					<div class="page-header-content">
						<ul class="breadcrumb" id="breadcrumb">
							<li><a href="${basePath}/admin/"><i class="fa fa-home"></i>首页</a></li>
						</ul>					
					</div>
				</div>
				<!-- Content area -->
				${body}
				<!-- /content area -->
			</div>
		</div>
	</div>
</body>
</html>