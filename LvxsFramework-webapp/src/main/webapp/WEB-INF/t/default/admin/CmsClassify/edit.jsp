<script type="text/javascript">
/*
//表单提交前数据预处理逻辑，处理成功返回True，处理失败请返回False，处理失败时则不再提交
function beforeSubmit(){
	return true;
}*/
function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){
		//如果存在表单提交前数据预处理逻辑，则执行，否则自动忽略
		if(typeof(beforeSubmit)=="function"){
			//如果数据预处理逻辑返回False，则提示并且终止数据提交
			if(!beforeSubmit()){
				layer.msg("数据预处理出错！");
				return false;
			}
		}
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
} 
$(document).ready(function(){
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	$('select').select2();
	$( "#form" ).validVal();
});
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">${(''==cmsClassify.classifyId!'')?'新增':'修改'}网站栏目基础信息</h4>				
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="${(''==cmsClassify.classifyId!'')?'save':'update'}">
					<input type="hidden" name="cmsClassify.classifyId" value="${cmsClassify.classifyId! }">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label>栏目名称：</label>
								<input type="text" name="cmsClassify.classifyName" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(cmsClassify.classifyName!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>网站ID：</label>
								<select class="select form-control required" name="cmsClassify.webSiteId">
									<option value="">请选择</option>
									<option value="1" ${cmsClassify.webSiteId! ==1?'selected'} >已选择的信息</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>父级栏目ID：</label>
								<select class="select form-control required" name="cmsClassify.parentId">
									<option value="">请选择</option>
									<option value="1" ${cmsClassify.parentId! ==1?'selected'} >已选择的信息</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>栏目编号：</label>
								<input type="text" name="cmsClassify.classifyNumber" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(cmsClassify.classifyNumber!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>排序编号：</label>
								<input type="number" name="cmsClassify.sortNumber" class="form-control  number" placeholder="越小越靠前显示，默认值100" value="${cmsClassify.sortNumber!}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>SEO关键词：</label>
								<input type="text" name="cmsClassify.keyWords" class="form-control " placeholder="每个关键词之间使用半角逗号分割。" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(cmsClassify.keyWords!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>栏目内容描述：</label>
								<input type="text" name="cmsClassify.description" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(cmsClassify.description!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>栏目图标1URL：</label>
								<input type="text" name="cmsClassify.ico1Url" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(cmsClassify.ico1Url!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>栏目图标2URL：</label>
								<input type="text" name="cmsClassify.ico2Url" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(cmsClassify.ico2Url!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>栏目图标3URL：</label>
								<input type="text" name="cmsClassify.ico3Url" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(cmsClassify.ico3Url!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>栏目HTML内容：</label>
								<textarea name="cmsClassify.classifyHtml" class="form-control " placeholder="" rows="5" cols="5" >${@org.cnitti.utils.EscapeUtils.escapeXml11(cmsClassify.classifyHtml!)}</textarea>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>用户级别要求：</label>
								<input type="number" name="cmsClassify.userLevel" class="form-control  number" placeholder="" value="${cmsClassify.userLevel!}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>浏览次数：</label>
								<input type="number" name="cmsClassify.browseCount" class="form-control  number" placeholder="" value="${cmsClassify.browseCount!}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>状态：</label>
								<div class="radio-list" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
									<label><input type="radio" name="cmsClassify.status" value="-1" ${cmsClassify.status+'' == '-1'?'checked="checked"'} class=" requiredgroup:status">逻辑删除</label>
									<label><input type="radio" name="cmsClassify.status" value="0" ${cmsClassify.status+'' == '0'?'checked="checked"'} class=" requiredgroup:status">已禁用/待审核</label>
									<label><input type="radio" name="cmsClassify.status" value="1" ${cmsClassify.status+'' == '1'?'checked="checked"'} class=" requiredgroup:status">正常</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>备注说明：</label>
								<textarea name="cmsClassify.remark" class="form-control " placeholder="" rows="5" cols="5" >${@org.cnitti.utils.EscapeUtils.escapeXml11(cmsClassify.remark!)}</textarea>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>