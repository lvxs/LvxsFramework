<% 
var contentBody = {
	%>
	<div class="content">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">站点基础信息管理</h4>
			</div>
			<div class="panel-body">
				
			</div>
			<div class="table-responsive">
				<table id="cmsWebsite_tablelist"  class="table responsive nowrap table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr style="border:1px solid #888;">
							<th data-priority="1"  style="width:20px;" onclick="selectAll(this);" class="select-checkbox" data-orderable="false"><input type="checkbox" value="1" /></th>
							<th>序号</th>
						    <th data-priority="10">网站名称</th>
						    <th data-priority="15">简短名称</th>
						    <th data-priority="20">域名</th>
						    <th data-priority="25">路径</th>
						    <th data-priority="30">域名别名</th>
						    <th data-priority="35">域名重定向</th>
						    <th data-priority="40">站点描述</th>
						    <th data-priority="45">站点关键字</th>
						    <th data-priority="50">终审级别</th>
						    <th data-priority="55">审核后</th>
						    <th data-priority="60">首页模板</th>
						    <th data-priority="65">图片不存在时默认图片</th>
						    <th data-priority="70">用户登录地址</th>
							<%/*<th data-priority="75">后台管理地址</th>*/%>
							<%/*<th data-priority="80">开启图片水印</th>*/%>
							<%/*<th data-priority="85">图片最小宽度</th>*/%>
							<%/*<th data-priority="90">图片最小高度</th>*/%>
							<%/*<th data-priority="95">图片水印</th>*/%>
							<%/*<th data-priority="100">文字水印内容</th>*/%>
							<%/*<th data-priority="105">文字水印大小</th>*/%>
							<%/*<th data-priority="110">文字水印颜色</th>*/%>
							<%/*<th data-priority="115">水印透明度（0-100）</th>*/%>
							<%/*<th data-priority="120">水印位置(0-5)</th>*/%>
							<%/*<th data-priority="125">x坐标偏移量</th>*/%>
							<%/*<th data-priority="130">y坐标偏移量</th>*/%>
							<%/*<th data-priority="135">下载防盗链md5混淆码</th>*/%>
							<%/*<th data-priority="140">下载有效时间（小时）</th>*/%>
							<%/*<th data-priority="145">邮件发送服务器</th>*/%>
							<%/*<th data-priority="150">邮件发送编码</th>*/%>
							<%/*<th data-priority="155">邮箱用户名</th>*/%>
							<%/*<th data-priority="160">邮箱密码</th>*/%>
							<%/*<th data-priority="165">邮箱发件人</th>*/%>
							<%/*<th data-priority="170">开启邮箱验证</th>*/%>
							<%/*<th data-priority="175">只有终审才能浏览内容页</th>*/%>
							<%/*<th data-priority="180">联系人</th>*/%>
							<%/*<th data-priority="185">联系手机</th>*/%>
							<%/*<th data-priority="190">联系QQ</th>*/%>
							<%/*<th data-priority="195">联系E-mail</th>*/%>
							<%/*<th data-priority="200">服务到期时间</th>*/%>
							<%/*<th data-priority="205">创建时间</th>*/%>
								<th data-priority="210">状态</th>
							<th style="width: 85px;" data-priority="2">操作</th>
							<th>展开</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	<%
};
var scriptAndCss = {
	%>
	<script type='text/javascript'>
	var _tablelist;
	function add() {
		$.ajax({
			url : "${basePath}/admin/cmsWebsite/add",
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '新增站点基础信息',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function edit(rowid) {
		$.ajax({
			url : "${basePath}/admin/cmsWebsite/edit?siteId=" + rowid,
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '修改站点基础信息',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function view(rowid) {
		$.ajax({
			url : "${basePath}/admin/cmsWebsite/view?siteId=" + rowid,
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '查看站点基础信息',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function del(rowid) {
		layer.confirm('确定要删除该记录吗？', {
			btn : [ '确定删除', '放弃删除' ]
		//按钮
		}, function() {
			$.ajax({
				url : "${basePath}/admin/cmsWebsite/delete?siteId=" + rowid,
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("删除成功");
						location.reload();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("删除失败");
				}
			});
		}, function() {
		});
	}
	function batchDel() {
		if (_tablelist.rows({
			selected : true
		}).count() < 1) {
			layer.msg("请选择记录！");
			return false;
		}
		layer.confirm('确定要删除该记录吗？', {
			btn : [ '确定删除', '放弃删除' ]
		//按钮
		}, function() {
			var selectedIds = "";
			var rows = _tablelist.rows({
				selected : true
			});
			for (var i = 0; i < rows.count(); i++) {
				if (selectedIds.length > 0) {
					selectedIds += "," + rows.data()[i].siteId;
				} else {
					selectedIds += rows.data()[i].siteId;
				}
			}
			$.ajax({
				url : "${basePath}/admin/cmsWebsite/delete",
				data : {
					siteId : selectedIds
				},
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("删除成功");
						location.reload();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("删除失败");
				}
			});
		}, function() {
		});
	}
	function selectAll(td) {
		if ($(td).find('input[type="checkbox"]').val() == "1") {
			_tablelist.rows().select();
			$(td).find('input[type="checkbox"]').val("0");
			$(td).find('input[type="checkbox"]')[0].checked = true;
		} else {
			_tablelist.rows().deselect();
			$(td).find('input[type="checkbox"]').val("1");
			$(td).find('input[type="checkbox"]')[0].checked = false;
		}
	}
	$(document).ready(function(){
		$.extend($.fn.dataTable.defaults, {
			responsive:{details:{type:'column',target:-1}},
			pages:5,
			pagingType:'full_numbers',
			autoWidth:true,
			colReorder:true,
			dom : 'fBtip',
			processing : true,
			buttons : [
				{text : '新增',action : add}, 'excel', 'print', 'colvis',
				'pageLength', {text : '删除',action : batchDel}
			],
			lengthMenu : [
					[ 10, 25, 50, 100, 200, -1 ],
					[ '10', '25', '50', '100', '200','显示所有记录' ] ],
			language:{
				buttons:{
					pageLength : {_ : "每页 %d 行",'-1' : "显示所有记录"},
					colvis : '选择显示列',print : '打印',excel : '导出Execl'
				},
				select:{rows:{_ : "选择了%d行",0:""}},
				search : '快速筛选: _INPUT_',
				searchPlaceholder : '输入关键字',
				lengthMenu : '每页_MENU_行',
				info : '第_PAGE_页，共_PAGES_页(_TOTAL_行)',
				infoEmpty : '没有符合条件的记录！',
				emptyTable : '没有符合条件的记录！',
				infoFiltered : ' - 筛选自_MAX_条记录',
				infoPostFix : '',
				zeroRecords : '没有匹配结果',
				paginate : {'first' : '首页','last' : '末页','next' : '下页','previous' : '上页'}
			}
		});
		$.fn.dataTable.Buttons.swfPath = '${basePath}/static/bird/js/datatables/extensions/swf/flashExport.swf';
		_tablelist = $('#cmsWebsite_tablelist').DataTable({
			select : {style : 'multi'},
			"processing" : true,
			"serverSide" : true,
			ajax : {
				url : '${basePath}/admin/cmsWebsite/searchData'
			},
			columns : [				{"class" : "select-checkbox","orderable" : false,"render" : function(data,type,row,meta) {	return "";}},
				{"class" : "text-right","orderable" : false,"render" : function(data,type,row,meta) {
						if (_tablelist != null && _tablelist.page.info() != null) {
							//服务器模式下获取分页信息，使用 DT 提供的 API 直接获取分页信息
							var page = _tablelist.page.info();
							//当前第几页，从0开始
							var pageno = page.page;
							//每页数据
							var length = page.length;
							//行号等于 页数*每页数据长度+行号
							return (pageno * length + meta.row + 1);
						} else {
							//如果获取不到分页信息
							return (meta.row + 1);
						}
					}
				},
				//{"data" : "siteId"},// siteId siteId
				{"data" : "siteName"},// siteName 网站名称
				{"data" : "shortName"},// shortName 简短名称
				{"data" : "domain"},// domain 域名
				{"data" : "sitePath"},// sitePath 路径
				{"data" : "domainAlias"},// domainAlias 域名别名
				{"data" : "domainRedirect"},// domainRedirect 域名重定向
				{"data" : "description"},// description 站点描述
				{"data" : "keywords"},// keywords 站点关键字
				{"data" : "finalStep"},// finalStep 终审级别
				{"data" : "afterCheck","render" : function(data,type,row,meta) {
					if(data=='1')return "不能修改删除";
					if(data=='2')return "修改后退回";
					if(data=='3')return "修改后不变";
					return "";
				}},// afterCheck 审核后
				{"data" : "tplIndex"},// tplIndex 首页模板
				{"data" : "defImg"},// defImg 图片不存在时默认图片
				{"data" : "loginUrl"},// loginUrl 用户登录地址
				//{"data" : "adminUrl"},// adminUrl 后台管理地址
				//{"data" : "markOn"},// markOn 开启图片水印
				//{"data" : "markWidth"},// markWidth 图片最小宽度
				//{"data" : "markHeight"},// markHeight 图片最小高度
				//{"data" : "markImage"},// markImage 图片水印
				//{"data" : "markContent"},// markContent 文字水印内容
				//{"data" : "markSize"},// markSize 文字水印大小
				//{"data" : "markColor"},// markColor 文字水印颜色
				//{"data" : "markAlpha"},// markAlpha 水印透明度（0-100）
				//{"data" : "markPosition"},// markPosition 水印位置(0-5)
				//{"data" : "markOffsetX"},// markOffsetX x坐标偏移量
				//{"data" : "markOffsetY"},// markOffsetY y坐标偏移量
				//{"data" : "downloadCode"},// downloadCode 下载防盗链md5混淆码
				//{"data" : "downloadTime"},// downloadTime 下载有效时间（小时）
				//{"data" : "emailHost"},// emailHost 邮件发送服务器
				//{"data" : "emailEncoding"},// emailEncoding 邮件发送编码
				//{"data" : "emailUsername"},// emailUsername 邮箱用户名
				//{"data" : "emailPassword"},// emailPassword 邮箱密码
				//{"data" : "emailPersonal"},// emailPersonal 邮箱发件人
				//{"data" : "emailValidate"},// emailValidate 开启邮箱验证
				//{"data" : "viewOnlyChecked"},// viewOnlyChecked 只有终审才能浏览内容页
				//{"data" : "contactPerson"},// contactPerson 联系人
				//{"data" : "contactMobile"},// contactMobile 联系手机
				//{"data" : "contactQQ"},// contactQQ 联系QQ
				//{"data" : "contactEmail"},// contactEmail 联系E-mail
				//{"data" : "serviceEndDate","type":"date"},// serviceEndDate 服务到期时间
				//{"data" : "createTime","type":"date"},// createTime 创建时间
				{"data" : "status","render" : function(data,type,row,meta) {
					if(data==1)return "正常";
					if(data==0)return "暂停使用";
					if(data==-1)return "逻辑删除";
					return "";
				}},// status 状态
				{"data":"siteId","render":function(data,type,row,meta){
						return '<ul class="icons-list">'
								+ '<li><a href="${basePath}/admin/cmsWebsite/view?siteId='+ data+ '" onclick="view(\''+ data+ '\');return false;" title="查看"><i class="fa fa-eye"></i></a></li>'
								+ '<li><a href="${basePath}/admin/cmsWebsite/edit?siteId='+ data+ '" onclick="edit(\''+ data+ '\');return false;" title="修改"><i class="fa fa-edit"></i></a></li>'
								+ '<li><a href="${basePath}/admin/cmsWebsite/delete?siteId='+ data+ '" onclick="del(\''+ data+ '\');return false;" title="删除"><i class="fa fa-trash"></i></a></li>'
								+ '</ul>';
					},
					"orderable" :false,
					"type" : "html",
					"width" : "30px"
				},
				{"data" : null,"class" : "control","orderable" : false,
					"render" : function(data,type,row,meta) {
						return "";
					}
				}],order : [ [ 2, 'desc' ] ]		});
	});
	</script>
	<%
};%>
<% 
layout("../_inc/_layout.jsp",{head:scriptAndCss,body:contentBody}){} %>