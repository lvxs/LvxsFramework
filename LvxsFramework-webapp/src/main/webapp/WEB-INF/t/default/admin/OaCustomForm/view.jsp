<script type="text/javascript">
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看自定义表单</h4>				
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>表单名称：</label>
					<span id="name">${oaCustomForm.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>物理表名称：</label>
					<span id="physicalName">${oaCustomForm.physicalName!}</span>
				</div>
				<div class="col-md-12 col-sm-12">
					<label>设计源码：</label>
					<pre id="designCode">${oaCustomForm.designCode!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>附加JavaScript和Css样式代码：</label>
					<pre id="jsAndCss">${oaCustomForm.jsAndCss!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>列表页面查询SQL：</label>
					<pre id="listWhereSql">${oaCustomForm.listWhereSql!}</pre>
				</div>
				<div class="col-md-12 col-sm-12">
					<label>备注说明：</label>
					<pre id="remark">${oaCustomForm.remark!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>类型：</label>
					<span id="type">${decode(oaCustomForm.type+"","1","自定义表单","2","实体表格","3","实体代码","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>排序编号：</label>
					<span id="sortNumber">${oaCustomForm.sortNumber!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>状态：</label>
					<span id="status">${decode(oaCustomForm.status+"","1","正常","0","审批中","-1","逻辑删除","未知")}</span>
				</div>
				<div class="col-md-12 col-sm-12">
					<label>其他设置JSON字符串：</label>
					<pre id="otherJson">${oaCustomForm.otherJson!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建用户：</label>
					<span id="createUserId">${oaCustomForm.createUser.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建时间：</label>
					<span id="createTime">${oaCustomForm.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新用户：</label>
					<span id="updateUserId">${oaCustomForm.updateUser.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新时间：</label>
					<span id="updateTime">${oaCustomForm.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
			</div>
		</fieldset>
	</div>
</div>