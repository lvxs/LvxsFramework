<script type="text/javascript">
function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
} 
$(document).ready(function(){
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	$( "#form" ).validVal();
});
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">${(''==website.siteId!'')?'新增':'修改'}站点信息</h4>				
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="${(''==website.siteId!'')?'save':'update'}">
					<input type="hidden" name="website.siteId" value="${website.siteId! }">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
									<label>网站名称：</label>
									<input type="text" name="website.siteName" class="form-control required" placeholder="网站名称" value="${website.siteName!}" />
							</div>
							<div class="col-md-6 col-sm-6">
									<label>网站简称：</label>
									<input type="text" name="website.shortName" class="form-control" placeholder="网站简称" value="${website.shortName!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>网站主域名：</label>
									<input type="text" name="website.domain" class="form-control required" placeholder="网站主域名" value="${website.domain!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>存放路径：</label>
									<input type="text" name="website.sitePath" class="form-control required" placeholder="存放路径" value="${website.sitePath!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>其他域名：</label>
									<input type="text" name="website.domainAlias" class="form-control" placeholder="使用这些域名也能访问网站，多个域名之间使用半角分号”;“隔开" value="${website.domainAlias!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>跳转域名：</label>
									<input type="text" name="website.domainRedirect" class="form-control" placeholder="访问这些域名将自动跳转至主域名，多个域名之间使用半角分号”;“隔开" value="${website.domainRedirect!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>站点描述：</label>
									<input type="text" name="website.description" class="form-control" placeholder="站点描述，方便搜索引擎检索识别" value="${website.description!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>关键字：</label>
									<input type="text" name="website.keywords" class="form-control" placeholder="关键字，方便搜索引擎检索，多个关键字之间使用半角逗号”,“隔开" value="${website.keywords!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>终审级别：</label>
									<input type="number" name="website.finalStep" class="form-control number required" placeholder="发布内容需要几级审核才能最终发布，1代表直接发布" value="${website.finalStep!}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>终审后：</label>
								<select class="select form-control required" name="website.afterCheck">
									<option value="">请选择</option>
									<option value="1" ${website.afterCheck! ==1?'selected="selected"'} >不能修改删除</option>
									<option value="2" ${website.afterCheck! ==2?'selected="selected"'}>修改后退回</option>
									<option value="3" ${website.afterCheck! ==3?'selected="selected"'}>修改后不变</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
									<label>联系人：</label>
									<input type="text" name="website.contactPerson" class="form-control" placeholder="联系人姓名" value="${website.contactPerson!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>手机号码：</label>
									<input type="text" name="website.contactMobile" class="form-control" placeholder="手机号码，方便接收系统通知短信或者验证码短信" value="${website.contactMobile!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>QQ号码：</label>
									<input type="text" name="website.contactQQ" class="form-control number" placeholder="常用联系QQ" value="${website.contactQQ!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>E-mail：</label>
									<input type="text" name="website.contactEmail" class="form-control email" placeholder="常用的联系E-mail" value="${website.contactEmail!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<label>服务截至日期：</label>
									<input type="date" name="website.serviceEndDate" class="form-control required" placeholder="服务截至日期" value="${website.serviceEndDate!}" >
							</div>
							<div class="col-md-6 col-sm-6">
									<div>状态：</div>
									<div class="radio-list">
									<label>
										<input type="radio" name="website.status" value="1" ${website.status! ==1?'checked="checked"'} class=" requiredgroup:status">
										正常
									</label>
									<label>
										<input type="radio" name="website.status" value="0" ${website.status! ==0?'checked="checked"'}  class=" requiredgroup:status">
										审批中/暂停使用
									</label>
									<label>
										<input type="radio" name="website.status" value="-1" ${website.status! ==-1?'checked="checked"'}  class=" requiredgroup:status">
										冻结/逻辑删除
									</label>
									</div>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>