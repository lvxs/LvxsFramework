<script type="text/javascript">
function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
} 
$(document).ready(function(){
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	$( "#form" ).validVal();
});
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">新增系统信息</h4>				
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="save">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label>系统全称：</label>
									<input type="text" name="baseSystem.fullName" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>系统简称：</label>
									<input type="text" name="baseSystem.shortName" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>访问URL：</label>
									<input type="url" name="baseSystem.url" class="form-control  url}" placeholder="到应用存放路径(ContextPath)为止，不必以“/”结尾" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>文件路径：</label>
									<input type="text" name="baseSystem.filePath" class="form-control " placeholder="附件、资源文件或者模板页面存储路径标识。本机上运行的子系统必填而且必须唯一，非本机系统应该为空" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>用户登录地址：</label>
									<input type="text" name="baseSystem.loginUrl" class="form-control " placeholder="" value="/login" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>后台管理地址：</label>
									<input type="text" name="baseSystem.adminUrl" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>联系人：</label>
									<input type="text" name="baseSystem.contactPerson" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>联系手机：</label>
									<input type="text" name="baseSystem.contactMobile" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>联系QQ：</label>
									<input type="text" name="baseSystem.contactQQ" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>联系E-mail：</label>
									<input type="email" name="baseSystem.contactEmail" class="form-control  email}" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>状态：</label>
									<div class="radio-list" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
									<label>
										<input type="radio" name="baseSystem.status" value="1" ${baseSystem.status! ==1?'checked="checked"'} class=" requiredgroup:status">
										正常
									</label>
									<label>
										<input type="radio" name="baseSystem.status" value="0" ${baseSystem.status! ==0?'checked="checked"'}  class=" requiredgroup:status">
										审批中/暂停使用
									</label>
									<label>
										<input type="radio" name="baseSystem.status" value="-1" ${baseSystem.status! ==-1?'checked="checked"'}  class=" requiredgroup:status">
										冻结/逻辑删除
									</label>
									</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>备注说明：</label>
									<textarea name="baseSystem.remark" class="form-control  email}" placeholder="" rows="3" cols="5" ></textarea>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>