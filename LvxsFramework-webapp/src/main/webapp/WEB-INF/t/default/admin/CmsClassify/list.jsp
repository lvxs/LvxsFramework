<% 
var contentBody = {
	%>
	<div class="content">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">网站栏目基础信息管理</h4>
			</div>
			<div class="panel-body">
				CMS的网站栏目基础信息表
			</div>
			<div class="table-responsive">
				<table id="cmsClassify_tablelist"  class="table responsive nowrap table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr style="border:1px solid #888;">
							<th data-priority="1"  style="width:20px;" onclick="selectAll(this);" class="select-checkbox" data-orderable="false"><input type="checkbox" value="1" /></th>
							<th>序号</th>
						    <th data-priority="10">栏目名称</th>
						    <th data-priority="15">网站ID</th>
						    <th data-priority="20">父级栏目ID</th>
						    <th data-priority="25">栏目编号</th>
						    <th data-priority="30">排序编号</th>
						    <th data-priority="35">SEO关键词</th>
						    <th data-priority="40">栏目内容描述</th>
						    <th data-priority="45">栏目图标1URL</th>
						    <th data-priority="50">栏目图标2URL</th>
						    <th data-priority="55">栏目图标3URL</th>
						    <th data-priority="60">栏目HTML内容</th>
						    <th data-priority="65">用户级别要求</th>
						    <th data-priority="70">浏览次数</th>
								<th data-priority="75">状态</th>
							<%/*<th data-priority="80">备注说明</th>*/%>
							<%/*<th data-priority="85">其他设置JSON字符串</th>*/%>
							<%/*<th data-priority="90">创建时间</th>*/%>
							<%/*<th data-priority="95">创建用户ID</th>*/%>
							<%/*<th data-priority="100">更新时间</th>*/%>
							<%/*<th data-priority="105">更新用户ID</th>*/%>
							<th style="width: 85px;" data-priority="2">操作</th>
							<th>展开</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	<%
};
var scriptAndCss = {
	%>
	<script type='text/javascript'>
	var _tablelist;
	function add() {
		$.ajax({
			url : "${basePath}/admin/cmsClassify/add",
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '新增网站栏目基础信息',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function edit(rowid) {
		$.ajax({
			url : "${basePath}/admin/cmsClassify/edit?classifyId=" + rowid,
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '修改网站栏目基础信息',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function view(rowid) {
		$.ajax({
			url : "${basePath}/admin/cmsClassify/view?classifyId=" + rowid,
			cache : false,
			success : function(html) {
				var index = layer.open({
					type : 1,
					title : '查看网站栏目基础信息',
					content : html,
					area : [ '97%', '95%' ]
				});
			}
		});
	}
	function del(rowid) {
		layer.confirm('确定要删除该记录吗？', {
			btn : [ '确定删除', '放弃删除' ]
		//按钮
		}, function() {
			$.ajax({
				url : "${basePath}/admin/cmsClassify/delete?classifyId=" + rowid,
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("删除成功");
						location.reload();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("删除失败");
				}
			});
		}, function() {
		});
	}
	function batchDel() {
		if (_tablelist.rows({
			selected : true
		}).count() < 1) {
			layer.msg("请选择记录！");
			return false;
		}
		layer.confirm('确定要删除该记录吗？', {
			btn : [ '确定删除', '放弃删除' ]
		//按钮
		}, function() {
			var selectedIds = "";
			var rows = _tablelist.rows({
				selected : true
			});
			for (var i = 0; i < rows.count(); i++) {
				if (selectedIds.length > 0) {
					selectedIds += "," + rows.data()[i].classifyId;
				} else {
					selectedIds += rows.data()[i].classifyId;
				}
			}
			$.ajax({
				url : "${basePath}/admin/cmsClassify/delete",
				data : {
					classifyId : selectedIds
				},
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("删除成功");
						location.reload();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("删除失败");
				}
			});
		}, function() {
		});
	}
	function selectAll(td) {
		if ($(td).find('input[type="checkbox"]').val() == "1") {
			_tablelist.rows().select();
			$(td).find('input[type="checkbox"]').val("0");
			$(td).find('input[type="checkbox"]')[0].checked = true;
		} else {
			_tablelist.rows().deselect();
			$(td).find('input[type="checkbox"]').val("1");
			$(td).find('input[type="checkbox"]')[0].checked = false;
		}
	}
	$(document).ready(function(){
		$.extend($.fn.dataTable.defaults, {
			responsive:{details:{type:'column',target:-1}},
			pages:5,
			pagingType:'full_numbers',
			autoWidth:true,
			colReorder:true,
			dom : 'fBtip',
			processing : true,
			buttons : [
				{text : '新增',action : add}, 'excel', 'print', 'colvis',
				'pageLength', {text : '删除',action : batchDel}
			],
			lengthMenu : [
					[ 10, 25, 50, 100, 200, -1 ],
					[ '10', '25', '50', '100', '200','显示所有记录' ] ],
			language:{
				buttons:{
					pageLength : {_ : "每页 %d 行",'-1' : "显示所有记录"},
					colvis : '选择显示列',print : '打印',excel : '导出Execl'
				},
				select:{rows:{_ : "选择了%d行",0:""}},
				search : '快速筛选: _INPUT_',
				searchPlaceholder : '输入关键字',
				lengthMenu : '每页_MENU_行',
				info : '第_PAGE_页，共_PAGES_页(_TOTAL_行)',
				infoEmpty : '没有符合条件的记录！',
				emptyTable : '没有符合条件的记录！',
				infoFiltered : ' - 筛选自_MAX_条记录',
				infoPostFix : '',
				zeroRecords : '没有匹配结果',
				paginate : {'first' : '首页','last' : '末页','next' : '下页','previous' : '上页'}
			}
		});
		$.fn.dataTable.Buttons.swfPath = '${basePath}/static/bird/js/datatables/extensions/swf/flashExport.swf';
		_tablelist = $('#cmsClassify_tablelist').DataTable({
			select : {style : 'multi'},
			"processing" : true,
			"serverSide" : true,
			ajax : {
				url : '${basePath}/admin/cmsClassify/searchData'
			},
			columns : [				{"class" : "select-checkbox","orderable" : false,"render" : function(data,type,row,meta) {	return "";}},
				{"class" : "text-right","orderable" : false,"render" : function(data,type,row,meta) {
						if (_tablelist != null && _tablelist.page.info() != null) {
							//服务器模式下获取分页信息，使用 DT 提供的 API 直接获取分页信息
							var page = _tablelist.page.info();
							//当前第几页，从0开始
							var pageno = page.page;
							//每页数据
							var length = page.length;
							//行号等于 页数*每页数据长度+行号
							return (pageno * length + meta.row + 1);
						} else {
							//如果获取不到分页信息
							return (meta.row + 1);
						}
					}
				},
				//{"data" : "classifyId"},// classifyId 栏目ID
				{"data" : "classifyName"},// classifyName 栏目名称
				{"data" : "webSiteId"},// webSiteId 网站ID
				{"data" : "parentId"},// parentId 父级栏目ID
				{"data" : "classifyNumber"},// classifyNumber 栏目编号
				{"data" : "sortNumber"},// sortNumber 排序编号
				{"data" : "keyWords"},// keyWords SEO关键词
				{"data" : "description"},// description 栏目内容描述
				{"data" : "ico1Url"},// ico1Url 栏目图标1URL
				{"data" : "ico2Url"},// ico2Url 栏目图标2URL
				{"data" : "ico3Url"},// ico3Url 栏目图标3URL
				{"data" : "classifyHtml"},// classifyHtml 栏目HTML内容
				{"data" : "userLevel"},// userLevel 用户级别要求
				{"data" : "browseCount"},// browseCount 浏览次数
				{"data" : "status","render" : function(data,type,row,meta) {
					if(data=='-1')return "逻辑删除";
					if(data=='0')return "已禁用/待审核";
					if(data=='1')return "正常";
					return "";
				}},// status 状态
				//{"data" : "remark"},// remark 备注说明
				//{"data" : "otherJson"},// otherJson 其他设置JSON字符串
				//{"data" : "createTime","type":"date"},// createTime 创建时间
				//{"data" : "createUserId"},// createUserId 创建用户ID
				//{"data" : "updateTime","type":"date"},// updateTime 更新时间
				//{"data" : "updateUserId"},// updateUserId 更新用户ID
				{"data":"classifyId","render":function(data,type,row,meta){
						return '<ul class="icons-list">'
								+ '<li><a href="${basePath}/admin/cmsClassify/view?classifyId='+ data+ '" onclick="view(\''+ data+ '\');return false;" title="查看"><i class="fa fa-eye"></i></a></li>'
								+ '<li><a href="${basePath}/admin/cmsClassify/edit?classifyId='+ data+ '" onclick="edit(\''+ data+ '\');return false;" title="修改"><i class="fa fa-edit"></i></a></li>'
								+ '<li><a href="${basePath}/admin/cmsClassify/delete?classifyId='+ data+ '" onclick="del(\''+ data+ '\');return false;" title="删除"><i class="fa fa-trash"></i></a></li>'
								+ '</ul>';
					},
					"orderable" :false,
					"type" : "html",
					"width" : "30px"
				},
				{"data" : null,"class" : "control","orderable" : false,
					"render" : function(data,type,row,meta) {
						return "";
					}
				}],order : [ [ 2, 'desc' ] ]		});
	});
	</script>
	<%
};%>
<% 
layout("../_inc/_layout.jsp",{head:scriptAndCss,body:contentBody}){} %>