<script type="text/javascript">
//表单提交前数据预处理逻辑，处理成功返回True，处理失败请返回False，处理失败时则不再提交
function beforeSumbit(){
	if($('#password').val()!=""){
		//客户端对密码进行加密，防止网络拦截泄露密码
		$('[name="baseUser.password"]').val($.md5("密码加密验证字符串，防止MD5反向解密"+$('#password').val()));
	} else {
		$('[name="baseUser.password"]').val("");
	}
	return true;
}
function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){
		//如果存在表单提交前数据预处理逻辑，则执行，否则自动忽略
		if(typeof(beforeSumbit)=="function"){
			//如果数据预处理逻辑返回False，则提示并且终止数据提交
			if(!beforeSumbit()){
				layer.msg("登录密码加密出错！");
				return false;
			}
		}
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
}
function initDepartmentSelect(){
	var departmentSelect = $('[name="baseUser.departmentId"]');
	var departmentIds = ","+$('[name="departmentIds"]').val()+",";
	$('select[name="baseUser.departmentId"] option').remove();
	$.ajax({
		url : "${basePath}/admin/baseDepartment/getTreeJson?systemId=${baseSystem.id!}" ,
		dataType : "json",
		success : function(data) {
			if (data.data!=null) {
				for(var i in data.data){
					var levelString = "",currentLevel=0;
					var department = data.data[i];
					if(department.treeNumber!=null && department.treeNumber.length>3){
						currentLevel = department.treeNumber.length/3 - 1;
					}
					for(var k=0;k<currentLevel;k++)levelString+="　";
					if(departmentIds.indexOf(","+department.id+",")>=0){
						departmentSelect.append('<option value="'+department.id+'" selected>'+levelString+department.name+'</option>');
					} else {
						departmentSelect.append('<option value="'+department.id+'">'+levelString+department.name+'</option>');
					}
				}
			} else {
			}
		},
		error : function() {
			layer.msg("组织架构加载错误");
		}
	});
	$('[name="baseUser.departmentId"]').select2();
}
function initRoleSelect(){
	var roleSelect = $('[name="baseUser.roleId"]');
	var roleIds = ","+$('[name="roleIds"]').val()+",";
	$('select[name="baseUser.roleId"] option').remove();
	$.ajax({
		url : "${basePath}/admin/baseRole/getListJson?systemId=${baseSystem.id!}" ,
		dataType : "json",
		success : function(data) {
			if (data.data!=null) {
				for(var i in data.data){
					var role = data.data[i];
					if(roleIds.indexOf(","+role.id+",")>=0){
						roleSelect.append('<option value="'+role.id+'" selected>'+role.name+'</option>');
					} else {
						roleSelect.append('<option value="'+role.id+'">'+role.name+'</option>');
					}
				}
			} else {
			}
		},
		error : function() {
			layer.msg("角色列表加载错误");
		}
	});
	$('[name="baseUser.roleId"]').select2();
} 
function initUser(){
	$.ajax({
		url : "${basePath}/admin/baseUser/detailJSON?id=${parameter.id!}" ,
		dataType : "json",
		success : function(data) {
			if (data.id!=null && data.errorCode==null) {
				$('#form').attr("action","update"),
				$('#actionTitle').html("修改");
				$('[name="baseUser.id"]').val(data.id);
				$('[name="baseUser.userName"]').val(data.userName);
				$('[name="baseUser.email"]').val(data.email);
				$('[name="baseUser.mobile"]').val(data.mobile);
				$('[name="baseUser.otherContact"]').val(data.otherContact);
				$('[name="baseUser.nikename"]').val(data.nikename);
				$('[name="baseUser.photoURL"]').val(data.photoURL);
				$('[name="baseUser.realName"]').val(data.realName);
				$('[name="roleIds"]').val(data.roleIds);
				$('[name="departmentIds"]').val(data.departmentIds);
				$('[name="baseUser.status"][value="'+data.status+'"]').attr("checked","checked");
				$('[name="baseUser.remark"]').val(data.remark);
			} else {
				layer.msg(data.message);
			}
			initDepartmentSelect();
			initRoleSelect();
		},
		error : function() {
			layer.msg("用户信息加载错误");
			initDepartmentSelect();
			initRoleSelect();
		}
	});
}
$(document).ready(function(){
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	initUser();
	$('[name="baseUser.roleId"]').select2();
	$("#form").validVal();
});
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title"><span id="actionTitle">新增</span>用户信息</h4>				
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="save">
					<input type="hidden" name="baseUser.id" value="">
					<input type="hidden" name="roleIds" value="">
					<input type="hidden" name="departmentIds" value="">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label>用户名：</label>
								<input type="text" name="baseUser.userName" class="form-control required" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>登录密码：</label>
								<input type="text" id="password" class="form-control " placeholder="不更改请留空" value="" >
								<input type="hidden" name="baseUser.password" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>E-mail：</label>
								<input type="email" name="baseUser.email" class="form-control  email" placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>手机：</label>
								<input type="text" name="baseUser.mobile" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>其他联系方式：</label>
								<input type="text" name="baseUser.otherContact" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>昵称：</label>
								<input type="text" name="baseUser.nikename" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>照片URL：</label>
								<input type="text" name="baseUser.photoURL" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>真实姓名：</label>
								<input type="text" name="baseUser.realName" class="form-control " placeholder="" value="" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>拥有角色：</label>
								<select class="form-control required " multiple="multiple" data-placeholder="请选择" name="baseUser.roleId"></select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>所属部门：</label>
								<select class="form-control required" multiple="multiple" data-placeholder="请选择" name="baseUser.departmentId"></select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>状态：</label>
								<div class="radio-list" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
									<label>
										<input type="radio" name="baseUser.status" value="1" checked class=" requiredgroup:status">
										正常
									</label>
									<label>
										<input type="radio" name="baseUser.status" value="0" class=" requiredgroup:status">
										审批中/暂停使用
									</label>
									<label>
										<input type="radio" name="baseUser.status" value="-1" class=" requiredgroup:status">
										冻结/逻辑删除
									</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>备注说明：</label>
								<textarea name="baseUser.remark" class="form-control " placeholder="" rows="5" cols="5"></textarea>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>