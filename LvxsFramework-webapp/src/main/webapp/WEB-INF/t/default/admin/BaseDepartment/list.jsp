<% 
var contentBody = {
	%>
	<div class="content">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">《${baseSystem.fullName }》部门管理</h4>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3 col-sm-3" >
						<div class="panel .panel-white">
							<div class="panel-heading">
								<h5 class="panel-title" style="text-align:right;">
									<a id="addRootDepartment"><i class="fa fa-plus-square">新增一级部门</i></a>
								</h5>
							</div>
							<div class="panel-body sidebar" style="display:block;width:100%;" title="可以通过拖拽调整部门位置和顺序;按住Ctrl或者Cmd然后拖拽，可以复制部门">
								<ul id="departmentTree" class="ztree">组织机构树</ul>
							</div>
						</div>
					</div>
					<div class="col-md-9 col-sm-9">
						<div class="tabbable" id="editDepartment" style="display:none;">
							<ul class="nav nav-tabs nav-tabs-highlight">
								<li class="active"><a>部门属性设置</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="highlight-tab1">
									<form id="departmentForm" class="form-validation" action="update">
										<input type="hidden" name="baseDepartment.id" value="">
										<input type="hidden" name="baseDepartment.systemId" value="">
										<input type="hidden" name="baseDepartment.parentId" value="">
										<input type="hidden" name="baseDepartment.sortNumber" value="">
										<input type="hidden" name="baseDepartment.treeNumber" value="">
										<fieldset>
											<div class="row">
												<div class="col-md-6 col-sm-6">
													<label>部门名称：</label>
														<input type="text" name="baseDepartment.name" class="form-control " placeholder="部门名称，必填" value="" >
												</div>
												<div class="col-md-6 col-sm-6">
													<label>部门编号：</label>
														<input type="text" name="baseDepartment.number" class="form-control " placeholder="部门编号，选填" value="" >
												</div>
												<div class="col-md-6 col-sm-6">
													<label>简要说明：</label>
														<input type="text" name="baseDepartment.briefDescription" class="form-control " placeholder="部门简要说明，选填" value="" >
												</div>
												<div class="col-md-6 col-sm-6">
													<label>状态：</label>
														<div class="radio-list" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
														<label>
															<input type="radio" name="baseDepartment.status" value="1" class=" requiredgroup:status">
															正常
														</label>
														<label>
															<input type="radio" name="baseDepartment.status" value="0" class=" requiredgroup:status">
															审批中/暂停使用
														</label>
														<label>
															<input type="radio" name="baseDepartment.status" value="-1" class=" requiredgroup:status">
															冻结/逻辑删除
														</label>
														</div>
												</div>
												<div class="col-xs-12">
													<label>备注说明：</label>
														<textarea name="baseDepartment.remark" class="form-control" placeholder="备注说明，选填" rows="5" cols="5" ></textarea>
												</div>
											</div>
										</fieldset>
										<div class="form-wizard-actions  " style="margin:10px;">
											<input class="btn btn-default" id="validation-back" value="重置" type="reset">
											<input class="btn btn-info" id="validation-next" value="提交" type="submit">
										</div>
									</form>	
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
		</div>
	<%
};
var scriptAndCss = {
	%>
	<link href="${basePath }/static/ztree/css/awesomeStyle/awesome.css" rel="stylesheet" type="text/css">
	<link href="${isNotEmpty(style)?basePath+'/static/bird/css/styles/'+style+'.css':basePath+'/static/bird/css/styles/lightblue.css'}" type="text/css" rel="stylesheet" id="style">
	<script src="${basePath}/static/bird/js/forms/select2.min.js" type="text/javascript"></script>
	<script src="${basePath }/static/ztree/js/jquery.ztree.core.js" type="text/javascript"></script>
	<script src="${basePath }/static/ztree/js/jquery.ztree.excheck.js" type="text/javascript"></script>
	<script src="${basePath }/static/ztree/js/jquery.ztree.exedit.js" type="text/javascript"></script>
	<script type='text/javascript'>
	//目录树属性设置
	var setting = {
        view: {
            addHoverDom: addHoverDom,
            removeHoverDom: removeHoverDom,
			showLine: true,
            selectedMulti: false
        },
        check: {
            enable: false
            ,chkStyle: 'radio'
            ,radioType: "level"
        },
        data: {
            simpleData: {
                enable: true,
    			idKey: "id",
    			pIdKey: "parentId",
    			rootPId: 0
            }
        },
        edit: {
            enable: true,
    		showRemoveBtn: showRemoveBtn,
    		removeTitle: "删除部门",
    		showRenameBtn: false
        },
		callback: {
			onDrop: zTreeOnDrop,
			onClick: zTreeOnClick,
			onRemove: zTreeOnRemove,
			beforeDrag: zTreeBeforeDrag
		}
    };
    var departmentTree;
    var newCount = 1,notSave=false;
    function initDepartmentTree(){
    	$.ajax({
			url : "${basePath}/admin/baseDepartment/getTreeJson?systemId=" + ${baseSystem.id},
			dataType : "json",
			success : function(data) {
				if (data.data!=null) {
					departmentTree = $.fn.zTree.init($("#departmentTree"), setting, data.data);
					newCount = departmentTree.getNodes().length;
				} else {
					departmentTree = $.fn.zTree.init($("#departmentTree"), setting, []);
				}
				departmentTree.expandAll(true);
			},
			error : function() {
				layer.msg("目录树加载错误");
			}
		});
    }
    function addRootNode(event) {
    	if(notSave){
    		layer.msg("有新建的子部门未保存，不能添加！",{time:1000});
    		return false;
    	}
    	$("#addRootDepartment").unbind("click");
    	var treeNumber = "000" +  (departmentTree.getNodes().length+1);
    	treeNumber = treeNumber.substring(treeNumber.length-3);
    	var baseDepartment={"baseDepartment.name":"部门" +treeNumber,"baseDepartment.sortNumber":(departmentTree.getNodes().length+1),"baseDepartment.treeNumber":treeNumber,"baseDepartment.systemId":${baseSystem.id},"baseDepartment.parentId":0,"baseDepartment.status":1};
    	$.post({
			url:"${basePath}/admin/baseDepartment/save",
			data:baseDepartment, 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("新增成功",{time:1000});
					$.ajax({
						url : "${basePath}/admin/baseDepartment/getTreeJson?systemId=" + ${baseSystem.id},
						dataType : "json",
						success : function(data) {
							if (data.data!=null) {
								departmentTree = $.fn.zTree.init($("#departmentTree"), setting, data.data);
								departmentTree.expandAll(true);
								newCount = departmentTree.getNodes().length;
								if(newCount>0){
									zTreeOnClick(event, departmentTree.treeId, departmentTree.getNodes()[newCount-1]);
									departmentTree.selectNode(departmentTree.getNodes()[newCount-1]);
								}
							} else {
								departmentTree = $.fn.zTree.init($("#departmentTree"), setting, []);
							}
						},
						error : function() {
							layer.msg("目录树加载错误");
						}
					});
				}else{
					layer.msg(data.message);
				}
				$("#addRootDepartment").bind("click",addRootNode);
			},
			error : function() {
				layer.msg("新增失败！");
				$("#addRootDepartment").bind("click",addRootNode);
			}
		});
    }
    function zTreeBeforeDrag(treeId, treeNodes) {
		if(notSave){
			layer.msg("有新建的子部门未保存，不能拖拽！",{time:1000});
			return false;
		}
		return true;
	}
    function zTreeOnRemove(event, treeId, treeNode) {
    	if(treeNode.id!=null){
	    	$.ajax({
				url:"${basePath}/admin/baseDepartment/delete?id=" + treeNode.id,
				dataType : "json", 
				success : function(data) { 
					if(data.errorCode == 0){
						layer.msg("删除成功",{time:1000});
					}else{
						initDepartmentTree();
						layer.msg(data.message);
			    		notSave = false;
					}
				},
				error : function() {
					initDepartmentTree();
					layer.msg("删除失败！");
		    		notSave = false;
				}
			});
    	} else {
    		notSave = false;
    	}
    	$("#editDepartment").hide();
    }
    
    function zTreeOnClick(event, treeId, treeNode) {
    	$("#editDepartment").show();
    	if(treeNode.id!=null && treeNode.id>0){
        	$('#departmentForm [name="baseDepartment.id"]').val(treeNode.id);
    		$('#departmentForm').attr("action","update");
    	}else{
        	$('#departmentForm [name="baseDepartment.id"]').val("");
    		$('#departmentForm').attr("action","save");
    	}
    	$('#departmentForm [name="baseDepartment.name"]').val(treeNode.name);
    	$('#departmentForm [name="baseDepartment.systemId"]').val(treeNode.systemId);
    	$('#departmentForm [name="baseDepartment.parentId"]').val(treeNode.parentId);
    	$('#departmentForm [name="baseDepartment.sortNumber"]').val(treeNode.sortNumber);
    	$('#departmentForm [name="baseDepartment.treeNumber"]').val(treeNode.treeNumber);
    	$('#departmentForm [name="baseDepartment.number"]').val(treeNode.number);
    	$('#departmentForm [name="baseDepartment.briefDescription"]').val(treeNode.briefDescription);
    	$('#departmentForm [name="baseDepartment.iconSkin"]').val(treeNode.iconSkin);
    	$('#iconSkin').removeClass();
    	$('#iconSkin').addClass(treeNode.iconSkin);
    	$('#departmentForm [name="baseDepartment.status"][value="'+treeNode.status+'"]').attr("checked","checked");
    	$('#departmentForm [name="baseDepartment.remark"]').val(treeNode.remark);
    }
    
    function showRemoveBtn(treeId, treeNode) {
    	if(treeNode.children!=null && treeNode.children.length>0){
    		return false;
    	} else {
        	return true;	
    	}
    }
    function addHoverDom(treeId, treeNode) {
        var sObj = $("#" + treeNode.tId + "_span");
        if (notSave || treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0 || treeNode.id==null) return;
        var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' title='添加子部门' onfocus='this.blur();'></span>";
        sObj.after(addStr);
        var btn = $("#addBtn_"+treeNode.tId);
        if (btn) btn.bind("click", function(){
        	newCount=0;
        	var sortNumber = 0;
        	if(treeNode!=null && treeNode.children!=null && treeNode.children.length>0){
        		newCount = treeNode.children.length;
        		sortNumber = newCount;
        		for(var i=newCount-1;i>=0;i--){
        			if(treeNode.children[i].sortNumber>sortNumber){
        				sortNumber=treeNode.children[i].sortNumber;
        			}
        		}
        	}
        	sortNumber = sortNumber+1;        	
        	var treeNumber="000"+sortNumber+"";
        	treeNumber = treeNumber.substring(treeNumber.length-3);
        	departmentTree.addNodes(treeNode, {"name":"未保存部门" +treeNumber,"title":"请点击右侧表单保存按钮，否则添加不生效！","parentId":treeNode.id,"sortNumber":sortNumber,"treeNumber":treeNode.treeNumber+treeNumber,"systemId":treeNode.systemId});
			zTreeOnClick(event, departmentTree.treeId, treeNode.children[newCount]);
			departmentTree.selectNode(treeNode.children[newCount]);
			notSave=true;
            return false;
        });
    };
    function removeHoverDom(treeId, treeNode) {
        $("#addBtn_"+treeNode.tId).unbind().remove();
    }
    function zTreeOnDrop(event, treeId, treeNodes, targetNode, moveType, isCopy) {
    	var peerNodes = [];
    	if(targetNode.parentId!=null && targetNode.parentId>0) {
    		var parentNode = departmentTree.getNodeByParam("id",targetNode.parentId, null);
    		if(parentNode!=null && parentNode.children){
    			peerNodes = parentNode.children;
    		}
    	} else {
    		peerNodes = departmentTree.getNodes();
    	}
    	var dropData={"treeNodesJSON":JSON.stringify(treeNodes),"targetNodeJSON":JSON.stringify(targetNode),"peerNodesJSON":JSON.stringify(peerNodes),"moveType":moveType,"systemId":${baseSystem.id},"isCopy":isCopy};
    	$.post({
			url:"${basePath}/admin/baseDepartment/drop",
			data:dropData,
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode >= 0){
					if(isCopy){
						layer.msg("复制成功",{time:1000});
					} else {
						layer.msg("移动成功",{time:1000});
					}
					initDepartmentTree();
				}else{
					layer.msg(data.message);
				}
				
			},
			error : function() {
				if(isCopy){
					layer.msg("复制失败");
				} else {
					layer.msg("移动失败");
				}
			}
		});
    }
	function doSubmit() {
		var form_data = $("#departmentForm").triggerHandler("submitForm");
		if (form_data) {
			$.post({
				url : $("#departmentForm").attr("action"),
				data : $("#departmentForm").serialize(),
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("保存成功");
						initDepartmentTree();
						notSave=false;
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("信息提交错误！");
				}
			});
		} else {
			layer.msg("数据验证错误！");
		}
		return false;
	}
    $(document).ready(function(){
    	initDepartmentTree();
    	$("#addRootDepartment").bind("click",addRootNode);
    	$('#departmentForm').on("submit",doSubmit);
    	$('input[type="radio"]').on("change",function(event){$("#departmentForm" ).triggerHandler( "validate" );});
    	$('input[type="checkbox"]').on("click",function(event){$( "#departmentForm" ).validVal();});
    	$( "#departmentForm" ).validVal();
    	$('.select-search').select2();
    });
	</script>
	<%
};%>
<% 
layout("../_inc/_layout.jsp",{head:scriptAndCss,body:contentBody}){} %>