<script type="text/javascript">
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">查看系统信息</h4>				
			</div>
			<div class="panel-body">
					<fieldset>
						<div class="row">
									<div class="col-md-6 col-sm-6">
										<label>系统全称：</label>
											${baseSystem.fullName!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>系统简称：</label>
											${baseSystem.shortName!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>访问URL：</label>
											<a href="${baseSystem.url!}" target="_blank">${baseSystem.url!}</a>
									</div>
									<div class="col-md-6 col-sm-6">
										<label>文件路径：</label>
											${baseSystem.filePath!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>用户登录地址：</label>
											${baseSystem.loginUrl!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>后台管理地址：</label>
											${baseSystem.adminUrl!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>联系人：</label>
											${baseSystem.contactPerson!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>联系手机：</label>
											${baseSystem.contactMobile!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>联系QQ：</label>
											${baseSystem.contactQQ!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>联系E-mail：</label>
											<a href="mailto:${baseSystem.contactEmail!}">${baseSystem.contactEmail!}</a>
									</div>
									<div class="col-md-6 col-sm-6">
										<label>状态：</label>
											${decode(baseSystem.status,1,"正常",0,"审批中/暂停使用",-1,"冻结/逻辑删除","未知")}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>备注说明：</label>
											<pre>${baseSystem.remark!}</pre>
									</div>
									<div class="col-md-6 col-sm-6">
										<label>其他设置JSON字符串：</label>
											<pre>${baseSystem.otherJson!}</pre>
									</div>
									<div class="col-md-6 col-sm-6">
										<label>创建时间：</label>
											 ${baseSystem.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>创建用户：</label>
											 ${baseSystem.createUser.realName!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>更新时间：</label>
											 ${baseSystem.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>更新用户：</label>
											 ${baseSystem.updateUser.realName!}
									</div>
						</div>
					</fieldset>
			</div>
		</div>