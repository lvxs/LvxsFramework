<script type="text/javascript">
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看网站栏目基础信息</h4>				
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>栏目名称：</label>
					<span id="classifyName">${cmsClassify.classifyName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>网站：</label>
					<span id="webSiteId">${cmsClassify.webSite.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>父级栏目：</label>
					<span id="parentId">${cmsClassify.parent.name!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>栏目编号：</label>
					<span id="classifyNumber">${cmsClassify.classifyNumber!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>排序编号：</label>
					<span id="sortNumber">${cmsClassify.sortNumber!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>SEO关键词：</label>
					<span id="keyWords">${cmsClassify.keyWords!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>栏目内容描述：</label>
					<span id="description">${cmsClassify.description!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>栏目图标1URL：</label>
					<span id="ico1Url">${cmsClassify.ico1Url!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>栏目图标2URL：</label>
					<span id="ico2Url">${cmsClassify.ico2Url!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>栏目图标3URL：</label>
					<span id="ico3Url">${cmsClassify.ico3Url!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>栏目HTML内容：</label>
					<pre id="classifyHtml">${cmsClassify.classifyHtml!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>用户级别要求：</label>
					<span id="userLevel">${cmsClassify.userLevel!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>浏览次数：</label>
					<span id="browseCount">${cmsClassify.browseCount!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>状态：</label>
					<span id="status">${decode(cmsClassify.status+"","-1","逻辑删除","0","已禁用/待审核","1","正常","未知")}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>备注说明：</label>
					<pre id="remark">${cmsClassify.remark!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>其他设置JSON字符串：</label>
					<pre id="otherJson">${cmsClassify.otherJson!}</pre>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建时间：</label>
					<span id="createTime">${cmsClassify.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>创建用户：</label>
					<span id="createUserId">${cmsClassify.createUser.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新时间：</label>
					<span id="updateTime">${cmsClassify.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>更新用户：</label>
					<span id="updateUserId">${cmsClassify.updateUser.realName!}</span>
				</div>
			</div>
		</fieldset>
	</div>
</div>