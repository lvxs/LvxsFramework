<script type="text/javascript">
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">查看用户部门</h4>				
	</div>
	<div class="panel-body">
		<fieldset>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<label>用户：</label>
					<span id="userId">${baseUserDepartment.user.realName!}</span>
				</div>
				<div class="col-md-6 col-sm-6">
					<label>部门：</label>
					<span id="departmentId">${baseUserDepartment.department.name!}</span>
				</div>
			</div>
		</fieldset>
	</div>
</div>