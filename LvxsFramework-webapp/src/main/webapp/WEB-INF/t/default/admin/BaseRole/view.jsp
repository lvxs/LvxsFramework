<script type="text/javascript">
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">查看角色信息</h4>				
			</div>
			<div class="panel-body">
					<fieldset>
						<div class="row">
									<div class="col-md-6 col-sm-6">
										<label>角色名称：</label>
											${baseRole.name!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>排序编号：</label>
											${baseRole.sortNumber!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>所属系统：</label>
											 ${baseRole.system.fullName!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>状态：</label>
											${decode(baseRole.status,1,"正常",0,"审批中/暂停使用",-1,"冻结/逻辑删除","未知")}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>备注说明：</label>
											<pre>${baseRole.remark!}</pre>
									</div>
									<div class="col-md-6 col-sm-6">
										<label>其他设置JSON字符串：</label>
											<pre>${baseRole.otherJson!}</pre>
									</div>
									<div class="col-md-6 col-sm-6">
										<label>创建用户：</label>
											 ${baseRole.createUser.realName!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>创建时间：</label>
											 ${baseRole.createTime,dateFormat="yyyy-MM-dd HH:mm:ss"}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>更新用户：</label>
											 ${baseRole.updateUser.realName!}
									</div>
									<div class="col-md-6 col-sm-6">
										<label>更新时间：</label>
											 ${baseRole.updateTime,dateFormat="yyyy-MM-dd HH:mm:ss"}
									</div>
						</div>
					</fieldset>
			</div>
		</div>