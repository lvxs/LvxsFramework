<link href="${basePath}/static/bird/assets/font-awesome/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/assets/icomoon/icomoon.css" media="all" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/animate.min.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/main.css" rel="stylesheet" type="text/css">	
<link href="${basePath}/static/bird/css/lvxsBase.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/bootstrap-extended.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/bird/css/plugins.css" rel="stylesheet" type="text/css">
<link href="${basePath}/static/Bootstrap-icon-picker/css/icon-picker.css" rel="stylesheet" type="text/css" />
<link href="${basePath }/static/ztree/css/awesomeStyle/awesome.css" rel="stylesheet" type="text/css">
<link href="${isNotEmpty(style)?basePath+'/static/bird/css/styles/'+style+'.css':basePath+'/static/bird/css/styles/lightblue.css'}" type="text/css" rel="stylesheet" id="style">
<script src="${basePath }/static/bird/js/jquery.min.js" type="text/javascript"></script>
<script src="${basePath }/static/bird/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${basePath}/static/bird/js/jquery.validVal/jquery.validVal.js" type="text/javascript"></script>
<script src="${basePath}/static/bird/js/forms/select2.min.js" type="text/javascript"></script>
<script src="${basePath }/static/ztree/js/jquery.ztree.core.js" type="text/javascript"></script>
<script src="${basePath }/static/ztree/js/jquery.ztree.excheck.js" type="text/javascript"></script>
<script src="${basePath }/static/ztree/js/jquery.ztree.exedit.js" type="text/javascript"></script>
<script src="${basePath}/static/layer/layer.js" type="text/javascript"></script>
<script src="${basePath}/static/Bootstrap-icon-picker/js/iconPicker.js" type="text/javascript"></script>
<script type="text/javascript">
	//目录树属性设置
	var setting = {
        view: {
            addHoverDom: addHoverDom,
            removeHoverDom: removeHoverDom,
			showLine: true,
            selectedMulti: false
        },
        check: {
            enable: false
            ,chkStyle: 'radio'
            ,radioType: "level"
        },
        data: {
            simpleData: {
                enable: true,
    			idKey: "id",
    			pIdKey: "parentId",
    			rootPId: 0
            }
        },
        edit: {
            enable: true,
    		showRemoveBtn: showRemoveBtn,
    		removeTitle: "删除节点",
    		showRenameBtn: false
        },
		callback: {
			onDrop: zTreeOnDrop,
			onClick: zTreeOnClick,
			onRemove: zTreeOnRemove,
			beforeDrag: zTreeBeforeDrag
		}
    };
    var menuTree;
    var newCount = 1,notSave=false;
    function initMenuTree(){
    	$.post({
			url : "${basePath}/admin/baseSystemMenu/getTreeJson?systemId=${baseSystem.id}",
			dataType : "json",
			success : function(data) {
				if (data.data!=null) {
					menuTree = $.fn.zTree.init($("#menuTree"), setting, data.data);
					newCount = menuTree.getNodes().length;
				} else {
					menuTree = $.fn.zTree.init($("#menuTree"), setting, []);
				}
				menuTree.expandAll(true);
			},
			error : function() {
				layer.msg("目录树加载错误");
			}
		});
    }
    function addRootNode(event) {
    	if(notSave){
    		layer.msg("有新建的子菜单未保存，不能添加！",{time:1000});
    		return false;
    	}
    	$("#addRootMenu").unbind("click");
    	var menuNumber = "000" +  (menuTree.getNodes().length+1);
    	menuNumber = menuNumber.substring(menuNumber.length-3);
    	var baseSystemMenu={"baseSystemMenu.name":"菜单" +menuNumber,"baseSystemMenu.sortNumber":(menuTree.getNodes().length+1),"baseSystemMenu.menuNumber":menuNumber,"baseSystemMenu.systemId":${baseSystem.id},"baseSystemMenu.parentId":0,"baseSystemMenu.status":1};
    	$.post({
			url:"${basePath}/admin/baseSystemMenu/save",
			data:baseSystemMenu, 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("新增成功",{time:1000});
					$.ajax({
						url : "${basePath}/admin/baseSystemMenu/getTreeJson?systemId=" + ${baseSystem.id},
						dataType : "json",
						success : function(data) {
							if (data.data!=null) {
								menuTree = $.fn.zTree.init($("#menuTree"), setting, data.data);
								menuTree.expandAll(true);
								newCount = menuTree.getNodes().length;
								if(newCount>0){
									zTreeOnClick(event, menuTree.treeId, menuTree.getNodes()[newCount-1]);
									menuTree.selectNode(menuTree.getNodes()[newCount-1]);
								}
							} else {
								menuTree = $.fn.zTree.init($("#menuTree"), setting, []);
							}
						},
						error : function() {
							layer.msg("目录树加载错误");
						}
					});
				}else{
					layer.msg(data.message);
				}
				$("#addRootMenu").bind("click",addRootNode);
			},
			error : function() {
				layer.msg("新增失败！");
				$("#addRootMenu").bind("click",addRootNode);
			}
		});
    }
    function zTreeBeforeDrag(treeId, treeNodes) {
		if(notSave){
			layer.msg("有新建的子菜单未保存，不能拖拽！",{time:1000});
			return false;
		}
		return true;
	}
    function zTreeOnRemove(event, treeId, treeNode) {
    	if(treeNode.id!=null){
	    	$.ajax({
				url:"${basePath}/admin/baseSystemMenu/delete?id=" + treeNode.id,
				dataType : "json", 
				success : function(data) { 
					if(data.errorCode == 0){
						layer.msg("删除成功",{time:1000});
					}else{
						initMenuTree();
						layer.msg(data.message);
			    		notSave = false;
					}
				},
				error : function() {
					initMenuTree();
					layer.msg("删除失败！");
		    		notSave = false;
				}
			});
    	} else {
    		notSave = false;
    	}
    	$("#editSystemMenu").hide();
    }
    
    function zTreeOnClick(event, treeId, treeNode) {
    	$("#editSystemMenu").show();
    	if(treeNode.id!=null && treeNode.id>0){
    		$("#actionManage").show();
        	$('#systemMenuForm [name="baseSystemMenu.id"]').val(treeNode.id);
        	$('#menuActionForm input[name="baseAction.menuId"]').val(treeNode.id);
        	initActionList(treeNode.id);
    		$('#systemMenuForm').attr("action","update");
    	}else{
    		$("#actionManage").hide();
        	$('#systemMenuForm [name="baseSystemMenu.id"]').val("");
        	$('#menuActionForm input[name="baseAction.menuId"]').val("");
    		$('#systemMenuForm').attr("action","save");
    	}
    	$('#systemMenuForm [name="baseSystemMenu.name"]').val(treeNode.name);
    	$('#systemMenuForm [name="baseSystemMenu.systemId"]').val(treeNode.systemId);
    	$('#systemMenuForm [name="baseSystemMenu.parentId"]').val(treeNode.parentId);
    	$('#systemMenuForm [name="baseSystemMenu.sortNumber"]').val(treeNode.sortNumber);
    	$('#systemMenuForm [name="baseSystemMenu.menuNumber"]').val(treeNode.menuNumber);
    	$('#systemMenuForm [name="baseSystemMenu.url"]').val(treeNode.url);
    	$('#systemMenuForm [name="baseSystemMenu.target"]').val(treeNode.target);
    	$('#systemMenuForm [name="baseSystemMenu.helpUrl"]').val(treeNode.helpUrl);
    	$('#systemMenuForm [name="baseSystemMenu.iconSkin"]').val(treeNode.iconSkin);
    	$('#iconSkin').removeClass();
    	$('#iconSkin').addClass(treeNode.iconSkin);
    	$('#systemMenuForm [name="baseSystemMenu.briefDescription"]').val(treeNode.briefDescription);
    	$('#systemMenuForm [name="baseSystemMenu.status"][value="'+treeNode.status+'"]').attr("checked","checked");
    	$('#systemMenuForm [name="baseSystemMenu.remark"]').val(treeNode.remark);
    }
    
    function showRemoveBtn(treeId, treeNode) {
    	if(treeNode.children!=null && treeNode.children.length>0){
    		return false;
    	} else {
        	return true;	
    	}
    }
    function addHoverDom(treeId, treeNode) {
        var sObj = $("#" + treeNode.tId + "_span");
        if (notSave || treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0 || treeNode.id==null) return;
        var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' title='添加子菜单' onfocus='this.blur();'></span>";
        sObj.after(addStr);
        var btn = $("#addBtn_"+treeNode.tId);
        if (btn) btn.bind("click", function(){
        	newCount=0;
        	var sortNumber = 0;
        	if(treeNode!=null && treeNode.children!=null && treeNode.children.length>0){
        		newCount = treeNode.children.length;
        		sortNumber = newCount;
        		for(var i=newCount-1;i>=0;i--){
        			if(treeNode.children[i].sortNumber>sortNumber){
        				sortNumber=treeNode.children[i].sortNumber;
        			}
        		}
        	}
        	sortNumber = sortNumber+1;        	
        	var menuNumber="000"+sortNumber+"";
        	menuNumber = menuNumber.substring(menuNumber.length-3);
        	menuTree.addNodes(treeNode, {"name":"未保存菜单" +menuNumber,"title":"请点击右侧表单保存按钮，否则添加不生效！","parentId":treeNode.id,"sortNumber":sortNumber,"menuNumber":treeNode.menuNumber+menuNumber,"systemId":treeNode.systemId});
			zTreeOnClick(event, menuTree.treeId, treeNode.children[newCount]);
			menuTree.selectNode(treeNode.children[newCount]);
			notSave=true;
            return false;
        });
    };
    function removeHoverDom(treeId, treeNode) {
        $("#addBtn_"+treeNode.tId).unbind().remove();
    }
    function zTreeOnDrop(event, treeId, treeNodes, targetNode, moveType, isCopy) {
    	var peerNodes = [];
    	if(targetNode.parentId!=null && targetNode.parentId>0) {
    		var parentNode = menuTree.getNodeByParam("id",targetNode.parentId, null);
    		if(parentNode!=null && parentNode.children){
    			peerNodes = parentNode.children;
    		}
    	} else {
    		peerNodes = menuTree.getNodes();
    	}
    	var dropData={"treeNodesJSON":JSON.stringify(treeNodes),"targetNodeJSON":JSON.stringify(targetNode),"peerNodesJSON":JSON.stringify(peerNodes),"moveType":moveType,"systemId":${baseSystem.id},"isCopy":isCopy};
    	$.post({
			url:"${basePath}/admin/baseSystemMenu/drop",
			data:dropData,
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode >= 0){
					if(isCopy){
						layer.msg("复制成功",{time:1000});
					} else {
						layer.msg("移动成功",{time:1000});
					}
					initMenuTree();
				}else{
					layer.msg(data.message);
				}
				
			},
			error : function() {
				if(isCopy){
					layer.msg("复制失败");
				} else {
					layer.msg("移动失败");
				}
			}
		});
    }
	function doSubmit() {
		var form_data = $("#systemMenuForm").triggerHandler("submitForm");
		if (form_data) {
			$.post({
				url : $("#systemMenuForm").attr("action"),
				data : $("#systemMenuForm").serialize(),
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("保存成功");
						initMenuTree();
						notSave=false;
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("信息提交错误！");
				}
			});
		} else {
			layer.msg("数据验证错误！");
		}
		return false;
	}
	var actionData = [];
    function initActionList(menuId){
    	$.ajax({
			url : "${basePath}/admin/baseAction/getDataJson?menuId=" + menuId,
			dataType : "json",
			success : function(data) {
				if (data.data!=null) {
					$("#actionList li:gt(0)").remove();
					actionData = data.data;
					for(var i=0;i<actionData.length;i++){
						$("#actionList").append("<li><a href=\"javascript:editAction("+actionData[i].id+");\"><i class=\"fa fa-edit\"></i>"+actionData[i].name+"</a> <a href=\"javascript:delAction("+actionData[i].id+");\"><i class=\"fa fa-times\"></a></li>");
					}
				}
			},
			error : function() {
				layer.msg("动作列表加载错误");
			}
		});
    }
	function actionFormSubmit() {
		var form_data = $("#menuActionForm").triggerHandler("submitForm");
		if (form_data) {
			var menuId = parseInt($('#menuActionForm input[name="baseAction.menuId"]').val());
			if(isNaN(menuId) || menuId<1){
				var selectedNodes = menuTree.getSelectedNodes();
				if(selectedNodes!=null && selectedNodes.length>0){
					menuId = parseInt(selectedNodes[0].id);
					$('#menuActionForm input[name="baseAction.menuId"]').val(menuId);
				}
			}
			if(isNaN(menuId) || menuId<1){
				 layer.alert("非法操作");
				 return false;
			}
			var submitData = $("#menuActionForm").serialize();
			var url = "${basePath}/admin/baseAction/save";
			if(!isNaN(parseInt($('#menuActionForm input[name="baseAction.id"]').val())) && parseInt($('#menuActionForm input[name="baseAction.id"]').val())>0){
				 url = "${basePath}/admin/baseAction/update";
			}
			$.post({
				url : url,data :submitData,
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("保存成功");
						initActionList(menuId);
						addAction();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("信息提交错误！");
				}
			});
		} else {
			layer.msg("数据验证错误！");
		}
		return false;
	}
	function addAction(){
		$("#menuActionForm :reset").click();
		var menuId = $('#systemMenuForm input[name="baseSystemMenu.id"]').val();
		var treeNode = menuTree.getNodeByParam("id",menuId, null);
		var menuUrl = treeNode.url;
		if(menuUrl.lastIndexOf("/")!=menuUrl.length-1)menuUrl+="/";
		$('#menuActionForm select[name="baseAction.id"]').val("");
		$('#menuActionForm select[name="baseAction.actionType"]').val("");
		$('#menuActionForm input[name="baseAction.sortNumber"]').val("");
		$('#menuActionForm input[name="baseAction.name"]').val("");
		$('#menuActionForm input[name="baseAction.methodName"]').val("");
		$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"");
		$('#menuActionForm input[name="baseAction.jsMethod"]').val("");
		$('#menuActionForm input[name="baseAction.linkUri"]').val("");
		$('#menuActionForm input[name="baseAction.status"][value="1"]').attr("checked","checked");
		$('#menuActionForm [name="baseAction.remark"]').val("");
	}
	function editAction(actionId){
		for(var i=0;i<actionData.length;i++){
			if(actionData[i].id!=actionId)continue;
			$('#menuActionForm input[name="baseAction.id"]').val(actionData[i].id);
			$('#menuActionForm select[name="baseAction.actionType"]').val(actionData[i].actionType);
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(actionData[i].sortNumber);
			$('#menuActionForm input[name="baseAction.name"]').val(actionData[i].name);
			$('#menuActionForm input[name="baseAction.methodName"]').val(actionData[i].methodName);
			$('#menuActionForm input[name="baseAction.fullUri"]').val(actionData[i].fullUri);
			$('#menuActionForm input[name="baseAction.jsMethod"]').val(actionData[i].jsMethod);
			$('#menuActionForm input[name="baseAction.linkUri"]').val(actionData[i].linkUri);
			$('#menuActionForm input[name="baseAction.status"][value="'+actionData[i].status+'"]').attr("checked","checked");
			$('#menuActionForm [name="baseAction.remark"]').val(actionData[i].remark);
			break;
		}
	}
	function delAction(actionId) {
		var menuId = parseInt($('#menuActionForm input[name="baseAction.menuId"]').val());
		if(isNaN(menuId) || menuId<1){
			 layer.alert("非法操作");
			 return false;
		}
		layer.confirm("确定要删除改记录么？" ,{
			btn : [ '确定删除', '放弃删除' ]//按钮
		}, function() {
			$.get({
				url : "${basePath}/admin/baseAction/delete?id="+actionId,
				dataType : "json",
				success : function(data) {
					if (data.errorCode == 0) {
						layer.msg("删除成功！");
						initActionList(menuId);
						addAction();
					} else {
						layer.msg(data.message);
					}
				},
				error : function() {
					layer.msg("删除出错！");
				}
			});
		});
		return false;
	}
	function changeActionType(actionTypeSelector){
		var actionType = $(actionTypeSelector).val();
		var menuId = $('#systemMenuForm input[name="baseSystemMenu.id"]').val();
		var treeNode = menuTree.getNodeByParam("id",menuId, null);
		var menuUrl = treeNode.url;
		if(menuUrl.lastIndexOf("/")!=menuUrl.length-1)menuUrl+="/";
		if(actionType==1){
			//列表页面
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(1);
			$('#menuActionForm input[name="baseAction.name"]').val("列表页面");
			$('#menuActionForm input[name="baseAction.methodName"]').val("index");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"index");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("index");
		} else if(actionType==2){
			//新增页面
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(3);
			$('#menuActionForm input[name="baseAction.name"]').val("新增页面");
			$('#menuActionForm input[name="baseAction.methodName"]').val("add");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"add");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("add();");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("add");
		}else if(actionType==3){
			//修改页面
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(5);
			$('#menuActionForm input[name="baseAction.name"]').val("修改页面");
			$('#menuActionForm input[name="baseAction.methodName"]').val("edit");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"edit");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("edit();");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("edit");
		}else if(actionType==4){
			//查看页面
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(9);
			$('#menuActionForm input[name="baseAction.name"]').val("查看页面");
			$('#menuActionForm input[name="baseAction.methodName"]').val("view");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"view");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("view();");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("view");
		}else if(actionType==5){
			//其他页面
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(15);
			$('#menuActionForm input[name="baseAction.name"]').val("");
			$('#menuActionForm input[name="baseAction.methodName"]').val("");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("");
		}else if(actionType==11){
			//新增保存
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(4);
			$('#menuActionForm input[name="baseAction.name"]').val("新增保存");
			$('#menuActionForm input[name="baseAction.methodName"]').val("save");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"save");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("save");
		}else if(actionType==12){
			//修改保存
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(6);
			$('#menuActionForm input[name="baseAction.name"]').val("修改保存");
			$('#menuActionForm input[name="baseAction.methodName"]').val("update");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"update");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("update");
		}else if(actionType==13){
			//删除
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(7);
			$('#menuActionForm input[name="baseAction.name"]').val("删除");
			$('#menuActionForm input[name="baseAction.methodName"]').val("delete");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"delete");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("del();");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("delete");
		}else if(actionType==14){
			//批量删除
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(8);
			$('#menuActionForm input[name="baseAction.name"]').val("批量删除");
			$('#menuActionForm input[name="baseAction.methodName"]').val("delete");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"delete");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("batchDel();");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("delete");
		}else if(actionType==15){
			//查询结果列表JSON
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(2);
			$('#menuActionForm input[name="baseAction.name"]').val("查询结果列表JSON");
			$('#menuActionForm input[name="baseAction.methodName"]').val("searchData");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"searchData");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("searchData");
		}else if(actionType==16){
			//其他操作
			$('#menuActionForm input[name="baseAction.sortNumber"]').val(20);
			$('#menuActionForm input[name="baseAction.name"]').val("");
			$('#menuActionForm input[name="baseAction.methodName"]').val("");
			$('#menuActionForm input[name="baseAction.fullUri"]').val(menuUrl+"");
			$('#menuActionForm input[name="baseAction.jsMethod"]').val("");
			$('#menuActionForm input[name="baseAction.linkUri"]').val("");
		}
	}
    $(document).ready(function(){
    	initMenuTree();
    	$("#addRootMenu").bind("click",addRootNode);
    	$('#systemMenuForm').on("submit",doSubmit);
    	$('input[type="radio"]').on("change",function(event){$("#systemMenuForm" ).triggerHandler( "validate" );});
    	$('input[type="checkbox"]').on("click",function(event){$( "#systemMenuForm" ).validVal();});
    	$( "#systemMenuForm" ).validVal();
    	$('.select-search').select2();
    	$(".picker-icons").iconPicker();
    	$('#menuActionForm').on("submit",actionFormSubmit);
    	$('input[type="radio"]').on("change",function(event){$("#menuActionForm" ).triggerHandler( "validate" );});
    	$('input[type="checkbox"]').on("click",function(event){$( "#menuActionForm" ).validVal();});
    	$( "#menuActionForm" ).validVal();
    });
</script>
<div class="panel panel-flat">
	<div class="panel-heading">
		<h4 class="panel-title">《${baseSystem.fullName }》功能菜单管理</h4>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3 col-sm-3" >
				<div class="panel .panel-white">
					<div class="panel-heading">
						<h5 class="panel-title" style="text-align:right;">
							<a id="addRootMenu"><i class="fa fa-plus-square">新增一级菜单</i></a>
						</h5>
					</div>
					<div class="panel-body sidebar" style="display:block;width:100%;" title="可以通过拖拽调整菜单位置和顺序;按住Ctrl或者Cmd然后拖拽，可以复制菜单">
						<ul id="menuTree" class="ztree">功能菜单树</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-9">
				<div class="tabbable" id="editSystemMenu" style="display:none;">
					<ul class="nav nav-tabs nav-tabs-highlight">
						<li class="active"><a href="#highlight-tab1" data-toggle="tab">菜单属性设置</a></li>
						<li id="actionManage" style="display:none;"><a href="#highlighted-tab2" data-toggle="tab">菜单动作管理</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="highlight-tab1">
							<form id="systemMenuForm" class="form-validation" action="update">
								<input type="hidden" name="baseSystemMenu.id" value="">
								<input type="hidden" name="baseSystemMenu.systemId" value="">
								<input type="hidden" name="baseSystemMenu.parentId" value="">
								<input type="hidden" name="baseSystemMenu.sortNumber" value="">
								<input type="hidden" name="baseSystemMenu.menuNumber" value="">
								<fieldset>
									<div class="row">
										<div class="col-md-6 col-sm-6">
											<label>菜单名称：</label>
												<input type="text" name="baseSystemMenu.name" class="form-control " placeholder="" value="" >
										</div>
										<div class="col-md-6 col-sm-6">
											<label>链接URL：</label>
												<input type="text" name="baseSystemMenu.url" class="form-control " placeholder="访问外部链接地址使用http开始的完整URL地址，访问本系统或者子系统使用以/开头的相对路径URL" value="" >
										</div>
										<div class="col-md-6 col-sm-6">
											<label>目标窗口：</label>
												<input type="text" name="baseSystemMenu.target" class="form-control " placeholder="目标窗口名称，新建窗口_blank，顶级窗口_top，本窗口请留空或者_self，其他固定名称的窗口请直接填写窗口名称" value="" >
										</div>
										<div class="col-md-6 col-sm-6">
											<label>帮助文档URL：</label>
												<input type="text" name="baseSystemMenu.helpUrl" class="form-control " placeholder="" value="" >
										</div>
										<div class="col-md-6 col-sm-6">
											<label>图标：</label>
											<input type="text" name="baseSystemMenu.iconSkin" class="picker-icons form-control " placeholder="图标CSS样式名称如fa-home、icon-attachment" />
										</div>
										<div class="col-md-6 col-sm-6">
											<label>简要说明：</label>
												<input type="text" name="baseSystemMenu.briefDescription" class="form-control " placeholder="" value="" >
										</div>
										<div class="col-md-6 col-sm-6">
											<label>状态：</label>
												<div class="radio-list" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
												<label>
													<input type="radio" name="baseSystemMenu.status" value="1" class=" requiredgroup:status">
													正常
												</label>
												<label>
													<input type="radio" name="baseSystemMenu.status" value="0" class=" requiredgroup:status">
													审批中/暂停使用
												</label>
												<label>
													<input type="radio" name="baseSystemMenu.status" value="-1" class=" requiredgroup:status">
													冻结/逻辑删除
												</label>
												</div>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>备注说明：</label>
												<textarea name="baseSystemMenu.remark" class="form-control" placeholder="" rows="5" cols="5" ></textarea>
										</div>
									</div>
								</fieldset>
								<div class="form-wizard-actions  " style="margin:10px;">
									<input class="btn btn-default" id="validation-back" value="重置" type="reset">
									<input class="btn btn-info" id="validation-next" value="提交" type="submit">
								</div>
							</form>	
						</div>
						<div class="tab-pane row" id="highlighted-tab2">
							<div class="col-xs-12"><ul id="actionList"><li><a href="javascript:addAction();"><i class="fa fa-plus"></i>新增动作</li></a></ul></div>
							<div class="col-xs-12">
								<form id="menuActionForm" class="form-validation" action="${basePath}/admin/baseAction/save">
									<input type="hidden" name="baseAction.id" value="" />
									<input type="hidden" name="baseAction.menuId" value="" />
									<fieldset>
										<div class="row">
											<div class="col-md-6 col-sm-6">
												<label>动作类型：</label>
												<select class="select form-control required" name="baseAction.actionType" onchange="changeActionType(this);">
													<option value="">请选择</option>
													<option value="1">列表页面</option>
													<option value="2">新增页面</option>
													<option value="3">修改页面</option>
													<option value="4">查看页面</option>
													<option value="5">其他页面</option>
													<option value="11">保存</option>
													<option value="12">更新</option>
													<option value="13">删除</option>
													<option value="14">批量删除</option>
													<option value="15">查询结果列表JSON</option>
													<option value="16">其他操作</option>
												</select>
											</div>
											<div class="col-md-6 col-sm-6">
												<label>排序编号：</label>
												<input type="number" name="baseAction.sortNumber" class="form-control  number required" placeholder="" value="10" >
											</div>
											<div class="col-md-6 col-sm-6">
												<label>名称：</label>
												<input type="text" name="baseAction.name" class="form-control required" placeholder="">
											</div>
											<div class="col-md-6 col-sm-6">
												<label>方法名：</label>
												<input type="text" name="baseAction.methodName" class="form-control required" placeholder="">
											</div>
											<div class="col-md-6 col-sm-6">
												<label>完整URI：</label>
												<input type="text" name="baseAction.fullUri" class="form-control required" placeholder="" >
											</div>
											<div class="col-md-6 col-sm-6">
												<label>JavaScript方法：</label>
												<input type="text" name="baseAction.jsMethod" class="form-control " placeholder="" >
											</div>
											<div class="col-md-6 col-sm-6">
												<label>链接URI：</label>
												<input type="text" name="baseAction.linkUri" class="form-control required" placeholder="页面内链接URI">
											</div>
											<div class="col-md-6 col-sm-6">
												<label>状态：</label>
												<div class="radio-list required" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
													<label>
														<input type="radio" name="baseAction.status" value="1" checked="checked" class="required requiredgroup:status">
														正常
													</label>
													<label>
														<input type="radio" name="baseAction.status" value="0" class="required requiredgroup:status">
														审批中/暂停使用
													</label>
													<label>
														<input type="radio" name="baseAction.status" value="-1" class="required requiredgroup:status">
														冻结/逻辑删除
													</label>
												</div>
											</div>
											<div class="col-md-6 col-sm-6">
												<label>备注说明：</label>
												<textarea name="baseAction.remark" class="form-control " placeholder="" rows="1" cols="5" ></textarea>
											</div>
										</div>
									</fieldset>
									<div class="form-wizard-actions" style="margin:10px;">
										<input class="btn btn-default" value="重置" type="reset">
										<input class="btn btn-info" value="提交" type="submit">
									</div>
								</form>	
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>