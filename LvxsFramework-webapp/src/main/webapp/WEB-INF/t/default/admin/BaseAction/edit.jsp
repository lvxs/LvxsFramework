<script type="text/javascript">
/*
//表单提交前数据预处理逻辑，处理成功返回True，处理失败请返回False，处理失败时则不再提交
function beforeSubmit(){
	return true;
}*/
function doSubmit(){
	var form_data = $( "#form" ).triggerHandler( "submitForm" );
	if(form_data){
		//如果存在表单提交前数据预处理逻辑，则执行，否则自动忽略
		if(typeof(beforeSubmit)=="function"){
			//如果数据预处理逻辑返回False，则提示并且终止数据提交
			if(!beforeSubmit()){
				layer.msg("数据预处理出错！");
				return false;
			}
		}
		$.ajax({
			url:$("#form").attr("action"),
			data:$("#form").serialize(), 
			dataType : "json", 
			success : function(data) { 
				if(data.errorCode == 0){
					layer.msg("保存成功");
					location.reload();
				}else{
					layer.msg(data.message);
				}
			},
			error : function() {
				layer.msg("信息提交错误！");
			}
		});
	} else {
		layer.msg("数据验证错误！");
	}
 	return false;
} 
$(document).ready(function(){
	$('#form').on("submit",doSubmit);
	$('input[type="radio"]').on("change",function(event){$("#form" ).triggerHandler( "validate" );});
	$('input[type="checkbox"]').on("click",function(event){$( "#form" ).validVal();});
	$('select').select2();
	$( "#form" ).validVal();
});
 </script>
 <div class="panel panel-flat">
			<div class="panel-heading">
				<h4 class="panel-title">${(''==baseAction.id!'')?'新增':'修改'}页面动作</h4>				
			</div>
			<div class="panel-body">
				<form id="form" class="form-horizontal" action="${(''==baseAction.id!'')?'save':'update'}">
					<input type="hidden" name="baseAction.id" value="${baseAction.id! }">
					<fieldset>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<label>系统菜单ID：</label>
								<select class="select form-control required" name="baseAction.menuId">
									<option value="">请选择</option>
									<option value="1" ${baseAction.menuId! ==1?'selected'} >已选择的信息</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>排序编号：</label>
								<input type="number" name="baseAction.sortNumber" class="form-control  number" placeholder="" value="${baseAction.sortNumber!}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>名称：</label>
								<input type="text" name="baseAction.name" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAction.name!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>动作类型：</label>
								<select name="baseAction.actionType" class="form-control " placeholder="请选择">
									<option value="1" ${baseAction.actionType+'' == '1'?'selected="selected"'}>列表页面</option>
									<option value="2" ${baseAction.actionType+'' == '2'?'selected="selected"'}>新增页面</option>
									<option value="3" ${baseAction.actionType+'' == '3'?'selected="selected"'}>修改页面</option>
									<option value="4" ${baseAction.actionType+'' == '4'?'selected="selected"'}>查看页面</option>
									<option value="5" ${baseAction.actionType+'' == '5'?'selected="selected"'}>其他页面</option>
									<option value="11" ${baseAction.actionType+'' == '11'?'selected="selected"'}>保存</option>
									<option value="12" ${baseAction.actionType+'' == '12'?'selected="selected"'}>更新</option>
									<option value="13" ${baseAction.actionType+'' == '13'?'selected="selected"'}>删除</option>
									<option value="14" ${baseAction.actionType+'' == '14'?'selected="selected"'}>批量删除</option>
									<option value="15" ${baseAction.actionType+'' == '15'?'selected="selected"'}>查询结果列表JSON</option>
									<option value="16" ${baseAction.actionType+'' == '16'?'selected="selected"'}>其他操作</option>
								</select>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>方法名：</label>
								<input type="text" name="baseAction.methodName" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAction.methodName!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>完整URI：</label>
								<input type="text" name="baseAction.fullUri" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAction.fullUri!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>JavaScript方法：</label>
								<input type="text" name="baseAction.jsMethod" class="form-control " placeholder="" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAction.jsMethod!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>链接URI：</label>
								<input type="text" name="baseAction.linkUri" class="form-control " placeholder="页面内链接URI" value="${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAction.linkUri!)}" >
							</div>
							<div class="col-md-6 col-sm-6">
								<label>状态：</label>
								<div class="radio-list" placeholder="-1:逻辑删除;0:已禁用/待审核;1:正常">
									<label><input type="radio" name="baseAction.status" value="-1" ${baseAction.status+'' == '-1'?'checked="checked"'} class=" requiredgroup:status">逻辑删除</label>
									<label><input type="radio" name="baseAction.status" value="0" ${baseAction.status+'' == '0'?'checked="checked"'} class=" requiredgroup:status">已禁用/待审核</label>
									<label><input type="radio" name="baseAction.status" value="1" ${baseAction.status+'' == '1'?'checked="checked"'} class=" requiredgroup:status">正常</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<label>备注说明：</label>
								<textarea name="baseAction.remark" class="form-control " placeholder="" rows="5" cols="5" >${@org.cnitti.utils.EscapeUtils.escapeXml11(baseAction.remark!)}</textarea>
							</div>
						</div>
					</fieldset>
					<div class="form-wizard-actions">
						<input class="btn btn-default" value="重置" type="reset">
						<input class="btn btn-info" id="submit" value="提交" type="submit">
					</div>
				</form>						
			</div>
		</div>