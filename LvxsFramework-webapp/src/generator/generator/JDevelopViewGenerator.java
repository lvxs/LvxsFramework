package generator;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.cnitti.model.OaCustomForm;
import org.cnitti.model.OaCustomFormField;
import org.cnitti.utils.StringUtils;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.dialect.Dialect;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.activerecord.generator.TableMeta;

public class JDevelopViewGenerator {

	private String viewDirectory;
	protected Dialect dialect = new MysqlDialect();

	public JDevelopViewGenerator(String modelPackageName, String baseModelPackageName, String viewDirectory) {
		this.viewDirectory = viewDirectory;
	}

	public void generate(List<TableMeta> tableMetas) {
		System.out.println("Generate Develop View Jsp ...");
		for (TableMeta tableMeta : tableMetas){
			boolean develop = false;
			OaCustomForm oaCustomForm = OaCustomForm.DAO.findFirst("Select * from oa_custom_form where physicalName=?",tableMeta.name);
			if(oaCustomForm!=null){
				List<OaCustomFormField> fieldList = OaCustomFormField.DAO.find("select * from oa_custom_form_field where formId=?",oaCustomForm.getId());
				if(fieldList!=null && fieldList.size()>0){
					generate(oaCustomForm,fieldList,"list.jsp",GeneratorDemo.developViewTempletFile_list);
					//generate(oaCustomForm,fieldList,"edit.jsp",GeneratorDemo.developViewTempletFile_edit);
					//generate(oaCustomForm,fieldList,"add.jsp",GeneratorDemo.developViewTempletFile_add);
					//generate(oaCustomForm,fieldList,"view.jsp",GeneratorDemo.developViewTempletFile_view);
					develop = true;
				}
			}
			if(!develop){
				generate(tableMeta,"list.jsp",GeneratorDemo.viewTempletFile_list);
				generate(tableMeta,"edit.jsp",GeneratorDemo.viewTempletFile_edit);
				generate(tableMeta,"add.jsp",GeneratorDemo.viewTempletFile_add);
				generate(tableMeta,"view.jsp",GeneratorDemo.viewTempletFile_view);
			}
		}
	}

	protected void generate(OaCustomForm oaCustomForm,List<OaCustomFormField> fieldList, String jspFileName, String templetFileName) {
		File dir = new File(this.viewDirectory + File.separator + buildModelName(oaCustomForm.getPhysicalName()));
		if (!dir.exists())dir.mkdirs();
		File file = new File(dir.getAbsoluteFile() + "/" + jspFileName);
		if (!file.exists() || file.length() < 10) {
			try {
				FileWriter fw = new FileWriter(file);
				ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader();
				Configuration cfg = Configuration.defaultConfiguration();
				GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
				Template t = gt.getTemplate(templetFileName);
				t.binding("oaCustomForm", oaCustomForm);
				t.binding("fieldList", fieldList);
				for(OaCustomFormField field:fieldList){
					if(field.getIsPrimaryKey()==1){
						t.binding("idField", field);
					}
				}
				try {
					String str = t.render();
					if (StringUtils.length(str) > 10) {
						fw.write(str);
						fw.close();
					} else {
						fw.close();
						file.delete();
					}
				} catch (Exception e) {
					fw.close();
					file.delete();
				}
			} catch (Exception e) {
			}
		}
	}

	protected void generate(TableMeta tableMeta, String jspFileName, String templetFileName) {
		File dir = new File(this.viewDirectory + File.separator + tableMeta.modelName);
		if (StrKit.isBlank(tableMeta.remarks))
			tableMeta.remarks = tableMeta.modelName;
		if (!dir.exists())
			dir.mkdirs();
		File file = new File(dir.getAbsoluteFile() + "/" + jspFileName);
		if (!file.exists() || file.length() < 10) {
			try {
				FileWriter fw = new FileWriter(file);
				ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader();
				Configuration cfg = Configuration.defaultConfiguration();
				GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
				Template t = gt.getTemplate(templetFileName);
				t.binding("tableMeta", tableMeta);
				try {
					String str = t.render();
					if (StringUtils.length(str) > 10) {
						fw.write(str);
						fw.close();
					} else {
						fw.close();
						file.delete();
					}
				} catch (Exception e) {
					fw.close();
					file.delete();
				}
			} catch (Exception e) {
			}
		}
	}
	
	/**
	 * 构造 modelName，mysql 的 tableName 建议使用小写字母，多单词表名使用下划线分隔，不建议使用驼峰命名
	 * oracle 之下的 tableName 建议使用下划线分隔多单词名，无论 mysql还是 oralce，tableName 都不建议使用驼峰命名
	 */
	protected String buildModelName(String tableName) {
		// 将 oralce 大写的 tableName 转成小写，再生成 modelName
		if (dialect instanceof OracleDialect) {
			tableName = tableName.toLowerCase();
		}
		
		return StrKit.firstCharToUpperCase(StrKit.toCamelCase(tableName));
	}
}
