/**
 * LvxsFramework自动代码生成工具
 * @author madj
 */
package generator;

import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.sql.DataSource;

import org.cnitti.model.OaCustomForm;
import org.cnitti.model.OaCustomFormField;
import org.cnitti.utils.StringUtils;

import com.jfinal.kit.DesKit;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.MetaBuilder;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.jfinal.plugin.druid.DruidPlugin;

public class JGenerator {

	
	private final String basePackage;
	private final String jdbcUrl;
	private final String dbUser;
	private String dbPassword;
	private String viewDirectory;
	
	
	public JGenerator(String basePackage, String jdbcUrl, String dbUser,String dbPassword,String viewDirectory) {
		
		this.basePackage = basePackage;
		this.jdbcUrl = jdbcUrl;
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;
		this.viewDirectory =viewDirectory;
	}



	public void doGenerate(){
		String modelPackage = basePackage+".model";
		String baseModelPackage = basePackage+".model.base";
		String adminControllerPackage = basePackage+".controller.admin";
		
		String viewDirectory = PathKit.getWebRootPath() + "/"+this.viewDirectory;
		boolean developGenerate = false;
		try {
			Class<?> aClass = Class.forName(basePackage+".model.OaCustomForm");
			System.out.println(aClass.getName()+"类存在");
			developGenerate = true;
		} catch (ClassNotFoundException e) {
		}
		if(developGenerate){
			try {
				Class<?> aClass = Class.forName(basePackage+".model.OaCustomFormField");
				System.out.println(aClass.getName()+"类存在");
				developGenerate = true;
			} catch (ClassNotFoundException e) {
				developGenerate = false;
			}	
		}
		
		List<TableMeta> tableMetaList = new MetaBuilder(getDataSource(developGenerate)).build();
		if(developGenerate){
			for(TableMeta tableMeta:tableMetaList){
				OaCustomForm oaCustomForm = OaCustomForm.DAO.findFirst("Select * from oa_custom_form where physicalName=?",tableMeta.name);
				if(oaCustomForm==null){
					oaCustomForm = new OaCustomForm();
					//设置表显示名称
					oaCustomForm.setName(tableMeta.getRemarks());
					//设置物理表名称
					oaCustomForm.setPhysicalName(tableMeta.name);
					//设置表备注说明
					oaCustomForm.setRemark(tableMeta.getHelpText());
					//设置表单类型为实体代码
					oaCustomForm.setType(3);
					//设置状态为正常
					oaCustomForm.setStatus(1);
					//如果ID为空，则表示要新增数据
					Random rand = new Random(System.currentTimeMillis()%3333);
					//自动生成ID
					oaCustomForm.setId(System.currentTimeMillis()*1000+rand.nextInt(999));
					//设置创建用户ID为1
					oaCustomForm.setCreateUserId(1L);
					//设置创建时间为当前时间
					oaCustomForm.setCreateTime(new Date());
					//设置更新用户ID为当1
					oaCustomForm.setUpdateUserId(1L);
					//设置更新时间为当前时间
					oaCustomForm.setUpdateTime(new Date());
					oaCustomForm.save();
				}
				int sortNumber = 0;
				for(ColumnMeta columnMeta:tableMeta.columnMetas){
					sortNumber++;
					OaCustomFormField field = OaCustomFormField.DAO.findFirst("Select * from oa_custom_form_field where formId=? and physicalName=?",oaCustomForm.getId(),columnMeta.name);
					if(field==null){
						field = new OaCustomFormField();
						//字段显示名称
						field.setName(columnMeta.remarks);
						//字段所属表单ID
						field.setFormId(oaCustomForm.getId());
						//字段物理名称
						field.setPhysicalName(columnMeta.name);
						//排序编号
						field.setSortNumber(sortNumber*5);
						//字段类型
						int fieldType = 1;//默认为文本类型
						if(columnMeta.type.toLowerCase().contains("char")){
							fieldType = 1;//文本类型
						}else if(columnMeta.type.toLowerCase().contains("text")){
							fieldType = 2;//大文本类型
						}else if(columnMeta.type.toLowerCase().contains("datetime")){
							fieldType = 4;//日期时间型
						}else if(columnMeta.type.toLowerCase().contains("date")){
							fieldType = 3;//日期型
						}else if(columnMeta.type.toLowerCase().contains("bigint")){
							fieldType = 6;//大整型
						}else if(columnMeta.type.toLowerCase().contains("int")){
							fieldType = 5;//整型
						}else if(columnMeta.type.toLowerCase().contains("numeric")
								||columnMeta.type.toLowerCase().contains("decimal")
								||columnMeta.type.toLowerCase().contains("double")
								||columnMeta.type.toLowerCase().contains("float")){
							fieldType = 7;//数值类型							
						}
						//设置字段类型
						field.setFieldType(fieldType);
						//是否必填
						field.setIsRequired("YES".equalsIgnoreCase(columnMeta.isNullable)?0:1);
						if((","+tableMeta.idField+",").contains(","+columnMeta.name+",")){
							field.setIsPrimaryKey(1);
						}
						//默认值
						if(!"NULL".equalsIgnoreCase(columnMeta.defaultValue)){
							field.setDefaultValue(columnMeta.defaultValue);
						}
						//输入框类型 1:文本框;2:大文本框;3:日期;4:日期时间;5:Email;6:密码;7:数值;8:整型;9:单选框;10:复选框;11:单选下拉列表;12:多选下拉列表;13:HTML编辑器;14:附件上传;15:图片上传;16:部门选择;17:用户选择;18:隐藏域;19:只读文本;
						int inputType = 1;
						if((","+tableMeta.idField+",").contains(","+columnMeta.name+",") || "otherJson".equalsIgnoreCase(columnMeta.attrName) || columnMeta.remarks.toUpperCase().contains("设置JSON字符串")){
							inputType=18;//隐藏域
						}else if("createUserId".equalsIgnoreCase(columnMeta.attrName) || columnMeta.remarks.equalsIgnoreCase("创建用户ID")
								||"createTime".equalsIgnoreCase(columnMeta.attrName) || columnMeta.remarks.equalsIgnoreCase("创建时间")
								||"AddUserId".equalsIgnoreCase(columnMeta.attrName) || columnMeta.remarks.equalsIgnoreCase("新增用户ID")
								||"AddTime".equalsIgnoreCase(columnMeta.attrName) || columnMeta.remarks.equalsIgnoreCase("新增时间")
								||"updateUserId".equalsIgnoreCase(columnMeta.attrName) || columnMeta.remarks.equalsIgnoreCase("更新用户ID")
								||"updateTime".equalsIgnoreCase(columnMeta.attrName) || columnMeta.remarks.equalsIgnoreCase("更新时间")){
							inputType=19;//只读文本
						}else if(columnMeta.attrName.endsWith("UserId") || columnMeta.remarks.toUpperCase().endsWith("用户ID")){
							inputType=17;//用户选择
						}else if(columnMeta.attrName.endsWith("DepartmentId") || columnMeta.remarks.toUpperCase().endsWith("部门ID")){
							inputType=16;//部门选择
						}else if(columnMeta.attrName.contains("Url") && (columnMeta.remarks.contains("图片") || columnMeta.remarks.contains("图像") || columnMeta.remarks.contains("照片"))){
							inputType=15;//图片上传
						}else if((columnMeta.remarks.contains("附件") || columnMeta.remarks.contains("文档")) && (columnMeta.remarks.contains("路径")||columnMeta.attrName.contains("Url"))){
							inputType=14;//附件上传
						}else if(columnMeta.remarks.contains("内容") || columnMeta.remarks.contains("HTML")||columnMeta.attrName.contains("Html")){
							inputType=13;//HTML编辑器
						}else if(columnMeta.remarks.toUpperCase().endsWith("IDS") || columnMeta.remarks.toUpperCase().endsWith("ID字符串")){
							inputType=12;//多选下拉列表
							field.setOptionType(2);
							//字段显示名称
							if(columnMeta.remarks.toUpperCase().endsWith("IDS")){
								field.setName(columnMeta.remarks.substring(0,columnMeta.remarks.length()-3));
							}
						}else if(columnMeta.attrName.endsWith("Id")){
							inputType=11;//单选下拉列表
							field.setOptionType(2);
							//字段显示名称
							if(columnMeta.remarks.toUpperCase().endsWith("ID")){
								field.setName(columnMeta.remarks.substring(0,columnMeta.remarks.length()-2));
							}
						}else if(StringUtils.isNotBlank(columnMeta.helpText) && columnMeta.helpText.matches("\\S+;\\S+")){
							String[] options = columnMeta.helpText.split(";");
							boolean checkBox = false;
							for(String option:options){
								String[] optionSS = option.split(":");
								if(optionSS.length<2 || !StringUtils.isNumeric(optionSS[0])){
									checkBox = true;
									break;
								}
							}
							if(options.length<5 && checkBox){
								inputType=10;//复选框
							} else if(options.length<5){
								inputType=9;//单选框
							} else if(checkBox){
								inputType=12;//下拉多选
							} else{
								inputType=11;//单选下拉列表
							}
							field.setOptionType(1);
							field.setOptionValue(columnMeta.helpText);
						}else if(columnMeta.javaType.toLowerCase().contains("long")||columnMeta.javaType.toLowerCase().contains("int")){
							inputType=8;//整型
						}else if(columnMeta.javaType.toLowerCase().contains("double")||columnMeta.javaType.toLowerCase().contains("float")){
							inputType=7;//数值型
						}else if(columnMeta.javaType.toLowerCase().contains("string") && (columnMeta.remarks.contains("密码")||columnMeta.name.contains("password") )){
							inputType=6;//密码
						}else if(columnMeta.javaType.toLowerCase().contains("string") && (columnMeta.remarks.contains("邮箱")||columnMeta.remarks.contains("email") )){
							inputType=5;//E-mail
						}else if("Date".equalsIgnoreCase(columnMeta.type) || columnMeta.remarks.contains("日期")){
							inputType=3;//日期
						}else if(columnMeta.javaType.toLowerCase().contains("date")){
							inputType=4;//日期时间
						}else if(columnMeta.javaType.toLowerCase().contains("string") && columnMeta.type.toLowerCase().contains("text") ){
							inputType=2;//大文本类型
						}else{
							inputType=1;//文本类型
						}
						field.setInputType(inputType);
						//设置附加的js和css样式 附加的JavaScript事件和Css样式
						field.setJsAndCss(" oninput=\"\" onchange=\"\" onfocus=\"\" onblur=\"\" style=\"\" class=\"\"");
						//设置编辑页面列展示宽度
						if(inputType==13){
							//如果字段为HTML编辑器则默认宽度为100%，否则为50%。
							field.setInputColSpan(12);
						}else{
							//否则为50%
							field.setInputColSpan(6);
						}
						//设置字段是否显示
						if(tableMeta.idField.equalsIgnoreCase(columnMeta.attrName)){
							//如果是ID字段，则在新增和修改页面不显示，在列表和显示页面显示
							field.setDisplayAdd(0);
							field.setDisplayEdit(0);
							field.setDisplayView(1);
							field.setDisplayList(1);
						} else if(inputType==18){
							//如果是隐藏域，则在新增和修改页面不显示，在列表和显示页面显示
							field.setDisplayAdd(0);
							field.setDisplayEdit(0);
							field.setDisplayView(1);
							field.setDisplayList(1);
						} else if(inputType==13 || inputType==2){
							//如果是HTML编辑器或者大文本类型，则在列表页面不显示，其他页面都显示
							field.setDisplayAdd(1);
							field.setDisplayEdit(1);
							field.setDisplayView(1);
							field.setDisplayList(0);
						} else {
							//否则都默认显示
							field.setDisplayAdd(1);
							field.setDisplayEdit(1);
							field.setDisplayView(1);
							field.setDisplayList(1);
						}
						//设置字段备注说明
						field.setRemark(columnMeta.helpText);
						//设置字段长度
						field.setTextLength(columnMeta.columnSize);
						//设置状态为正常
						field.setStatus(1);
						//如果ID为空，则表示要新增数据
						Random rand = new Random(System.currentTimeMillis()%3333);
						//自动生成ID
						field.setId(System.currentTimeMillis()*1000+rand.nextInt(999));
						//设置创建用户ID为1
						field.setCreateUserId(1L);
						//设置创建时间为当前时间
						field.setCreateTime(new Date());
						//设置更新用户ID为当1
						field.setUpdateUserId(1L);
						//设置更新时间为当前时间
						field.setUpdateTime(new Date());
						field.save();
					}
				}
			}
		}
		new JBaseModelGenerator(baseModelPackage).generate(tableMetaList);
		new JModelGenerator(modelPackage, baseModelPackage).generate(tableMetaList);
		if(!developGenerate){
			new JControllerGenerator(modelPackage, baseModelPackage, adminControllerPackage).generate(tableMetaList);
			//基于表结构的展示层代码自动生成
			new JViewGenerator(adminControllerPackage, modelPackage, viewDirectory).generate(tableMetaList);
		} else {
			new JControllerGenerator(modelPackage, baseModelPackage, adminControllerPackage).generate(tableMetaList);
			//由于基于表单定制版的代码自动生成功能还没有完成开发，因此先注释采用原来的基于表结构的代码自动生成
			//基于表单定制版的展示层代码自动生成
			//new JDevelopViewGenerator(adminControllerPackage, modelPackage, viewDirectory).generate(tableMetaList);
			//基于表结构的展示层代码自动生成
			new JViewGenerator(adminControllerPackage, modelPackage, viewDirectory).generate(tableMetaList);
		}
		System.out.println("代码生成完毕！");
	}
	
	
	
	public DataSource getDataSource(boolean developGenerate) {
		if(dbPassword.startsWith("DES{") && dbPassword.endsWith("}")){
			dbPassword = DesKit.decoderByDES(dbPassword.substring(4,dbPassword.length()-1),"cnitti.net");
		}
		DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, dbUser,dbPassword);
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		if(developGenerate){
			arp.addMapping("oa_custom_form", OaCustomForm.class);
			arp.addMapping("oa_custom_form_field",OaCustomFormField.class);
		}
		// 与web环境唯一的不同是要手动调用一次相关插件的start()方法
		druidPlugin.start();
		arp.start();
		return druidPlugin.getDataSource();
	}

}
