package generator;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.cnitti.utils.StringUtils;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.TableMeta;

public class JControllerGenerator{

	private String modelPackageName;
	private String baseModelPackageName;
	private String adminControllerPackageName;
	private String adminControllerDir;
	
	public JControllerGenerator(String modelPackageName, String baseModelPackageName, String adminControllerPackageName) {
		if (StrKit.isBlank(modelPackageName))
			throw new IllegalArgumentException("modelPackageName can not be blank.");
		if (modelPackageName.contains("/") || modelPackageName.contains("\\"))
			throw new IllegalArgumentException("modelPackageName error : " + modelPackageName);
		if (StrKit.isBlank(baseModelPackageName))
			throw new IllegalArgumentException("baseModelPackageName can not be blank.");
		if (baseModelPackageName.contains("/") || baseModelPackageName.contains("\\"))
			throw new IllegalArgumentException("baseModelPackageName error : " + baseModelPackageName);
		if (StrKit.isBlank(adminControllerPackageName))
			throw new IllegalArgumentException("adminControllerPackageName can not be blank.");
		if (adminControllerPackageName.contains("/") || adminControllerPackageName.contains("\\"))
			throw new IllegalArgumentException("adminControllerPackageName error : " + adminControllerPackageName);
		
		this.modelPackageName = modelPackageName;
		this.baseModelPackageName = baseModelPackageName;
		this.adminControllerPackageName = adminControllerPackageName;
		
		this.adminControllerDir = PathKit.getWebRootPath() + "/src/main/java/"+adminControllerPackageName.replace(".", "/");
	}
	public void generate(List<TableMeta> tableMetas) {
		System.out.println("Generate AdminController ...");
		for (TableMeta tableMeta : tableMetas){
			generate(tableMeta);
		}
	}
	
	protected void generate(TableMeta tableMeta){
		File dir = new File(adminControllerDir);
		if (!dir.exists())dir.mkdirs();
		String target = adminControllerDir + File.separator + "_"+tableMeta.modelName + "Controller.java";
		
		File file = new File(target);
		if (file.exists()) {
			return ;	// 若 Model 存在，不覆盖
		}
		if (!file.exists()) {
			try{
				FileWriter fw = new FileWriter(file);
				ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader();
				Configuration cfg = Configuration.defaultConfiguration();
				GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
				Template t = gt.getTemplate(GeneratorDemo.controllerTempletFile);
				t.binding("modelPackageName",modelPackageName);
				t.binding("baseModelPackageName", baseModelPackageName);
				t.binding("adminControllerPackageName", adminControllerPackageName);
				t.binding("tableMeta",tableMeta);
				try {
					String str = t.render();
					if(StringUtils.length(str)>10){
						fw.write(str);
						fw.close();
					} else {
						fw.close();
						file.delete();
					}
				} catch(Exception e){
					fw.close();
					file.delete();
				}
			} catch(Exception e){}
		}
	}
}
