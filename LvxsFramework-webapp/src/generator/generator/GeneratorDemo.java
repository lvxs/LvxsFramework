package generator;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;

/**
 * 代码自动生成
 */
public class GeneratorDemo {

	public static String baseModelTempletFile="_BaseModel.html";
	public static String modelTempletFile="_Model.html";
	public static String controllerTempletFile="_Controller.html";
	public static String viewTempletFile_list="_list.html";
	public static String viewTempletFile_edit="_edit.html";
	public static String viewTempletFile_add="_add.html";
	public static String viewTempletFile_view="_view.html";
	public static String developViewTempletFile_list="_developList.html";
	public static String developViewTempletFile_edit="_developEdit.html";
	public static String developViewTempletFile_add="_developAdd.html";
	public static String developViewTempletFile_view="_developView.html";
	
	public static void main(String[] args) {
		Prop p = PropKit.use("config.properties");
		String defaultDbId = p.get("default_db_id");
		//生成代码所使用的包名
		String basePackage = "org.cnitti";
		//生成代码所使用的包名
		String viewDirectory = "/src/main/webapp/WEB-INF/t/default/admin";
		// 创建生成器
		JGenerator gernerator = new JGenerator(basePackage,p.get(defaultDbId+".jdbcUrl"), p.get(defaultDbId+".user"), p.get(defaultDbId+".password"),viewDirectory);
		// 生成
		gernerator.doGenerate();
	}
}




