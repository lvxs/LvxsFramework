package generator;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.cnitti.utils.StringUtils;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.TableMeta;

public class JDevelopDataGenerator{

	private String viewDirectory;

	public JDevelopDataGenerator(String modelPackageName,String baseModelPackageName, String viewDirectory) {
		this.viewDirectory =viewDirectory;
	}
	
	public void generate(List<TableMeta> tableMetas) {
		System.out.println("Generate View Jsp ...");
		for (TableMeta tableMeta : tableMetas){
			generate(tableMeta,"list.jsp",GeneratorDemo.viewTempletFile_list);
			generate(tableMeta,"edit.jsp",GeneratorDemo.viewTempletFile_edit);
			generate(tableMeta,"add.jsp",GeneratorDemo.viewTempletFile_add);
			generate(tableMeta,"view.jsp",GeneratorDemo.viewTempletFile_view);
		}
	}	
	
	protected void generate(TableMeta tableMeta,String jspFileName,String templetFileName){
		File dir = new File(this.viewDirectory+File.separator+tableMeta.modelName);
		if(StrKit.isBlank(tableMeta.remarks))tableMeta.remarks=tableMeta.modelName;
		if (!dir.exists())dir.mkdirs();
		File file = new File(dir.getAbsoluteFile()+"/"+jspFileName);
		if (!file.exists() || file.length()<10) {
			try{
				FileWriter fw = new FileWriter(file);
				ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader();
				Configuration cfg = Configuration.defaultConfiguration();
				GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
				Template t = gt.getTemplate(templetFileName);
				t.binding("tableMeta",tableMeta);
				try {
					String str = t.render();
					if(StringUtils.length(str)>10){
						fw.write(str);
						fw.close();
					} else {
						fw.close();
						file.delete();
					}
				} catch(Exception e){
					fw.close();
					file.delete();
				}
			} catch(Exception e){}
		}
	}
}
