package generator;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.cnitti.utils.StringUtils;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.TableMeta;

public class JModelGenerator {

	private String modelPackageName;
	private String baseModelPackageName;
	private String modelPackageDir;
	
	public JModelGenerator(String modelPackageName, String baseModelPackageName) {
		if (StrKit.isBlank(modelPackageName))
			throw new IllegalArgumentException("modelPackageName can not be blank.");
		if (modelPackageName.contains("/") || modelPackageName.contains("\\"))
			throw new IllegalArgumentException("modelPackageName error : " + modelPackageName);
		if (StrKit.isBlank(baseModelPackageName))
			throw new IllegalArgumentException("baseModelPackageName can not be blank.");
		if (baseModelPackageName.contains("/") || baseModelPackageName.contains("\\"))
			throw new IllegalArgumentException("baseModelPackageName error : " + baseModelPackageName);
		
		this.modelPackageName = modelPackageName;
		this.baseModelPackageName = baseModelPackageName;
		
		this.modelPackageDir = PathKit.getWebRootPath() + "/src/main/java/"+modelPackageName.replace(".", "/");
	}
	public void generate(List<TableMeta> tableMetas) {
		System.out.println("Generate model ...");
		File dir = new File(modelPackageDir);
		if (!dir.exists())dir.mkdirs();
		for (TableMeta tableMeta : tableMetas){
			generate(tableMeta);
		}
	}
	
	protected void generate(TableMeta tableMeta){
		String target = modelPackageDir + File.separator + tableMeta.modelName + ".java";
		
		File file = new File(target);
		if (file.exists()) {
			return ;	// 若 Model 存在，不覆盖，如果需要重新生成，请注释本行并去掉下行的注释
			//file.delete(); //若Model存在，则删除后重新生成
		}
		if (!file.exists()) {
			try{
				FileWriter fw = new FileWriter(file);
				ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader();
				Configuration cfg = Configuration.defaultConfiguration();
				GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
				Template t = gt.getTemplate(GeneratorDemo.modelTempletFile);
				t.binding("modelPackageName",modelPackageName);
				t.binding("baseModelPackageName", baseModelPackageName);
				t.binding("tableMeta",tableMeta);
				try {
					String str = t.render();
					if(StringUtils.length(str)>10){
						fw.write(str);
						fw.close();
					} else {
						fw.close();
						file.delete();
					}
				} catch(Exception e){
					fw.close();
					file.delete();
				}
			} catch(Exception e){}
		}
	}
}
